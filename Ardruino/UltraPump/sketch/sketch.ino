
#include <Wire.h>//Include the Wire library to talk I2C
//Black magic
#define MCP4725_ADDR 0x60

const int CMDBUFFER_SIZE = 6;
int system_on;
int rate;
bool forward;

void setup()
{
  Wire.begin();

  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);

  digitalWrite(A2, LOW);//Set A2 as GND
  digitalWrite(A3, HIGH);//Set A3 as Vcc
 
    
    for (int i = 0; i < 8; i++) {
      pinMode(2+i, OUTPUT);
      digitalWrite(i + 2, HIGH);
    }

  delay(1000);
  //set up serial communication
  Serial.begin(9600);
  system_on=0;
  rate=100;
  forward=false;
}
void loop()
{


  if (system_on != 0)
  {
    //System is on
    digitalWrite(2 + system_on, LOW);
    for (int i = 1; i < 8; i++)
    {
      if (i != system_on)
      {
        digitalWrite(2 + i, HIGH);
      }
    }
  } else {
    //System is off
    for (int i = 1; i < 8; i++)
      digitalWrite(i + 2, HIGH);
  }

//Set direction
  if (forward)
  {
    digitalWrite(2, LOW);

  } else
  {
    digitalWrite(2, HIGH);
  }

//Set speed regulator voltage

  Wire.beginTransmission(MCP4725_ADDR);
  Wire.write(64);                     // cmd to update the DAC
  int thing = 4095.0 * (((double) rate) / 100.0);
  Wire.write(thing >> 4);        // the 8 most significant bits...
  Wire.write((thing & 15) << 4); // the 4 least significant bits...
  Wire.endTransmission();
 
//Not too fast
delay(50);
}


void serialEvent()
{
  static char cmdBuffer[CMDBUFFER_SIZE] = "";
  char c;
  while (Serial.available())
  {
    c = processCharInput(cmdBuffer, Serial.read());
    if (c == '\n')
    {

//        Serial.println("r:");
//  Serial.println(rate);
//Serial.println("system_on:");
//  Serial.println(system_on);
//Serial.println("forward:");
//  Serial.println(forward);

      if (strncmp("1GO", cmdBuffer, 3) == 0)
      {
        Serial.println("1GO");
        system_on = 1;
      } else if (strncmp("2GO", cmdBuffer, 3) == 0)
      {
        Serial.println("2GO");
        system_on = 2;
      } else if (strncmp("3GO", cmdBuffer, 3) == 0)
      {
        Serial.println("3GO");
        system_on = 3;
      } else if (strncmp("4GO", cmdBuffer, 3) == 0)
      {
        Serial.println("4GO");
        system_on = 4;
      } else if (strncmp("5GO", cmdBuffer, 3) == 0)
      {
        Serial.println("5GO");
        system_on = 5;
      } else if (strncmp("6GO", cmdBuffer, 3) == 0)
      {
        Serial.println("6GO");
        system_on = 6;
      } else if (strncmp("7GO", cmdBuffer, 3) == 0)
      {
        Serial.println("7GO");
        system_on = 7;
      } else if (strncmp("FWD", cmdBuffer, 3) == 0)
      {
        Serial.println("FWD");
        forward = true;
      } else if (strncmp("REVERS", cmdBuffer, 6) == 0)
      {
        Serial.println("REVERS");
        forward = false;
      }
      else if (strncmp("1ST", cmdBuffer, 3) == 0)
      {
        Serial.println("1ST");
        system_on = 0;
      } else if (strncmp("1SP", cmdBuffer, 3) == 0)
      {
        String rate_s = cmdBuffer;
        rate_s = rate_s.substring(3);
        rate = rate_s.toInt();
        Serial.println("1SP"+rate);
        
      } else if (strncmp("YoAdruinoAreYaAlive", cmdBuffer, 6) == 0)
      {
        Serial.println("HeartBeat");
      }
      
//      Serial.println(cmdBuffer);
      cmdBuffer[0] = 0;
//
//Serial.println("r:");
//  Serial.println(rate);
//Serial.println("system_on:");
//  Serial.println(system_on);
//Serial.println("forward:");
//  Serial.println(forward);
  
    }
  }
  delay(1);
}

char processCharInput(char* cmdBuffer, const char c)
{
  //Store the character in the input buffer
  if (c >= 32 && c <= 126) //Ignore control characters and special ascii characters
  {
    if (strlen(cmdBuffer) < CMDBUFFER_SIZE)
    {
      strncat(cmdBuffer, &c, 1);   //Add it to the buffer
    }
    else
    {
      return '\n';
    }
  }
  else if ((c == 8 || c == 127) && cmdBuffer[0] != 0) //Backspace
  {

    cmdBuffer[strlen(cmdBuffer) - 1] = 0;
  }

  return c;
}


