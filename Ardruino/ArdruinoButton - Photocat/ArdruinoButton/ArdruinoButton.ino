const int powerPin = 10;
const int dryPin  = 11;
const int magnet_off = HIGH;
const int magnet_on =  LOW;

boolean system_on = false;
boolean system_drying = false;

const int CMDBUFFER_SIZE = 8;

void setup() {


  //set up serial communication
  Serial.begin(9600);

pinMode(powerPin, OUTPUT);
pinMode(dryPin, OUTPUT);
}


void loop() {


  if (system_on)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(powerPin, magnet_off);

  } else {
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(powerPin, magnet_on);
  }

   if (system_drying)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(dryPin, magnet_off);

  } else {
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(dryPin, magnet_on);
  }
  
}



void serialEvent()
{
 static char cmdBuffer[CMDBUFFER_SIZE] = "";
 char c;
 while(Serial.available()) 
 {
   c = processCharInput(cmdBuffer, Serial.read());
   if (c == '\n') 
   {
    if (strncmp("sjaak1", cmdBuffer,6) == 0)
    {
      system_on = true;
    } else if (strncmp("sjaak0", cmdBuffer,6) == 0)
    {
      system_on = false;
    }else if (strncmp("dry1", cmdBuffer,6) == 0)
    {
      system_drying = true;
    } else if (strncmp("dry0", cmdBuffer,6) == 0)
    {
      system_drying = false;
    } else if (strncmp("YoAdruinoAreYaAlive", cmdBuffer,6) == 0)
    {
      Serial.println("HeartBeat");
    }
    
     Serial.println(cmdBuffer); 
     
     cmdBuffer[0] = 0;
   }
 }
 delay(1);
}

char processCharInput(char* cmdBuffer, const char c)
{
 //Store the character in the input buffer
 if (c >= 32 && c <= 126) //Ignore control characters and special ascii characters
 {
   if (strlen(cmdBuffer) < CMDBUFFER_SIZE) 
   { 
     strncat(cmdBuffer, &c, 1);   //Add it to the buffer
   }
   else  
   {   
     return '\n';
   }
 }
 else if ((c == 8 || c == 127) && cmdBuffer[0] != 0) //Backspace
 {

   cmdBuffer[strlen(cmdBuffer)-1] = 0;
 }

 return c;
}

