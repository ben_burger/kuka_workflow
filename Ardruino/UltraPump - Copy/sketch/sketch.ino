
#include <Wire.h>//Include the Wire library to talk I2C

//This is the I2C Address of the MCP4725, by default (A0 pulled to GND).
//Please note that this breakout is for the MCP4725A0. 
#define MCP4725_ADDR 0x60

const int CMDBUFFER_SIZE = 6;

int system_on;
int rate;
bool forward;
void setup()
{
  system_on=1 ;
  rate=1;
  forward=true;
  
   Wire.begin(MCP4725_ADDR);

  // Set A2 and A3 as Outputs to make them our GND and Vcc,
  //which will power the MCP4725
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);

  digitalWrite(A2, LOW);//Set A2 as GND
  digitalWrite(A3, HIGH);//Set A3 as Vcc

  //set up serial communication
  Serial.begin(9600);
}
void loop()
{

  Serial.println("loop:                           " + rate);
  Serial.println(rate);



  
  {
  Serial.println("system_on");

    Wire.beginTransmission(MCP4725_ADDR);
  Serial.println("system_on1");
  Wire.write(64);                     // cmd to update the DAC
  Serial.println("system_on2");
  int thing = (4095.0*((double) (100-rate))/100.0) ;

  Serial.println("system_on3");  
    Wire.write(thing >> 4);        // the 8 most significant bits...
  Serial.println("system_on4");
  Wire.write((thing & 15) << 4); // the 4 least significant bits...
  Serial.println("system_on5");
  Wire.endTransmission();
 Serial.println("system_on6");

  } 

  Serial.println("system ran");

}


void serialEvent()
{
//  static char cmdBuffer[CMDBUFFER_SIZE] = "";
//  char c;
//  while (Serial.available())
//  {
//    c = processCharInput(cmdBuffer, Serial.read());
//    if (c == '\n')
//    {
//      if (strncmp("1GO", cmdBuffer, 3) == 0)
//      {
//        Serial.println("1GO");
//        system_on = 1;
//      } else if (strncmp("2GO", cmdBuffer, 3) == 0)
//      {
//        Serial.println("2GO");
//        system_on = 2;
//      } else if (strncmp("3GO", cmdBuffer, 3) == 0)
//      {
//        Serial.println("3GO");
//        system_on = 3;
//      } else if (strncmp("4GO", cmdBuffer, 3) == 0)
//      {
//        Serial.println("4GO");
//        system_on = 4;
//      } else if (strncmp("5GO", cmdBuffer, 3) == 0)
//      {
//        Serial.println("5GO");
//        system_on = 5;
//      } else if (strncmp("6GO", cmdBuffer, 3) == 0)
//      {
//        Serial.println("6GO");
//        system_on = 6;
//      } else if (strncmp("7GO", cmdBuffer, 3) == 0)
//      {
//        Serial.println("7GO");
//        system_on = 7;
//      } else if (strncmp("FWD", cmdBuffer, 3) == 0)
//      {
//        forward = true;
//      } else if (strncmp("REV", cmdBuffer, 3) == 0)
//      {
//        forward = false;
//      }
//      else if (strncmp("1ST", cmdBuffer, 3) == 0)
//      {
//        Serial.println("1ST");
//        system_on = 0;
//      } else if (strncmp("1SP", cmdBuffer, 3) == 0)
//      {
//        Serial.println("old rate: ");
//        Serial.println(rate);
//        rate = (cmdBuffer[3] - '0') * 100 + (cmdBuffer[4] - '0') * 10 + (cmdBuffer[5] - '0');
//      } else if (strncmp("YoAdruinoAreYaAlive", cmdBuffer, 6) == 0)
//      {
//        Serial.println("HeartBeat");
//      }
//      
//      Serial.println(cmdBuffer);
//      cmdBuffer[0] = 0;
//
//      Serial.println("status");
//      Serial.println(system_on);
//      Serial.println("rate: ");
//      Serial.println(rate);
//
//    }
//  }
  delay(1);
}
//
//char processCharInput(char* cmdBuffer, const char c)
//{
//  //Store the character in the input buffer
//  if (c >= 32 && c <= 126) //Ignore control characters and special ascii characters
//  {
//    if (strlen(cmdBuffer) < CMDBUFFER_SIZE)
//    {
//      strncat(cmdBuffer, &c, 1);   //Add it to the buffer
//    }
//    else
//    {
//      return '\n';
//    }
//  }
//  else if ((c == 8 || c == 127) && cmdBuffer[0] != 0) //Backspace
//  {
//
//    cmdBuffer[strlen(cmdBuffer) - 1] = 0;
//  }
//
//  return c;
//}
//

