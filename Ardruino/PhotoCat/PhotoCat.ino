const int magnetPins[] =  {2, 3, 4, 5, 6, 7, 8, 9}; // digital pins connecting to relay switch for each magnet
const int powerPin = 10;
const int numberofmagnet = 8; // using sizeof creates a bug, gives a value of 12 for an int array of 6
const int frequency = 15  ; // Hz
const int groupsize = 2; // the size of magnetpins must be divisible by the groupsize, if there is a fraction there are uneven groups and the code cannot handle this.

const int magnet_off = LOW;
const int magnet_on =  HIGH;

const long interval = 1000 / frequency;
const int numberofgroup = numberofmagnet / groupsize;

int state = 0;// describes currently active magnet group
unsigned long previousMillis = 0;
int i; // loop variable
boolean system_on = false;

const int CMDBUFFER_SIZE = 6;









void setup() {


  //set up serial communication
  Serial.begin(9600);

pinMode(powerPin, OUTPUT);
  //Set up magnets pins as output and off
  for (i = 0; i <  numberofmagnet; i = i + 1) {
    pinMode(magnetPins[i], OUTPUT);
  }
  for (i = 0; i <  numberofmagnet; i = i + 1) {
    digitalWrite(magnetPins[i], magnet_off);
  }
}


void loop() {


  if (system_on)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(powerPin, magnet_on);

    // Check if it is time to change the state
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {



      previousMillis = currentMillis;

      // turn off magnets
      for (i = state; i < numberofmagnet; i = i + numberofgroup) {
        digitalWrite(magnetPins[i], magnet_off);
        //      Serial.print (magnetPins[i]);
        //      Serial.print ("-");
        //      Serial.print (magnet_off);

      }

      // advance to next new state
      state = (state + 1) % (numberofgroup);
      //    Serial.print ("|new state ");
      //Serial.print (state);
      //Serial.print ("|");

      // turn on magnets
      for (i = state; i < numberofmagnet; i = i + numberofgroup) {
        digitalWrite(magnetPins[i], magnet_on);

        //      Serial.print (magnetPins[i]);
        //      Serial.print ("-");
        //      Serial.print (magnet_on);
      }
      //    Serial.print('\n');
    }

  } else {
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(powerPin, magnet_off);
    for (i = 0; i < numberofmagnet; i = i + 1) {
      digitalWrite(magnetPins[i], magnet_off);
    }
  }
}


void serialEvent()
{
 static char cmdBuffer[CMDBUFFER_SIZE] = "";
 char c;
 while(Serial.available()) 
 {
   c = processCharInput(cmdBuffer, Serial.read());
   if (c == '\n') 
   {
    if (strncmp("sjaak1", cmdBuffer,6) == 0)
    {
      system_on = true;
    } else if (strncmp("sjaak0", cmdBuffer,6) == 0)
    {
      system_on = false;
    } else if (strncmp("YoAdruinoAreYaAlive", cmdBuffer,6) == 0)
    {
      Serial.println("HeartBeat");
    }
    
     Serial.println(cmdBuffer); 
     
     cmdBuffer[0] = 0;
   }
 }
 delay(1);
}


char processCharInput(char* cmdBuffer, const char c)
{
 //Store the character in the input buffer
 if (c >= 32 && c <= 126) //Ignore control characters and special ascii characters
 {
   if (strlen(cmdBuffer) < CMDBUFFER_SIZE) 
   { 
     strncat(cmdBuffer, &c, 1);   //Add it to the buffer
   }
   else  
   {   
     return '\n';
   }
 }
 else if ((c == 8 || c == 127) && cmdBuffer[0] != 0) //Backspace
 {

   cmdBuffer[strlen(cmdBuffer)-1] = 0;
 }

 return c;
}

