
#include <Wire.h>//Include the Wire library to talk I2C
//Black magic
#define MCP4725_ADDR 0x60   

const int CMDBUFFER_SIZE = 6;
boolean system_on = false;
int rate = 100;
void setup()
{
  Wire.begin();

  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);

  digitalWrite(A2, LOW);//Set A2 as GND
  digitalWrite(A3, HIGH);//Set A3 as Vcc
        digitalWrite(2, HIGH);
pinMode(2, OUTPUT);

  delay(100);
 //set up serial communication
  Serial.begin(9600);
    system_on = false;
      digitalWrite(2, HIGH);
}
void loop()
{
        
        if(system_on)
  {
  Wire.beginTransmission(MCP4725_ADDR);
  Wire.write(64);                     // cmd to update the DAC
    int thing = (4095.0*((double) (100-rate))/100.0) ;
    
  Wire.write(thing  >> 4);        // the 8 most significant bits...
  Wire.write((thing & 15) << 4); // the 4 least significant bits...
 
  Wire.endTransmission();
digitalWrite(2, LOW);
  } else {
    Wire.beginTransmission(MCP4725_ADDR);
    
  Wire.write(64);                     // cmd to update the DAC

  Wire.endTransmission();
      digitalWrite(2, HIGH);
  }
  



}


void serialEvent()
{
 static char cmdBuffer[CMDBUFFER_SIZE] = "";
 char c;
 while(Serial.available()) 
 {
   c = processCharInput(cmdBuffer, Serial.read());
   if (c == '\n') 
   {
    if (strncmp("1GO", cmdBuffer,6) == 0)
    {
      system_on = true;
    } else if (strncmp("1ST", cmdBuffer,6) == 0)
    {
      system_on = false;
    } else if (strncmp("1SP", cmdBuffer,3) == 0)
    {
              Serial.println("rate: ");
        Serial.println(rate);
      rate= (cmdBuffer[3] - '0')*100 + (cmdBuffer[4] - '0')*10 + (cmdBuffer[5] - '0');
        Serial.println("rate: ");
        Serial.println(rate);

    } else if (strncmp("YoAdruinoAreYaAlive", cmdBuffer,6) == 0)
    {
      Serial.println("HeartBeat");
    }
    
     Serial.println(cmdBuffer); 
     
     cmdBuffer[0] = 0;
   }
 }
 delay(1);
}

char processCharInput(char* cmdBuffer, const char c)
{
 //Store the character in the input buffer
 if (c >= 32 && c <= 126) //Ignore control characters and special ascii characters
 {
   if (strlen(cmdBuffer) < CMDBUFFER_SIZE) 
   { 
     strncat(cmdBuffer, &c, 1);   //Add it to the buffer
   }
   else  
   {   
     return '\n';
   }
 }
 else if ((c == 8 || c == 127) && cmdBuffer[0] != 0) //Backspace
 {

   cmdBuffer[strlen(cmdBuffer)-1] = 0;
 }

 return c;
}


