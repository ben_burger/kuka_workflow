/**
 * Created by Benjamin on 6/14/2017.
 */
package Workflow.Com.Serial;

import Serialio.SerialConfig;
import Serialio.SerialPortLocal;
import Workflow.Configuration.Configuration;
import Workflow.Exception.TimeOutException;
import com.sun.deploy.util.StringUtils;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class CapperCom extends SerialComSuper {

    ScheduledExecutorService exec;
    protected Boolean isRunning = false;

    public CapperCom(String port) throws IOException, InterruptedException {
        super("Capper", port);
        CR = false;
        LF = false;
    }

    protected void InitializeSerialIO() throws IOException {
        super.InitializeSerialIO();
        Config = new SerialConfig(PortIdentifier);

        Config.setBitRate(SerialConfig.BR_9600); //9600
        Config.setDataBits(SerialConfig.LN_8BITS); //8 bit
        Config.setStopBits(SerialConfig.ST_1BITS); // 1bit
        Config.setParity(SerialConfig.PY_NONE); // None Parity
        Config.setHandshake(SerialConfig.HS_NONE); // None Flow Control
        SerialPort = new SerialPortLocal(Config);

        try {
            Thread.sleep(2000); //delay required for arduino commsunication is possible, without this it would miss commands
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        exec = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                CheckAlive();
            }
        }, 0, Configuration.getHeartBeatFrequencyCapper(), TimeUnit.MILLISECONDS);


    }


    protected void CheckAlive() {
        Boolean aliveResult = null;
        boolean runningResult = true;
        Date start = new Date();

        synchronized (CommandLock) {
            try {
                this.LisenLoop();
                LinkedList<String> response = new LinkedList<String>();
                this.SendCommandNaive("GETS");
                Thread.sleep(100);
                while (aliveResult == null) {
                    LinkedList<String> new_responses = this.LisenLoop();
                    response.addAll(new_responses);


                    String message_back = StringUtils.join(response, "");
                    switch(message_back)
                    {
                        case "Running\r\n":
                        case "started Running\r\n":
                            runningResult = true;
                            aliveResult = true;
                            break;
                        case "Ready for Load\r\n":
                        case "Ready for Unload\r\n":
                            aliveResult = true;
                            runningResult = false;
                            break;
                        default:
                            aliveResult = false;
                            break;
                    }
                    long diffInMillies = (new Date()).getTime() - start.getTime();
                    if (TimeUnit.MILLISECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS) > Configuration.getTimeoutCapper())
                        throw new TimeOutException(getMachineIdentifier());
                }

            } catch (Exception e) {
                aliveResult = false;
            }


        }

        synchronized ((isAlive)) {
            isAlive = aliveResult;
            isRunning = runningResult;
        }
    }

    @Override
    public void Destruct() throws IOException {
        super.Destruct();
        if (exec != null)
            exec.shutdown();
    }


    public boolean Initialise() throws InterruptedException {
        synchronized (CommandLock) {
            boolean result = SendCommandNaive("INIT");
            Thread.sleep(1000);
            return result;
        }
    }




    public boolean SetIntegerParameter(CapperParameter param, int value) throws InterruptedException {
        return SetParameter(param, Integer.toString(value));
    }

    public boolean SetPurge(boolean value) throws InterruptedException {
        synchronized (CommandLock) {
            boolean result = SendCommandNaive("SETP PUOO " + (value ? "1" : "0"));
            Thread.sleep(1000);
            return result;
        }
    }

    public boolean SetParameter(CapperParameter param, String value) throws InterruptedException {
        synchronized (CommandLock) {
            boolean result = SendCommandNaive("SETP " + param.getCommand() + " " + value);
            Thread.sleep(1000);
            return result;
        }
    }

    public boolean Start() throws TimeOutException, IOException, InterruptedException {
        Date start = new Date();
        boolean result;
        synchronized (CommandLock) {
            result = SendCommandNaive("STRT");
            Thread.sleep(1000);
        }
        return result;

    }
    public boolean StartAndWait() throws TimeOutException, IOException, InterruptedException {
       if(Start())
       {
           Thread.sleep(10*1000);
           boolean running = true;
           while(running)
           {
               synchronized (isAlive)
               {
                   running = isRunning;
               }
               Thread.sleep(500);
           }
           return true;
       } else
       {
           return false;
       }

    }

    public boolean Stop() throws InterruptedException {
        synchronized (CommandLock) {
            boolean result = SendCommandNaive("STOP");
            Thread.sleep(1000);
            return result;
        }
    }

    public boolean GetLastError() throws InterruptedException {
        synchronized (CommandLock) {
            boolean result = SendCommandNaive("GETE");
            Thread.sleep(1000);
            return result;
        }
    }

    public boolean ClearLastError() throws InterruptedException {
        synchronized (CommandLock) {
            boolean result =SendCommandNaive("CLRE");
            Thread.sleep(1000);
            return result;
        }
    }


    public enum CapperParameter {
        BowlFeederStopDelay("BFSD","BowlFeederStopDelay"), BowlFeederTimeout("BFTO", "BowlFeederTimeout"),
        HandlerVacuumPickTimeout("HVPT", "HandlerVacuumPickTimeout"),
        Purge("PUOO", "Purge"),
        PurgeVacuumPickTimeout("PVPT","PurgeVacuumPickTimeout"), PurgeTime("PUTI","PurgeTime");

        String Command;
        String Name;

        CapperParameter(String command, String name) {
            Command = command; Name= name;
        }

        public String getName() {return Name;}

        public String getCommand() {
            return Command;
        }

        @Override
        public String toString()
        {
            return Name;
        }
    }

}
