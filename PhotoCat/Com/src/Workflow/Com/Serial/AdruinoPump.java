package Workflow.Com.Serial;

import Workflow.Exception.TimeOutException;

import java.io.IOException;

public class AdruinoPump extends ArdruinoCom implements IPump {
    public AdruinoPump(String port) throws IOException, InterruptedException {
        super(port, "LiquidPump");
        this.CR = false;
        this.LF = true;
    }

    private float PID_P = 20;
    private float PID_I = 0.1f;//-0.3f;
    private float PID_D = 0.8f;//-1;


    @Override
    public float GetP() {
        return PID_P;
    }

    @Override
    public float GetI() {
        return PID_I;
    }

    @Override
    public float GetD() {
        return PID_D;
    }

    @Override
    public int GetNumberOfPoints() {
        return 20;
    }

    @Override
    public int getMinimalSpeed() {
        return 10;
    }

    public boolean Forward() {
        return SendCommandNaive("FWD");
    }

    public boolean Reverse() {
        return SendCommandNaive("REVERS");
    }

    @Override
    public boolean Start(int i) {
        return SendCommandNaive(i + "GO");
    }

    @Override
    public boolean Stop() {
        return SendCommandNaive("1ST");
    }

    public boolean SetRate(int rate) {
        String rate_s = Integer.toString(rate);
        while (rate_s.length() < 3) {
            rate_s = "0" + rate_s;
        }

        return SendCommandNaive("1SP" + rate_s);
    }

    @Override
    public boolean GetRunningStatus() throws TimeOutException, IOException, InterruptedException {
        return false;
    }

}
