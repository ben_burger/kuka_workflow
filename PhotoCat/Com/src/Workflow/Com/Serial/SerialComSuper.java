package Workflow.Com.Serial;

import Workflow.Com.ComSuper;
import Serialio.*;
import Workflow.Util.SimpleLogger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;

/**
 * Created by taurnist on 21/07/2017.
 */
public abstract class SerialComSuper extends ComSuper {
    private final SimpleLogger logger;
    protected int POLLING_WAIT_TIME = 100;

    public String getMachineIdentifier() {
        return MachineIdentifier;
    }

    protected String MachineIdentifier = "SerialComSuper";
    protected String PortIdentifier;
    protected SerialConfig Config;
    protected SerialPort SerialPort;
    String latest_completed_command = "";

    protected boolean CR = true;
    protected boolean LF = true;


    private PrintWriter writer;
    String buffer = "";

    protected Object CommandLock = new Object();


    public SerialComSuper(String MachineName, String port) throws IOException, InterruptedException {
        PortIdentifier = port;
        MachineIdentifier = MachineName;
        logger = new SimpleLogger(MachineIdentifier);
        boolean exists = false;

        String[] ports;
        try {
            ports = SerialPortLocal.getPortList();

            for (int i = 0; i < ports.length; i++)
                if (ports[i].equals(port))
                    exists = true;
        } catch (IOException e) {
            // Throw later...
        }

        if (!exists) {
            ports = SerialPortLocal.getPortList();

            throw new IOException("COM port not found or inactive");
        }

        SerialPortLocal.addPortName(PortIdentifier);
        InitializeSerialIO();
    }

    protected  void InitializeSerialIO() throws IOException{}
    protected abstract void CheckAlive() ;

    public void Destruct() throws IOException {

        try {
            if (SerialPort != null)
                SerialPort.close();
        } catch (IOException e)
        {
            //whatever, it is closed
        }
        SerialPort = null;
        logger.Destruct();

    }

    protected void Log(boolean received, String data)  {
       logger.LogMessage(received,data);
    }
    /**
     * Deletes the target if it exists then creates a new empty file.
     */
    private File createOrReplaceFileAndDirectories(String target) throws IOException{

        Path path = Paths.get(target);
        Files.deleteIfExists(path);
        return Files
                .createFile(
                        Files.createDirectories(path))
                .toFile();
    }


    public boolean SendCommandNaive(String cmd) {

        boolean result = true;
        try {
            String send_command = cmd + get_delimiter();
            SerialPort.putData(send_command.getBytes());
            Log(false, send_command);
        } catch (IOException e) {
            result = false;
        }

        return result;
    }

    public void StartLisening() {
        (new Thread() {
            public void run() {
                while(true)
                {
                    try {
                        LisenLoop();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    public LinkedList<String> LisenLoop() throws IOException, InterruptedException {
        LinkedList<String> result = new LinkedList<String>();
        String command_delimiter = get_delimiter();
        // Give this system time to send, and give device time to respond, but more importantly
        // ensure we are not needlessly using computational resources on polling
        Thread.sleep(POLLING_WAIT_TIME);

        byte[] responsebytes = new byte[512];


        if(SerialPort!=null)
        {
        int chars_sent = SerialPort.getData(responsebytes);;

        String data = new String(responsebytes, 0, chars_sent, "UTF-8");

        if (!data.equals("") && data != null) {
            Log(true, data);
            String token = buffer + data.replaceAll("[\u0000]", "");
            buffer = "";


            if (!token.contains(command_delimiter)) {
                buffer = token;

            } else {

                String[] responses_in_token = token.split(command_delimiter); //Splitting into commands
                if (token.endsWith(command_delimiter)) {
                    if(responses_in_token.length > 0) {
                        for (int i = 0; i < responses_in_token.length; i++) {
                            result.add(responses_in_token[i]);
                        }
                        synchronized (latest_completed_command) {
                            latest_completed_command = responses_in_token[responses_in_token.length - 1];
                        }
                        NewCommandHook();
                    }
                } else {
                    for (int i = 0; i < responses_in_token.length - 1; i++) {
                        result.add(responses_in_token[i]);
                    }
                    synchronized (latest_completed_command) {
                        latest_completed_command = responses_in_token[responses_in_token.length - 2];
                    }
                    NewCommandHook();
                    buffer = responses_in_token[responses_in_token.length - 1];
                }
            }
        }
        }
        return result;
    }

    public void NewCommandHook() {}

    private String get_delimiter() {
        String result = "";
        if (CR) {
            result += '\r';
        }
        if (LF) {
            result += '\n';
        }
        return result;
    }
}
