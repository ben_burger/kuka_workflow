/**
 * Created by Benjamin on 6/14/2017.
 */
package Workflow.Com.Serial;

import Serialio.SerialConfig;
import Serialio.SerialPortLocal;
import Workflow.Configuration.Configuration;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import Workflow.Exception.*;
import com.sun.deploy.util.StringUtils;


public class ArdruinoCom extends SerialComSuper  {

    ScheduledExecutorService exec;
    public ArdruinoCom(String machine_name, String port) throws IOException, InterruptedException {
        super(port,machine_name);
        CR = false;
    }

    protected void InitializeSerialIO() throws IOException {
        super.InitializeSerialIO();
        Config = new SerialConfig(PortIdentifier);
        Config.setBitRate(SerialConfig.BR_9600); //9600
        Config.setDataBits(SerialConfig.LN_8BITS); //8 bit
        Config.setStopBits(SerialConfig.ST_1BITS); // 1bit
        Config.setParity(SerialConfig.PY_NONE); // None Parity
        Config.setHandshake(SerialConfig.HS_NONE); // None Flow Control
        SerialPort = new SerialPortLocal(Config);

        try {
            Thread.sleep(2000); //delay required for arduino to boot
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        exec  = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                CheckAlive();
            }
        }, 2000,Configuration.getHeartBeatFrequencyAdruino(), TimeUnit.MILLISECONDS);
    }



    protected void CheckAlive() {
        Boolean aliveResult = null;
        aliveResult = TestActive(aliveResult);
//        if(!aliveResult)
//        {
//            aliveResult = TestActive(aliveResult);
//            if(!aliveResult)
//            {
//                aliveResult = TestActive(aliveResult);
//            }
//        }

        synchronized ((isAlive))
        {
          isAlive = aliveResult;
        }
    }

    private Boolean TestActive(Boolean aliveResult) {
        synchronized (CommandLock)
        {
            Date start = new Date();
            try{
            //Flush buffer
            this.LisenLoop();

            this.SendCommandNaive("YoAdruinoAreYaAlive?");
            while (aliveResult == null) {
                LinkedList<String> new_responses = this.LisenLoop();
                String response = StringUtils.join(new_responses, "");
                response = response.replace("\n", "").replace("\r", "");
                if(response.contains("HeartBeat"))
                {
                    aliveResult = true;
                }

                long diffInMillies = (new Date()).getTime() -  start.getTime();
                if( TimeUnit.MILLISECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS) >  Configuration.getTimeoutAdruino())
                    throw new TimeOutException(getMachineIdentifier());
            }



            } catch(Exception e)
                {
                    aliveResult = false;
                }

        }
        return aliveResult;
    }

    @Override
    public void Destruct() throws IOException {
        super.Destruct();
        if(exec != null)
            exec.shutdown();
    }
    public boolean Start() {
        synchronized (CommandLock) {
            return SendCommandNaive("sjaak1");
        }
    }

    public boolean Stop()
    {
        synchronized (CommandLock) {
        return SendCommandNaive("sjaak0");
    }}

}
