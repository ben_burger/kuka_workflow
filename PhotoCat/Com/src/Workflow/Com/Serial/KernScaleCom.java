package Workflow.Com.Serial;

import Serialio.SerialConfig;
import Serialio.SerialPortLocal;
import Workflow.Configuration.Configuration;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Benjamin on 8/1/2017.
 */
public class KernScaleCom extends SerialComSuper implements IScale {
    public Integer latest_measurements_index = -1;
    public int min_measurements_stable = 15;
    public float latest_measurements[] = new float[min_measurements_stable];
    boolean ignore_first_value = true;
    Date last_weight = new Date();


    public KernScaleCom(String port) throws IOException, InterruptedException {
        super("LiquidScale",port);
        CR = false;
        POLLING_WAIT_TIME = 10;
        StartLisening();
    }
    @Override
    protected void CheckAlive() {
    }
    public boolean isAlive()
    {
        synchronized (isAlive) {
            long diffInMillies = (new Date()).getTime() -  last_weight.getTime();
            return TimeUnit.MILLISECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS) < Configuration.getTimeoutScale();
        }
    }

    protected void InitializeSerialIO() throws IOException {
        super.InitializeSerialIO();
        Config = new SerialConfig(PortIdentifier);
        Config.setBitRate(SerialConfig.BR_9600);
        Config.setDataBits(SerialConfig.LN_8BITS);
        Config.setParity(SerialConfig.PY_NONE);
        Config.setHandshake(SerialConfig.HS_NONE);
        Config.setStopBits(SerialConfig.ST_1BITS);
        SerialPort = new SerialPortLocal(Config);
    }

    public void NewCommandHook() {
        if(!ignore_first_value) {
            synchronized (latest_measurements_index) {
                synchronized (latest_completed_command) {
                    latest_measurements_index = (latest_measurements_index +1) % min_measurements_stable;
                    String whitespace_free_command = latest_completed_command.replaceAll("\\s+","").replaceAll("g","");
                    latest_measurements[latest_measurements_index] = Float.parseFloat(whitespace_free_command);
                }
            }
            synchronized (isAlive)
            {
                last_weight = new Date();
            }
        } else {
            ignore_first_value = false;
        }

    }


    public float getWeight() {
        float result = 0.f;
        boolean no_value = true;
        //Waiting on first value

        while(no_value)
        {
            try {

                synchronized (latest_measurements_index)
                {
                    no_value = latest_measurements_index == -1;
                    if(!no_value)
                        result = latest_measurements[latest_measurements_index];

                }
                if(no_value)
                    Thread.sleep(100);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public float getStableWeight()
    {
        //Hack to make sure we have A weight
        getWeight();
        float result = 0.0f;
        boolean stable = false;

        while(!stable)
        {
            synchronized (latest_measurements_index)
            {
                stable = true;
                result = latest_measurements[0];
                for(int i=0;i<latest_measurements.length;i++)
                {
                    stable = stable && latest_measurements[i] == result;
                }

            }
        }

        return result;
    }
}
