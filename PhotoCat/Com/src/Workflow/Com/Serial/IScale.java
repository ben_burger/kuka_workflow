package Workflow.Com.Serial;

import java.io.IOException;

public interface IScale {
    float getWeight();
    float getStableWeight();
    void Destruct() throws IOException;


}
