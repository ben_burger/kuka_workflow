package Workflow.Com.Serial;

import Serialio.SerialConfig;
import Serialio.SerialPortLocal;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;

/**
 * Created by Benjamin on 8/1/2017
 */
@Deprecated //The sacle is broken...
public class FisherScaleCom extends SerialComSuper implements IScale{
    public Float latest_measurement = 0.0f;
    boolean ignore_first_value = true;


    public FisherScaleCom(String port) throws IOException, InterruptedException {
        super("Liquidpump1Scale",port);
        CR = false;
        POLLING_WAIT_TIME = 10;
        StartLisening();
    }

    @Override
    protected void CheckAlive() {
        synchronized ((isAlive))
        {

            isAlive = false;
        }
    }

    protected void InitializeSerialIO() throws IOException {
        super.InitializeSerialIO();
        Config = new SerialConfig(PortIdentifier);
        Config.setBitRate(SerialConfig.BR_2400);
        Config.setDataBits(SerialConfig.LN_8BITS);
        Config.setParity(SerialConfig.PY_NONE);
        Config.setHandshake(SerialConfig.HS_XONXOFF);
        SerialPort = new SerialPortLocal(Config);
    }

    public void NewCommandHook() {
        if(!ignore_first_value) {
            synchronized (latest_measurement) {
                synchronized (latest_completed_command) {
                        latest_measurement = Float.parseFloat(latest_completed_command);
                }
            }
        } else {
            ignore_first_value = false;
        }
    }


    public float getWeight() {
        float result;
        synchronized (latest_measurement)
        {
            result = latest_measurement;
        }
        return result;
    }
    public float getStableWeight()
    {
        throw new NotImplementedException();
    }

}
