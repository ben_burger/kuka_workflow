package Workflow.Com.Serial;

import Serialio.SerialConfig;
import Serialio.SerialPortLocal;
import Workflow.Configuration.Configuration;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TemperatureProbe extends SerialComSuper  {
    public Double latest_measurement;
    private Date last_measurement;


    public TemperatureProbe(String port) throws IOException, InterruptedException {
        super("TemperatureProbe",port);
        LF = false;
        StartLisening();
    }
    @Override
    protected void CheckAlive() {
    }
    public boolean isAlive()
    {
        if(last_measurement == null)
            return false;
        synchronized (isAlive) {
            long diffInMillies = (new Date()).getTime() -  last_measurement.getTime();
            return TimeUnit.MILLISECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS) < 30*1000;
        }
    }

    protected void InitializeSerialIO() throws IOException {
        super.InitializeSerialIO();
        Config = new SerialConfig(PortIdentifier);
        Config.setBitRate(SerialConfig.BR_9600);
        Config.setDataBits(SerialConfig.LN_8BITS);
        Config.setParity(SerialConfig.PY_NONE);
        Config.setHandshake(SerialConfig.HS_NONE);
        Config.setStopBits(SerialConfig.ST_1BITS);

        SerialPort = new SerialPortLocal(Config);
        SerialPort.setDTR(true);
    }

    public Double getTemperature() {
            synchronized (isAlive) {
                return latest_measurement;
            }
    }

    public void NewCommandHook() {
            synchronized (isAlive) {
                synchronized (latest_completed_command) {
                    String whitespace_free_command = latest_completed_command.replaceAll("\\s+","").replaceAll("g","");
                    latest_measurement = Double.parseDouble(whitespace_free_command.substring(0, whitespace_free_command.length()-1));
                }
            }
            synchronized (isAlive)
            {
                last_measurement = new Date();
            }
    }

}
