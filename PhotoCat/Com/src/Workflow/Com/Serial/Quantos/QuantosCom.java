/**
 * Created by Benjamin on 6/14/2017.
 */
package Workflow.Com.Serial.Quantos;

import Workflow.Com.Serial.SerialComSuper;
import Workflow.Configuration.Configuration;
import Workflow.Util.SimpleStatus;
import Workflow.Util.Tuple;
import Serialio.SerialConfig;
import Serialio.SerialPortLocal;
import Workflow.Exception.QuantosException;
import Workflow.Exception.QuantosInputException;
import Workflow.Exception.UnknownResponseException;
import com.sun.deploy.util.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;

import java.io.IOException;
import java.rmi.UnexpectedException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import Workflow.Exception.*;

public class QuantosCom extends SerialComSuper {
    ScheduledExecutorService exec;

    public QuantosCom(String port_host) throws IOException, InterruptedException {
        super("Quantos",port_host);

        exec  = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                CheckAlive();
            }
        }, 1, Configuration.getHeartBeatFrequencyQuantos(), TimeUnit.MILLISECONDS);

    }

    @Override
    public void Destruct() throws IOException {
     super.Destruct();
     if(exec != null)
        exec.shutdown();
    }
    @Override
    protected void CheckAlive() {
        boolean aliveResult ;
        synchronized (CommandLock)
        {
            //Random command (Door Position)
            QuantosCommand cmd = new QuantosCommand("QRD 2 3 7");
                try {
                    Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
                aliveResult = true;
            } catch(Exception e)
            {
                aliveResult = false;
            }
        }

        synchronized ((isAlive))
        {
            isAlive = aliveResult;
        }

    }

    protected void InitializeSerialIO() throws IOException {
        super.InitializeSerialIO();
        Config = new SerialConfig(PortIdentifier);
        Config.setBitRate(SerialConfig.BR_9600); //9600
        Config.setDataBits(SerialConfig.LN_8BITS); //8 bit
        Config.setStopBits(SerialConfig.ST_1BITS); // 1bit
        Config.setParity(SerialConfig.PY_NONE); // None Parity
        Config.setHandshake(SerialConfig.HS_XONXOFF); // XONXOFF Flow Control
        SerialPort = new SerialPortLocal(Config);
    }


    // PowderDose p. 6
    public QuantosResponse DosePowder() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {

            QuantosCommand cmd = new QuantosCommand("QRA 61 1");
            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }
    }

    // CutLabel p. 7, not present

    // StopDosing p. 8
    public QuantosResponse StopDosing() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {
            QuantosCommand cmd = new QuantosCommand("QRA 61 4");
            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }}

    // GetDoorPosition p. 9
    public enum DoorPosition {
        Closed, Open, NotDetectable, Running

    }

    public DoorPosition GetDoorPosition() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosException, UnknownResponseException {
        synchronized (CommandLock) {QuantosCommand cmd = new QuantosCommand("QRD 2 3 7");
        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);

        if (!result.item1.equals(QuantosResponse.Success))
            throw new QuantosException(result.item1.toString());

        switch (result.item2[result.item2.length - 1]) {
            case "QRD 2 3 7 2 A":
                return DoorPosition.Closed;
            case "QRD 2 3 7 3 A":
                return DoorPosition.Open;
            case "QRD 2 3 7 8 A":
                return DoorPosition.NotDetectable;
            case "QRD 2 3 7 9 A":
                return DoorPosition.Running;
            default:
                throw new UnknownResponseException(getMachineIdentifier());
        }
    }}

    // GetSamplerPosition p. 10 does not actually work, move do door position is the only alternative
    // it more or less guarentees where the door is afterwards
    @Deprecated
    public int GetSamplerPositionFront() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosException {
        synchronized (CommandLock) {  QuantosCommand cmd = new QuantosCommand("QRD 2 3 8");
        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);

        if (!result.item1.equals(QuantosResponse.Success))
            throw new QuantosException(result.item1.toString());


        return Integer.parseInt(result.item2[result.item2.length - 1].split("\\s+")[4]);
    }}


    //GetSamplerStatus p. 11
    public SimpleStatus GetSamplerStatus() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosException, UnknownResponseException {
        synchronized (CommandLock) {
            QuantosCommand cmd = new QuantosCommand("QRD 2 2 8");

        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);

        if (!result.item1.equals(QuantosResponse.Success))
            throw new QuantosException(result.item1.toString());

        switch (result.item2[result.item2.length - 1]) {
            case "QRD 2 2 8 1 A":
                return SimpleStatus.Enabled;
            case "QRD 2 2 8 0 A":
                return SimpleStatus.Disabled;
            default:
                throw new UnknownResponseException(getMachineIdentifier());
        }
    }}

    //Gets weight in grams not milligrams like other method
    public float GetStableWeight() throws UnexpectedException {
        Date start = new Date();
        Float weightResult = null;
        synchronized (CommandLock) {
            try {
                this.LisenLoop();
                LinkedList<String> response = new LinkedList<String>();
                this.SendCommandNaive("S");
                Thread.sleep(100);
                while (weightResult == null) {
                    LinkedList<String> new_responses = this.LisenLoop();
                    response.addAll(new_responses);


                    String message_back = StringUtils.join(response, "");
                    String[] messages_back = message_back.split(" ");
                    for(String message :messages_back)
                    {
                        try {
                            weightResult = Float.parseFloat(message);
                        } catch (Exception e)
                        {

                        }
                    }
                    long diffInMillies = (new Date()).getTime() - start.getTime();
                    if (TimeUnit.MILLISECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS) > Configuration.getTimeoutCapper())
                        throw new TimeOutException(getMachineIdentifier());
                }

            } catch (Exception e) {
                throw new UnexpectedException("Can not get stable weight from Quantos");
            }

            return weightResult;
        }
    }

    //GetWeighingPanStatus p. 12
    public enum WeighingPanStatus {
        Empty, NotEmpty
    }

    public WeighingPanStatus GetWeighingPanStatus() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosException, UnknownResponseException {
        synchronized (CommandLock) {  QuantosCommand cmd = new QuantosCommand("QRD 2 2 9");
        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);

        if (!result.item1.equals(QuantosResponse.Success))
            throw new QuantosException(result.item1.toString());

        switch (result.item2[result.item2.length - 1]) {
            case "QRD 2 2 9 0 A":
                return WeighingPanStatus.Empty;
            case "QRD 2 2 9 1 A":
                return WeighingPanStatus.NotEmpty;
            default:
                throw new UnknownResponseException(getMachineIdentifier());
        }
    }}
    //GetHeadData p. 13,14
    public QuantosHeadInfo GetHeadData() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosException, ParseException, DocumentException {
        synchronized (CommandLock) {  QuantosCommand cmd = new QuantosCommand("QRD 2 4 11");
        Tuple<QuantosResponse, String[]> response = cmd.SendCommand(this);

        if ((response.item1.equals(QuantosResponse.NotMounted)))
            return null;
        else if (!(response.item1.equals(QuantosResponse.Success)))
            throw new QuantosException(response.item1.toString());

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < response.item2.length; i++) {
            String token = (String) response.item2[i];
            if (!(token.startsWith(cmd.CMDnoparameters))) {
                sb.append(token);
            }
        }


        Document document = DocumentHelper.parseText(sb.toString());
        QuantosHeadInfo result = new QuantosHeadInfo();

        result.Substance = document.selectNodes("/Info_head/Substance").iterator().next().getStringValue();
        result.RemainingDosages = Integer.parseInt(document.selectNodes("/Info_head/Rem._dosages").iterator().next().getStringValue());
        result.RemainingQuantity = Float.parseFloat(document.selectNodes("/Info_head/Rem._quantity").iterator().next().getStringValue());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
      //  result.ExpDate = simpleDateFormat.parse(document.selectNodes("/Info_head/Exp._date").iterator().next().getStringValue());
        result.HeadID = Long.parseLong(document.selectNodes("/Info_head/Head_ID").iterator().next().getStringValue());

        return result;
    }}

    //region Example Output for GetHead
    /*
    <?xml version="1.0" encoding="ISO-8859-1"?>
<Info_head xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="Info_head.xsd">
<Timestamp>2017-07-28T15:42:59</Timestamp>
<Substance>Sand</Substance>
<Lot_ID>0</Lot_ID>
<User_ID>Z</User_ID>
<Filling_date>2017-07-28</Filling_date>
<Exp._date>2019-07-28</Exp._date>
<Retest_date></Retest_date>
<Content Unit="mg">5000.000</Content>
<Rem._dosages>0</Rem._dosages>
<Terminal_SNR>B551860100</Terminal_SNR>
<Bridge_SNR>B551860100</Bridge_SNR>
<Balance_Type>XPE206DR</Balance_Type>
<Balance_ID>B551860100</Balance_ID>
<Last_cal.>2017-07-28</Last_cal.>
<Option_SNR>11670910A</Option_SNR>
<Powder_module_SNR>B604033568</Powder_module_SNR>
<Appl._Name>-------- DOSING --------</Appl._Name>
<ID1>
<Label>SAND</Label>
<Value></Value>
</ID1>
<ID2>
<Label>Var 2</Label>
<Value>Value 2</Value>
</ID2>
<ID3>
<Label>Var 3</Label>
<Value>Value 3</Value>
</ID3>
<ID4>
<Label>Var 4</Label>
<Value>Value 4</Value>
</ID4>
<Title_1>T1</Title_1>
<Title_2>T2</Title_2>
<Date_Time>2017-07-28T15:42:59</Date_Time>
<Levelcontrol>Balance is leveled</Levelcontrol>
<Head_prod._date>2015-12-23</Head_prod._date>
<Head_type>QH002-CNMW</Head_type>
<Head_ID>247049008140</Head_ID>
<Dose_limit>36</Dose_limit>
<Accuracy Unit="%">0.50</Accuracy>
<Dosing_counter>36</Dosing_counter>
<Rem._quantity Unit="mg">4985.280</Rem._quantity>
</Info_head>
     */
    //endregion

    //GetSampleData p. 15, 16
    public QuantosDispenseInformation GetSampleData() throws QuantosException, InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, DocumentException, ParseException {
        synchronized (CommandLock) {
            QuantosCommand cmd = new QuantosCommand("QRD 2 4 12");
            Tuple<QuantosResponse, String[]> response = cmd.SendCommand(this);

            if (!response.item1.equals(QuantosResponse.Success))
                throw new QuantosException(response.item1.toString());

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < response.item2.length; i++) {
                String token = (String) response.item2[i];
                if (!(token.startsWith(cmd.CMDnoparameters))) {
                    sb.append(token);
                }
            }

            Document document = DocumentHelper.parseText(sb.toString());

            QuantosDispenseInformation s = new QuantosDispenseInformation();
            s.Substance = document.selectNodes("/Dosing/Substance").iterator().next().getStringValue();
            s.SampleID = document.selectNodes("/Dosing/Sample_ID").iterator().next().getStringValue();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-dd-MM");
            s.Timestamp = simpleDateFormat.parse(document.selectNodes("/Dosing/Date_Time").iterator().next().getStringValue());
            s.Content = Float.parseFloat(document.selectNodes("/Dosing/Content").iterator().next().getStringValue());
            s.Target = Float.parseFloat(document.selectNodes("/Dosing/Target_quantity").iterator().next().getStringValue());
            s.Sample_position = Integer.parseInt(document.selectNodes("/Dosing/Sample_position").iterator().next().getStringValue());
            return s;
        }
    }
    //example XML GetSampleData
    /*
<?xml version="1.0" encoding="ISO-8859-1"?>
<Dosing xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="Dosing.xsd">
<Timestamp>2017-07-28T15:57:38</Timestamp>
<Substance>Sand</Substance>
<Sample_ID>BBQUANTOSTEST</Sample_ID>
<Lot_ID>0</Lot_ID>
<User_ID>Z</User_ID>
<Dispense_date>2017-07-28</Dispense_date>
<Exp._date>2019-07-28</Exp._date>
<Retest_date></Retest_date>
<Content Unit="mg">7.685</Content>
<Validity>INVALID</Validity>
<Target_quantity Unit="mg">5.000</Target_quantity>
<Tolerance Unit="%">10.0</Tolerance>
<Samples>0</Samples>
<Act._Sample_NR>0</Act._Sample_NR>
<Rem._samples>0</Rem._samples>
<Rem._dosages>959</Rem._dosages>
<Sample_position>1</Sample_position>
<Terminal_SNR>B551860100</Terminal_SNR>
<Bridge_SNR>B551860100</Bridge_SNR>
<Balance_Type>XPE206DR</Balance_Type>
<Balance_ID>B551860100</Balance_ID>
<Last_cal.>2017-07-28</Last_cal.>
<Option_SNR>11670910A</Option_SNR>
<Powder_module_SNR>B604033568</Powder_module_SNR>
<Appl._Name>-------- DOSING --------</Appl._Name>
<ID1>
<Label>SAND</Label>
<Value></Value>
</ID1>
<ID2>
<Label>Var 2</Label>
<Value>Value 2</Value>
</ID2>
<ID3>
<Label>Var 3</Label>
<Value>Value 3</Value>
</ID3>
<ID4>
<Label>Var 4</Label>
<Value>Value 4</Value>
</ID4>
<Title_1>T1</Title_1>
<Title_2>T2</Title_2>
<Date_Time>2017-07-28T15:57:39</Date_Time>
<Levelcontrol>Balance is leveled</Levelcontrol>
<Head_prod._date>2015-12-23</Head_prod._date>
<Head_type>QH002-CNMW</Head_type>
<Head_ID>247049008140</Head_ID>
<Dose_limit>999</Dose_limit>
<Accuracy Unit="%">0.50</Accuracy>
<Dosing_counter>40</Dosing_counter>
<Rem._quantity Unit="mg">4961.650</Rem._quantity>
<MinWeigh Name="">Off</MinWeigh>
<Dose_duration Unit="s">20</Dose_duration>
<Powder_dosing_mode>Standard</Powder_dosing_mode>
<Tapping_while_dosing>On</Tapping_while_dosing>
<Tapping_before_dosing>Off</Tapping_before_dosing>
<Intensity>50</Intensity>
<Duration Unit="s">1</Duration>
<Tolerance_Mode>+/- Tolerance</Tolerance_Mode>
<SW-Version>2.00</SW-Version>
</Dosing>
     */

    //PrintLabelSampleData p. 17
    public QuantosResponse PrintLabelSampleData() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {   QuantosCommand cmd = new QuantosCommand("QRD 2 5 12");
        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
        return result.item1;
    }}

    //PrintProtocoLSampleDataToStripprinter p. 18
    public QuantosResponse PrintProtocoLSampleDataToStripprinter() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {        QuantosCommand cmd = new QuantosCommand("QRD 2 6 12");
        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
        return result.item1;
    }}

    //MoveDosingHeadPin p.19
    public QuantosResponse LockDosingHeadPin(boolean lock) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {
            QuantosCommand cmd = new QuantosCommand("QRA 60 2");

            if (lock)
                cmd.CMD += " 4";
            else
                cmd.CMD += " 3";

            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }    }

    //MoveDoor p.20
    public QuantosResponse MoveDoor(boolean open) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {
            QuantosCommand cmd = new QuantosCommand("QRA 60 7");

            if (open)
                cmd.CMD += " 3";
            else
                cmd.CMD += " 2";


            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }
    }

    //MoveAutoSampler p.21
    public QuantosResponse MoveAutoSampler(int front_position) throws QuantosInputException, InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {    if (front_position < 0 || front_position > 30)
            throw new QuantosInputException("Only positions 0(home) and 1-30 are allowed");

        QuantosCommand cmd = new QuantosCommand("QRA 60 8");

        cmd.CMD += " " + front_position;
        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
        return result.item1;
    }}

    public QuantosResponse SetTappingBeforeDosing(boolean tap) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {

            String command;

            if(tap)
            {
                command = "QRD 1 1 1 1";

            } else
            {
                command = "QRD 1 1 1 0";
            }
            QuantosCommand cmd = new QuantosCommand(command);
            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }
    }


    public QuantosResponse SetTappingWhileDosing(boolean tap) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {

            String command;

            if(tap)
            {
                command = "QRD 1 1 2 1";

            } else
            {
                command = "QRD 1 1 2 0";
            }
            QuantosCommand cmd = new QuantosCommand(command);
            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }
    }

    public QuantosResponse SetTappingIntensity(int intensity) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosInputException {
        synchronized (CommandLock) {
            if (intensity < 10 || intensity > 100)
                throw new QuantosInputException("Only positive intensity are allowed wintin the range 10  - 100 allowed");

            QuantosCommand cmd = new QuantosCommand("QRD 1 1 3 " + intensity);
            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }
    }

    //Tapper actions to set tapping: before,after, intensity, duration p.22-25 ignored for now

    //SetTargetValue p.26
    public QuantosResponse SetTargetValue(float target_value_mg) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosInputException {
        synchronized (CommandLock) {  if (target_value_mg < 0.10 || target_value_mg > 250000)
            throw new QuantosInputException("Only positive target weights are allowed wintin the range 0.10  - 250000 allowed");

        QuantosCommand cmd = new QuantosCommand("QRD 1 1 5");

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        cmd.CMD += " " + df.format(target_value_mg);

        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
        return result.item1;
    }}

    //SetTargetTolerance p.27
    public QuantosResponse SetTargetTolerance(float target_tolerance_percentage) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosInputException {
        synchronized (CommandLock) {
            if (target_tolerance_percentage < 0.10f || target_tolerance_percentage > 40.0f)
                throw new QuantosInputException("Only positive target weights are allowed wintin the range 0.1  - 40.0 allowed");

            QuantosCommand cmd = new QuantosCommand("QRD 1 1 6");

            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(1);
            cmd.CMD += " " + df.format(target_tolerance_percentage);

            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }    }

    //SetTargetToleranceMode p.28
    public enum ToleranceMode {
        PlusMinus, ZeroPlus
    }

    public QuantosResponse SetToleranceMode(ToleranceMode mode) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {
            QuantosCommand cmd = new QuantosCommand("QRD 1 1 7");

            switch (mode) {
                case PlusMinus:
                    cmd.CMD += " 0";
                    break;
                case ZeroPlus:
                    cmd.CMD += " 1";
                    break;
            }


            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }
    }
    //SetSampleIdentifier p.29
    public QuantosResponse SetSampleIdentifier(String sample_identifier) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosInputException {
        synchronized (CommandLock) {
            if (sample_identifier.length() > 20)
                throw new QuantosInputException("The maximal sample identifier is 20 characters long");

            QuantosCommand cmd = new QuantosCommand("QRD 1 1 8");

            if (sample_identifier != null)
                cmd.CMD += " " + sample_identifier;
            else
                cmd.CMD += " NULL";


            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }
    }

    //SetValuePan p.30
    //Use of this field is unclear
    @Deprecated
    public QuantosResponse SetWeighingPanAsEmpty() throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {
        QuantosCommand cmd = new QuantosCommand("QRD 1 1 9");
        cmd.CMD += " 0";

        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
        return result.item1;
    }}

    //SetUserID p.31
    public QuantosResponse SetUserID(String userID) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosInputException {
        synchronized (CommandLock) {  if (userID.length() > 20)
            throw new QuantosInputException("The maximal userID is 20 characters long");
        QuantosCommand cmd = new QuantosCommand("QRD 1 1 13");
        cmd.CMD += " " + userID;

        Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
        return result.item1;
    }}

    //SetDispensingAlgorithm p.32


    public QuantosResponse SetDispensingAlgorithm(DispensingAlgorithm algorithm) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {
            QuantosCommand cmd = new QuantosCommand("QRD 1 1 14");

            switch (algorithm) {
                case Standard:
                    cmd.CMD += " 0";
                    break;
                case Advanced:
                    cmd.CMD += " 1";
                    break;
            }

            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }    }

    //SetAntiStaticKit p.33
    public QuantosResponse SetAntiStaticKit(SimpleStatus status) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        synchronized (CommandLock) {
            QuantosCommand cmd = new QuantosCommand("QRD 1 1 15");

            switch (status) {
                case Enabled:
                    cmd.CMD += " 1";
                    break;
                case Disabled:
                    cmd.CMD += " 0";
                    break;
            }

            Tuple<QuantosResponse, String[]> result = cmd.SendCommand(this);
            return result.item1;
        }    }

    public enum SamplerBackPosition{Vials1TO5,Vials6TO10,Vials11TO15, Vials16TO20, Vials21TO25, Vials26TO30}

    public QuantosResponse SetBackSamplerPosition(SamplerBackPosition position) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException, QuantosInputException {
        synchronized (CommandLock) {int home_position;
        switch (position) {
            case Vials1TO5:
                home_position = 19;
                break;
            case Vials6TO10:
                home_position = 24 ;
                break;
            case Vials11TO15:
                home_position = 29;
                break;
            case Vials16TO20:
                home_position = 3;
                break;
            case Vials21TO25:
                home_position = 8;
                break;
            default:
            case Vials26TO30:
                home_position = 13;
                break;
        }
        return MoveAutoSampler(home_position);
    }
    }

}