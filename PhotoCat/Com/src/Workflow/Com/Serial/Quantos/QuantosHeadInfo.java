package Workflow.Com.Serial.Quantos;

import java.util.Date;

/**
 * Created by taurnist on 28/07/2017.
 */
public class QuantosHeadInfo {
    public String Substance;
    public float RemainingQuantity;
    public int RemainingDosages;
    public Date ExpDate;
    public long HeadID;


    public QuantosHeadInfo() {

    }
}
