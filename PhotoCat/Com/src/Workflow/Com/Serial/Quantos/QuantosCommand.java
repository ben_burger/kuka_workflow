package Workflow.Com.Serial.Quantos;

import Workflow.Com.Serial.Command;
import Workflow.Com.Serial.SerialComSuper;
import Workflow.Util.Tuple;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import Workflow.Configuration.Configuration;
import Workflow.Exception.TimeOutException;
import Workflow.Exception.UnknownErrorResponseException;

public class QuantosCommand extends Command<QuantosResponse> {

    public QuantosCommand(String cmd) {
        super(cmd);

        SuccessResponse = QuantosResponse.Success;
        FaiLParameterResponse = QuantosResponse.FailParameter;
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    public QuantosCommand clone() throws CloneNotSupportedException {
        QuantosCommand clone = new QuantosCommand(this.CMDnoparameters);
        return clone;
    }

    QuantosResponse SuccessResponse;
    QuantosResponse FaiLParameterResponse;

    private static final Tuple<Integer, QuantosResponse>[] QuantosReponseCodes =
            new Tuple[]{
                    new Tuple(1, QuantosResponse.NotMounted),
                    new Tuple(2, QuantosResponse.AnotherJob),
                    new Tuple(3, QuantosResponse.Timeout),
                    new Tuple(4, QuantosResponse.NotSelected),
                    new Tuple(5, QuantosResponse.NotAllowedNow),
                    new Tuple(6, QuantosResponse.WeightInstable),
                    new Tuple(7, QuantosResponse.PowderFlowError),
                    new Tuple(8, QuantosResponse.ExternalAction),
                    new Tuple(9, QuantosResponse.SafeposError),
                    new Tuple(10, QuantosResponse.HeadNotAllowed),
                    new Tuple(11, QuantosResponse.HeadLimitReached),
                    new Tuple(12, QuantosResponse.HeadExpirydateReached),
                    new Tuple(13, QuantosResponse.SamplerBlocked),
            };



    public Tuple<QuantosResponse, String[]> SendCommand(SerialComSuper RS232) throws InterruptedException, TimeOutException, UnknownErrorResponseException, IOException {
        return SendCommand(RS232, 0);
    }

    public Tuple<QuantosResponse, String[]> SendCommand(SerialComSuper RS232, int counter) throws TimeOutException, InterruptedException, UnknownErrorResponseException, IOException {

        Date start = new Date();

        QuantosResponse result = null;
        LinkedList<String> Responses = new LinkedList();

        //Flush buffer
        RS232.LisenLoop();

        RS232.SendCommandNaive(CMD);
        while (result == null) {
            LinkedList<String> new_responses = RS232.LisenLoop();
            if (new_responses != null && !new_responses.isEmpty()) {
                Responses.addAll(new_responses);
                String latest_completed_command_local = new_responses.getLast();

                if (latest_completed_command_local != null) {
                    String[] command_split = CMDnoparameters.split(" ");

                    if (latest_completed_command_local.endsWith(" A"))
                        result = SuccessResponse;
                    else if (latest_completed_command_local.equals(String.join(" ", Arrays.copyOf(command_split, command_split.length - 1)) + " L"))
                        result = FaiLParameterResponse;
                    else {
                        if (latest_completed_command_local.equals(CMDnoparameters + " I")) {
                            throw new UnknownErrorResponseException(RS232.getMachineIdentifier());
                        } else if (latest_completed_command_local.startsWith(CMDnoparameters + " I")) {
                            result = FaiLParameterResponse;//needless, but compiler warning suppression
                            String[] bits = latest_completed_command_local.split(" ");
                            int error_number = Integer.parseInt(bits[bits.length - 1]);
                            for (Tuple<Integer, QuantosResponse> tuple : QuantosReponseCodes)
                                if (tuple.item1 == error_number) {
                                    result = tuple.item2;
                                    break;
                                }
                        } else {
                            if (!latest_completed_command_local.endsWith("B")) {
                                // We can be receiving XML here, but not sure how to TODO error detection
                                // So disable this error for it hsa been running strong like this...
                                // throw new Exception("unknown error returned, last command received: " + latest_completed_command);
                            }
                        }
                    }
                }
            }
            long diffInMillies = (new Date()).getTime() -  start.getTime();
            if( TimeUnit.MILLISECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS) > Configuration.getTimeoutQuantos())
                throw new TimeOutException(RS232.getMachineIdentifier());
        }

        // some recursion on the side
        if ((result.equals(QuantosResponse.NotAllowedNow)))
            //Not now? we have try again a couple of times later
            if (counter <= 5) {
                Thread.sleep(2000);
                return SendCommand(RS232, counter + 1);
            }

        String[] response_array = new String[Responses.size()];
        response_array = Responses.toArray(response_array);
        return new Tuple(result, response_array);
    }
}
