package Workflow.Com.Serial.Quantos;

import java.util.Date;

/**
 * Created by taurnist on 28/07/2017.
 */
public class QuantosDispenseInformation {
    public String Substance;
    public String SampleID;
    public Date Timestamp;
    public float Content;
    public float Target;
    public int Sample_position;
}
