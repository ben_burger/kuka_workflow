package Workflow.Com.Serial.Quantos;

/**
 * Created by benjamin on 7/27/17.
 */
public enum QuantosResponse {
    Success, FailParameter, NotMounted, AnotherJob, Timeout, NotSelected, NotAllowedNow, WeightInstable, PowderFlowError, ExternalAction, SafeposError, HeadNotAllowed, HeadLimitReached, HeadExpirydateReached, SamplerBlocked
}
