/**
 * Created by Benjamin on 6/14/2017.
 */
package Workflow.Com.Serial;
import Serialio.*;
import Workflow.Configuration.Configuration;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import Workflow.Exception.TimeOutException;

@Deprecated //We think this is a needlessly expensive device for our purposes
public class LiquidPumpCom extends SerialComSuper  implements IPump{

private int rate = 100;
    public LiquidPumpCom(String port_pump) throws IOException, InterruptedException {
        super("Liquidpump1",port_pump);
        LF = false;
    }
    private float PID_P = 60;
    private float PID_I = -0.3f;
    private float PID_D = 1;


    @Override
    public float GetP() {
        return PID_P;
    }

    @Override
    public float GetI() {
        return PID_I;
    }

    @Override
    public float GetD() {
        return PID_D;
    }

    @Override
    public int GetNumberOfPoints() {
        return 50;
    }

    @Override
    public int getMinimalSpeed() {
        return 2;
    }

    ScheduledExecutorService exec;

    protected void InitializeSerialIO() throws IOException {
        super.InitializeSerialIO();
        Config = new SerialConfig(PortIdentifier);
        Config.setBitRate(SerialConfig.BR_9600); //9600
        Config.setDataBits(SerialConfig.LN_8BITS); //8 bit
        Config.setStopBits(SerialConfig.ST_2BITS); // 2bit
        Config.setParity(SerialConfig.PY_NONE); // None Parity
        Config.setHandshake(SerialConfig.HS_NONE); // None Flow Control
        SerialPort = new SerialPortLocal(Config);
        exec  = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                CheckAlive();
            }
        }, 0,100, TimeUnit.MILLISECONDS);
    }
    @Override
    protected void CheckAlive() {
            Boolean result;
            try
            {
                GetRunningStatus();
                result = true;

            } catch(TimeOutException e)
            {
                result = false;
            } catch (InterruptedException e) {
                result = false;
            } catch (IOException e) {
                result = false;
            }
        synchronized (isAlive)
        {
            isAlive  = result;
        }
    }


    @Override
    public boolean Forward() {
        return false;
    }

    @Override
    public boolean Reverse() {
        return false;
    }

    public boolean Start(int i ) {
        synchronized (CommandLock) {
            return SendCommandNaive(i+"GO");
        }
    }

    public boolean Stop()
    {
        synchronized (CommandLock) {
            return SendCommandNaive("1ST");
        }
    }

    public boolean SetRate(int rate)
    {
        synchronized (CommandLock) {
        String rate_s = Integer.toString(rate);
        while(rate_s.length() < 3)
        {
            rate_s = "0" + rate_s;
        }

        return SendCommandNaive("1SP"+rate_s);
    }}

    public boolean GetRunningStatus() throws TimeOutException, IOException, InterruptedException {

        synchronized (CommandLock) {
            String rate_s = Integer.toString(rate);
            while(rate_s.length() < 3)
            {
                rate_s = "0" + rate_s;
            }


                LisenLoop();
                SendCommandNaive("1ZY" + rate_s);
            Date start = new Date();

                while( TimeUnit.MILLISECONDS.convert((new Date()).getTime() -  start.getTime(), TimeUnit.MILLISECONDS) < Configuration.getTimeoutAdruino())
                {
                    LinkedList<String> new_responses = LisenLoop();
                    if(new_responses.size() !=0) {
                        return new_responses.getFirst().equals("1");
                    }

                }
            throw new TimeOutException(getMachineIdentifier());
        }}
    public void Destruct() throws IOException {
        super.Destruct();
        if(exec != null)
            exec.shutdown();
    }
}
