package Workflow.Com.Serial;

import Workflow.Exception.TimeOutException;

import java.io.IOException;

/**
 * Created by Benjamin on 3/27/2018.
 */
public interface IPump {

    boolean Forward() ;
    boolean Reverse() ;
    boolean Start(int i) ;
    boolean Stop();
    boolean SetRate(int rate);
    boolean GetRunningStatus() throws TimeOutException, IOException, InterruptedException ;
    void Destruct() throws IOException;

    float GetP();
    float GetI();
    float GetD();

    int GetNumberOfPoints();

    int getMinimalSpeed();

}
