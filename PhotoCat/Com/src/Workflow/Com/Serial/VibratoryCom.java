/**
 * Created by Benjamin on 6/14/2017.
 */
package Workflow.Com.Serial;

import Serialio.SerialConfig;
import Serialio.SerialPortLocal;
import Workflow.Configuration.Configuration;
import Workflow.Exception.TimeOutException;
import com.sun.deploy.util.StringUtils;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class VibratoryCom extends ArdruinoCom  {

      public VibratoryCom(String port) throws IOException, InterruptedException {
        super(port,"VibPhotocat");

    }
    @Override
    public void Destruct() throws IOException {
        super.Destruct();
    }
    public boolean StartDry() {
        synchronized (CommandLock) {
            return SendCommandNaive("dry1");
        }
    }

    public boolean StopDry()
    {
        synchronized (CommandLock) {
            return SendCommandNaive("dry0");
        }
    }

}
