package Workflow.Com.TCP;

import Workflow.Com.ComSuper;
import Workflow.Exception.*;
import Workflow.KMR.KMRRobotStatus;
import Workflow.KMR.Messages.KMRTask;
import Workflow.KMR.Messages.KMRTaskTypeEnum;
import Workflow.LBR.LBRRobotStatus;
import Workflow.LBR.LBRTCPServer;
import Workflow.LBR.Messages.*;
import Workflow.TCP.Messages.TaskRequest;
import Workflow.TCP.Messages.TaskResponse;
import Workflow.TCP.RobotStatusEnum;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
//import Workflow.TCP.TCPServerSuper2;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Benjamin on 3/11/2018.
 */
public class LBRCom extends ComSuper {
    LBRTCPServer lbr_server;

    public LBRCom() throws IOException {
        lbr_server = new LBRTCPServer();

    }

    @Override
    public boolean isAlive() {
        return lbr_server.getClientConnected();
    }

    public void Destruct() throws IOException {
        if (lbr_server != null)
            lbr_server.Destruct();
    }

    public LBRRobotStatus SixPointCalibrationAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(SixPointCalibration());
    }

    public TaskResponse SixPointCalibration() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.SixPointCalibration);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    private void ProcessResponse(TaskResponse taskResponse) throws RobotBusyException, NotAllowedException, RobotErrorStateException {

        switch (taskResponse.getResponse())
        {

            case Accepted:
                break;
            case Busy:
                throw new RobotBusyException();
            case Error:
                throw new RobotErrorStateException();
            case NotAllowed:
                throw new NotAllowedException();
        }
    }

    public LBRRobotStatus TorqueAndPositionReferencingAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(TorqueAndPositionReferencing());
    }

    public TaskResponse TorqueAndPositionReferencing() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.CalibratePosAndTorque);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRTCPServer GetServer() {
        return lbr_server;
    }

    public LBRRobotStatus ArmToDrivePosAndWait() throws CloneNotSupportedException, IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(ArmToDrivePos());
    }

    public TaskResponse ArmToDrivePos() throws CloneNotSupportedException, IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.DrivePos);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public void FinishArmManipulationsAndWait(KMRCom kmr) throws IOException, InterruptedException, CloneNotSupportedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        FinishArmManipulations();

        kmr.WaitForCommandable();
    }

    public LBRTask FinishArmManipulations() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.Finish);

        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);

        Thread.sleep(1000);

        Date date_finish = lbr_server.getLastHeartBeatReceived();
        boolean lbr_offline = (new Date()).getTime() - date_finish.getTime() > 1000;
        while (!lbr_offline) {
            lbr_offline = (new Date()).getTime() - date_finish.getTime() > 1000;
            date_finish = lbr_server.getLastHeartBeatReceived();
            Thread.sleep(50);
        }
        return task;
    }

    public LBRRobotStatus WaitForTaskCompletion(TaskResponse response) throws RobotErrorStateException, CloneNotSupportedException, InterruptedException, RobotNoHeartBeatException, DeviceOfflineException {
        WaitForTaskToStart(response);
        return WaitForCommandable();
    }

    public LBRRobotStatus WaitForTaskToStart(TaskResponse response) throws RobotErrorStateException, CloneNotSupportedException, InterruptedException {
        //Still sleep, but now only to make sure there is time to allow a new heartbeat to be sent,
        // otherwise this function will always instantly finish before a new heartbeat!!
        LBRTask new_task = (LBRTask) response.getTask();
        LBRRobotStatus staat = lbr_server.GetStatus();
        boolean robot_started_task = false;
        LBRTask active_task = ((LBRTask) staat.getTask());
        robot_started_task = active_task != null && active_task.getType().equals(new_task.getType());
        while (!robot_started_task) {
            Thread.sleep(50);
            staat = lbr_server.GetStatus();
            active_task = ((LBRTask) staat.getTask());
            robot_started_task = active_task != null && active_task.equals(new_task);
        }

        if (staat.getStatus().equals(RobotStatusEnum.Error)) {
            throw new RobotErrorStateException("Abort all, robot is in collision or other severe error");
        }

        return staat;
    }


    public LBRRobotStatus WaitForCommandable() throws CloneNotSupportedException, InterruptedException, DeviceOfflineException, RobotErrorStateException, RobotNoHeartBeatException {
        //Still sleep, but now only to make sure there is time to allow a new heartbeat to be sent,
        // otherwise this function will always instantly finish before a new heartbeat!!

        //Wait for robot to get online for maximum of 10 seconds
        Date before_loop = new Date();
        Date now = new Date();

        LBRRobotStatus staat = lbr_server.GetStatus();
        boolean robot_disconnected = staat.getStatus().equals(RobotStatusEnum.Disconnected);
        if (robot_disconnected) {
            while (robot_disconnected && (now.getTime() - before_loop.getTime() < (180 * 1000))) //This scenario typically
            // happens when the arm is started, in such case, the server may be waiting since the KMR was instructed to
            // MOVE and simultainously start the arm, seeing a move can take up to 90 seconds, we give it some extra tolerance
            {
                Thread.sleep(50);
                staat = lbr_server.GetStatus();
                robot_disconnected = staat.getStatus().equals(RobotStatusEnum.Disconnected);
                now = new Date();
            }

            if (robot_disconnected)
                throw new DeviceOfflineException("LBR");
        }


        before_loop = new Date();
        now = new Date();
        boolean robot_not_alive_heartbeat = this.isAlive();
        while (!robot_not_alive_heartbeat && (now.getTime() - before_loop.getTime() < (10 * 1000))) {
            Thread.sleep(50);
            robot_not_alive_heartbeat = this.isAlive();
            now = new Date();
        }

        if (!robot_not_alive_heartbeat)
            throw new RobotNoHeartBeatException("LBR");

        staat = lbr_server.GetStatus();
        boolean task_running = staat.getStatus().equals(RobotStatusEnum.TaskRunning) && isAlive();
        while (task_running) {
            Thread.sleep(50);
            staat = lbr_server.GetStatus();
            task_running = staat.getStatus().equals(RobotStatusEnum.TaskRunning) && isAlive();
        }

        if (!isAlive())
            throw new DeviceOfflineException("LBR");

        if (staat.getStatus().equals(RobotStatusEnum.Error) || staat.getStatus().equals(RobotStatusEnum.Disconnected)) {
            throw new RobotErrorStateException("Abort all, robot is in collision or other severe error: " + staat.getStatus().toString());
        } else if (staat.getStatus().equals(RobotStatusEnum.Commandable))
            return staat;

        throw new NotImplementedException();
    }

    public LBRRobotStatus PlaceRackInBufferAndWait(int robot_index, int station_index) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(PlaceRackInBuffer(robot_index, station_index));

    }

    public TaskResponse PlaceRackInBuffer(int robot_index, int station_index) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTakeRackFromRobotAndPlaceOnStation task = new LBRTakeRackFromRobotAndPlaceOnStation(LBRTaskTypeEnum.TakeRackFromRobotAndPlaceOnStation, robot_index, station_index);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus GraspRackFromBufferAndWait(int robot_index, int station_index) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(GraspRackFromBuffer(robot_index, station_index));
    }

    public TaskResponse GraspRackFromBuffer(int robot_index, int station_index) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTakeRackFromStationAndPlaceOnRobot task = new LBRTakeRackFromStationAndPlaceOnRobot(LBRTaskTypeEnum.TakeRackFromStationAndPlaceOnRobot, robot_index, station_index);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus StartCartridgeManipulationsAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(StartCartridgeManipulations());
    }

    public TaskResponse StartCartridgeManipulations() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.CartridgeStart);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus FinishCartridgeManipulationsAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(FinishCartridgeManipulations());
    }

    public TaskResponse FinishCartridgeManipulations() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.CartridgeFinish);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus PlaceCartridgeManipulationsAndWait(int index) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(PlaceCartridgeManipulations(index));
    }

    public TaskResponse PlaceCartridgeManipulations(int index) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRCartridgePlaceCartidgeInHotel task = new LBRCartridgePlaceCartidgeInHotel(LBRTaskTypeEnum.CartridgePlaceInHotel, index);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus CartridgeRemoveFromHotelAndWait(int index) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(CartridgeRemoveFromHotel(index));
    }

    public TaskResponse CartridgeRemoveFromHotel(int index) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRCartridgeRemoveCartidgeFromHotel task = new LBRCartridgeRemoveCartidgeFromHotel(LBRTaskTypeEnum.CartridgeRemoveFromHotel, index);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus StartVialHandlingAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(StartVialHandling());
    }

    public TaskResponse StartVialHandling() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.StartVialHandling);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus StopVialHandlingAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(StopVialHandling());
    }

    public TaskResponse StopVialHandling() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.StopVialHandling);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus GraspVialFromBufferAndWait(int index) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(GraspVialFromBuffer(index));
    }

    public TaskResponse GraspVialFromBuffer(int index) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRGraspVialFromBuffer task = new LBRGraspVialFromBuffer(LBRTaskTypeEnum.GraspVialFromBuffer, index);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus PlaceVialInBufferAndWait(int index) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(PlaceVialInBuffer(index));
    }

    public TaskResponse PlaceVialInBuffer(int index) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRPlaceVialInBuffer task = new LBRPlaceVialInBuffer(LBRTaskTypeEnum.PlaceVialInBuffer, index);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus RemoveCartridgeFromQuantosAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(RemoveCartridgeFromQuantos());
    }

    public TaskResponse RemoveCartridgeFromQuantos() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.CartridgeRemoveFromQuantos);

        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus PlaceCartridgeFromQuantosAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(PlaceCartridgeFromQuantos());
    }

    public TaskResponse PlaceCartridgeFromQuantos() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.CartridgePlaceInQuantos);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus GraspSampleFromQuantosAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(GraspSampleFromQuantos());
    }

    public TaskResponse GraspSampleFromQuantos() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.GraspVialFromQuantos);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus PlaceSampleInQuantosAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(PlaceSampleInQuantos());
    }

    public TaskResponse PlaceSampleInQuantos() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.PlaceVialInQuantos);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus PlaceVialInCapperAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(PlaceVialInCapper());
    }

    public TaskResponse PlaceVialInCapper() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.PlaceVialInCapper);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus GraspVialFromCapperAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(GraspVialFromCapper());
    }

    public TaskResponse GraspVialFromCapper() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.GraspVialFromCapper);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus PlaceVialInSonicatorAndWait(int index) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(PlaceVialInSonicator(index));
    }

    public TaskResponse PlaceVialInSonicator(int index) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRPlaceVialInSonicator task = new LBRPlaceVialInSonicator(LBRTaskTypeEnum.PlaceVialInSonicator, index);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus GraspVialFromSonicatorAndWait(int index) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(GraspVialFromSonicator(index));
    }

    public TaskResponse GraspVialFromSonicator(int index) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRGraspVialFromSonicator task = new LBRGraspVialFromSonicator(LBRTaskTypeEnum.GraspVialFromSonicator, index);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus PressParkHeadspaceButtonAndWait() throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(PressParkHeadspaceButton());
    }

    public TaskResponse PressParkHeadspaceButton() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.PressHeadSpaceParkButton);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus PlaceVialInLiquidStationAndWait(int i) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(PlaceVialInLiquidStation(i));
    }

    public TaskResponse PlaceVialInLiquidStation(int i) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRPlaceVialInLiquidStation(LBRTaskTypeEnum.PlaceVialInLiquidStation, i);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public LBRRobotStatus GraspVialFromLiquidStationAndWait(int i) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        ;
        return WaitForTaskCompletion(GraspVialFromLiquidStation(i));
    }

    public TaskResponse GraspVialFromLiquidStation(int i) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRGraspVialFromLiquidStation(LBRTaskTypeEnum.GraspVialInLiquidStation, i);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public TaskResponse MoveVialFromStationOneToStationTwo() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.MoveVialFromLiquidStationOneToTwo);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }
    public LBRRobotStatus MoveVialFromStationOneToStationTwoAndWait() throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(MoveVialFromStationOneToStationTwo());
    }
    public TaskResponse GraspSign() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.GraspSign);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public void ResetInactiveConnection() throws IOException {
        if (!isAlive) {
            if (lbr_server != null)
                lbr_server.Destruct();
            lbr_server = new LBRTCPServer();
        } else {
            throw new NotImplementedException();
        }
    }

    public LBRRobotStatus AllowChargingOfRobotAndWait() throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(AllowChargingOfRobot());
    }

    public TaskResponse AllowChargingOfRobot() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.AllowCharge);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }
    public LBRRobotStatus DisallowChargingOfRobotAndWait() throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(DisallowChargingOfRobot());
    }

    public TaskResponse DisallowChargingOfRobot() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.DisallowCharge);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }



    public TaskResponse PlaceSampleInPhotolysisStation(int i) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRPlaceSampleInPhotolysisStation(i);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }
    public TaskResponse GraspSampleFromPhotolysisStation(int i) throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRGraspSampleFromPhotolysisStation(i);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public TaskResponse MoveToDryPosition() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.MoveToDryPosition);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }
    public TaskResponse DryProcedure() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.DryProcedure);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }
    public TaskResponse MoveToNeutralAfterDry() throws IOException, InterruptedException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.MoveToNeutralAfterDry);
        TaskResponse response = lbr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }


    public LBRRobotStatus PlaceSampleInPhotolysisStationAndWait(int i) throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(PlaceSampleInPhotolysisStation(i));
    }
    public LBRRobotStatus GraspSampleFromPhotolysisStationAndWait(int i) throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(GraspSampleFromPhotolysisStation(i));
    }

    public LBRRobotStatus MoveToDryPositionAndWait() throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(MoveToDryPosition());
    }
    public LBRRobotStatus DryProcedureAndWait() throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(DryProcedure());
    }
    public LBRRobotStatus MoveToNeutralAfterDryAndWait() throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(MoveToNeutralAfterDry());
    }


    public LBRRobotStatus GetStatus() {
        return lbr_server.getStatus();
    }
}
