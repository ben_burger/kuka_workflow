package Workflow.Com.TCP;

import Workflow.Configuration.Location;
import Workflow.Exception.*;
import Workflow.KMR.KMRRobotStatus;
import Workflow.KMR.KMRTCPServer;
import Workflow.KMR.Messages.KMRTask;
import Workflow.KMR.Messages.KMRTaskTypeEnum;
import Workflow.LBR.LBRRobotStatus;
import Workflow.LBR.Messages.LBRTask;
import Workflow.LBR.Messages.LBRTaskTypeEnum;
import Workflow.TCP.Messages.TaskResponse;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TCPRobotServerSuper;
import Workflow.Com.ComSuper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Benjamin on 3/11/2018.
 */
public class KMRCom extends ComSuper {
    KMRTCPServer kmr_server;

    public KMRCom() throws IOException {
        kmr_server = new KMRTCPServer();

    }

    public KMRRobotStatus WaitForTaskToStart(TaskResponse response) throws RobotErrorStateException, CloneNotSupportedException, InterruptedException {
        //Still sleep, but now only to make sure there is time to allow a new heartbeat to be sent,
        // otherwise this function will always instantly finish before a new heartbeat!!

        KMRRobotStatus staat = kmr_server.GetStatus();
        KMRTask task = (KMRTask) response.getTask();
        boolean robot_started_task = false;
        KMRTask active_task = ((KMRTask) staat.getTask());
        robot_started_task = active_task != null && active_task.getType().equals(task.getType()) && active_task.equals(active_task);
        while (!robot_started_task) {
            Thread.sleep(50);
            staat = kmr_server.GetStatus();
            active_task = ((KMRTask) staat.getTask());
            robot_started_task = active_task != null && active_task.getType().equals(task.getType());
        }

        if (staat.getStatus().equals( RobotStatusEnum.Error)) {
            throw new RobotErrorStateException("Abort all, robot is in collision or other severe error");
        }

        return staat;
    }


    public KMRRobotStatus WaitForTaskCompletion(TaskResponse response) throws RobotErrorStateException, CloneNotSupportedException, InterruptedException, RobotNoHeartBeatException, DeviceOfflineException {
        WaitForTaskToStart(response);
        return WaitForCommandable();
    }

    public KMRRobotStatus WaitForCommandable() throws CloneNotSupportedException, InterruptedException, DeviceOfflineException, RobotErrorStateException, RobotNoHeartBeatException {
        //Still sleep, but now only to make sure there is time to allow a new heartbeat to be sent,
        // otherwise this function will always instantly finish before a new heartbeat!!

        //Wait for robot to get online for maximum of 10 seconds
        Date before_loop = new Date();
        Date now = new Date();

        KMRRobotStatus staat = kmr_server.GetStatus();
        boolean robot_disconnected = staat.getStatus().equals(RobotStatusEnum.Disconnected);
        if(robot_disconnected) {
            while (robot_disconnected && (now.getSeconds() - before_loop.getSeconds() < 180)) //This scenario typically
            // happens when the arm is started, in such case, the server may be waiting since the KMR was instructed to
            // MOVE and simultainously start the arm, seeing a move can take up to 90 seconds, we give it some extra tolerance
            {
                Thread.sleep(50);
                staat = kmr_server.GetStatus();
                robot_disconnected = staat.getStatus().equals(RobotStatusEnum.Disconnected);
                now = new Date();
            }

            if(robot_disconnected)
                throw new DeviceOfflineException("KMR");
        }


        before_loop = new Date();
        now = new Date();
        boolean robot_not_alive_heartbeat = this.isAlive();
        if(!robot_not_alive_heartbeat) {
            while (!robot_not_alive_heartbeat && (now.getSeconds() - before_loop.getSeconds() < 10)) {
                Thread.sleep(50);
                robot_not_alive_heartbeat = this.isAlive();
                now = new Date();
            }

            if(!robot_not_alive_heartbeat)
                throw new RobotNoHeartBeatException("KMR");
        }

        staat = kmr_server.GetStatus();
        boolean task_running = staat.getStatus().equals( RobotStatusEnum.TaskRunning)&& isAlive();
        if(task_running) {
            while (task_running) {
                Thread.sleep(250);
                staat = kmr_server.GetStatus();
                task_running = staat.getStatus().equals(RobotStatusEnum.TaskRunning) && isAlive();
            }
        }


        if(!isAlive())
            throw new DeviceOfflineException("KMR");

        if (staat.getStatus().equals( RobotStatusEnum.Error) || staat.getStatus().equals(RobotStatusEnum.Disconnected)) {
            throw new RobotErrorStateException("Abort all, robot is in collision or other severe error: " + staat.getStatus().toString());
        } else if (staat.getStatus().equals( RobotStatusEnum.Commandable))
            return staat;

        throw new NotImplementedException();
    }




    @Override
    public boolean isAlive() {
        if (kmr_server == null)
            return false;
        return kmr_server.getClientConnected();
    }

    public void Destruct() throws IOException {
        if (kmr_server != null)
            kmr_server.Destruct();
    }

    public KMRRobotStatus StartMoveToLocationAndWait(Location location) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(StartMoveToLocation(location));
    }


    private void ProcessResponse(TaskResponse taskResponse) throws RobotBusyException, NotAllowedException, RobotErrorStateException {

        switch (taskResponse.getResponse())
        {

            case Accepted:
                break;
            case Busy:
                throw new RobotBusyException();
            case Error:
                throw new RobotErrorStateException();
            case NotAllowed:
                throw new NotAllowedException();
        }
    }
    public TaskResponse StartMoveToLocation(Location location) throws IOException, InterruptedException, RobotBusyException, RobotErrorStateException, NotAllowedException {
        KMRTask task = new KMRTask(location, KMRTaskTypeEnum.TransportKMR);
        TaskResponse response = kmr_server.SentTask(task);
        ProcessResponse(response);
        return response;

    }

    public TaskResponse StartLBR(LBRCom lbr, Location location) throws IOException, InterruptedException, RobotBusyException, RobotErrorStateException, NotAllowedException {
        lbr.ResetInactiveConnection();

        KMRTask task = new KMRTask(location, KMRTaskTypeEnum.StartLBR);
        TaskResponse response = kmr_server.SentTask(task);
        ProcessResponse(response);
        return response;


    }

    public void TransPortKMRAndStartLBRAndWait(LBRCom lbr, Location location) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        lbr.ResetInactiveConnection();
        KMRTask task = new KMRTask(location, KMRTaskTypeEnum.TransportKMRAndStartLBR);
        TaskResponse response = kmr_server.SentTask(task);
        ProcessResponse(response);
        WaitForTaskToStart(response);
        lbr.WaitForCommandable();
    }

    public void StartLBRAndWait(LBRCom lbr, Location location) throws IOException, InterruptedException, CloneNotSupportedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        WaitForTaskToStart(StartLBR(lbr, location));

        lbr.WaitForCommandable();
    }

    public TCPRobotServerSuper GetServer() {
        return kmr_server;
    }

    public KMRRobotStatus ArmToDrivePosAndWait() throws CloneNotSupportedException, IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {
         return WaitForTaskCompletion(ArmToDrivePos());
    }

    public TaskResponse ArmToDrivePos() throws CloneNotSupportedException, IOException, InterruptedException, RobotBusyException, RobotErrorStateException, NotAllowedException {
        Location loc = ((KMRRobotStatus) kmr_server.GetStatus()).getLastCommandedLocation();
        KMRTask task = new KMRTask(loc, KMRTaskTypeEnum.SetToDrivePos);
        TaskResponse response = kmr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public KMRRobotStatus TestProgramAndWait() throws CloneNotSupportedException, IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, RobotBusyException, NotAllowedException {

        return WaitForTaskCompletion(TestProgram());
    }

    public TaskResponse TestProgram() throws CloneNotSupportedException, IOException, InterruptedException, RobotBusyException, RobotErrorStateException, NotAllowedException {
        Location loc = ((KMRRobotStatus) kmr_server.GetStatus()).getLastCommandedLocation();

        KMRTask task = new KMRTask(loc, KMRTaskTypeEnum.TestProgram);
        TaskResponse response = kmr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public KMRRobotStatus AllowChargingOfRobotAndWait() throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(AllowChargingOfRobot());
    }

    public TaskResponse AllowChargingOfRobot() throws IOException, InterruptedException, CloneNotSupportedException, RobotBusyException, RobotErrorStateException, NotAllowedException {
        Location loc = ((KMRRobotStatus) kmr_server.GetStatus()).getLastCommandedLocation();
        KMRTask task = new KMRTask(loc,KMRTaskTypeEnum.AllowCharge);
        TaskResponse response = kmr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }
    public KMRRobotStatus DisallowChargingOfRobotAndWait() throws IOException, InterruptedException, RobotErrorStateException, RobotNoHeartBeatException, DeviceOfflineException, CloneNotSupportedException, RobotBusyException, NotAllowedException {
        return WaitForTaskCompletion(DisallowChargingOfRobot());
    }

    public TaskResponse DisallowChargingOfRobot() throws IOException, InterruptedException, CloneNotSupportedException, RobotBusyException, RobotErrorStateException, NotAllowedException {
        Location loc = ((KMRRobotStatus) kmr_server.GetStatus()).getLastCommandedLocation();
        KMRTask task = new KMRTask(loc,KMRTaskTypeEnum.DisallowCharge);
        TaskResponse response = kmr_server.SentTask(task);
        ProcessResponse(response);
        return response;
    }

    public KMRRobotStatus GetStatus() throws CloneNotSupportedException {
        return kmr_server.GetStatus();
    }
}
