package Workflow.Com.TCP;

import Workflow.Batch.Batch;
import Workflow.Batch.Sample;
import Workflow.Com.ComSuper;
import Workflow.Exception.NotAllowedException;
import Workflow.Exception.RobotBusyException;
import Workflow.Exception.RobotErrorStateException;
import Workflow.TCP.Messages.TaskResponse;
import Workflow.TCP.RobotStatusEnum;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import workflow.GC.GCStationStatus;
import workflow.GC.GCTCPClient;
import workflow.GC.Messages.GCTask;
import workflow.GC.Messages.GCTaskTypeEnum;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

/**
 * Created by Benjamin on 12/11/2018.
 */
public class GCCom extends ComSuper {
    GCTCPClient gc_client;
    private Object client;

    public GCCom() throws IOException {
        // Date timestamp, RobotStatusEnum status, TaskSuper task, String error,
        // Date taskStarted, boolean station_running
        GCStationStatus Status =
                new GCStationStatus(new Date(), RobotStatusEnum.Disconnected, null, null, null, false);
        gc_client = new GCTCPClient(Status);
    }

    public GCStationStatus WaitForTaskToStart() throws RobotErrorStateException, CloneNotSupportedException, InterruptedException {
        //Still sleep, but now only to make sure there is time to allow a new heartbeat to be sent,
        // otherwise this function will always instantly finish before a new heartbeat!!

        GCStationStatus staat = gc_client.GetStatus();
        boolean robot_commandable = staat.getStatus().equals(RobotStatusEnum.Commandable);
        while (robot_commandable) {
            Thread.sleep(50);
            staat = gc_client.GetStatus();
            robot_commandable = staat.getStatus().equals(RobotStatusEnum.Commandable);
        }

        if (staat.getStatus().equals(RobotStatusEnum.Error)) {
            throw new RobotErrorStateException("Abort all, robot is in collision or other severe error");
        }

        return staat;
    }

    public GCStationStatus WaitForTaskCompletion() throws RobotErrorStateException, CloneNotSupportedException, InterruptedException {
        WaitForTaskToStart();
        return WaitForCommandable();
    }

    public GCStationStatus WaitForCommandable() throws RobotErrorStateException, CloneNotSupportedException, InterruptedException {
        //Still sleep, but now only to make sure there is time to allow a new heartbeat to be sent,
        // otherwise this function will always instantly finish before a new heartbeat!!

        //Wait for robot to get online for maximum of 10 seconds
        Date before_loop = new Date();
        Date now = new Date();

        GCStationStatus staat = gc_client.GetStatus();
        boolean robot_disconnected = staat.getStatus().equals(RobotStatusEnum.Disconnected);
        if (robot_disconnected) {
            while (robot_disconnected && (now.getSeconds() - before_loop.getSeconds() < 30)) {
                Thread.sleep(50);
                staat = gc_client.GetStatus();
                robot_disconnected = staat.getStatus().equals(RobotStatusEnum.Disconnected);
                now = new Date();
            }

            if (robot_disconnected)
                throw new NotImplementedException();

            Thread.sleep(1000);
        }


        staat = gc_client.GetStatus();
        boolean task_running = staat.getStatus().equals(RobotStatusEnum.TaskRunning);
        while (task_running) {
            Thread.sleep(50);
            staat = gc_client.GetStatus();
            task_running = staat.getStatus().equals(RobotStatusEnum.TaskRunning);
        }

        if (staat.getStatus().equals(RobotStatusEnum.Error) || staat.getStatus().equals(RobotStatusEnum.Disconnected)) {
            throw new RobotErrorStateException("Abort all, robot is in collision or other severe error: " + staat.getStatus().toString());
        } else if (staat.getStatus().equals(RobotStatusEnum.Commandable))
            return staat;

        throw new NotImplementedException();
    }

    public boolean GCRunning() throws CloneNotSupportedException {
        GCStationStatus status = gc_client.GetStatus();
        return status.getStation_running();
    }

    //bathces parameter can not be larger than 3 and all batches have to be in the GC, thus including location
    public void StartBatch(Batch[] batches) throws IOException, InterruptedException, RobotBusyException, RobotErrorStateException, NotAllowedException {
        GCTask task = new GCTask(GCTaskTypeEnum.RunBatch, batches);
        TaskResponse response = gc_client.SentTask(task);
        ProcessResponse(response);
    }
    private void ProcessResponse(TaskResponse taskResponse) throws RobotBusyException, NotAllowedException, RobotErrorStateException {

        switch (taskResponse.getResponse())
        {

            case Accepted:
                break;
            case Busy:
                throw new RobotBusyException();
            case Error:
                throw new RobotErrorStateException();
            case NotAllowed:
                throw new NotAllowedException();
        }
    }
    public boolean GetResults(Batch[] batches) throws IOException, InterruptedException, RobotBusyException, RobotErrorStateException, NotAllowedException {
        boolean result = true;
        gc_client.RemoveUnprocessedResults();
        GCTask task = new GCTask(GCTaskTypeEnum.GetResults, batches);
        TaskResponse response = gc_client.SentTask(task);
        ProcessResponse(response);
        //Write values to local objects

        Batch[] results =  gc_client.GetResultsFromUnprocessedMessages();
        if(results != null && results.length > 0 && Arrays.stream(results) != null)
        {
            for ( Batch original_batch_instance : batches) {
                Optional<Batch> gc_results_batch_instance = Arrays.stream(results)
                        .filter(x -> x.getBatch_name().equals(original_batch_instance.getBatch_name()))
                        .findAny();
                if(gc_results_batch_instance.isPresent())
                {
                    for (Sample sample : gc_results_batch_instance.get().getSamples())
                    {
                        original_batch_instance.setGCResults(sample, sample.getHydrogen_evolution(), sample.getOxygen_evolution());
                    }
                } else {
                    result =false;
                    //TODO LOG THIS!
                    System.out.println("Results requested not present for hbtach " + original_batch_instance.getBatch_name());
                }

            }
        } else {
            result =false;
            //TODO LOG THIS!
            System.out.println("no results");
        }
        return result;
    }

    @Override
    public boolean isAlive() {
        if (gc_client == null)
            return false;

        int tries = 0;
        while(!gc_client.isConnected() && tries<3)
        {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e)
            {}
            tries++;
        }
        return gc_client.isConnected();
    }


    public void Destruct() throws IOException {
        if (gc_client != null)
            gc_client.Destruct();
    }

    public GCTCPClient getClient() {
        return gc_client;
    }
}
