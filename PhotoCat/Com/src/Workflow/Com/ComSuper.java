package Workflow.Com;

/**
 * Created by Benjamin on 3/8/2018.
 */
public abstract class ComSuper {
    protected Boolean isAlive = false;

    public boolean isAlive()
    {
        synchronized (isAlive) {
            return isAlive;
        }
    }
}
