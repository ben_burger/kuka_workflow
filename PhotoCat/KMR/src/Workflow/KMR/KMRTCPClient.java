package Workflow.KMR;

import Workflow.Configuration.Location;
import Workflow.TCP.TCPRobotClientSuper;
import com.kuka.nav.Pose;
import com.kuka.nav.robot.SafetyState;
import com.kuka.roboticsAPI.controllerModel.sunrise.SunriseSafetyState;
import com.kuka.roboticsAPI.controllerModel.sunrise.state.kmp.IMobilePlatformSafetyState;
import com.kuka.task.ITaskLogger;

public class KMRTCPClient extends TCPRobotClientSuper<KMRRobotStatus> {
    public KMRTCPClient(KMRRobotStatus status, String IP, int Port,ITaskLogger logger) {
        super(status, IP, Port, logger);
    }

    public void UpdateLastCommandedLocation(Location loc)
    {
        ((KMRRobotStatus)Status).setLastCommandedLocation(loc);
    }
    public Location getLastCommandedLocation() {
        synchronized(Status) {
            return ((KMRRobotStatus)Status).getLastCommandedLocation();
        }
    }

    public void setPose(Pose pose)
    {
        synchronized (Status)
        {
            Status.setKMR_Position(pose);
        }
    }

    public void SetBatteryState(double battery)
    {
        synchronized (Status)
        {
            Status.setBatteryState(battery);
        }
    }

    public void setCharging(boolean charging)
    {
        synchronized (Status)
        {
            Status.setCharging(charging);
        }
    }

    public void setSafetyState(SafetyState safetyState)
    {
        synchronized (Status)
        {
            Status.setSafetyState(safetyState);
        }
    }
}
