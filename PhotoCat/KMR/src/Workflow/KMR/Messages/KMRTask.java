package Workflow.KMR.Messages; /**
 * Created by Benjamin on 3/10/2018.
 */

import Workflow.TCP.TaskSuper;

import java.io.Serializable;

public class KMRTask extends TaskSuper implements Serializable {
    public KMRTaskTypeEnum getType() {
        return Type;
    }
    private Workflow.Configuration.Location Location;
    private KMRTaskTypeEnum Type;
    public Workflow.Configuration.Location getLocation() {
        return Location;
    }

    public KMRTask(Workflow.Configuration.Location location, KMRTaskTypeEnum type) {
        super();
        Type = type;
        Location = location;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("KMRTask\r\n");
        sb.append("Location: " + Location.toString());
        sb.append("\r\n");
        sb.append("Type: " + Type.toString());
        sb.append("\r\n");

        return sb.toString();
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof KMRTask)) {
            return false;
        }

        KMRTask cc = (KMRTask)o;
        if(cc.getType().equals( this.getType()) && cc.getLocation().equals(this.getLocation()))
            return true;
        return false;
    }
}
