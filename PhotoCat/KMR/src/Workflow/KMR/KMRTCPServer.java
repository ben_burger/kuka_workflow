package Workflow.KMR;

import Workflow.Configuration.Configuration;
import Workflow.KMR.Messages.KMRTask;

import Workflow.TCP.Messages.TCPMessageSuper;
import Workflow.TCP.Messages.TaskRequest;
import Workflow.TCP.Messages.TaskResponse;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TCPRobotServerSuper;
import com.kuka.nav.robot.SafetyState;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.util.*;

public class KMRTCPServer extends TCPRobotServerSuper<KMRRobotStatus> {

    public KMRTCPServer() throws IOException {
        super(new KMRRobotStatus(null, RobotStatusEnum.Disconnected, null, null, null,0,null, false, SafetyState.SAFE), Configuration.getSideKMRPort(),"KMR-TCP");
    }

    public TaskResponse SentTask(KMRTask task) throws IOException, InterruptedException {
        TaskResponse result = null;
        TaskRequest request = new TaskRequest(new Date(), task);
        logger.LogMessage(true,request.toString());

        synchronized (SendingMessageLock) {
            outToClient.writeObject(request);
            outToClient.flush();
        }

        TCPMessageSuper message = null;
        while (message == null) {

            synchronized (UnprocessedMessages) {
                if (UnprocessedMessages.size() > 0)
                    message = UnprocessedMessages.remove();
                else {
                    message = null;
                }
            }

            if (message == null)
                Thread.sleep(100);

        }
        switch (message.getType()) {
            case Response:
                result = (TaskResponse) message;
                break;
            default:
                throw new NotImplementedException();
        }


        return result;
    }
}
