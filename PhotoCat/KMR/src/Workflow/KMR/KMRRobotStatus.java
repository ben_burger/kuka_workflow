package Workflow.KMR;

import Workflow.Configuration.Location;
import Workflow.TCP.RobotStatus;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TaskSuper;
import com.kuka.nav.Pose;
import com.kuka.nav.robot.SafetyState;

import java.util.Date;

public class KMRRobotStatus extends RobotStatus implements Cloneable{
    private Location LastCommandedLocation;

    private double BatteryState;
    private Pose KMR_Position;
    private boolean Charging;

    private SafetyState SafetyState;

    public Location getLastCommandedLocation() {
        return LastCommandedLocation;
    }



    public void setLastCommandedLocation(Location lastCommandedLocatione) {
        LastCommandedLocation = lastCommandedLocatione;
    }

    public double getBatteryState() {
        return BatteryState;
    }

    public void setBatteryState(double batteryState) {
        BatteryState = batteryState;
    }

    public Pose getKMR_Position() {
        return KMR_Position;
    }

    public void setKMR_Position(Pose KMR_Position) {
        this.KMR_Position = KMR_Position;
    }

    public boolean isCharging() {
        return Charging;
    }

    public void setCharging(boolean charging) {
        Charging = charging;
    }

    public com.kuka.nav.robot.SafetyState getSafetyState() {
        return SafetyState;
    }

    public void setSafetyState(com.kuka.nav.robot.SafetyState safetyState) {
        SafetyState = safetyState;
    }

    public KMRRobotStatus(Date timestamp, RobotStatusEnum status, TaskSuper task, String error, Date taskStarted, double BatteryState, Pose pose, boolean charging, SafetyState safetyState) {
        super(timestamp, status, task, error, taskStarted);
        LastCommandedLocation = Location.Unknown;
        this.BatteryState = BatteryState;
        this.KMR_Position = pose;
        this.Charging = charging;
        this.SafetyState = safetyState;
    }


    public boolean GetCharging() {
        return Charging;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Heartbeat\r\nLast commanded location:"); sb.append(LastCommandedLocation.name());
        sb.append("\r\nBattery charge:"); sb.append(BatteryState);
        sb.append("\r\nPosition:"); sb.append(KMR_Position.toString());
        sb.append("\r\nState of charging:"); sb.append(Charging?"Charging" : "not charging");
        sb.append("\r\nstate of charge:"); sb.append(SafetyState.name());
        return sb.toString();
    }
}
