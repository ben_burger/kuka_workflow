// Sam Carruthers
// February 2020
// Java 1.8

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import workflow.GC.GCTCPServer;
import workflow.GC.Messages.GCTask;
import Workflow.Batch.Batch;
import Workflow.Batch.Sample;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import java.io.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * GC Holding Program
 * TODO Check if every 10 minutes sample is finished after 5 minutes, if NOT REPORT ERROR STATE OF GC WITH:   server.updateStatus(false, "GC CRASHED");
 * TODO FInd a way to save the status, if for example the GC program and WORKFLOW program crash (can be something manual)
 * Communicates with the main workflow program, runs the AutoGC macro, and retrieves GC results
 */
public class GCHoldingProgram {

    private static GCGui gui = new GCGui();

    public static void main(String[] args) {

        gui.startGui();
        gui.printToTextArea("Starting GC Program");

        try {

            // Initialise the server
            GCTCPServer server = new GCTCPServer();
            gui.printToTextArea("Established server");
//
//            // Lists for processing batches
//            LinkedList<Batch> submitted = new LinkedList<>();
//            LinkedList<Batch> running = new LinkedList<>();
//            LinkedList<Batch> completed = new LinkedList<>();



            ScheduledExecutorService reconnect_Executer_service = Executors.newSingleThreadScheduledExecutor();
            reconnect_Executer_service.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    try {

                        // Batches are waiting in the submitted queue
                        if (//server.getBatchesCompleted().size() == 0 &&
                                server.getBatchesRunning().size() == 0 && server.getBatchesSubmitted().size() != 0) {
                            Batch batch = server.getBatchesSubmitted().getFirst();
                            // A batch is currently running
                            server.StartRunningBatch(batch);
                            server.SetMeasurementInProgress(true);

                            // Run the macro and move the batch to the running list
                            // todo Surround with try catch, and deal with exceptions accordingly; e.g. retry, or report ERROR to status
                            runGCMacro(batch);
                            gui.printToTextArea("Macro completed");
                        } else if (server.getBatchesRunning().size() == 1 && checkRunCompleted( server.getBatchesRunning().getFirst())) {
                            Batch batch = server.getBatchesRunning().getFirst();
                            getResults(batch);

                            server.SetBatchCompleted(batch);
                            server.SetMeasurementInProgress(false);

                            // A batch has finished running
    //                    } else if (completed.size() != 0) {
    //
    //                        // Create list of batches to be sent to client
    //                        // Batch objects must be cloned so that they do not have the same position in memory
    //                        Batch[] batches = new Batch[1];
    //                        batches[0] = getResults(completed.pollFirst());
    //
    //                        server.SentResults(batches);
    //                        server.SetBatchCompleted(batches[0]);
    //
    //                        // Nothing is happening, wait
                        }
                        else {
                            Thread.sleep(1000);
                        }
                    } catch(Exception e)
                    {
                        //TODO probably not a nicely formatted text now, but a incomplete text ......
                        gui.printToTextArea(e.getMessage());
                        e.printStackTrace();
                    }
                }
            }, 2000, 5000, MILLISECONDS);

            while (true) {

                try {

                    // TODO (Sam) potentially put the server communication in a separate thread
                    // Receive a task from the server
                    GCTask task;
                    task = (GCTask) server.getNewTask();

                    if (task != null) {

                        switch (task.getType()) {
                            case RunBatch: {

                                Batch[] batches = task.getBatches();

                                for (int i = 0; i < task.getBatches().length; i++) {
                                    batches[i].StopSavingToDisk();
                                }

                                // Add batches received to submitted list
                                server.AddBatchesToSubmittedQueue(task.getBatches());
                                gui.printToTextArea("Batches Received");


                                server.updateStatus(true, null);
                                break;
                            }
                            case Reset: {
                                server.ResetStatus();
                                server.updateStatus(true, null);
                                break;
                            }
                            case GetResults: {

                                // Create list of batches to be sent to client
                                // Batch objects must be cloned so that they do not have the same position in memory
//                            Batch[] batchesToSend = new Batch[task.getBatches().length];
//                            for (int i = 0; i < task.getBatches().length; i ++) {
//                                batchesToSend[i] = getResults(task.getBatches()[i]);
//                            }
                                Batch[] task_batches = task.getBatches();
                                Batch[] cloned_batches = new Batch[task_batches.length];

                                for (int i = 0; i < task_batches.length; i++) {
                                    cloned_batches[i] = cloneBatch(task_batches[i]);
                                    if (checkRunCompleted(cloned_batches[i])) {

                                        //@BB: Can we do this like, or should they come from memory ?! Like does the funciton work for historic batches ?
                                        getResults(cloned_batches[i]);
                                    }
                                    else {
                                        server.updateStatus(false, "No results present for : " + cloned_batches[i].getBatch_name());
                                    }
                                }
                                gui.printToTextArea("Sending Results");
                                server.SentResults(cloned_batches);
                                server.updateStatus(true, null);
                                break;
                            }
                            default:
                                throw new NotImplementedException();
                        }


                    }

                   Thread.sleep(100);

                }
                catch (Exception e) {
                    gui.printToTextArea(e.getMessage());
                    server.updateStatus(false, e.getMessage());// Error has occurred at some point
                }
            }

        } catch (Exception e) {
            gui.printToTextArea(e.getMessage()); // Problem with initialising server
        }
    }

    /**
     * Function to run the AutoGC macro, supplying the batch details as arguments
     * @param batch The batch to be run
     * @throws IOException  If fails to run AutoGC.exe
     */
    private static void runGCMacro(Batch batch) throws IOException {
        String macroExePath = "C:\\Users\\Agilent GC\\Documents\\AutoGC\\bin\\Debug\\net461\\AutoGC.exe";

        // Obtain the details of the batch
        String gc_method = batch.getGc_method();
        Sample[] samples = batch.getSamples();

        String argsList = "";
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < samples.length; i ++) {

            // "well1,name1"
            sb.append(samples[i].getGC_well_number());
            sb.append(","); // Well/Name delimiter
            sb.append(samples[i].getName());

            // "well1,name1.well2,name2..." etc
            if (i != samples.length - 1) {
                sb.append("."); // Sample delimiter - don't put on end of list
            }

            argsList = sb.toString();
        }

        gui.printToTextArea("\r\nStarting GC Macro run:");
        gui.printToTextArea("Method: " + gc_method);
        gui.printToTextArea("Args: " + argsList);
        gui.printToTextArea("");

        // Start the AutoGC program with supplied arguments
        Process process = new ProcessBuilder(macroExePath, gc_method, argsList).start();

        // Capture the output of the program
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        String line;

        // Print program output in main window
        while ((line = bufferedReader.readLine()) != null) {
            //TODO Get errors from this, and generate exceptions, so that they will be caught in existing try catch
            gui.printToTextArea(line);
        }

    }

    /**
     * Function to search for and retrieve the sample folders of a given batch
     * Returns null if the batch is not found
     * TODO remove assumption sample names unique, instead, check if batch is currently running. If it is running for less than 15 minutes, folder does not exist. In all other cases, return most recent folder with sample name
     * @param batch The batch to be searched for
     * @return A list of the batch folders
     */
    private static File[] getSampleFolders(Batch batch) {

        // Get a list of directories inside the data folder
        File outputDirectory = new File("C:\\Chem32\\1\\Data");
        File[] outputFolders = outputDirectory.listFiles(File::isDirectory);

        if (outputFolders != null) {

            for (File outputFolder: outputFolders) {

                // Gets a list of the folders inside each completed run folder
                File[] sampleAndMethodFolders = outputFolder.listFiles(File::isDirectory);

                if (sampleAndMethodFolders != null && sampleAndMethodFolders.length > 1) {

                    File[] sampleFolders = new File[sampleAndMethodFolders.length -1];
                    int folderCounter = 0;

                    // Removes the method folder, leaving only the sample results folders
                    // Method folder ends with ".M", sample folders end with ".D"
                    for (File sampleOrMethodFolder : sampleAndMethodFolders) {
                        if (sampleOrMethodFolder.getName().endsWith("D")) {

                            sampleFolders[folderCounter] = sampleOrMethodFolder;
                            folderCounter++;
                        }
                    }

                    // Checks if the sample folders are for the right batch by comparing the sample folder names
                    // Batch names should be unique
                    for (File sampleFolder : sampleFolders) {
                        if (sampleFolder.getName().contains(batch.getBatch_name())) {

                            return sampleFolders;
                        }
                    }
                }
            }
        }
        return null;    // No results found
    }

    /**
     * Function to search through the contents of a run folder and check that all the results are present
     * @param batch The batch to be checked
     * @return If all files are present
     */
    private static boolean checkRunCompleted(Batch batch) {
        // get the folders of the most recent gc run
        File[] sampleFolders = getSampleFolders(batch);

        if (sampleFolders != null) {
            // check that the right amount of batches have been run
            Sample[] samples = batch.getSamples();

            if (samples.length == sampleFolders.length) {

                // try to find "H O rc.xls" for each sample
                for (File sampleFolder : sampleFolders) {

                    String path = sampleFolder.getAbsolutePath();
                    File excelFile = new File(path + "\\H O rc.xls");

                    if (!excelFile.exists()) {
                        return false;   // at least one file is missing
                    }
                }
                return true;    // all files are present
            }
        }
        return false;   // at least one file is missing
    }

    /**
     * Function to read the results of a batch run from each of its sample's excel files
     * @param batch The batch for which the results are to be retrieved
     * @return The same batch, filled out with GC results
     * @throws IOException If there is a problem with file handling
     */
    private static Batch getResults(Batch batch) throws IOException {

        // Get the sample folders of the correct batch
        File[] sampleFolders = getSampleFolders(batch);

        if (sampleFolders != null) {

            for (File sampleFolder : sampleFolders) {

                // The path of the results excel file
                String path = sampleFolder.getAbsolutePath() + "\\H O rc.xls";

                FileInputStream inputStream = new FileInputStream(path);

                // Open excel file
                HSSFWorkbook excelWorkbook = new HSSFWorkbook(inputStream);
                HSSFSheet excelSheet = excelWorkbook.getSheetAt(0);
                HSSFRow row;

                int hydrogenArea = 0;
                int oxygenArea = 0;

                // Get the name of the sample
                String sampleName = excelSheet.getRow(10).getCell(2).getStringCellValue();
                Cell retentionTimeCell;
                Cell areaCell;

                // Table starts at row 11 (index 10)
                for (int i = 10; i <= excelSheet.getLastRowNum(); i++) {

                    row = excelSheet.getRow(i);

                    if (row != null) {

                        retentionTimeCell = row.getCell(5);
                        areaCell = row.getCell(8);

                        // Read retention times from the file
                        // [0]: min hydrogen
                        // [1]: max hydrogen
                        // [2]: min oxygen
                        // [3]: max oxygen
                        double[] retentionTimes = getRetentionTimes();

                        double retentionTimeOfPeak = 0.0;

                        // Read the retention time
                        if (!retentionTimeCell.getStringCellValue().equals("")) {
                            retentionTimeOfPeak = Double.parseDouble(retentionTimeCell.getStringCellValue());
                        }

                        // When retention time is in range, add to hydrogen or oxygen total area
                        if (retentionTimeOfPeak >= retentionTimes[0] && retentionTimeOfPeak <= retentionTimes[1]) {
                            hydrogenArea += Integer.parseInt(areaCell.getStringCellValue());
                        } else if (retentionTimeOfPeak >= retentionTimes[2] && retentionTimeOfPeak <= retentionTimes[3]) {
                            oxygenArea += Integer.parseInt(areaCell.getStringCellValue());
                        }
                    }
                }
                // Set the hydrogen and oxygen evolution values of the sample
                Sample[] samples = batch.getSamples();
                for (Sample sample : samples) {
                    if (sample.getName().equals(sampleName)) {
                        sample.setHydrogen_evolution(hydrogenArea);
                        sample.setOxygen_evolution(oxygenArea);
                    }
                }
            }
        }
        return batch;
    }

    /**
     * Clones the batch object, as it does not have its own clone() method
     * @param originalBatch The batch to be cloned
     * @return The cloned batch
     * @throws IOException If couldn't write values to the clone
     */
    private static Batch cloneBatch(Batch originalBatch) throws IOException {

        // Declare new batch of equal sample length
        Batch cloneBatch = new Batch(originalBatch.getSamples().length);

        // Info required to be copied: batch name, sample names, sample indexes, hydrogen and oxygen evolution values
        cloneBatch.setName(originalBatch.getBatch_name());
        cloneBatch.setGc_method(originalBatch.getGc_method());

        int i  = 0;
        for(Sample original_sample : originalBatch.getSamples())
        {
            if(original_sample != null) {
                // Create a new sample object and copy the relevant data
                Sample cloneSample = new Sample();
                cloneSample.setGC_well_number(original_sample.getGC_well_number());
                cloneSample.setSampleIndex(original_sample.getSampleIndex());
                cloneSample.setName(original_sample.getName());
                Integer original_hydrogen = original_sample.getHydrogen_evolution();
                if (original_hydrogen != null) cloneSample.setHydrogen_evolution(original_hydrogen);
                Integer original_oxygen = original_sample.getOxygen_evolution();
                if (original_oxygen != null) cloneSample.setOxygen_evolution(original_oxygen);
                cloneBatch.getSamples()[i] = cloneSample;
                i++;
            }
        }
        return cloneBatch;
    }

    /**
     * Function to read the hydrogen and oxygen retention times from a file
     * @return A list of the values
     */
    private static double[] getRetentionTimes() {

        // The path of the file
        File retentionTimesFile = new File("RetentionTimes.txt");

        try {

            FileReader fileReader = new FileReader(retentionTimesFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            bufferedReader.readLine(); // Ignore first line - contains description

            // "2.1,3.0,4.1,5.0"
            String line = bufferedReader.readLine();
            String[] lineSplit = line.split(",");
            // ["2.1", "3.0", "4.1", "5.0"]

            // Make sure there is the right amount of values
            if (lineSplit.length == 4) {

                double[] retentionValues = new double[4];

                for (int i = 0; i < 4; i ++) {
                    retentionValues[i] = Double.parseDouble(lineSplit[i]);
                }
                // [2.1, 3.0, 4.1, 5.0]
                return retentionValues;
            }
        }
        catch (Exception e) {
            // If file not present
            gui.printToTextArea("Retention time file not found.");
            gui.printToTextArea("Using default values.");
        }
        // Default values
        return new double[]{2.0, 3.0, 4.0, 5.0};
    }
}
