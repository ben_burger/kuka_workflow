package Workflow.GUI.Forms;

import Workflow.Com.ComSuper;
import Workflow.Configuration.ComStation;
import Workflow.Exception.DeviceOfflineException;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class StatusBar extends SuperTab {

    private JPanel mainPanel;

    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    private JLabel lblQuantosConnection;
    private JLabel lblPumpConnection;
    private JLabel lblScaleConnection;
    private JLabel lblCapper;
    private JLabel lblSonicatorConnection;
    private JLabel lblPhotocatConnection;
    private JLabel lblKMRConnected;
    private JLabel lblPump2Connection;
    private JLabel lblScale2Connection;
    private JLabel lblGCStatus;
    private JLabel lblVibratoryConnection;
    private JLabel lblWebcamSonicator;
    private JLabel lblWebcamPhotolysis;

    public StatusBar() {
        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    UpdateStatusLEDs();
                } catch (DeviceOfflineException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1, TimeUnit.SECONDS);


    }

    private void UpdateStatusLEDs() throws DeviceOfflineException {
        if (workflowSystem != null) {
            UpdateLEDLabel(ComStation.VibratoryPhotolysis, lblVibratoryConnection);
            UpdateLEDLabel(ComStation.Capper, lblCapper);
            UpdateLEDLabel(ComStation.Quantos, lblQuantosConnection);
            UpdateLEDLabel(ComStation.Scale, lblScaleConnection);
            UpdateLEDLabel(ComStation.Liquid, lblPumpConnection);
            UpdateLEDLabel(ComStation.Photo, lblPhotocatConnection);
            UpdateLEDLabel(ComStation.Sonicator, lblSonicatorConnection);
            UpdateLEDLabel(ComStation.KMR, lblKMRConnected);
            UpdateLEDLabel(ComStation.Scale2, lblScale2Connection);
            UpdateLEDLabel(ComStation.Liquid2, lblPump2Connection);
            UpdateLEDLabel(ComStation.GC, lblGCStatus);
            UpdateLEDLabel(ComStation.WebcamPhotolysis, lblWebcamPhotolysis);
            UpdateLEDLabel(ComStation.WebcamSonicator, lblWebcamSonicator);

        }

    }



    private void UpdateLEDLabel(ComStation station, JLabel label) {
        try {
            if(station.equals(ComStation.WebcamPhotolysis))
            {
                if (workflowSystem.CheckCameraStatus(1)) {
                    label.enableInputMethods(false);
                    label.setForeground(Color.GREEN);
                } else {
                    label.enableInputMethods(true);
                    label.setForeground(Color.RED);
                }
            } else if( station.equals(ComStation.WebcamSonicator))
            {
                if (workflowSystem.CheckCameraStatus(0)) {
                    label.enableInputMethods(false);
                    label.setForeground(Color.GREEN);
                } else {
                    label.enableInputMethods(true);
                    label.setForeground(Color.RED);
                }
            } else {
                ComSuper COM = workflowSystem.getComNoCheck(station);
                if (COM == null) {
                    label.enableInputMethods(true);
                    label.setForeground(Color.YELLOW);
                } else if (COM.isAlive()) {
                    label.enableInputMethods(false);
                    label.setForeground(Color.GREEN);
                } else {
                    label.enableInputMethods(true);
                    label.setForeground(Color.RED);
                }
            }
        } catch (DeviceOfflineException e)
        {
            label.enableInputMethods(true);
            label.setForeground(Color.CYAN);
        }
    }

}
