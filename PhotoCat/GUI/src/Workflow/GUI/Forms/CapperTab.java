package Workflow.GUI.Forms;

import Workflow.Com.Serial.CapperCom;
import Workflow.IMessageHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by Benjamin on 8/5/2018.
 */
public class CapperTab extends SuperTab {
    private JPanel mainPanel;
    private JButton startButton;
    private JButton stopButton;
    private JComboBox cmbParamter;
    private JTextField txtParameter;
    private JButton setParameterButton;
    public CapperTab() throws IOException
    {
        cmbParamter.setModel( new DefaultComboBoxModel(CapperCom.CapperParameter.values()) );

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getCapperCom().Start();
                } catch (Exception e1)
                {
                    messageHandler.HandleException( IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getCapperCom().Stop();
                } catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        setParameterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getCapperCom().SetParameter((CapperCom.CapperParameter) cmbParamter.getSelectedItem(), txtParameter.getText());
                } catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
    }
    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    private void createUIComponents() {
    }
}
