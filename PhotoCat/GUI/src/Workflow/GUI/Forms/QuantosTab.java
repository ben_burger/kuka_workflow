package Workflow.GUI.Forms;

import Workflow.Batch.Solid;
import Workflow.IMessageHandler;
import Workflow.WorkflowSystem;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Benjamin on 3/8/2018.
 */
public class QuantosTab extends SuperTab{
    private JButton openDoorButton;
    private JButton moveSamplerButton;
    private JButton closeDoorButton;
    private JTextField samplerPositionTextField;

    private JPanel mainPanel;
    private JTable tblSolid;
    private JComboBox solidCombo;
    private JTextField addSolidText;
    private JButton btnRemoveSolid;
    private JButton btnSetUnblocked;
    private JButton btnChangeHotelIndex;
    private JButton btnAddSolid;
    private JTextField changeHotelIndexText;
    private JTextField txtMin;
    private JTextField txtMax;
    private JButton lockCartridgeButton;
    private String[] Solids;
    ScheduledExecutorService executer_service_tables ;
    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    public QuantosTab() {


        openDoorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OpenQuantosDoor();
            }
        });
        closeDoorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CloseQuantosDoor();
            }
        });
        moveSamplerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SetSamplerPosition();
            }
        });
        btnChangeHotelIndex.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ChangeHotelIndexForSolid();
            }
        });
        btnAddSolid.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddSolid();
            }
        });

        ScheduledExecutorService executer_service_tables = Executors.newSingleThreadScheduledExecutor();
        executer_service_tables.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    updateSolids();
                } catch (Exception e1) {
                    try {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                    } catch (Exception e )
                    {
                        System.out.println("Something irrelevant is really messed up with our GUI");
                        // but we dont know what, and it only occurs once during the execution without any consequences.
                        // So its irrelevant
                    }
                }
            }
        }, 1000, 1000, TimeUnit.MILLISECONDS);


        btnSetUnblocked.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    SetSolidUnblocked();
            }
        });
        btnRemoveSolid.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    RemoveSolid();
            }
        });
        lockCartridgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {UnlockCartridge();
            }
        });
    }

    private void UnlockCartridge() {
        try {
            workflowSystem.getQuantosCom().LockDosingHeadPin(false);
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    private void RemoveSolid() {
        try {
        String name_thingy = (String) solidCombo.getSelectedItem();
        String[] split = name_thingy.split(",",-1);
        Solid solid = Arrays.stream(workflowSystem.getState().getSolids()).filter(x->x.getName().equals(split[1])&&x.getHotelIndex() == Integer.parseInt(split[0])).findFirst().get();
        workflowSystem.getState().RemoveSolid(solid);
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    private void SetSolidUnblocked() {
        try {


        String name_thingy = (String) solidCombo.getSelectedItem();
        String[] split = name_thingy.split(",",-1);
        Solid solid = Arrays.stream(workflowSystem.getState().getSolids()).filter(x->x.getName().equals(split[1])&&x.getHotelIndex() == Integer.parseInt(split[0])).findFirst().get();
        workflowSystem.getState().SetSolidUnblocked(solid);
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    private void AddSolid() {
        try {
        String name = addSolidText.getText();

        workflowSystem.getState().AddSolid(name, Integer.parseInt(changeHotelIndexText.getText()), Float.parseFloat(txtMin.getText()), Float.parseFloat(txtMax.getText()));
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    private void ChangeHotelIndexForSolid() {
        try {
            String name_thingy = (String) solidCombo.getSelectedItem();

        String[] split = name_thingy.split(",",-1);
        Solid solid = Arrays.stream(workflowSystem.getState().getSolids()).filter(x->x.getName().equals(split[1]) && x.getHotelIndex() == Integer.parseInt(split[0])).findFirst().get();
        workflowSystem.getState().MoveSolid(solid,solid.getHotelIndex(),Integer.parseInt(changeHotelIndexText.getText()));
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    private String[] UpdateSolidsNameCombobox() {
        String[] names = Arrays.stream(workflowSystem.getState().getSolids()).sorted((e1,e2) -> Integer.compare(e1.getHotelIndex(), e2.getHotelIndex())).map(x -> x.getHotelIndex() +","+ x.getName()).toArray(String[]::new);
        if(Solids != null)
        {
            boolean allSame=true;
            if(Solids.length == names.length)
            {

                for(int i=0;i<Solids.length&&allSame;i++)
                {
                    String name_to_Find = Solids[i];
                    allSame = Arrays.stream(names).filter(x -> x.equals(name_to_Find)).findFirst().isPresent();
                }

            } else {
                allSame =false;
            }

            if(!allSame)
                solidCombo.setModel(new DefaultComboBoxModel(names));

        } else {
            solidCombo.setModel(new DefaultComboBoxModel(names));
        }

        Solids = names;
        return names;
    }
    private void updateSolids() {
        String current = (String) solidCombo.getSelectedItem();
        String[] names = UpdateSolidsNameCombobox();
       if(names.length > 0 && !(Arrays.stream(names).filter(x -> x.equals(current)).findFirst().isPresent())){
            solidCombo.setSelectedItem(names[0]);
        }
        SolidItemModel tiModel = (SolidItemModel) tblSolid.getModel();
        tiModel.Refresh(workflowSystem);
    }

    private void SetSamplerPosition() {
        try {
            int sampler_position = Integer.parseInt(samplerPositionTextField.getText());
            workflowSystem.getQuantosCom().MoveAutoSampler(sampler_position);
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    private void OpenQuantosDoor() {
        try
        {
            workflowSystem.getQuantosCom().MoveDoor(true);
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }
    private void CloseQuantosDoor() {
        try
        {
            workflowSystem.getQuantosCom().MoveDoor(false);
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }


    @Override
    public void setWorkflow(WorkflowSystem workflow) {
        super.setWorkflow(workflow);

        SetUpSolidsTable();
    }

    private void SetUpSolidsTable() {
        Solid[] solids = workflowSystem.getState().getSolids();
        SolidItemModel tiModel = new SolidItemModel(solids);
        tblSolid.setModel(tiModel);
    }
}
