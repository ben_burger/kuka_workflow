package Workflow.GUI.Forms;


import Workflow.Batch.*;
import Workflow.Exception.CompoundNotPResentException;
import Workflow.IMessageHandler;
import Workflow.WorkflowSystem;
import javafx.util.Pair;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class BatchTab extends SuperTab {
    private boolean Historicbatches;
    private JPanel mainPanel;
    private JTable runTable;
    private JComboBox runSelectComboBox;
    private JLabel lblbatch_name;
    private JLabel lblpurge_time;
    private JButton stopRunningButton;
    private JButton startRunningButton;
    private JLabel lblRunning;
    private JLabel lblQuantosRunning;
    private JLabel lblStationsRunning;
    private JLabel lblJobExecutor;
    private JLabel lblStatus;
    private JLabel lblJobProgress;
    private JLabel lblJobExecutorActive;
    private JLabel lblStationsActive;
    private JLabel lblQuantosActive;
    private JButton quantosActiveButton;
    private JButton stationThreadButton;
    private JButton robotJobExecutorButton;
    private JTable solidTable;
    private JTable liquidTable;
    private JComboBox comboStatus;
    private JComboBox comboProgress;
    private JScrollPane runTableScrolLPane;
    private JLabel lblMinCapsPresent;
    private JLabel lblBatchLocation;
    private JComboBox comboBatchLocation;
    private JLabel lblrack_identifier;
    private JButton btnSonicationTimea;
    private JButton btnIlluminationtime;
    private JButton btnGCMethod;
    private JButton btnCapVIals;
    private JPanel start_times;
    private JPanel finish_times;
    private JTextPane txtDispense;
    private JTextArea txtComment;
    private JLabel lblTStart;
    private JLabel lblTEnd;
    private JPanel sampleEditing;
    private JTextField txtSampleIndex;
    private JButton btnRemoveSample;
    private JButton btnCapSample;
    private JComboBox comboDispensedList;
    private JTextField txtDispensedAmount;
    private JButton btnEditDispensedAmount;
    private JButton calculateInternalStandardMetricsButton;
    ScheduledExecutorService executer_service;
    private HashMap<Pair<BatchTimestampEnum, BatchTimestampType>, JLabel> startlabels;
    private JButton btnResubmitBatch;
    private JPanel resubmitPanel;

    public BatchTab(boolean historic_batches) {
        Historicbatches = historic_batches;
        if (Historicbatches) {
            resubmitPanel.setVisible(true);
        }

        Vector<BatchStatus> batchStatuses = new Vector<>();
        for (BatchStatus status : BatchStatus.values()) {
            batchStatuses.addElement(status);
        }
        comboStatus.setModel(new DefaultComboBoxModel(batchStatuses));

        Vector<BatchLocation> batchLocations = new Vector<>();
        for (BatchLocation location : BatchLocation.values()) {
            batchLocations.addElement(location);
        }
        comboBatchLocation.setModel(new DefaultComboBoxModel(batchLocations));

        Vector<Progress> batchProgresses = new Vector<>();
        for (Progress progress : Progress.values()) {
            batchProgresses.addElement(progress);
        }
        comboProgress.setModel(new DefaultComboBoxModel(batchProgresses));

        btnResubmitBatch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String batch_name = (String) runSelectComboBox.getSelectedItem();
                    Optional<Batch> batch = getBatches().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst();
                    if (batch.isPresent()) {
                        batch.get().resubmitBatch();
                    }
                }
                catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        btnEditDispensedAmount.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    int sampleIndex = Integer.parseInt(txtSampleIndex.getText());
                    float newDispensedAmount = Float.parseFloat(txtDispensedAmount.getText());
                    String batch_name = (String) runSelectComboBox.getSelectedItem();
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst().get();
                    if (batch != null) {
                        if (batch.getProgress() == Progress.Running) {
                            throw new Exception("Sample is in a batch that is currently running.");
                        }
                        if (batch.getProgress() == Progress.Waiting && sampleIndex < 16 && sampleIndex > -1 ) {
                            int proceed = 2;
                            Object[] options = {"Yes, please", "No way!"};
                            proceed = JOptionPane.showOptionDialog(null, "Are you sure you want to edit this sample?",
                                    "Edit Sample Dispensed Amount",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null, options, options[1]);
                            if ( proceed == JOptionPane.YES_OPTION ) {
                                String editDispensedName = (String) comboDispensedList.getSelectedItem();
                                Optional<Solid> potentialSolid = Arrays.stream(batch.getSolids()).filter(x->x.getName().equals(editDispensedName)).findFirst();
                                Optional<Liquid> potentialLiquid = Arrays.stream(batch.getLiquids()).filter(x->x.getName().equals(editDispensedName)).findFirst();
                                if (potentialSolid.isPresent()) {
                                    batch.editDispensed(sampleIndex, potentialSolid.get(), newDispensedAmount);
                                }
                                else if (potentialLiquid.isPresent()) {
                                    batch.editDispensed(sampleIndex, potentialLiquid.get(), newDispensedAmount);
                                }


                            }
                        }
                        else {
                            throw new Exception("Sample Index Out of Range");
                        }
                    }
                    else {
                        throw new Exception("Batch selected is no longer present.");
                    }
                }
                catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        btnRemoveSample.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    int sampleIndex = Integer.parseInt(txtSampleIndex.getText());
                    String batch_name = (String) runSelectComboBox.getSelectedItem();
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst().get();
                    if (batch != null) {
                        if (batch.getProgress() == Progress.Running) {
                            throw new Exception("Sample is in a batch that is currently running.");
                        }
                        if (batch.getProgress() == Progress.Waiting && sampleIndex < 16 && sampleIndex > -1 ) {
                            int proceed = 2;
                            Object[] options = {"Yes, please", "No way!"};
                            proceed = JOptionPane.showOptionDialog(null, "Are you sure you want to delete this sample?",
                                    "Sample Removal",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null, options, options[1]);
                            if ( proceed == JOptionPane.YES_OPTION ) {
                                batch.removeSample(sampleIndex);

                            }
                        }
                        else {
                            throw new Exception("Sample Index Out of Range");
                        }
                    }
                    else {
                        throw new Exception("Batch selected is no longer present.");
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        btnCapSample.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    int sampleIndex = Integer.parseInt(txtSampleIndex.getText());
                    String batch_name = (String) runSelectComboBox.getSelectedItem();
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst().get();
                    if (batch != null) {
                        if (batch.getProgress() == Progress.Running) {
                            throw new Exception("Sample is in a batch that is currently running.");
                        }
                        if (batch.getProgress() == Progress.Waiting && sampleIndex < 16 && sampleIndex > -1 ) {
                            int proceed = 2;
                            Object[] options = {"Yes, please", "No way!"};
                            proceed = JOptionPane.showOptionDialog(null, "Are you sure you want to cap this sample?",
                                    "Sample Capping",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null, options, options[1]);
                            if ( proceed == JOptionPane.YES_OPTION ) {
                                Sample sample  = Arrays.stream(batch.getSamples()).filter(x -> x.getSampleIndex() == sampleIndex).findFirst().get();
                                batch.setSampleCapped(sample);
                                batch.AppendComment(String.format("Vial manually set to capped by operator ample %s", sample.getName()));
                            }
                        }
                        else {
                            throw new Exception("Sample Index Out of Range");
                        }
                    }
                    else {
                        throw new Exception("Batch selected is no longer present.");
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        comboStatus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String batch_name = (String) runSelectComboBox.getSelectedItem();
                    Optional<Batch> batch = getBatches().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst();
                    if (batch.isPresent()) {
                        batch.get().setStatus((BatchStatus) comboStatus.getSelectedItem());
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        comboBatchLocation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String batch_name = (String) runSelectComboBox.getSelectedItem();
                    Optional<Batch> batch = getBatches().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst();
                    if (batch.isPresent()) {
                        BatchLocation location = (BatchLocation) comboBatchLocation.getSelectedItem();
                        if (workflowSystem.getState().getRackAtLocation(location) != null) {
                            throw new InputMismatchException("Can not place rack, where there is already an rack.");
                        }

                        workflowSystem.getState().MoveRack(batch.get().getBatchLocation(), location, batch.get());
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });

        comboProgress.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String batch_name = (String) runSelectComboBox.getSelectedItem();
                    Optional<Batch> batch = getBatches().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst();
                    if (batch.isPresent()) {
                        batch.get().setProgress((Progress) comboProgress.getSelectedItem());
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });


        btnSonicationTimea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals((String) runSelectComboBox.getSelectedItem())).findFirst().get();

                    if (batch.getSonication_time_min() == 0)
                        batch.setSonication_time_min(10);
                    else
                        batch.setSonication_time_min(0);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        btnIlluminationtime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals((String) runSelectComboBox.getSelectedItem())).findFirst().get();

                    if (batch.getIllumination_time_min() == 0)
                        batch.setIllumination_time_min(60);
                    else
                        batch.setIllumination_time_min(0);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        btnGCMethod.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals((String) runSelectComboBox.getSelectedItem())).findFirst().get();

                    if (batch.getGc_method() == null || batch.getGc_method().equals(""))
                        batch.setGc_method("constant-HS rc");
                    else
                        batch.setGc_method("");
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        btnCapVIals.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals((String) runSelectComboBox.getSelectedItem())).findFirst().get();

                    batch.setCap_vials(!batch.getCap_vials());
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });

        txtComment.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                try {
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals((String) runSelectComboBox.getSelectedItem())).findFirst().get();
                    batch.setComment(txtComment.getText());
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        calculateInternalStandardMetricsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Batch batch = getBatches().stream().filter(x -> x.getBatch_name().equals((String) runSelectComboBox.getSelectedItem())).findFirst().get();
                    batch.CalculateWeightedHydrogen();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
    }


    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    @Override
    public void setWorkflow(WorkflowSystem workflow) {
        runTable.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        super.setWorkflow(workflow);
        {
            String[] names = UpdateBatchNameCombobox(workflowSystem.getState().getRunsInTheSystem());

            for (ActionListener listener : runSelectComboBox.getActionListeners())
                runSelectComboBox.removeActionListener(listener);

            runSelectComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    try {
                        Optional<Batch> batch = workflowSystem.getState().ReadRunsInSystem().stream().filter(x -> x.getBatch_name().equals((String) runSelectComboBox.getSelectedItem())).findFirst();

                        if (batch.isPresent()) {
                            updateRunTable(batch.get());
                            updateBatchLabels(batch.get());
                            updateSamples(batch.get());
                        }
                    } catch (Exception e1) {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                    }
                }
            });

            if (names.length > 0) {
                Batch batch = workflowSystem.getState().getRunsInTheSystem().stream().filter(x -> x.getBatch_name().equals(names[0])).findFirst().get();
                updateBatchLabels(batch);
                RunOverviewModel runModel = new RunOverviewModel(batch);
                runTable.setModel(runModel);
                updateSamples(batch);
            } else {

                updateBatchLabels(null);
                RunOverviewModel runModel = new RunOverviewModel(null);
                runTable.setModel(runModel);
            }
        }
        executer_service = Executors.newSingleThreadScheduledExecutor();
        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {

                    String current = (String) runSelectComboBox.getSelectedItem();
                    List<Batch> runs_in_system = getBatches();


                    String[] names = UpdateBatchNameCombobox(runs_in_system);
                    if (current != null && current != "" && Arrays.stream(names).filter(x -> x.equals(current)).findFirst().isPresent()) {
                        Batch batch = runs_in_system.stream().filter(x -> x.getBatch_name().equals(current)).findFirst().get();
                        updateRunTable(batch);
                        updateBatchLabels(batch);

                    } else if (names.length > 0) {
                        runSelectComboBox.setSelectedItem(names[0]);
                        Batch batch = runs_in_system.stream().filter(x -> x.getBatch_name().equals(names[0])).findFirst().get();
                        updateRunTable(batch);
                        updateBatchLabels(batch);

                    } else {
                        updateRunTable(null);
                        updateBatchLabels(null);
                    }


                } catch (Exception e) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e);
                }
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);

        runTableScrolLPane.setViewportView(runTable);
    }

    private List<Batch> getBatches() throws ParseException, IOException, CompoundNotPResentException {
       List<Batch> result = null;
       if(Historicbatches) {
           result = workflowSystem.getState().ReadRunsInSystem(false, true).stream().filter(x->x.getBatchLocation().equals(BatchLocation.NoRackAssigned)).collect(Collectors.toList());;
        }
        else
           result = workflowSystem.getState().getRunsInTheSystem();
       return result;
    }


    private static String FormatBatch(Batch batch) {
        if (batch == null)
            return "";
        else
            return batch.getBatch_name();
    }

    private void updateBatchLabels(Batch batch) {
        DateFormat df2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        if (batch != null) {
            runSelectComboBox.getModel().setSelectedItem((batch.getBatch_name()));
            lblBatchLocation.setText(batch.getBatchLocation().toString());
            lblStatus.setText(batch.getStatus().getName());
            lblrack_identifier.setText(batch.getRack_identifier());
            lblJobProgress.setText(batch.getProgress().name());
            btnSonicationTimea.setText(batch.getSonication_time_min() + "");
            btnIlluminationtime.setText(batch.getIllumination_time_min() + "");
            btnGCMethod.setText(batch.getGc_method().equals("") ? "DO NOT MEASURE GC" : batch.getGc_method());
            lblpurge_time.setText(batch.getPurge_time_sec() + "");
            btnCapVIals.setText(batch.getCap_vials() ? "CAP" : "DO NOT CAP");
            StringBuilder solid_dispense_sb = new StringBuilder();
            for(int index=0;index<batch.getSolids().length;index++) {
                solid_dispense_sb.append(batch.getSolids()[index].getName());
                        solid_dispense_sb.append( ": ");
                        solid_dispense_sb.append(batch.getSolid_dispensing_finish_date_time(index) == null ? "" : df2.format(batch.getSolid_dispensing_finish_date_time(index)));
                        solid_dispense_sb.append("\r\n");
            }
            txtDispense.setText(solid_dispense_sb.toString());

            if(!txtComment.hasFocus())
            {
                txtComment.setText(batch.getComment());
            }

            lblTStart.setText(batch.getTemperature_Photolysis_start()==null?"":""+batch.getTemperature_Photolysis_start());
            lblTEnd.setText(batch.getTemperature_Photolysis_end()==null?"":""+batch.getTemperature_Photolysis_end());


        } else {
            JLabel[] lbls = new JLabel[]{
                    lblStatus,
                    lblBatchLocation,
                    lblrack_identifier,
                    lblJobProgress,
                    lblpurge_time
            };
            for (JLabel lbl : lbls) {
                lbl.setText("");
            }
            runSelectComboBox.setSelectedItem(null);

            btnSonicationTimea.setText("");
            btnIlluminationtime.setText("");
            btnGCMethod.setText("");
            lblpurge_time.setText("");
            btnCapVIals.setText("");
            txtDispense.setText("");
            txtComment.setText("");
        }
        for (BatchTimestampEnum process_step : BatchTimestampEnum.values()) {
            for (BatchTimestampType time_step : BatchTimestampType.values()) {
                {
                    JLabel label = startlabels.get(new Pair(process_step, time_step));
                    if (batch != null && label != null) {
                        if (batch.getTimestamp(process_step, time_step) != null)
                            label.setText(df2.format(batch.getTimestamp(process_step, time_step)));
                        else
                            label.setText("");
                    }
                }
            }
        }
    }

    private static String FormatDate(Date date) {
        DateFormat df2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

        if (date == null)
            return "";
        return df2.format(date);
    }

    private String[] UpdateBatchNameCombobox(List<Batch> runs_in_system) {


        String[] names = runs_in_system.stream().map(Batch::getBatch_name)
                .filter(x->!x.contains("FreshRack"))
                .sorted().collect(Collectors.toList()).toArray(new String[0]);
        String[] NAMES = new String[runSelectComboBox.getItemCount()];
        for (int i = 0; i < NAMES.length; i++) {
            NAMES[i] = (String) runSelectComboBox.getItemAt(i);
        }

        if (NAMES != null) {
            boolean allSame = true;
            if (NAMES.length == names.length) {

                for (int i = 0; i < NAMES.length && allSame; i++) {
                    final int j = i;
                    allSame = Arrays.stream(names).filter(x -> x.equals(NAMES[j])).findFirst().isPresent();
                }

            } else {
                allSame = false;
            }

            if (!allSame)
                runSelectComboBox.setModel(new DefaultComboBoxModel(names));

        } else {
            runSelectComboBox.setModel(new DefaultComboBoxModel(names));
        }

        return names;
    }


    private void updateRunTable(Batch batch) {
        if(batch != null)
        ((RunOverviewModel) runTable.getModel()).setBatch(batch);
    }

    private void updateSamples(Batch batch) {
        Vector<String> batchDispensed = new Vector<>();
        if  ( batch.getLiquids() != null ) {
            if (batch.getLiquids().length != 0) {
                for (Liquid batchLiquid : batch.getLiquids()) {
                    batchDispensed.addElement(batchLiquid.getName());
                }
            }
        }
        if ( batch.getSolids() != null ) {
            if (batch.getSolids().length != 0) {
                for (Solid batchSolid : batch.getSolids()) {
                    batchDispensed.addElement(batchSolid.getName());
                }
            }
        }
        comboDispensedList.setModel(new DefaultComboBoxModel(batchDispensed));
    }

    private void createUIComponents() {
        sampleEditing = new JPanel();
        sampleEditing.setBorder(new LineBorder(Color.BLACK, 1));
        resubmitPanel = new JPanel();
        resubmitPanel.setBorder(new LineBorder(Color.BLACK, 1));

        start_times = new JPanel();
        start_times.setMaximumSize(new Dimension(300, 400));
        start_times.setPreferredSize(new Dimension(300, 400));
        start_times.setMinimumSize(new Dimension(300, 400));
        start_times.setBorder(new LineBorder(Color.BLACK, 1));
        start_times.setLayout(new java.awt.GridLayout(BatchTimestampEnum.values().length, 2, 5, 5));

        startlabels = new HashMap<Pair<BatchTimestampEnum, BatchTimestampType>, JLabel>();
        for (BatchTimestampEnum process_step : BatchTimestampEnum.values()) {
            BatchTimestampType time_step = BatchTimestampType.start;
            JLabel label = new JLabel("");
            label.setMaximumSize(new Dimension(140, -1));
            label.setPreferredSize(new Dimension(140, -1));
            label.setMinimumSize(new Dimension(140, -1));
            startlabels.putIfAbsent(new Pair(process_step, time_step), label);

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    JLabel field_label = new JLabel(process_step.name() + "_" + time_step.name());
                    field_label.setMaximumSize(new Dimension(140, -1));
                    field_label.setPreferredSize(new Dimension(140, -1));
                    field_label.setMinimumSize(new Dimension(140, -1));
                    start_times.add(field_label);
                    start_times.add(label);
                    start_times.validate();
                    start_times.repaint();
                }
            });
        }
        finish_times = new JPanel();

        finish_times.setMaximumSize(new Dimension(300, 400));
        finish_times.setPreferredSize(new Dimension(300, 400));
        finish_times.setMinimumSize(new Dimension(300, 400));
        finish_times.setBorder(new LineBorder(Color.BLACK, 1));
        finish_times.setLayout(new java.awt.GridLayout(BatchTimestampEnum.values().length, 2, 5, 5));

        for (BatchTimestampEnum process_step : BatchTimestampEnum.values()) {
            BatchTimestampType time_step = BatchTimestampType.finish;
            JLabel label = new JLabel("");
            label.setMaximumSize(new Dimension(140, -1));
            label.setPreferredSize(new Dimension(140, -1));
            label.setMinimumSize(new Dimension(140, -1));
            startlabels.putIfAbsent(new Pair(process_step, time_step), label);

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    JLabel field_label = new JLabel(process_step.name() + "_" + time_step.name());
                    field_label.setMaximumSize(new Dimension(140, -1));
                    field_label.setPreferredSize(new Dimension(140, -1));
                    field_label.setMinimumSize(new Dimension(140, -1));
                    finish_times.add(field_label);
                    finish_times.add(label);
                    finish_times.validate();
                    finish_times.repaint();
                }
            });
        }
    }
}
