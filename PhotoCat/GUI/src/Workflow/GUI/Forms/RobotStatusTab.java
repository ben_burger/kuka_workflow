package Workflow.GUI.Forms;

import Workflow.TCP.RobotStatus;
import Workflow.TCP.TCPRobotServerSuper;
import Workflow.TCP.TCPStationClientSuper;
import Workflow.TCP.TaskSuper;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Benjamin on 3/18/2018.
 */
public class RobotStatusTab {
    private JLabel lbl_date_heartbeat;
    private JLabel lbl_status;
    private JLabel lbl_error;
    private JLabel lbl_date_task_started;
    private JTextPane lbl_task;
    private JPanel mainPanel;

    public RobotStatusTab() {

    }

    public void UpdateStatus(boolean Started, TCPRobotServerSuper server) throws CloneNotSupportedException {
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        if (!Started) {
            lbl_date_heartbeat.setText("Not Started");
            lbl_status.setText("Not Started");
            lbl_error.setText("Not Started");
            lbl_date_task_started.setText("Not Started");
            lbl_task.setText("Not Started");
        } else if (server != null) {
            RobotStatus status = server.GetStatus();
            if(status == null)
            {
                lbl_date_heartbeat.setText("Disconnected");
                lbl_status.setText("Disconnected");
                lbl_error.setText("Disconnected");
                lbl_date_task_started.setText("Disconnected");
                lbl_task.setText("Disconnected");
            }
            else {
                Date last_heartbeat = server.getLastHeartBeatReceived();
                if (last_heartbeat != null)
                    lbl_date_heartbeat.setText(df2.format(last_heartbeat));
                else
                    lbl_date_heartbeat.setText("null");

                lbl_status.setText(status.getStatus().toString());

                if (status.getError() == null)
                    lbl_error.setText("No error");
                else
                    lbl_error.setText(status.getError());

                Date task_started = status.getTaskStarted();
                if (task_started != null)
                    lbl_date_task_started.setText(df2.format(task_started));
                else
                    lbl_date_task_started.setText("null");

                TaskSuper task = status.getTask();
                if(task != null)
                    lbl_task.setText(task.toString());
                else
                     lbl_task.setText("None Started");
            }
        }

    }

    public void UpdateStatus(boolean Started, TCPStationClientSuper client) throws CloneNotSupportedException {
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        if (!Started) {
            lbl_date_heartbeat.setText("Not Started");
            lbl_status.setText("Not Started");
            lbl_error.setText("Not Started");
            lbl_date_task_started.setText("Not Started");
            lbl_task.setText("Not Started");
        } else if (client != null) {
            RobotStatus status = client.GetStatus();
            if(status == null)
            {
                lbl_date_heartbeat.setText("Disconnected");
                lbl_status.setText("Disconnected");
                lbl_error.setText("Disconnected");
                lbl_date_task_started.setText("Disconnected");
                lbl_task.setText("Disconnected");
            }
            else {
                Date last_heartbeat = client.getLastHeartBeatReceived();
                if (last_heartbeat != null)
                    lbl_date_heartbeat.setText(df2.format(last_heartbeat));
                else
                    lbl_date_heartbeat.setText("null");

                lbl_status.setText(status.getStatus().toString());

                if (status.getError() == null)
                    lbl_error.setText("No error");
                else
                    lbl_error.setText(status.getError());

                Date task_started = status.getTaskStarted();
                if (task_started != null)
                    lbl_date_task_started.setText(df2.format(task_started));
                else
                    lbl_date_task_started.setText("null");

                TaskSuper task = status.getTask();
                if(task != null)
                    lbl_task.setText(task.toString());
                else
                    lbl_task.setText("None Started");
            }
        }

    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
