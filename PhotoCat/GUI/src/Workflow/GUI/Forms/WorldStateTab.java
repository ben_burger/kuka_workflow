package Workflow.GUI.Forms;

import Workflow.Batch.*;
import Workflow.Configuration.Configuration;
import Workflow.Configuration.PhotolysisStation;
import Workflow.Configuration.SchedulingStyle;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.ProcessManagement.ProcessStatus;
import Workflow.ProcessManagement.ProcessTypeEnum;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class WorldStateTab extends SuperTab {
    private JPanel mainPanel;
    private JScrollPane solidTablss;
    private JTable solidTable;
    private JTable liquidTable;
    private JLabel lblMinCapsPresent;
    private JButton startRunningButton;
    private JLabel lblQuantosRunning;
    private JLabel lblQuantosActive;
    private JLabel lblStationsRunning;
    private JLabel lblStationsActive;
    private JLabel lblJobExecutor;
    private JLabel lblJobExecutorActive;
    private JLabel lblRunning;
    private JButton stopRunningButton;
    private JButton quantosActiveButton;
    private JButton stationThreadButton;
    private JButton robotJobExecutorButton;
    private JPanel rack_locations_panel;
    private JPanel processes_panel;
    private JComboBox cmbPhotolysis;
    private JComboBox cmbScheduling;
    private JComboBox cmbFreshRackLocation;
    private JTextField txtFreshRack;
    private JButton addFreshRackButton;
    private JButton removeBatchFromWorkflowButton;
    private JComboBox cmbRemoveBatch;
    private JButton add100CapsToButton;
    private JPanel workflowsteps_panel;
    private HashMap<ProcessTypeEnum, JLabel> ProcessEnabledLabels;
    private HashMap<ProcessTypeEnum, JLabel> ProcessRunningLabels;
    private HashMap<WorkflowStepEnum, JLabel> WorkflowStepsEnabledLabels;
    private JTextField txtRemoveBatchRange;
    private JButton removeBatchRangeFromWorkflowButton;
    private JTextField txtFreshRackRangeLabels;
    private JComboBox cmbFreshRackRangeStart;
    private JComboBox cmbFreshRackRangeEnd;
    private JButton addFreshRackRangeButton;

    @Override
    public void setWorkflow(WorkflowSystem workflow) {
        super.setWorkflow(workflow);
        updateStateLabels();


        executer_service = Executors.newSingleThreadScheduledExecutor();
        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    updateJobSystemLabels();
                    updateStateLabels();

                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
        executer_service_tables = Executors.newSingleThreadScheduledExecutor();
        executer_service_tables.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    updateSolidsTable();
                    updateLiquidsTable();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);

        Solid[] solids = workflowSystem.getState().getSolids();
        SolidItemModel solidModel = new SolidItemModel(solids);
        solidTable.setModel(solidModel);

        Liquid[] liquids = workflowSystem.getState().getLiquids();
        LiquidItemModel liquidModel = new LiquidItemModel(liquids);
        liquidTable.setModel(liquidModel);

        cmbPhotolysis.setModel(new DefaultComboBoxModel<>(PhotolysisStation.values()));
        cmbPhotolysis.setSelectedItem(Configuration.getPhotolysisStation());

        cmbScheduling.setModel(new DefaultComboBoxModel<>(SchedulingStyle.values()));
        cmbScheduling.setSelectedItem(Configuration.getScheduler());

        cmbFreshRackLocation.setModel(new DefaultComboBoxModel<>(BatchLocation.values()));
        cmbFreshRackLocation.setSelectedItem(BatchLocation.values()[21]);

        cmbFreshRackRangeStart.setModel(new DefaultComboBoxModel<>(BatchLocation.values()));
        cmbFreshRackRangeStart.setSelectedItem(BatchLocation.values()[21]);

        cmbFreshRackRangeEnd.setModel(new DefaultComboBoxModel<>(BatchLocation.values()));
        cmbFreshRackRangeEnd.setSelectedItem(BatchLocation.values()[21]);

        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {

                    String current = (String) cmbRemoveBatch.getSelectedItem();
                    List<Batch> runs_in_system = workflowSystem.getState().getRunsInTheSystem();
                    String[] names = UpdateBatchNameCombobox(runs_in_system);
                    if (current != null && current != "" && Arrays.stream(names).filter(x -> x.equals(current)).findFirst().isPresent()) {
                        Batch batch = runs_in_system.stream().filter(x -> x.getBatch_name().equals(current)).findFirst().get();

                    } else if (names.length > 0) {
                        cmbRemoveBatch.setSelectedItem(names[0]);
                        Batch batch = runs_in_system.stream().filter(x -> x.getBatch_name().equals(names[0])).findFirst().get();
                    } else {
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);


    }

    private String[] UpdateBatchNameCombobox(List<Batch> runs_in_system) {


        String[] names = runs_in_system.stream().map(Batch::getBatch_name).sorted().collect(Collectors.toList()).toArray(new String[0]);
        String[] NAMES = new String[cmbRemoveBatch.getItemCount()];
        for (int i = 0; i < NAMES.length; i++) {
            NAMES[i] = (String) cmbRemoveBatch.getItemAt(i);
        }

        if (NAMES != null) {
            boolean allSame = true;
            if (NAMES.length == names.length) {

                for (int i = 0; i < NAMES.length && allSame; i++) {
                    final int j = i;
                    allSame = Arrays.stream(names).filter(x -> x.equals(NAMES[j])).findFirst().isPresent();
                }

            } else {
                allSame = false;
            }

            if (!allSame)
                cmbRemoveBatch.setModel(new DefaultComboBoxModel(names));

        } else {
            cmbRemoveBatch.setModel(new DefaultComboBoxModel(names));
        }

        return names;
    }

    private HashMap<BatchLocation, JLabel> BatchLocationLabels;

    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    ScheduledExecutorService executer_service;
    ScheduledExecutorService executer_service_tables;

    public WorldStateTab() {
        startRunningButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ProcessManager manager = workflowSystem.getProcessManager();
                    manager.setProcesses_enabled(true);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        stopRunningButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ProcessManager manager = workflowSystem.getProcessManager();
                    manager.setProcesses_enabled(false);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });

        cmbScheduling.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                try {
                    if (e.getStateChange() == ItemEvent.SELECTED) {

                        SchedulingStyle new_algorithm = (SchedulingStyle) e.getItem();
                        Configuration.setScheduler(new_algorithm);
                        Configuration.saveUserSettings();
                        ;
                        // do something with object
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        cmbPhotolysis.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                try {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        PhotolysisStation new_station = (PhotolysisStation) e.getItem();
                        Configuration.setPhotolysisStation(new_station);
                        Configuration.saveUserSettings();
                        ;
                        // do something with object
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        addFreshRackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    BatchLocation location = (BatchLocation) cmbFreshRackLocation.getSelectedItem();
                    if(workflowSystem.getState().getRackAtLocation(location) != null)
                    {
                        throw new InputMismatchException("Can not place rack, where there is already an rack.");
                    }
                    int batch_identifier = Integer.parseInt(txtFreshRack.getText());
                    // previously the same rack could be added in two rack locations
                    // you could either throw an exception (current choice) or just remove it from where it was
                    String batch_name = "FreshRack"+batch_identifier;
                    Optional<Batch> batch = workflowSystem.getState().getRunsInTheSystem().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst();
                    if (batch.isPresent()) {
                        throw new InputMismatchException("This rack is already located elsewhere, please remove it first.");
                    }
                    workflowSystem.getState().addFreshRack(location, batch_identifier);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        addFreshRackRangeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String[] batchRange = txtFreshRackRangeLabels.getText().split("-");
                    if ( batchRange.length != 2 ) {
                        throw new Exception("Rack Label Range Incorrectly Specified (\"start-end\", inclusive)");
                    }

                    Integer batchStart = Integer.parseInt(batchRange[0]);
                    Integer batchEnd = Integer.parseInt(batchRange[1]);
                    BatchLocation locationStart = (BatchLocation) cmbFreshRackRangeStart.getSelectedItem();
                    BatchLocation locationEnd = (BatchLocation) cmbFreshRackRangeEnd.getSelectedItem();
                    if ( (batchEnd-batchStart) != (locationEnd.ordinal()-locationStart.ordinal()) ) {
                        throw new Exception("Number of Locations Does Not Match Number of Rack Labels");
                    }

                    for ( int i = locationStart.ordinal(); i < (locationEnd.ordinal()+1); i++ ) {
                        BatchLocation newLocation = BatchLocation.values()[i];
                        if (workflowSystem.getState().getRackAtLocation(newLocation) != null) {
                            throw new InputMismatchException("Can not place rack, where there is already an rack.");
                        }
                        String batch_name = "FreshRack"+batchStart;
                        Optional<Batch> batch = workflowSystem.getState().getRunsInTheSystem().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst();
                        if (batch.isPresent()) {
                            throw new InputMismatchException("This rack is already located elsewhere, please remove it first.");
                        }
                        workflowSystem.getState().addFreshRack(newLocation, batchStart);
                        Thread.sleep(1);
                        batchStart++;
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        removeBatchFromWorkflowButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String batch_name = (String) cmbRemoveBatch.getSelectedItem();
                    Optional<Batch> batch = workflowSystem.getState().getRunsInTheSystem().stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst();
                    if (batch.isPresent()) {
                        workflowSystem.getState().RemoveRack(batch.get());
                    }

                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        removeBatchRangeFromWorkflowButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String[] batch_range = txtRemoveBatchRange.getText().split("-");
                    if ( batch_range.length != 2 ) {
                        throw new Exception("Batch Range Incorrectly Specified (\"start-end\", inclusive)");
                    }
                    Integer batch_start = Integer.parseInt(batch_range[0]);
                    Integer batch_end = Integer.parseInt(batch_range[1]);

                    for ( int i = batch_start; i < batch_end+1; i++ ) {
                        String batch_location_name = "InputStation_"+i;
                        BatchLocation batch_location = BatchLocation.valueOf(batch_location_name);
                        Batch batch = workflowSystem.getState().getRackAtLocation(batch_location);
                        if (batch != null) {
                            workflowSystem.getState().RemoveRack(batch);
                        }
                        else {
                            throw new Exception("No Batch Found at Input Station "+i);
                        }
                    }

                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        add100CapsToButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getState().AddPackOfCaps();

                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
    }

    private void updateLiquidsTable() {
        LiquidItemModel model = (LiquidItemModel) liquidTable.getModel();
        model.Refresh(workflowSystem);
    }

    private void updateSolidsTable() {
        SolidItemModel model = (SolidItemModel) solidTable.getModel();
        model.Refresh(workflowSystem);
    }

    private void updateJobSystemLabels() throws CloneNotSupportedException {
        ProcessManager manager = workflowSystem.getProcessManager();
        lblRunning.setText(manager.isProcesses_enabled() ? "Enabled" : "Disabled");
        for (ProcessTypeEnum processTypeEnum : ProcessTypeEnum.values()) {
            JLabel enabled = ProcessEnabledLabels.get(processTypeEnum);
            JLabel running = ProcessRunningLabels.get(processTypeEnum);

            ProcessStatus status = manager.GetStatus(processTypeEnum);
            enabled.setText(status.isEnabled() ? "Enabled" : "Disabled");
            running.setText(status.isRunning() ? "Running" : "Not running");
        }

        for (WorkflowStepEnum processTypeEnum : WorkflowStepEnum.values()) {
            JLabel enabled = WorkflowStepsEnabledLabels.get(processTypeEnum);

            enabled.setText(manager.GetStepEnabled(processTypeEnum) ? "Enabled" : "Disabled");
        }

    }

    private static String FormatBatch(Batch batch) {
        DateFormat df2 = new SimpleDateFormat("dd-MM HH");

        if (batch == null)
            return "";
        else {

            Date added = null;
            if(batch.isFreshRack())
                added = batch.getTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start);
            return String.format("%s:%s - %s %s ", batch.getProgress().equals(Progress.Running) ? "R" : "W", batch.getStatus().getShortName() + "", batch.getBatch_name()
                    ,added == null ? "" :  df2.format(added));
        }
    }

    private void updateStateLabels() {
        WorldState state = workflowSystem.getState();
        for (BatchLocation location : BatchLocation.values()) {
            JLabel label = BatchLocationLabels.get(location);
            label.setText(FormatBatch(state.getRackAtLocation(location)));
        }
        lblMinCapsPresent.setText(state.getMinimal_Caps_Present() + "");
    }

    private void FillRackLocationsPanel() {
        BatchLocationLabels = new HashMap<BatchLocation, JLabel>();

        for (BatchLocation location : BatchLocation.values()) {
            JLabel label = new JLabel("");
            label.setMaximumSize(new Dimension(245, -1));
            label.setPreferredSize(new Dimension(245, -1));
            label.setMinimumSize(new Dimension(245, -1));
            BatchLocationLabels.putIfAbsent(location, label);

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    JLabel field_label = new JLabel(location.name());
                    field_label.setMaximumSize(new Dimension(145, -1));
                    field_label.setPreferredSize(new Dimension(145, -1));
                    field_label.setMinimumSize(new Dimension(145, -1));
                    rack_locations_panel.add(field_label);
                    rack_locations_panel.add(label);
                    rack_locations_panel.validate();
                    rack_locations_panel.repaint();
                }
            });
        }
    }

    private void createUIComponents() {
        processes_panel = new JPanel();
//        processes_panel.setLayout(new GridLayout(ProcessTypeEnum.values().length, 3));
        processes_panel.setMaximumSize(new Dimension(300, 150));
        processes_panel.setPreferredSize(new Dimension(300, 150));
        processes_panel.setMinimumSize(new Dimension(300, 150));
        processes_panel.setLayout(new java.awt.GridLayout(ProcessTypeEnum.values().length, 3, 5, 5));

        FillProcessesPanel();

        rack_locations_panel = new JPanel();
        rack_locations_panel.setMaximumSize(new Dimension(500, 750));
        rack_locations_panel.setPreferredSize(new Dimension(500, 750));
        rack_locations_panel.setMinimumSize(new Dimension(500, 750));
        rack_locations_panel.setBorder(new LineBorder(Color.BLACK, 1));
        rack_locations_panel.setLayout(new java.awt.GridLayout(BatchLocation.values().length, 2, 5, 5));
        FillRackLocationsPanel();

        workflowsteps_panel = new JPanel();
//        workflowsteps_panel.setLayout(new GridLayout(WorkflowStepEnum.values().length, 2));
        workflowsteps_panel.setMaximumSize(new Dimension(300, 175));
        workflowsteps_panel.setPreferredSize(new Dimension(300, 175));
        workflowsteps_panel.setMinimumSize(new Dimension(300, 175));
        workflowsteps_panel.setLayout(new java.awt.GridLayout(WorkflowStepEnum.values().length, 2, 5, 5));
        FillWorkflowstepsPanel();


    }



    private void FillProcessesPanel() {
        ProcessEnabledLabels = new HashMap<ProcessTypeEnum, JLabel>();
        ProcessRunningLabels = new HashMap<ProcessTypeEnum, JLabel>();

        for (ProcessTypeEnum processTypeEnum : ProcessTypeEnum.values()) {
            JButton button_enable = new JButton(processTypeEnum.name());
            button_enable.setMaximumSize(new Dimension(145, -1));
            button_enable.setPreferredSize(new Dimension(145, -1));
            button_enable.setMinimumSize(new Dimension(145, -1));

            button_enable.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        ProcessManager manager = workflowSystem.getProcessManager();
                        boolean enabled = manager.getProcessEnabled(processTypeEnum);
                        manager.EnableProcess(processTypeEnum, !enabled);
                    } catch (Exception e1) {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                    }
                }
            });

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {

                        processes_panel.add(button_enable);

                        processes_panel.validate();
                        processes_panel.repaint();
                    } catch (Exception e1) {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                    }

                }
            });
            JLabel label_enabled = new JLabel("");
            label_enabled.setMaximumSize(new Dimension(145, -1));
            label_enabled.setPreferredSize(new Dimension(145, -1));
            label_enabled.setMinimumSize(new Dimension(145, -1));
            ProcessEnabledLabels.putIfAbsent(processTypeEnum, label_enabled);

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        processes_panel.add(label_enabled);

                        processes_panel.validate();
                        processes_panel.repaint();
                    } catch (Exception e1) {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                    }
                }
            });
            JLabel label_running = new JLabel("");
            label_running.setMaximumSize(new Dimension(145, -1));
            label_running.setPreferredSize(new Dimension(145, -1));
            label_running.setMinimumSize(new Dimension(145, -1));
            ProcessRunningLabels.putIfAbsent(processTypeEnum, label_running);


            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        processes_panel.add(label_running);
                        processes_panel.validate();
                        processes_panel.repaint();
                    } catch (Exception e1) {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                    }
                }
            });
        }
    }


    private void FillWorkflowstepsPanel() {
        WorkflowStepsEnabledLabels = new HashMap<WorkflowStepEnum, JLabel>();

        for (WorkflowStepEnum workflowstep : WorkflowStepEnum.values()) {
            JButton button_enable = new JButton(workflowstep.name());
            button_enable.setMaximumSize(new Dimension(145, -1));
            button_enable.setPreferredSize(new Dimension(145, -1));
            button_enable.setMinimumSize(new Dimension(145, -1));

            button_enable.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        ProcessManager manager = workflowSystem.getProcessManager();
                        boolean enabled = manager.GetStepEnabled(workflowstep);
                        manager.EnableStep(workflowstep, !enabled);
                    } catch (Exception e1) {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                    }
                }
            });

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {

                        workflowsteps_panel.add(button_enable);

                        workflowsteps_panel.validate();
                        workflowsteps_panel.repaint();
                    } catch (Exception e1) {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                    }

                }
            });

            JLabel label_enabled = new JLabel("");
            label_enabled.setMaximumSize(new Dimension(145, -1));
            label_enabled.setPreferredSize(new Dimension(145, -1));
            label_enabled.setMinimumSize(new Dimension(145, -1));
            WorkflowStepsEnabledLabels.putIfAbsent(workflowstep, label_enabled);

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        workflowsteps_panel.add(label_enabled);

                        workflowsteps_panel.validate();
                        workflowsteps_panel.repaint();
                    } catch (Exception e1) {
                        messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                    }
                }
            });

        }
    }

}
