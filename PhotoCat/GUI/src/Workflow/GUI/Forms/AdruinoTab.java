package Workflow.GUI.Forms;

import Workflow.IMessageHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benjamin on 3/9/2018.
 */
public class AdruinoTab extends SuperTab{
    private JPanel mainPanel;
    private JButton startButton;
    private JButton stopButton;

    public AdruinoTab() {
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Start();
            }
        });
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Stop();
            }
        });
    }

    public void Start() {}

    public void Stop() {}


    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }


    public static AdruinoTab GetSonicatorTab() {return new AdruinoTab() {
        @Override
        public void Start()
        {
            try {
                workflowSystem.getSonicatorCom().Start();
            } catch (Exception e1)
            {
                messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
            }
        }

        @Override
        public void Stop()
        {
            try {
                workflowSystem.getSonicatorCom().Stop();
            } catch (Exception e1)
            {
                messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
            }
        }
    };
    }
}
