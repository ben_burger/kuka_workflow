package Workflow.GUI.Forms;

import Workflow.Batch.Liquid;
import Workflow.Batch.Solid;
import Workflow.WorkflowSystem;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.swing.table.AbstractTableModel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Benjamin on 9/8/2018.
 */
public class LiquidItemModel extends AbstractTableModel {
    String[] ColumnNames = new String[] {"Name","Density","Station","Pump","VolumePresent","min","max","expiryDate"};
    Liquid[] Liquids;

    public LiquidItemModel(Liquid[] liquids) {
        super();
        Liquids = liquids;
    }

    public void Refresh(WorkflowSystem workflowSystem)
    {

        int old_number_of_liquids = Liquids.length;


        Liquids = workflowSystem.getState().getLiquids();

        int new_number_of_liquids = Liquids.length;

        if(old_number_of_liquids < new_number_of_liquids)
            fireTableRowsInserted(old_number_of_liquids,new_number_of_liquids-1);
        else if(old_number_of_liquids >new_number_of_liquids)
            fireTableRowsDeleted(new_number_of_liquids,old_number_of_liquids-1);
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return Liquids.length;
    }

    @Override
    public String getColumnName(int column) {
        return ColumnNames[column];
    }

    @Override
    public int getColumnCount() {
        return ColumnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Liquid liquid = Liquids[rowIndex];
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        switch(columnIndex)
        {
            case 0:
                return liquid.getName();
            case 1:
                return liquid.getDensity();
            case 2:
                return liquid.getStationID();
            case 3:
                return liquid.getPumpID();
            case 4:
                return String.format("%.2f",liquid.getMinimalVolumePresent());
            case 5:
                return liquid.getMin();
            case 6:
                return liquid.getMax();
            case 7:
                return liquid.getExpiryDate()!=null ? df2.format(liquid.getExpiryDate()) : "";
        }
        throw new NotImplementedException();
    }


    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class; // Return the class that best represents the column...
    }
}
