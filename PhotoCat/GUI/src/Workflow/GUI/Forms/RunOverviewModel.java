package Workflow.GUI.Forms;

import Workflow.Batch.Batch;
import Workflow.Batch.Liquid;
import Workflow.Batch.Sample;
import Workflow.Batch.Solid;

import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.stream.Stream;

public class RunOverviewModel extends AbstractTableModel {
    public RunOverviewModel(Batch batch) {
        super();
        this.batch = batch;
    }

    int number_of_columns = 11;

    private Batch batch;

    @Override
    public String getColumnName(int column) {

        String [] ColumnNames = Stream.concat( Arrays.asList(new String []{"SampleIndex","SampleNumber","Name","vial_capped","gc_well_number","hydrogen_evolution","oxygen_evolution","hydrogen_evolution_micromol","weighted_is_hydrogen_micromol","weighted_is_sl_hydrogen_micromol","oxygen_evolution_micromol"}).stream(), Stream.concat(
                Stream.concat(Arrays.asList(batch.getSolids()).stream().map(x -> x.getName()),
                        Arrays.asList(batch.getLiquids()).stream().map(x -> x.getName())),
                Stream.concat(Arrays.asList(batch.getSolids()).stream().map(x -> x.getName()+"_dispensed"),
                        Arrays.asList(batch.getLiquids()).stream().map(x -> x.getName()+"_dispensed"))
        )).toArray(String[]::new);

        return ColumnNames[column];
    }
    public void setBatch(Batch batch)
    {
        Batch old_batch = this.batch;
        this.batch = batch;

        int old_samples;
        int new_samples;

        if(batch != null)
            new_samples = batch.getSamples().length;
        else
            new_samples = 0;
        if(old_batch != null)
            old_samples = old_batch.getSamples().length;
        else
            old_samples = 0;

        if(old_samples < new_samples)
            fireTableRowsInserted(old_samples,new_samples-1);

        if(old_samples > new_samples)
            fireTableRowsDeleted(new_samples,old_samples-1);

        fireTableStructureChanged();

        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        if(batch == null)
            return 0;
        else
            return batch.getSamples().length;
    }

    @Override
    public int getColumnCount() {
        int result;
        if(batch == null)
            result =  0;
        else
            result = number_of_columns + 2*batch.getSolids().length + 2*batch.getLiquids().length;
        return result;
    }

    //SampleIndex,SampleNumber,Name,capped,gc_well_number,hydrogen_evolution,oxygen_evolution,Water,Sand,Sand2,Water_dispensed,Sand_dispensed,
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(batch == null)
            return "";

        Sample sample = batch.getSamples()[rowIndex];
        if(columnIndex == 0) {
            return sample.getSampleIndex();
        }   else if(columnIndex == 1) {
            return sample.getSampleNumber();
        }else if(columnIndex == 2) {
            return sample.getName();
        }else if(columnIndex == 3) {
            boolean vial_capped = sample.getCapped();
            return vial_capped?"1":"0";
        }else if(columnIndex == 4) {
            Integer int_value =  sample.getGC_well_number();
            return int_value==null?"":int_value.intValue();
        }else if(columnIndex == 5) {
            Integer int_value =   sample.getHydrogen_evolution();
            return int_value==null?"":String.format("%s",int_value.intValue() + "");
        }else if(columnIndex == 6) {
            Integer int_value =   sample.getOxygen_evolution();
            return int_value==null?"":String.format("%s",int_value.intValue() + "");
        }else if(columnIndex == 7) {
            Double double_value =   sample.getHydrogen_evolution_micromol();
            return double_value==null?"":String.format("%.2f",double_value.doubleValue());
        }else if(columnIndex == 8) {
            Double double_value =   sample.getWeightedHydrogenMicromol();
            return double_value==null?"":String.format("%.2f",double_value.doubleValue());
        }else if(columnIndex == 9) {
            Double double_value =   sample.getWeightedISSLHydrogenMicromol();
            return double_value==null?"":String.format("%.2f",double_value.doubleValue());
        }else if(columnIndex == 10) {
            Double double_value =   sample.getOxygen_evolution_micromol();
            return double_value==null?"":String.format("%.2f",double_value.doubleValue());
        }
        else if(number_of_columns <= columnIndex && columnIndex < number_of_columns + batch.getSolids().length)
        {
            Solid sol = batch.getSolids()[columnIndex-number_of_columns];
            return String.format("%.2f",sample.getSolidDispenseAmount(sol));
        }
        else if(number_of_columns + batch.getSolids().length <= columnIndex && columnIndex < number_of_columns + batch.getSolids().length+batch.getLiquids().length)
        {
            Liquid liq = batch.getLiquids()[columnIndex-number_of_columns-batch.getSolids().length];
            return String.format("%.2f",sample.getLiquidDispenseAmount(liq));
        }
        else if(number_of_columns + batch.getSolids().length + batch.getLiquids().length <= columnIndex && columnIndex < number_of_columns + 2*batch.getSolids().length+batch.getLiquids().length)
        {
            Solid sol = batch.getSolids()[columnIndex-number_of_columns-batch.getSolids().length-batch.getLiquids().length];
            Float float_value = sample.getSolidDispensed(sol);
            return float_value==null?"":String.format("%.3f",float_value);
        }
        else if(number_of_columns + 2*batch.getSolids().length + batch.getLiquids().length <= columnIndex && columnIndex < number_of_columns + 2*batch.getSolids().length+2*batch.getLiquids().length)
        {
            Liquid liq = batch.getLiquids()[columnIndex-number_of_columns-2*batch.getSolids().length-batch.getLiquids().length];
            Float float_value = sample.getLiquidDispensed(liq);
            return float_value==null?"":String.format("%.2f",float_value);
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class; // Return the class that best represents the column...
    }

}
