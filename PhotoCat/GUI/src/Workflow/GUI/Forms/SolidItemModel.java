package Workflow.GUI.Forms;

import Workflow.Batch.Solid;
import Workflow.WorkflowSystem;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SolidItemModel extends AbstractTableModel {

    private List<Solid> solids;
    private String[] ColumnNames = new String[] {"Name", "HotelIndex", "Blocked","dispense algorithm","min","max","Dosages"};

    public void Refresh(WorkflowSystem workflowSystem)
    {
        int old_number_of_solids = solids.size();


        solids = new ArrayList<Solid>(Arrays.asList(workflowSystem.getState().getSolids()));
        int new_number_of_solids = solids.size();

        if(old_number_of_solids < new_number_of_solids)
            fireTableRowsInserted(old_number_of_solids,new_number_of_solids-1);
        else if(old_number_of_solids >new_number_of_solids)
            fireTableRowsDeleted(new_number_of_solids,old_number_of_solids-1);
        fireTableDataChanged();
    }

    public SolidItemModel(Solid[] users) {
        super();
        this.solids = new ArrayList<Solid>(Arrays.asList(users));
    }
    @Override
    public String getColumnName(int column) {
        return ColumnNames[column];
    }

    @Override
    public int getRowCount() {
        return solids.size();
    }

    @Override
    public int getColumnCount() {
        return ColumnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object value = "??";
        Solid user = solids.get(rowIndex);
        switch (columnIndex) {
            case 0:
                value = user.getName();
                break;
            case 1:
                value = user.getHotelIndex();
                break;
            case 2:
                value = user.getBlocked()?"1":"0";
                break;
            case 3:
                return user.getDispenseAlgorithm();
            case 4:
                return user.getMin();
            case 5:
                return user.getMax();
            case 6:
                return user.getRemainingDosages();
        }

        return value;

    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class; // Return the class that best represents the column...
    }

    public Solid getSolidAt(int row) {
        return solids.get(row);
    }

}