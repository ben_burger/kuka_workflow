package Workflow.GUI.Forms;

import javax.swing.*;
import java.io.IOException;

import Workflow.WorkflowSystem;

/**
 * Created by Benjamin on 3/8/2018.
 */
public class Dashboard {
    private JPanel panelMain;
    private JTabbedPane tabbedPane1;
    private JTextArea textArea1;
    private JPanel QuantosPanel;
    private JPanel StatusBarPanel;
    private JPanel ConfigurationPanel;
    private JPanel LiquidEnsemblePanel;
    private JPanel SonicatorPanel;
    private JPanel PhotocatPanel;
    private JPanel KMRPanel;
    private JPanel CapperPanel;
    private JPanel batchPanel;
    private JScrollPane textAreaScrollpane;
    private JPanel GCPanel;
    private JPanel BatchPanel;
    private JPanel AlertPanel;
    private JPanel ArchivedRunsPanel;
    private JPanel BatchMakerPanel;


    private boolean initialised = false;


    private WorkflowSystem WorkflowSystem;

    private QuantosTab QuantosTab;
    private Workflow.GUI.Forms.StatusBar StatusBar;
    private Workflow.GUI.Forms.ConfigurationTab ConfigurationTab;
    private MessageHandler MessageHandler;
    private Workflow.GUI.Forms.LiguidEnsembleTab LiguidEnsembleTab;
    private AdruinoTab SonicatorTab;
    private PhotolysisTab PhotocatTab;
    private CapperTab CapperTab;
    private Workflow.GUI.Forms.KMRTab KMRTab;
    private Workflow.GUI.Forms.BatchTab batchTab;
    private GCTab GCTab;
    private WorldStateTab worldStateTab;
    private AlertSystemTab AlertSystemTab;
    private BatchTab archivedRunsTab;
    private Workflow.GUI.Forms.BatchMakerTab BatchMakerTab;

    public Dashboard(WorkflowSystem workflow)
    {
        WorkflowSystem = workflow;
//        tabbedPane1.addChangeListener(new ChangeListener() {
//            @Override
//            public void stateChanged(ChangeEvent e) {
//                int index = tabbedPane1.getSelectedIndex();
//                if (index == 0)
//                {
//                    ConfigurationTab.RefreshUIControls();
//                } else if (index == 1)
//                {
//                    worldStateTab.RefreshUIControls();
//                } else if (index == 2)
//                {
//                    batchTab.RefreshUIControls();
//                } else if (index == 3)
//                {
//                    KMRTab.RefreshUIControls();
//                } else if (index == 4)
//                {
//                    QuantosTab.RefreshUIControls();
//                } else if (index == 5)
//                {
//                    LiguidEnsembleTab.RefreshUIControls();
//                } else if (index == 6)
//                {
//                    CapperTab.RefreshUIControls();
//                } else if (index == 7)
//                {
//                    SonicatorTab.RefreshUIControls();
//                } else if (index == 8)
//                {
//                    PhotocatTab.RefreshUIControls();
//                } else if (index == 9)
//                {
//                    GCTab.RefreshUIControls();
//                }
//            }
//        });
    }





    public void InitializeComponents()  {
        if(initialised)
            throw new IllegalStateException("System already initialised");

        initialised = true;


        MessageHandler = new MessageHandler(textArea1);

        for(SuperTab tab : new SuperTab[] {archivedRunsTab,AlertSystemTab,worldStateTab,GCTab, CapperTab, ConfigurationTab,QuantosTab,StatusBar, LiguidEnsembleTab, PhotocatTab,SonicatorTab, KMRTab, batchTab, BatchMakerTab
        })
        {
            tab.setWorkflow(WorkflowSystem);
            tab.setMessageHandler(MessageHandler);
        }

        WorkflowSystem.SetMessageHandler(MessageHandler);

    }


    public JPanel getMainPanel()
    {
        return panelMain;
    }

    public void MakeItHappen()  {
        try {
            // It is time for Metal!
            UIManager.setLookAndFeel(
                    UIManager.getCrossPlatformLookAndFeelClassName());
        }
        catch (Exception e) {
            // handle exception
            //We accept GUI being ugly
        }


        JFrame frame = new JFrame("IIWA Photocatalysis - Development Dashboard");


        frame.setContentPane(this.getMainPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        this.InitializeComponents();
        textAreaScrollpane.setViewportView(textArea1);

    }


    private void createUIComponents() throws IOException {
        QuantosTab = new QuantosTab();
        QuantosPanel = QuantosTab.getMainPanel();

        StatusBar = new StatusBar();
        StatusBarPanel = StatusBar.getMainPanel();

        ConfigurationTab = new ConfigurationTab();
        ConfigurationPanel = ConfigurationTab.getMainPanel();

        LiguidEnsembleTab = new LiguidEnsembleTab();
        LiquidEnsemblePanel =  LiguidEnsembleTab.getMainPanel();

        SonicatorTab = AdruinoTab.GetSonicatorTab();
        SonicatorPanel =  SonicatorTab.getMainPanel();

        PhotocatTab = new PhotolysisTab();
        PhotocatPanel =  PhotocatTab.getMainPanel();

        CapperTab = new CapperTab();
        CapperPanel = CapperTab.getMainPanel();

        KMRTab = new KMRTab();
        KMRPanel =  KMRTab.getMainPanel();

        batchTab = new BatchTab(false);
        batchPanel = batchTab.getMainPanel();

        archivedRunsTab = new BatchTab(true);
        ArchivedRunsPanel = archivedRunsTab.getMainPanel();

        GCTab = new GCTab();
        GCPanel = GCTab.getMainPanel();

        worldStateTab = new WorldStateTab();
        BatchPanel = worldStateTab.getMainPanel();

        AlertSystemTab = new AlertSystemTab();
        AlertPanel = AlertSystemTab.getMainPanel();

        BatchMakerTab = new BatchMakerTab();
        BatchMakerPanel = BatchMakerTab.getMainPanel();

    }
}
