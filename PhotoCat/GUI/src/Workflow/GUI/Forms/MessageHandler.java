package Workflow.GUI.Forms;



import Workflow.IMessageHandler;

import javax.swing.*;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageHandler  implements IMessageHandler {
    private JTextArea MessageOutput;
    private String Messages;

    public MessageHandler(JTextArea output)
    {
        MessageOutput = output;
        Messages = "";
    }

    public void HandleException(ExceptionSource source, Exception e)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String stackTrace = format.format( new Date()   ) + " " + source.name() +": \r\n" + e.getMessage() +  "\r\n"+ sw.toString();
        Messages = stackTrace + Messages;

        Messages = TakeFirstLines(5000,Messages);

        MessageOutput.setText(Messages);
    }

    @Override
    public void WriteMessage(String s) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

            Messages = s+"\r\n" + Messages;


        Messages = TakeFirstLines(5000,Messages);

        MessageOutput.setText(Messages);
    }

    private String TakeFirstLines(int lines,String s) {
        StringBuilder result = new StringBuilder();
        String[] split = s.split("\\r?\\n");
        for(int i=0;i<split.length && i < lines;i++)
        {
            result.append(split[i]);
            result.append("\r\n");
        }
        return result.toString();
    }


}
