package Workflow.GUI.Forms;

import Workflow.AlertSystem.AlertSystem;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Batch.Batch;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.swing.table.AbstractTableModel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RuleItemModel extends AbstractTableModel {
    private AlertSystem AlertSystem;
    private ScheduledExecutorService executer_service;
    private final String [] ColumnNames = new String []{"Rule name" ,"Status", "Last activation date"};

    public RuleItemModel(Workflow.AlertSystem.AlertSystem alertSystem)
    {
        AlertSystem = alertSystem;

        executer_service = Executors.newSingleThreadScheduledExecutor();
        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                fireTableDataChanged();
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);
    }


    @Override
    public String getColumnName(int column) {

        return ColumnNames[column];
    }

    @Override
    public int getRowCount() {
        return AlertSystem.GetRules().length;
    }

    @Override
    public int getColumnCount() {
        return ColumnNames.length;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class; // Return the class that best represents the column...
    }


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        RuleSuper rule = AlertSystem.GetRules()[rowIndex];
        DateFormat df2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

        switch (columnIndex)
        {
            case 0:
                return rule.getClass().getSimpleName();
            case 1:
                return rule.RuleActivated() ? "Alert" : "OK";
            case 2: {
                Date date  = rule.GetLastActivation();
                return date == null ? "" : df2.format(date);
            }
            default:
                throw new NotImplementedException();
        }
    }
}
