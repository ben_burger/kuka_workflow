package Workflow.GUI.Forms;

import Workflow.IMessageHandler;
import Workflow.ProcessManagement.ProcessTypeEnum;
import Workflow.WorkflowSystem;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class AlertSystemTab  extends SuperTab{
    private JPanel mainPanel;
    private JTable RuleTable;
    private JButton clearAlertsAndResetButton;
    private JButton btnEnabled;
    private JTable Alerttable;
    private JButton btnSentEmails;
    private JButton btnSentTxt;

    ScheduledExecutorService executer_service;

    public AlertSystemTab() {
        executer_service = Executors.newSingleThreadScheduledExecutor();
        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {

                    UpdateFieldLabels();

                } catch (Exception e) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e);
                }
            }


        }, 1000, 250, TimeUnit.MILLISECONDS);

        clearAlertsAndResetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getAlertSystem().ClearAlerts();
                } catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        btnEnabled.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getProcessManager().EnableProcess(ProcessTypeEnum.AlertProcess,!workflowSystem.getProcessManager().getProcessEnabled(ProcessTypeEnum.AlertProcess));
                } catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        btnSentEmails.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getAlertSystem().SendEmails(!workflowSystem.getAlertSystem().getSentEmails());
                } catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        btnSentTxt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getAlertSystem().SendTexts(!workflowSystem.getAlertSystem().getSentTexts());
                } catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
    }

    private void UpdateFieldLabels() {
        btnEnabled.setText(workflowSystem.getProcessManager().getProcessEnabled(ProcessTypeEnum.AlertProcess)? "Enabled" : "Disabled");
        btnSentEmails.setText(workflowSystem.getAlertSystem().getSentEmails()? "Enabled" : "Disabled");
        btnSentTxt.setText(workflowSystem.getAlertSystem().getSentTexts()? "Enabled" : "Disabled");
    }

    @Override
    public void setWorkflow(WorkflowSystem workflow) {
        super.setWorkflow(workflow);

        RuleItemModel liquidModel = new RuleItemModel(workflow.getAlertSystem());
        RuleTable.setModel(liquidModel);

        AlertItemModel alertModel = new AlertItemModel(workflow.getAlertSystem());
        Alerttable.setModel(alertModel);
    }
        @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }
}
