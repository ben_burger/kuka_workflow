package Workflow.GUI.Forms;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertSystem;
import Workflow.AlertSystem.RuleSuper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.swing.table.AbstractTableModel;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AlertItemModel extends AbstractTableModel {

    private Workflow.AlertSystem.AlertSystem AlertSystem;
    private ScheduledExecutorService executer_service;
    private final String [] ColumnNames = new String []{"AlertType","Message"};
    private Alert[] Alerts;

    public AlertItemModel(Workflow.AlertSystem.AlertSystem alertSystem)
    {
        AlertSystem = alertSystem;
        Alerts = new Alert[0];

        executer_service = Executors.newSingleThreadScheduledExecutor();
        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    UpdateDateAndFireEvents();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);
    }


    public Object deepclone(Object o) throws CloneNotSupportedException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(bos);
            oos.writeObject(o);
            oos.flush();
            oos.close();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] byteData = bos.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
        ObjectInputStream ois;
        Object res=null;
        try {
            ois = new ObjectInputStream(bais);
            res  = (Object) ois.readObject();
            ois.close();
            bais.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return res;
    }

    private void UpdateDateAndFireEvents() throws CloneNotSupportedException {
        Alert[] new_alerts = (Alert[]) deepclone(AlertSystem.getAlerts());

        if(Alerts.length > new_alerts.length)
            if(Alerts.length < new_alerts.length)
                fireTableRowsInserted(Alerts.length,new_alerts.length-1);
            else if(Alerts.length >new_alerts.length)
                fireTableRowsDeleted(new_alerts.length,Alerts.length-1);

        fireTableDataChanged();
        Alerts = new_alerts;


    }


    @Override
    public String getColumnName(int column) {

        return ColumnNames[column];
    }

    @Override
    public int getRowCount() {
        return Alerts.length;
    }

    @Override
    public int getColumnCount() {
        return ColumnNames.length;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class; // Return the class that best represents the column...
    }


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(rowIndex < 0 || rowIndex >= Alerts.length)
            return "";

        Alert alert= Alerts[rowIndex];
        DateFormat df2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

        switch (columnIndex)
        {
            case 0:
                return alert.getType().name();
            case 1:
//                return df2.format(alert.getTimeStamp());
//            case 2:
            {
                String message = alert.getMessage();
                return message == null ? "" : /*df2.format(alert.getTimeStamp())+*/message;
            }
            default:
                throw new NotImplementedException();
        }
    }

}
