package Workflow.GUI.Forms;

import Serialio.SerialPortLocal;
import Workflow.CollectData;
import Workflow.Configuration.Configuration;
import Workflow.IMessageHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.IOException;

public class ConfigurationTab extends SuperTab {
    private JPanel mainPanel;

    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    public ConfigurationTab() throws IOException {


        String[] ports = SerialPortLocal.getPortList();

        cmbQuantos.setModel(new DefaultComboBoxModel(ports));
        cmbPhoto.setModel(new DefaultComboBoxModel(ports));
        cmbPump.setModel(new DefaultComboBoxModel(ports));
        cmbScale.setModel(new DefaultComboBoxModel(ports));
        cmbSonicator.setModel(new DefaultComboBoxModel(ports));
        cmbCapper.setModel(new DefaultComboBoxModel(ports));
        cmbPump2.setModel(new DefaultComboBoxModel(ports));
        cmbScale2.setModel(new DefaultComboBoxModel(ports));
        cmbVibratory.setModel(new DefaultComboBoxModel(ports));
        cmbProbe.setModel(new DefaultComboBoxModel(ports));

        cmbQuantos.setSelectedItem(Configuration.getQuantosPort());
        cmbPump.setSelectedItem(Configuration.getLiquidPumpPort());
        cmbScale.setSelectedItem(Configuration.getScalePort());
        cmbSonicator.setSelectedItem(Configuration.getSonicatorPort());
        cmbPhoto.setSelectedItem(Configuration.getPhotoCatPort());
        cmbCapper.setSelectedItem(Configuration.getCapperPort());
        cmbProbe.setSelectedItem(Configuration.getProbePort());

        cmbPump2.setSelectedItem(Configuration.getLiquidPumpPort2());
        cmbScale2.setSelectedItem(Configuration.getScalePort2());

        txtGCIP.setText(Configuration.getGCIP());
        txtGCPort.setText("" + Configuration.getGCPort());

        txtCapperTimeout.setText(String.valueOf(Configuration.getTimeoutCapper()));
        txtQuantosTimeout.setText(String.valueOf(Configuration.getTimeoutQuantos()));
        txtScaleTimeout.setText(String.valueOf(Configuration.getTimeoutScale()));
        txtAdruinoTimeout.setText(String.valueOf(Configuration.getTimeoutAdruino()));
        txtLBRPort.setText(String.valueOf(Configuration.getSideLBRPort()));
        txtKMRPort.setText(String.valueOf(Configuration.getSideKMRPort()));
        txtKMRTimeout.setText(String.valueOf(Configuration.getTimeoutTCP()));
        txtCapperFreq.setText(String.valueOf(Configuration.getHeartBeatFrequencyCapper()));
        txtQuantosFreq.setText(String.valueOf(Configuration.getHeartBeatFrequencyQuantos()));
        txtScaleFreq.setText(String.valueOf(Configuration.getHeartBeatFrequencyScale()));
        txtArduinoFreq.setText(String.valueOf(Configuration.getHeartBeatFrequencyAdruino()));
        txtKmrFreq.setText(String.valueOf(Configuration.getHeartBeatFrequencyTCP()));


        VibratoryReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectVibratoryPhotolysis();
            }
        });


        QuantosReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectQuantos();
            }
        });

        PumpReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectPump();
            }
        });
        ScaleReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectScale();
            }
        });
        SonicatorReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectSonicator();
            }
        });
        PhotocatReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectPhotocat();
            }
        });
        KMRReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectKMR();
            }
        });
        ConnectAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConnectAllModules();
            }
        });
        saveAsDefaultButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SaveConfiguration();
            }
        });
        loadDefaultButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoadConfiguration();
            }
        });
        cmbVibratory.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveVibratoryPort();
            }
        });
        cmbProbe.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveProbePort();
            }
        });
        cmbQuantos.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveQuantosPort();
            }
        });
        cmbPump.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SavePumpPort();
            }
        });
        cmbScale.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveScalePort();
            }
        });
        cmbSonicator.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveSonicatorPort();
            }
        });
        cmbPhoto.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SavePhotocatPort();
            }
        });

        txtCapperTimeout.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveCapperTimeout();
            }
        });
        txtQuantosTimeout.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveQuantosTimeout();
            }
        });
        txtScaleTimeout.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveScaleTimeout();
            }
        });
        txtAdruinoTimeout.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveAdruinoTimeout();
            }
        });
        txtKMRPort.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveKMRPort();
            }
        });
        txtLBRPort.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveLBRPort();
            }
        });
        txtKMRTimeout.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveKMRTimeout();
            }
        });
        txtCapperFreq.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveCapperFreq();
            }
        });
        txtQuantosFreq.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveQuantosFreq();
            }
        });
        txtScaleFreq.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveScaleFreq();
            }
        });
        txtArduinoFreq.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveAdruinoFreq();
            }
        });
        txtKmrFreq.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveKMRFreq();
            }
        });
        LoadDataInForm();

        CapperReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectCapper();
            }
        });
        cmbCapper.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveCapperPort();
            }
        });
        Pump2ReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectPump2();

            }
        });
        Scale2ReconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectScale2();
            }
        });
        cmbPump2.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SavePump2Port();
            }
        });
        cmbScale2.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveScale2Port();
            }
        });
        txtGCIP.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SaveGCPort();
            }
        });
        txtGCPort.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);

                Configuration.setGCIP(txtGCIP.getText());

            }
        });
        btnConnectGC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectGC();
            }
        });
        ProbeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TryConnectProbe();
            }
        });
        createOverviewFromCompletedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                CollectData.CreateOverview(workflowSystem,"completed");
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        });
    }

    private void TryConnectProbe() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectProbe();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private void SaveProbePort() {
        try {
            Configuration.setProbePort((String) cmbProbe.getSelectedItem());
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveVibratoryPort() {
        try {
            Configuration.setVibratoryPhotoCatPort((String) cmbVibratory.getSelectedItem());
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveGCPort() {
        try {
            Configuration.setGCPort(Integer.parseInt(txtGCPort.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }


    private void SaveKMRFreq() {
        try {
            Configuration.setHeartBeatFrequencyTCP(Integer.parseInt(txtKmrFreq.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveAdruinoFreq() {
        try {
            Configuration.setHeartBeatFrequencyAdruino(Integer.parseInt(txtArduinoFreq.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveScaleFreq() {
        try {
            Configuration.setHeartBeatFrequencyScale(Integer.parseInt(txtScaleFreq.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveQuantosFreq() {
        try {
            Configuration.setHeartBeatFrequencyQuantos(Integer.parseInt(txtQuantosFreq.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveCapperFreq() {
        try {
            Configuration.setHeartBeatFrequencyCapper(Integer.parseInt(txtCapperFreq.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveKMRTimeout() {
        try {
            Configuration.setTimeoutTCP(Integer.parseInt(txtKMRTimeout.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }


    private void SaveCapperPort() {
        try {
            Configuration.setCapperPort((String) cmbCapper.getSelectedItem());
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveCapperTimeout() {
        try {
            Configuration.setTimeoutCapper(Integer.parseInt(txtCapperTimeout.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void TryConnectCapper()

    {
        try {
            workflowSystem.ConnectCapper();
        } catch (Exception e1) {
            DisplayException(e1);
        }

    }

    private void SaveLBRPort() {
        try {
            Configuration.setLBRPort(Integer.parseInt(txtLBRPort.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveKMRPort() {
        try {
            Configuration.setKMRPort(Integer.parseInt(txtKMRPort.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveQuantosTimeout() {
        try {
            Configuration.setTimeoutQuantos(Integer.parseInt(txtQuantosTimeout.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveScaleTimeout() {
        try {
            Configuration.setTimeoutScale(Integer.parseInt(txtScaleTimeout.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SaveAdruinoTimeout() {
        try {
            Configuration.setTimeoutAdruino(Integer.parseInt(txtAdruinoTimeout.getText()));
        } catch (NumberFormatException e) {
            DisplayException(e);
        }
    }

    private void SavePhotocatPort() {
        Configuration.setPhotoCatPort((String) cmbPhoto.getSelectedItem());
    }

    private void SaveSonicatorPort() {
        Configuration.setSonicatorPort((String) cmbSonicator.getSelectedItem());
    }

    private void SaveScalePort() {
        Configuration.setScalePort((String) cmbScale.getSelectedItem());
    }

    private void SaveScale2Port() {
        Configuration.setScale2Port((String) cmbScale2.getSelectedItem());
    }

    private void SavePumpPort() {
        Configuration.setLiquidPumpPort((String) cmbPump.getSelectedItem());
    }

    private void SavePump2Port() {
        Configuration.setLiquidPump2Port((String) cmbPump2.getSelectedItem());
    }

    private void SaveQuantosPort() {
        Configuration.setQuantosPort((String) cmbQuantos.getSelectedItem());
    }

    private void LoadConfiguration() {
        try {
            Configuration.loadSettings();
        } catch (Exception e) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e);
        }


        LoadDataInForm();
    }

    private void LoadDataInForm() {
        cmbQuantos.getModel().setSelectedItem(Configuration.getVibratoryPhotoCatPort());
        cmbQuantos.getModel().setSelectedItem(Configuration.getQuantosPort());
        cmbPump.getModel().setSelectedItem(Configuration.getLiquidPumpPort());
        cmbScale.getModel().setSelectedItem(Configuration.getScalePort());
        cmbSonicator.getModel().setSelectedItem(Configuration.getSonicatorPort());
        cmbPhoto.getModel().setSelectedItem(Configuration.getPhotoCatPort());
        txtQuantosTimeout.setText(Integer.toString(Configuration.getTimeoutQuantos()));
        txtScaleTimeout.setText(Long.toString(Configuration.getTimeoutScale()));
        txtAdruinoTimeout.setText(Integer.toString(Configuration.getTimeoutAdruino()));
        txtKMRPort.setText(Integer.toString(Configuration.getSideKMRPort()));
        txtLBRPort.setText(Integer.toString(Configuration.getSideLBRPort()));
        cmbVibratory.getModel().setSelectedItem(Configuration.getVibratoryPhotoCatPort());
        cmbProbe.getModel().setSelectedItem(Configuration.getProbePort());
    }

    private void SaveConfiguration() {
        try {
            Configuration.saveUserSettings();
        } catch (Exception e)
        {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e);
        }
    }

    private void ConnectAllModules() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    TryConnectVibratoryPhotolysis();
                    Thread.sleep(250);
                    TryConnectQuantos();
                    Thread.sleep(250);
                    TryConnectPump();
                    Thread.sleep(250);
                    TryConnectScale();
                    Thread.sleep(250);
                    TryConnectSonicator();
                    Thread.sleep(250);
//                    TryConnectPhotocat();
//                    Thread.sleep(250);
                    TryConnectCapper();
                    Thread.sleep(250);
                    TryConnectKMR();
                    Thread.sleep(250);
                    TryConnectScale2();
                    Thread.sleep(250);
                    TryConnectPump2();
                    Thread.sleep(250);
                    TryConnectGC();
                    Thread.sleep(250);
                    TryConnectProbe();
                } catch (Exception e) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e);
                }
            }
        })).start();
    }

    private void TryConnectVibratoryPhotolysis() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectVibratory();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private void TryConnectGC() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectGC();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private void TryConnectQuantos() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectQuantos();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private void TryConnectPump() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectLiquid();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private void TryConnectPump2() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectLiquid2();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private void TryConnectScale() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectScale();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private void TryConnectScale2() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectScale2();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }


    private void TryConnectSonicator() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectSonicator();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }


    private void DisplayException(Exception e1) {
        messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
    }

    private void TryConnectPhotocat() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectPhoto();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private void TryConnectKMR() {
        (new Thread(new Runnable() {
            public void run() {
                try {
                    workflowSystem.ConnectKMR();
                } catch (Exception e1) {
                    DisplayException(e1);
                }
            }
        })).start();
    }

    private JButton QuantosReconnectButton;
    private JButton PumpReconnectButton;
    private JButton ScaleReconnectButton;
    private JButton SonicatorReconnectButton;
    private JButton PhotocatReconnectButton;
    private JButton ConnectAllButton;
    private JButton saveAsDefaultButton;
    private JButton loadDefaultButton;
    private JTextField txtQuantosTimeout;
    private JTextField txtScaleTimeout;
    private JTextField txtAdruinoTimeout;
    private JTextField txtKMRPort;
    private JButton KMRReconnectButton;
    private JTextField txtLBRPort;
    private JComboBox cmbQuantos;
    private JComboBox cmbPump;
    private JComboBox cmbScale;
    private JComboBox cmbSonicator;
    private JComboBox cmbPhoto;
    private JComboBox cmbCapper;
    private JButton CapperReconnectButton;
    private JTextField txtCapperTimeout;
    private JTextField txtKMRTimeout;
    private JTextField txtCapperFreq;
    private JTextField txtQuantosFreq;
    private JTextField txtScaleFreq;
    private JTextField txtArduinoFreq;
    private JTextField txtKmrFreq;
    private JComboBox cmbPump2;
    private JComboBox cmbScale2;
    private JButton Pump2ReconnectButton;
    private JButton Scale2ReconnectButton;
    private JTextField txtGCIP;
    private JButton btnConnectGC;
    private JTextField txtGCPort;
    private JButton VibratoryReconnectButton;
    private JComboBox cmbVibratory;
    private JComboBox cmbProbe;
    private JButton ProbeButton;
    private JButton createOverviewFromCompletedButton;


}
