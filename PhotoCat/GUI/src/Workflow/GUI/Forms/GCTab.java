package Workflow.GUI.Forms;

import Workflow.Batch.Batch;
import Workflow.Com.TCP.GCCom;
import Workflow.Configuration.ComStation;
import Workflow.Exception.DeviceOfflineException;
import Workflow.IMessageHandler;
import Workflow.Scripts.RobustnessTesting;
import Workflow.TCP.TCPStationClientSuper;
import Workflow.WorkflowSystem;
import workflow.GC.GCStationStatus;
import workflow.GC.GCTCPClient;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Benjamin on 12/11/2018.
 */
public class GCTab extends SuperTab {
    private ScheduledExecutorService executer_service;
    private JPanel mainPanel;
    private JPanel status_Panel;
    private JLabel lblGCActive;
    private JButton startMeasuringButton;
    private JButton getResultsButton;
    private JComboBox cmbBatch1;
    private JComboBox cmbBatch3;
    private JComboBox cmbBatch2;
    private JLabel lblRunning;
    private JLabel lblCompleted;
    private JButton runGCNdriteRobustnessButton;
    private JTextField txtGcRobustness;
    RobotStatusTab robot_status_tab;
    private String[] NAMES;
    public GCTab()
    {
        executer_service = Executors.newSingleThreadScheduledExecutor();
        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    UpdateStatus();
                } catch (CloneNotSupportedException e) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent,e);
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
        startMeasuringButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                (new Thread(new Runnable(){
                    public void run(){
                        try {
                            Batch[] batch_array = getBatchesInComboBoxs();

                            workflowSystem.getGCCom().StartBatch(batch_array);
                        } catch (Exception e1) {
                            messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);

                        }
                    }
                })).start();
            }
        });
        getResultsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                (new Thread(new Runnable(){
                    public void run(){
                        try {
                            Batch[] batch_array = getBatchesInComboBoxs();
                            workflowSystem.getGCCom().GetResults(batch_array);
                        } catch (Exception e1) {
                            messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);

                        }
                    }
                })).start();
            }
        });
        runGCNdriteRobustnessButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            RobustnessTesting.GCnDriteRobustness(workflowSystem, Integer.parseInt(txtGcRobustness.getText()));
                        } catch (Exception e) {
                            messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e);

                        }
                    }
                };
                t.start();
            }
        });
    }

    private Batch[] getBatchesInComboBoxs() {
        LinkedList<String> batch_names = new LinkedList<String>();
        LinkedList<Batch> result = new LinkedList<Batch>();
            batch_names.add((String) cmbBatch1.getSelectedItem());

            batch_names.add((String) cmbBatch2.getSelectedItem());

            batch_names.add((String) cmbBatch3.getSelectedItem());

        List<Batch> runs_in_system = workflowSystem.getState().getRunsInTheSystem();
        for (String batch_name: batch_names) {
            Optional<Batch> batch = runs_in_system.stream().filter(x -> x.getBatch_name().equals(batch_name)).findAny();
            if(batch.isPresent())
            {
                result.add(batch.get());
            }
        }
        return result.toArray(new Batch[0]);
    }



    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    private void createUIComponents() {
        robot_status_tab = new RobotStatusTab();
        status_Panel = robot_status_tab.getMainPanel();
    }

    private void UpdateStatus() throws CloneNotSupportedException {


        GCCom gcCom = null;
        GCTCPClient client = null;
        if (workflowSystem != null) {

            List<Batch> runs_in_system = workflowSystem.getState().getRunsInTheSystem();
            String[] names = UpdateBatchNameCombobox(runs_in_system);
            try {
                gcCom = (GCCom) workflowSystem.getComNoCheck(ComStation.GC);

                if (gcCom != null) {
                    client = gcCom.getClient();

                    robot_status_tab.UpdateStatus(true, client);
                } else {
                    robot_status_tab.UpdateStatus(false, (TCPStationClientSuper) null);
                }
            } catch (DeviceOfflineException e) {
                robot_status_tab.UpdateStatus(false, (TCPStationClientSuper) null);
            }
        } else {
            robot_status_tab.UpdateStatus(false, (TCPStationClientSuper) null);
        }

        if (client != null && client.isConnected()) {


            GCStationStatus status = client.GetStatus();
            if(status.getStation_running())
                lblGCActive.setText("Running");
            else
                lblGCActive.setText("Not running");
            try {
                String[] running = (String[]) status.getBatches_running().stream().map(x -> x.getBatch_name()).toArray(String[]::new);
                String[] completed = (String[]) status.getBatches_completed().stream().map(x -> x.getBatch_name()).toArray(String[]::new);

                lblRunning.setText(String.join(",", running));
                lblCompleted.setText(String.join(",", completed));
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        } else
        {
            lblGCActive.setText("Offline");
            lblRunning.setText("");
            lblCompleted.setText("");
        }
    }

    private String[] UpdateBatchNameCombobox(List<Batch> runs_in_system) {
        String[] names = runs_in_system.stream().map(Batch::getBatch_name).collect(Collectors.toList()).toArray(new String[0]);
        String[] copyFrom  = names;
        String[] copyTo    = new String[names.length + 1];
        copyTo[0] = "";
        System.arraycopy(copyFrom, 0, copyTo, 1, copyFrom.length);
        names = copyTo;


        if(NAMES != null)
        {
            boolean allSame=true;
            if(NAMES.length == names.length)
            {

                for(int i=0;i<NAMES.length&&allSame;i++)
                {
                    allSame = Arrays.stream(names).filter(x -> x.equals(NAMES[0])).findFirst().isPresent();
                }

            } else {
                allSame =false;
            }

            if(!allSame)
                setComboBoxModels( names);

        } else {
            setComboBoxModels(names);
        }

        NAMES = names;
        return names;
    }

    @Override
    public void setWorkflow(WorkflowSystem workflowSystem)
    {
        super.setWorkflow(workflowSystem);

        UpdateBatchNameCombobox(workflowSystem.getState().getRunsInTheSystem());
    }

    private void setComboBoxModels(String[] names)
    {
        String selectedItem_1 = (String) cmbBatch1.getSelectedItem();
        cmbBatch1.setModel(new DefaultComboBoxModel(names));
        if(Arrays.stream(names).anyMatch(x -> x.equals(selectedItem_1)))
            cmbBatch1.setSelectedItem(selectedItem_1);

        String selectedItem_2 = (String) cmbBatch2.getSelectedItem();
        cmbBatch2.setModel(new DefaultComboBoxModel(names));
        if(Arrays.stream(names).anyMatch(x -> x.equals(selectedItem_2)))
            cmbBatch2.setSelectedItem(selectedItem_2);

        String selectedItem_3 = (String) cmbBatch3.getSelectedItem();
        cmbBatch3.setModel(new DefaultComboBoxModel(names));
        if(Arrays.stream(names).anyMatch(x -> x.equals(selectedItem_3)))
            cmbBatch3.setSelectedItem(selectedItem_3);
    }
}
