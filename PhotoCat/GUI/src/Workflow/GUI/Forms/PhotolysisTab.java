package Workflow.GUI.Forms;

import Workflow.IMessageHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benjamin on 3/9/2018.
 */
public class PhotolysisTab extends SuperTab {
    private JPanel mainPanel;
    private JButton startButton;
    private JButton stopButton;
    private JButton startVibratoryButton;
    private JButton stopVibratoryButton;
    private JButton startDryButton;
    private JButton stopDryButton;

    public PhotolysisTab() {
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StartMagnetic();
            }
        });
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StopMagnetic();
            }
        });
        startVibratoryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StartVibratory();
            }
        });
        stopVibratoryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StopVibratory();
            }
        });
        startDryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StartVibratoryDry();
            }
        });
        stopDryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StopVibratoryDry();
            }
        });
    }


    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }


    public void StartMagnetic() {
        try {
            workflowSystem.getPhotoCom().Start();
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }


    public void StopMagnetic() {
        try {
            workflowSystem.getPhotoCom().Stop();
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    public void StartVibratory() {
        try {
            workflowSystem.getVibratoryCom().Start();

        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }


    public void StopVibratory() {
        try {
            workflowSystem.getVibratoryCom().Stop();
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    public void StartVibratoryDry() {
        try {
            workflowSystem.getVibratoryCom().StartDry();
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }


    public void StopVibratoryDry() {
        try {
            workflowSystem.getVibratoryCom().StopDry();
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }


}
