package Workflow.GUI.Forms;

import Workflow.Batch.Liquid;
import Workflow.Exception.DeviceOfflineException;
import Workflow.Exception.InconsistentStateException;
import Workflow.IMessageHandler;
import Workflow.LiquidDispensingProcessManager;
import Workflow.WorkflowSystem;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Benjamin on 3/9/2018.
 */
public class LiguidEnsembleTab extends SuperTab {
    private JButton startPumpButton;
    private JButton stopPumpButton;
    private JButton setRateButton;
    ScheduledExecutorService executer_service;
    ScheduledExecutorService executer_service2;
    ScheduledExecutorService executer_service_tables;

    public LiguidEnsembleTab() {


        startPumpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StartPump();
            }
        });
        stopPumpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StopPump();
            }
        });

        setRateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SetPumpFlowRate();
            }
        });

        dispensePIDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());
                    LiquidDispensingProcessManager pm = new LiquidDispensingProcessManager(workflowSystem, liq.getStationID());
                    pm.DispenseLiquidPController(liq.getPumpID(), Float.parseFloat(txtVolumePIDDispense.getText()) * liq.Density);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }
            }
        });
        dispenseTimeBasedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ;
                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());
                    LiquidDispensingProcessManager pm = new LiquidDispensingProcessManager(workflowSystem, liq.getStationID());
                    pm.DispenseLiquidTime(liq.getPumpID(), Integer.parseInt(txtTimeBasedDispenseTime.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }
            }
        });
        dispenseTimeBasedAmountButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ;
                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());
                    LiquidDispensingProcessManager pm = new LiquidDispensingProcessManager(workflowSystem, liq.getStationID());
                    pm.DispenseLiquidNoScale(((Liquid) comboBoxLiquids.getSelectedItem()), Float.parseFloat(txtTimeBasedVolume.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }

            }
        });
        gatherDataForFitLineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());
                    LiquidDispensingProcessManager pm = new LiquidDispensingProcessManager(workflowSystem, liq.getStationID());

                    String[] ams = txtAmounts.getText().split(",");
                    Integer[] ss = (Arrays.asList(ams).stream().map(car -> Integer.parseInt(car)).collect(Collectors.toList())).toArray(new Integer[ams.length]);
                    Integer samples = Integer.parseInt(txtSamples.getText());
                    String location = txtLocation.getText();
                    pm.LiquidHandlingSystemFitLineData(liq, location, ss);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        fitLineWithLeastButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());
                    LiquidDispensingProcessManager pm = new LiquidDispensingProcessManager(workflowSystem, liq.getStationID());

                    String[] ams = txtAmounts.getText().split(",");
                    Integer[] ss = (Arrays.asList(ams).stream().map(car -> Integer.parseInt(car)).collect(Collectors.toList())).toArray(new Integer[ams.length]);
                    Integer samples = Integer.parseInt(txtSamples.getText());
                    String location = txtLocation.getText();
                    pm.LiquidHandlingSystemLeastSquares(liq);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        PIDControlTestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());
                    LiquidDispensingProcessManager pm = new LiquidDispensingProcessManager(workflowSystem, liq.getStationID());


                    String[] ams = txtAmounts.getText().split(",");
                    Integer[] ss = (Arrays.asList(ams).stream().map(car -> Integer.parseInt(car)).collect(Collectors.toList())).toArray(new Integer[ams.length]);
                    Integer samples = Integer.parseInt(txtSamples.getText());
                    String location = txtLocation.getText();
                    pm.LiquidHandlingSystemAutomatedTest(liq, samples, location, new float[]{5f});
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        timeBasedDispensingTestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());
                    LiquidDispensingProcessManager pm = new LiquidDispensingProcessManager(workflowSystem, liq.getStationID());


                    String[] ams = txtAmounts.getText().split(",");
                    Integer[] ss = (Arrays.asList(ams).stream().map(car -> Integer.parseInt(car)).collect(Collectors.toList())).toArray(new Integer[ams.length]);
                    Integer samples = Integer.parseInt(txtSamples.getText());
                    String location = txtLocation.getText();
                    pm.LiquidHandlingSystemNoScaleTest(liq, samples, location);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        forwardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());

                    workflowSystem.getLiquidCom(liq.getStationID()).Forward();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        dispenseForLampTestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    LiquidDispensingProcessManager pm = new LiquidDispensingProcessManager(workflowSystem, 0);
                    float volume_TEA = 2.182694954f;
                    float dispensed_tea = pm.DispenseLiquidPController(2, volume_TEA);
                    messageHandler.WriteMessage("Dispensed TEA:" + dispensed_tea);
                    float volume_RU = 1.330067425f;
                    float dispensed_ru = pm.DispenseLiquidPController(3, volume_RU);
                    messageHandler.WriteMessage("Dispensed RuCl:" + dispensed_ru);
                    float dispensed_water = pm.DispenseLiquidPController(1, 5 - volume_TEA - volume_RU);
                    messageHandler.WriteMessage("Dispensed Water:" + dispensed_water);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        backwardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());

                    workflowSystem.getLiquidCom(liq.getStationID()).Reverse();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }
            }
        });
        addLiquidButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = txtaddLiquidName.getText();
                    int station = Integer.parseInt(txtaddLiquidStation.getText());
                    int pump = Integer.parseInt(txtaddLiquidPump.getText());
                    float density = Float.parseFloat(txtaddLiquidDensity.getText());
                    float stock = Float.parseFloat(txtaddLiquidStock.getText());
                    float min = Float.parseFloat(txtMin.getText());
                    float max = Float.parseFloat(txtMax.getText());
                    Date date = new Date();
                    date.setTime((Integer.parseInt(txtdaystillexpire.getText())*24*60*60*1000)+ date.getTime());
                    workflowSystem.getState().addLiquid(name, station, pump, density, stock,min,max, date);

                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }
            }
        });
        removeLiquidButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                DeleteLiquid();
                } catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        retstockButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    RestockLiquid();
                }catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        removeLiquidButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    RemoveLiquid();
                } catch (Exception e1)
                {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
    }

    private void RemoveLiquid() throws InconsistentStateException {
        cmbRemoveLiquid.getSelectedItem();
        Optional<Liquid> liquid = Arrays.stream(workflowSystem.getState().getLiquids()).filter(x -> x.getName().equals((String) cmbRemoveLiquid.getSelectedItem())).findFirst();
        if(liquid.isPresent())
            workflowSystem.getState().RemoveLiquid(liquid.get());
        else {
            throw new InconsistentStateException("Liquid is not present");
        }
    }

    private void RestockLiquid() throws InconsistentStateException {
        Optional<Liquid> liquid = Arrays.stream(workflowSystem.getState().getLiquids()).filter(x -> x.getName().equals((String) cmbRestockLiquid.getSelectedItem())).findFirst();


        if(liquid.isPresent()) {
            float new_stock = Float.parseFloat(txtRestock.getText());
            Integer days_till_expired = 365;
            try{
                days_till_expired = Integer.parseInt(txtDaysTilLExperiyRestock.getText());
            } catch (Exception e)
            {
                messageHandler.WriteMessage("Warning: Expiry date for restocking liquid not set");
                //not important, user can easily correct by changing number and redoing stock
            }
            workflowSystem.getState().RestockLiquid(liquid.get(), new_stock, days_till_expired);
        }
        else {
            throw new InconsistentStateException("Liquid is not present");
        }
    }

    private void DeleteLiquid() throws InconsistentStateException {
        cmbRemoveLiquid.getSelectedItem();
        Optional<Liquid> liquid = Arrays.stream(workflowSystem.getState().getLiquids()).filter(x -> x.getName().equals((String) cmbRemoveLiquid.getSelectedItem())).findFirst();
        if(liquid.isPresent())
            workflowSystem.getState().DeleteLiquid(liquid.get());
        else {
            throw new InconsistentStateException("Liquid is not present");
        }
    }

    private void TryUpdateWeight() {
        try {
            if (workflowSystem != null && workflowSystem.getScaleCom(0) != null)
                lbl_Weight0.setText(Float.toString(workflowSystem.getScaleCom(0).getWeight()));
            else
                lbl_Weight0.setText("Disconnected");
        } catch(DeviceOfflineException e)
        {
            lbl_Weight0.setText("Disconnected");
        }
        catch (Exception e) {
            //DO not record this -> will happen continuously
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e);
        }

        try {
            if (workflowSystem != null && workflowSystem.getScaleCom(1) != null)
                lbl_Weight1.setText(Float.toString(workflowSystem.getScaleCom(1).getWeight()));
            else
                lbl_Weight1.setText("Disconnected");
        } catch(DeviceOfflineException e)
        {
            lbl_Weight1.setText("Disconnected");
        }
        catch (Exception e) {
            //DO not record this -> will happen continuously
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e);
        }
    }

    private void SetPumpFlowRate() {
        try {
            int flowrate = Integer.parseInt(flowRateTextField.getText());
            Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());

            workflowSystem.getLiquidCom(liq.getStationID()).SetRate(flowrate);
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }


    private void StopPump() {
        try {
            Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());

            workflowSystem.getLiquidCom(liq.getStationID()).Stop();
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    private void StartPump() {
        try {
            Liquid liq = ((Liquid) comboBoxLiquids.getSelectedItem());
            workflowSystem.getLiquidCom(liq.getStationID()).Start(liq.getPumpID());
        } catch (Exception e1) {
            messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
        }
    }

    private JPanel mainPanel;
    private JTextField flowRateTextField;
    private JLabel lbl_Weight0;
    private JButton dispensePIDButton;
    private JComboBox comboBoxLiquids;
    private JButton PIDControlTestButton;
    private JButton gatherDataForFitLineButton;
    private JButton fitLineWithLeastButton;
    private JButton timeBasedDispensingTestButton;
    private JTextField txtVolumePIDDispense;
    private JTextField txtAmounts;
    private JTextField txtLocation;
    private JTextField txtSamples;
    private JButton dispenseTimeBasedButton;
    private JTextField txtTimeBasedDispenseTime;
    private JButton dispenseTimeBasedAmountButton;
    private JTextField txtTimeBasedVolume;
    private JButton forwardButton;
    private JButton dispenseForLampTestButton;
    private JButton backwardButton;
    private JTable liquidTable;
    private JButton removeLiquidButton;
    private JComboBox cmbRemoveLiquid;
    private JButton addLiquidButton;
    private JTextField txtaddLiquidStation;
    private JTextField txtaddLiquidPump;
    private JTextField txtaddLiquidDensity;
    private JTextField txtaddLiquidName;
    private JTextField txtaddLiquidStock;
    private JLabel lbl_Weight1;
    private JButton retstockButton;
    private JTextField txtRestock;
    private JComboBox cmbRestockLiquid;
    private JTextField txtMin;
    private JTextField txtMax;
    private JTextField txtdaystillexpire;
    private JTextField txtDaysTilLExperiyRestock;
    private JButton removeLiquidButton1;
    private JTextArea removeLiquidIsToTextArea;

    private String[] UpdateBatchNameCombobox(JComboBox comboBox) {


        String[] names = Arrays.stream(workflowSystem.getState().getLiquids()).map(Liquid::getName).collect(Collectors.toList()).toArray(new String[0]);
        String[] NAMES = new String[comboBox.getItemCount()];
        for (int i = 0; i < NAMES.length; i++) {
            NAMES[i] = (String) comboBox.getItemAt(i);
        }

        if (NAMES != null) {
            boolean allSame = true;
            if (NAMES.length == names.length) {

                for (int i = 0; i < NAMES.length && allSame; i++) {
                    final int j = i;
                    allSame = Arrays.stream(names).filter(x -> x.equals(NAMES[j])).findFirst().isPresent();
                }

            } else {
                allSame = false;
            }

            if (!allSame)
                comboBox.setModel(new DefaultComboBoxModel(names));

        } else {
            comboBox.setModel(new DefaultComboBoxModel(names));
        }

        return names;
    }

    private void updateLiquidsTable() {
        LiquidItemModel model = (LiquidItemModel) liquidTable.getModel();
        model.Refresh(workflowSystem);
    }

    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    @Override
    public void setWorkflow(WorkflowSystem workflow) {
        super.setWorkflow(workflow);
        comboBoxLiquids.removeAllItems();
        for (Liquid liq : workflowSystem.getState().getLiquids()) {
            comboBoxLiquids.addItem(liq);
        }

        Liquid[] liquids = workflowSystem.getState().getLiquids();
        LiquidItemModel liquidModel = new LiquidItemModel(liquids);
        liquidTable.setModel(liquidModel);


        executer_service_tables = Executors.newSingleThreadScheduledExecutor();
        executer_service_tables.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    updateLiquidsTable();
                } catch (Exception e) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e);
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);


        executer_service2 = Executors.newSingleThreadScheduledExecutor();
        executer_service2.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {

                    String[] liquid_names = UpdateBatchNameCombobox(cmbRemoveLiquid);
                    String current_remove = (String) cmbRemoveLiquid.getSelectedItem();
                    if (current_remove != null && current_remove != "" && Arrays.stream(liquid_names).filter(x -> x.equals(current_remove)).findFirst().isPresent()) {

                    } else if (liquid_names.length > 0) {
                        cmbRemoveLiquid.setSelectedItem(liquid_names[0]);
                    }

                    liquid_names = UpdateBatchNameCombobox(cmbRestockLiquid);
                    String current_restock = (String) cmbRemoveLiquid.getSelectedItem();
                    if (current_restock != null && current_restock != "" && Arrays.stream(liquid_names).filter(x -> x.equals(current_restock)).findFirst().isPresent()) {

                    } else if (liquid_names.length > 0) {
                        cmbRemoveLiquid.setSelectedItem(liquid_names[0]);
                    }


                } catch (Exception e) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e);
                }
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);
        executer_service = Executors.newSingleThreadScheduledExecutor();

        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                TryUpdateWeight();
            }
        }, 0, 100, TimeUnit.MILLISECONDS);

    }

    private void createUIComponents() {

    }

}
