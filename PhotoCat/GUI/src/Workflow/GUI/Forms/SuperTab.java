package Workflow.GUI.Forms;

import Workflow.WorkflowSystem;

import javax.swing.*;

/**
 * Created by Benjamin on 3/9/2018.
 */
public abstract class SuperTab {
    protected MessageHandler messageHandler;

    public abstract JPanel getMainPanel();

    protected WorkflowSystem workflowSystem;

    public void setWorkflow(WorkflowSystem workflow) {
        this.workflowSystem = workflow;
    }

    public void setMessageHandler(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public void RefreshUIControls() {
    }
}
