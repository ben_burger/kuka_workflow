package Workflow.GUI.Forms;

import Workflow.Batch.*;
import Workflow.Com.TCP.LBRCom;
import Workflow.Exception.*;
import Workflow.IMessageHandler;
import Workflow.LBR.LBRRobotStatus;

import Workflow.LBR.LBRTCPServer;
import Workflow.LBR.SerializableFrame;
import Workflow.ProcessManagement.ProcessTypeEnum;

//import Workflow.TCP.TCPServerSupe/**/r2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.Date;
/**
 * Created by Benjamin on 3/18/2018.
 */
public class LBRTab extends SuperTab{

    ScheduledExecutorService executer_service;

    RobotStatusTab robot_status_tab;
    private JPanel mainPanel;
    private JButton calibratePosAndTorqueButton;
    private JButton finishButton;
    private JPanel status_Panel;
    private JButton drivePosButton;
    private JLabel lblStation;
    private JLabel lblCube;
    private JTextField txt6PRepetitions;
    private JButton sixPointCalibrationButton;
    private JButton PlaceRackInBufferButton;
    private JButton GraspRackFromBuffer;
    private JButton startCartridgeManipulationButton;
    private JButton removeCartridgeHotelButton;
    private JButton placeCartridgeHotelButton;
    private JButton finishCartridgeManipulationButton;
    private JTextField txtRemoveCartridgeHotel;
    private JTextField txtPlaceCartridgeHotel;
    private JLabel quantosSampleStationLabel;
    private JButton stopVialHandlingButton;
    private JButton startVialHandlingButton;
    private JButton graspVialFromBufferButton;
    private JButton placeVialInBufferButton;
    private JTextField graspVialFromBufferText;
    private JTextField placeVialInBufferText;
    private JButton removeCartridgeQuantosButton;
    private JButton placeCartridgeQuantosButton;
    private JButton placeVialInQuantosButton;
    private JButton graspVialFromQuantosButton;
    private JButton placeVialInCapperButton;
    private JButton graspVialFromCapperButton;
    private JButton placeVialInSonicatorButton;
    private JButton graspVialFromSonicatorButton;
    private JTextField txtSonicatorIndex;
    private JTextField robot_rack_index;
    private JTextField station_rack_index;
    private JButton pressParkHeadspaceButtonButton;
    private JButton placeVialInLiquidStationButton;
    private JButton graspVialInLiquidStationButton;
    private JTextField liquidstationlabel;
    private JButton moveVialFromStation1To2Button;
    private JButton GraspSign;
    private JButton allowChargeButton;
    private JButton moveToNeutralAfterDryButton;
    private JButton moveToDryPositionButton;
    private JButton dryProcedureButton;
    private JButton graspFromAluminumRackButton;
    private JTextField txtGraspPhotolysis;
    private JButton placeeInAluminiumRackButton;
    private JTextField txtPlacePhotolysis;
    private JLabel lblPosition;
    private JLabel lblSafety;
    private JButton staggerBatchBatchIndexRobotButton;
    private JButton kineticsExperimentButton;

    public LBRTab()
    {
        executer_service = Executors.newSingleThreadScheduledExecutor();
//        if(robot_status_tab == null)
//

        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    UpdateStatus();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent,e1);
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
        calibratePosAndTorqueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().TorqueAndPositionReferencing();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });

        drivePosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().ArmToDrivePos();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        finishButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().FinishArmManipulations();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        sixPointCalibrationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().SixPointCalibration();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        PlaceRackInBufferButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceRackInBuffer(Integer.parseInt(robot_rack_index.getText()), Integer.parseInt(station_rack_index.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        GraspRackFromBuffer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().GraspRackFromBuffer(Integer.parseInt(robot_rack_index.getText()), Integer.parseInt(station_rack_index.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        startCartridgeManipulationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().StartCartridgeManipulations();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });





        finishCartridgeManipulationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().FinishCartridgeManipulations();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });








        removeCartridgeHotelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().CartridgeRemoveFromHotel(Integer.parseInt(txtRemoveCartridgeHotel.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });







        placeCartridgeHotelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceCartridgeManipulations(Integer.parseInt(txtPlaceCartridgeHotel.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        startVialHandlingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().StartVialHandling();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        stopVialHandlingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().StopVialHandling();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        graspVialFromBufferButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().GraspVialFromBuffer(Integer.parseInt(graspVialFromBufferText.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        placeVialInBufferButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceVialInBuffer(Integer.parseInt(placeVialInBufferText.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        removeCartridgeQuantosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().RemoveCartridgeFromQuantos();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        placeCartridgeQuantosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceCartridgeFromQuantos();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });

        placeVialInQuantosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceSampleInQuantos();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        graspVialFromQuantosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().GraspSampleFromQuantos();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        placeVialInCapperButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceVialInCapper();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        graspVialFromCapperButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().GraspVialFromCapper();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        placeVialInSonicatorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceVialInSonicator(Integer.parseInt(txtSonicatorIndex.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        graspVialFromSonicatorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().GraspVialFromSonicator(Integer.parseInt(txtSonicatorIndex.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        pressParkHeadspaceButtonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PressParkHeadspaceButton();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        placeVialInLiquidStationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceVialInLiquidStation(Integer.parseInt(liquidstationlabel.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        graspVialInLiquidStationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().GraspVialFromLiquidStation(Integer.parseInt(liquidstationlabel.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        moveVialFromStation1To2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().MoveVialFromStationOneToStationTwo();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        GraspSign.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().GraspSign();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        allowChargeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().AllowChargingOfRobot();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        placeeInAluminiumRackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().PlaceSampleInPhotolysisStation(Integer.parseInt(txtPlacePhotolysis.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        graspFromAluminumRackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().GraspSampleFromPhotolysisStation(Integer.parseInt(txtGraspPhotolysis.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        moveToDryPositionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().MoveToDryPosition();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        dryProcedureButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().DryProcedure();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        moveToNeutralAfterDryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getLBRCom().MoveToNeutralAfterDry();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        staggerBatchBatchIndexRobotButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getProcessManager().EnableProcess(ProcessTypeEnum.Tantalus, false);
                    workflowSystem.getLBRCom().DisallowChargingOfRobot();
                    for(int i = 0;i<3;i++)
                        StaggerExperiment(i);
                    workflowSystem.getLBRCom().AllowChargingOfRobot();
                    for(int i = 0;i<3;i++) {
                        Batch batch  = workflowSystem.getState().getRackAtLocation(BatchLocation.valueOf("Robot_Transport_" + (i+1)));
                        batch.setStatus(BatchStatus.LoadGC);
                    }
                    workflowSystem.getProcessManager().EnableProcess(ProcessTypeEnum.Tantalus, true);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        kineticsExperimentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getProcessManager().EnableProcess(ProcessTypeEnum.Tantalus, false);
                    workflowSystem.getLBRCom().DisallowChargingOfRobot();
                    for(int i = 0;i<2;i++)
                        KineticsExperiment(i,3);
                    workflowSystem.getLBRCom().AllowChargingOfRobot();
                    for(int i = 0;i<2;i++) {
                        Batch batch  = workflowSystem.getState().getRackAtLocation(BatchLocation.valueOf("Robot_Transport_" + (i+1)));
                        batch.setStatus(BatchStatus.LoadGC);
                    }
                    workflowSystem.getProcessManager().EnableProcess(ProcessTypeEnum.Tantalus, true);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
    }

    private void KineticsExperiment(int rackIndex, int minutes_in_between) throws IOException, DeviceOfflineException, CloneNotSupportedException, InterruptedException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        //assume we are calibrated
        Batch batch  = workflowSystem.getState().getRackAtLocation(BatchLocation.valueOf("Robot_Transport_" + (rackIndex +1)));
        if(batch == null)
        {
            messageHandler.WriteMessage("Rack index " + rackIndex + " on robot is empty");
            return;
        }

        if(workflowSystem.getState().getRackAtLocation(BatchLocation.Vibratory_Photocat_Manipulation) != null)
        {
            messageHandler.WriteMessage("there is a rack in vibratory station -> can not run stagger experiment");
            return;
        }

        batch.AppendComment("Running kinetics experiment " + minutes_in_between);

        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.start, new Date());

        workflowSystem.getLBRCom().PlaceRackInBufferAndWait(rackIndex, 0);

        workflowSystem.getLBRCom().StartVialHandlingAndWait();
        for(int i=2;i<=16;i++)
        {
                int vial_index = i-1;
                if (vial_index >= 8) {
                    vial_index++;
                    vial_index++;
                }
                workflowSystem.getLBRCom().GraspVialFromBufferAndWait(vial_index);
                workflowSystem.getLBRCom().PlaceSampleInPhotolysisStationAndWait(i-1);
        }
        workflowSystem.getVibratoryCom().Start();
        Date start = new Date();
        for(int i=2;i<=16;i++)
        {
            long sleep = (start.getTime() + (i-1) * minutes_in_between * /*1 min*/60 * 1000 ) - new Date().getTime();
            if(sleep > 0)
                Thread.sleep(sleep);

                int vial_index = i-1;
                if (vial_index >= 8) {
                    vial_index++;
                    vial_index++;
                }
                workflowSystem.getLBRCom().GraspSampleFromPhotolysisStationAndWait(i-1);
                workflowSystem.getLBRCom().PlaceVialInBufferAndWait(vial_index);


        }
        workflowSystem.getVibratoryCom().Stop();
        workflowSystem.getLBRCom().StopVialHandlingAndWait();
        workflowSystem.getLBRCom().GraspRackFromBufferAndWait(rackIndex, 0);

        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.finish, new Date());

    }

    private void StaggerExperiment(int rackIndex) throws DeviceOfflineException, CloneNotSupportedException, InterruptedException, RobotNoHeartBeatException, IOException, NotAllowedException, RobotBusyException, RobotErrorStateException {
        //assume we are calibrated
        Batch batch  = workflowSystem.getState().getRackAtLocation(BatchLocation.valueOf("Robot_Transport_" + (rackIndex +1)));
        if(batch == null)
        {
            messageHandler.WriteMessage("Rack index " + rackIndex + " on robot is empty");
            return;
        }

        if(workflowSystem.getState().getRackAtLocation(BatchLocation.Vibratory_Photocat_Manipulation) != null)
        {
            messageHandler.WriteMessage("there is a rack in vibratory station -> can not run stagger experiment");
            return;
        }

        batch.AppendComment("Running stagger experiment");

        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.start, new Date());

        workflowSystem.getLBRCom().PlaceRackInBufferAndWait(rackIndex, 0);

        workflowSystem.getLBRCom().StartVialHandlingAndWait();
        Date start = new Date();
        workflowSystem.getVibratoryCom().Start();
        for(int i=1;i<=22;i++)
        {

            if(i<=16)
            {
                int vial_index = i-1;
                if (vial_index >= 8) {
                    vial_index++;
                    vial_index++;
                }
                workflowSystem.getLBRCom().GraspVialFromBufferAndWait(vial_index);
                workflowSystem.getLBRCom().PlaceSampleInPhotolysisStationAndWait(i-1);
            } else {
                //estimated time of a placement
                Thread.sleep(13640);
            }

            if((i - 6) >= 1)
            {
                int rack_index = i-7;
                if (rack_index >= 8) {
                    rack_index++;
                    rack_index++;
                }
                workflowSystem.getLBRCom().GraspSampleFromPhotolysisStationAndWait(i-7);
                workflowSystem.getLBRCom().PlaceVialInBufferAndWait(rack_index);
            }


            long sleep = (start.getTime() + i * 5 * /*1 min*/60 * 1000 ) - new Date().getTime();
            if(sleep > 0)
                Thread.sleep(sleep);
        }
        workflowSystem.getVibratoryCom().Stop();
        workflowSystem.getLBRCom().StopVialHandlingAndWait();
        workflowSystem.getLBRCom().GraspRackFromBufferAndWait(rackIndex, 0);

        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.finish, new Date());

    }


    private void UpdateStatus() throws CloneNotSupportedException {
        LBRCom lbrCom = null;
        LBRTCPServer server = null;
        if (workflowSystem != null) {
            try {
                lbrCom = workflowSystem.getLBRComNoCheck();

                if (lbrCom != null) {
                    server = lbrCom.GetServer();

                    robot_status_tab.UpdateStatus(true, server);
                } else
                {
                    robot_status_tab.UpdateStatus(false, (LBRTCPServer) null);
                }
            } catch (DeviceOfflineException e)
            {
                robot_status_tab.UpdateStatus(false,(LBRTCPServer)  null);
            }
        } else {
            robot_status_tab.UpdateStatus(false, (LBRTCPServer) null);
        }
        if(server != null) {
            LBRRobotStatus status = server.GetStatus();
            SerializableFrame s = status.getCubeOrigin();

            if (status.getStation() != null)
                lblStation.setText(status.getStation().toString());
            else
                lblStation.setText("");
            if (s != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(String.format("%.0f", s.X));
                sb.append(",");
                sb.append(String.format("%.0f", s.Y));
                sb.append(",");
                sb.append(String.format("%.0f", s.Z));
                sb.append(",");
                sb.append(String.format("%.2f", s.A));
                sb.append(",");
                sb.append(String.format("%.2f", s.B));
                sb.append(",");
                sb.append(String.format("%.2f", s.C));
                lblCube.setText(sb.toString());
            } else {
                lblCube.setText("");

            }
            if(status.getCurrentCartesisnPosition() != null)
                lblPosition.setText(String.format("%.2f,%.2f,%.2f",status.getCurrentCartesisnPosition().getX(), status.getCurrentCartesisnPosition().getY(), status.getCurrentCartesisnPosition().getZ()));
            else
                lblPosition.setText("");

            lblSafety.setText(status.getSafetyStopType().name());


        } else {

            lblSafety.setText("");
            lblPosition.setText("");
            lblStation.setText("");
            lblCube.setText("");
        }
    }

    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    private void createUIComponents() {
        robot_status_tab = new RobotStatusTab();
        robot_status_tab = new RobotStatusTab();
        status_Panel = robot_status_tab.getMainPanel();
    }
}
