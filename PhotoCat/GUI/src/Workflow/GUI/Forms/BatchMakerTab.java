package Workflow.GUI.Forms;

import Workflow.Batch.*;
import Workflow.IMessageHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import javax.swing.filechooser.FileFilter;

public class BatchMakerTab extends SuperTab {
    private JPanel mainPanel;
    private JTextField txtExpName;
    private JTextField txtGCMethod;
    private JTextField txtSonicTime;
    private JTextField txtCapVials;
    private JTextField txtPhotoTime;
    private JTextField txtControlsPerBatch;
    private JTextField txtRepetitionConditional;
    private JTextField txtThresholdConditional;
    private JTextField txtLeakThreshConditional;
    private JTextField txtPurgeTime;
    private JButton openCSVFileButton;
    private JButton writeBatchesButton;
    private JTextField txtNumBatches;
    private JTextField txtNumExperiments;
    private JButton beginConditionalResampling;
    private JCheckBox hydrogenCheck;
    private JCheckBox leakCheck;
    File csvFile = null;
    Thread t;

    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    public BatchMakerTab() {

        txtExpName.setText("hypothesis");
        txtGCMethod.setText("constant-HS rc");
        txtSonicTime.setText("0");
        txtCapVials.setText("1");
        txtPhotoTime.setText("60");
        txtControlsPerBatch.setText("2");
        txtPurgeTime.setText("60");
        txtNumExperiments.setText("1");
        txtNumBatches.setText("0");
        txtRepetitionConditional.setText("4");
        txtThresholdConditional.setText("5.0");
        txtLeakThreshConditional.setText("5.0");
        hydrogenCheck.setSelected(true);

        openCSVFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    final JFileChooser fc = new JFileChooser(".");
                    fc.setFileFilter(new FileFilter() {

                        public String getDescription() {
                            return "CSV Files (*.csv)";
                        }

                        public boolean accept(File f) {
                            if (f.isDirectory()) {
                                return true;
                            } else {
                                String filename = f.getName().toLowerCase();
                                return filename.endsWith(".csv");
                            }
                        }
                    });
                    int returnVal = fc.showOpenDialog(mainPanel);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        csvFile = fc.getSelectedFile();
                    } else {
                        return;
                    }
                } catch (Exception ex) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, ex);
                }
            }
        });
        writeBatchesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String[] batchInfo = {txtExpName.getText(), txtControlsPerBatch.getText(),
                            txtCapVials.getText(), txtGCMethod.getText(), txtPhotoTime.getText(),
                            txtPurgeTime.getText(), txtSonicTime.getText(), txtNumExperiments.getText(),
                            txtNumBatches.getText()};

                    boolean controlBatchOnly = false;

                    if (csvFile != null) {
                        Batch.makeBatches(workflowSystem.getState().getSolids(), workflowSystem.getState().getLiquids(), csvFile, batchInfo, controlBatchOnly);
                    }
                } catch (Exception ex) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, ex);
                }
            }
        });
        beginConditionalResampling.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ( t != null ) {
                    if ( t.isAlive() ) {
                        t.stop();
                        beginConditionalResampling.setText("Start Conditional Resampling");
                        return;
                    }
                }
                t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            checkDirectoriesForBatches();

                            List<Compound> compoundsFound = new LinkedList<Compound>();
                            List<Solid> solids_used = new LinkedList();
                            List<Liquid> liquids_used = new LinkedList();
                            if (csvFile == null) {
                                throw new Exception("Please Load CSV File First!");
                            }
                            if (!hydrogenCheck.isSelected() && !leakCheck.isSelected() ) {
                                throw new Exception("No Hydrogen or Leak Detection Selected!");
                            }
                            FileReader csvFileReader = new FileReader(csvFile);
                            BufferedReader csvRead = new BufferedReader(csvFileReader);
                            Integer numControls = Integer.parseInt(txtControlsPerBatch.getText());
                            String startingLine;
                            String[] controls = new String[numControls];
                            if ( (startingLine = csvRead.readLine()) != null && !startingLine.equals("") ) {
                                String line;
                                String[] compounds = startingLine.split(",");
                                for (String compound : compounds) {
                                    String fix_compound_name = compound.replaceAll("\\s","").replaceAll("\uFEFF"/*white space character not in regex /s */,"");
                                    Optional<Solid> solid = Arrays.stream(workflowSystem.getState().getSolids()).filter(x -> x.getName().equals(fix_compound_name)).findFirst();
                                    Optional<Liquid> liquid = Arrays.stream(workflowSystem.getState().getLiquids()).filter(x -> x.getName().equals(fix_compound_name)).findFirst();
                                    if (solid.isPresent()) {
                                        solids_used.add(solid.get());
                                        compoundsFound.add(solid.get());
                                    } else if (liquid.isPresent()) {
                                        liquids_used.add(liquid.get());
                                        compoundsFound.add(liquid.get());
                                    } else {
                                        throw new Exception("Solid or Liquid is Not in Current State: " + compound);
                                    }
                                }

                                for (int i = 0; i < controls.length; i++) {
                                    if ((line = csvRead.readLine()) != null && !line.equals("")) {
                                        controls[i] = line;
                                    }
                                }
                            }
                            csvRead.close();
                            csvFileReader.close();

                            List<Sample> samplesToCheckUnique = new ArrayList<Sample>();
                            Float comparisonFloat;
                            for ( int j = 0; j < controls.length; j++ ) {
                                String[] compoundAmounts = controls[j].split(",");
                                Sample sample = new Sample();
                                List<CompoundDispense> controlSolidDispenses = new LinkedList<CompoundDispense>();
                                List<CompoundDispense> controlLiquidDispenses = new LinkedList<CompoundDispense>();
                                if (compoundsFound.size() != compoundAmounts.length) {
                                    throw new Exception("Number of Compound Amounts Do NOT Equal Number of Compounds in Header!");
                                }
                                for (int i = 0; i < compoundsFound.size(); i++) {
                                    comparisonFloat = Float.parseFloat(compoundAmounts[i]);
                                    if (compoundsFound.get(i).getType() == CompoundType.Liquid) {
                                        controlLiquidDispenses.add(new CompoundDispense(compoundsFound.get(i), comparisonFloat));
                                    } else {
                                        controlSolidDispenses.add(new CompoundDispense(compoundsFound.get(i), comparisonFloat));
                                    }
                                }
                                sample.setSolidDispenses(controlSolidDispenses);
                                sample.setLiquidDispenses(controlLiquidDispenses);
                                samplesToCheckUnique.add(sample);
                            }

                            List<Batch> runs_in_system = workflowSystem.getState().ReadRunsInSystem(false,
                                    true).stream().filter(x->x.getBatchLocation()
                                    .equals(BatchLocation.NoRackAssigned)).collect(Collectors.toList());
                            String[] names = runs_in_system.stream().map(Batch::getBatch_name)
                                    .filter(x->!x.contains("FreshRack"))
                                    .sorted().collect(Collectors.toList()).toArray(new String[0]);
                            boolean checkDuplicate;
                            Integer duplicateCount;
                            for ( String batch_name : names ) {
                                Batch batch = runs_in_system.stream().filter(x -> x.getBatch_name().equals(batch_name)).findFirst().get();
                                if (!batch_name.contains(txtExpName.getText())) {
                                    continue;
                                }
                                Sample[] samples = batch.getSamples();
                                for (Sample sample : samples) {
                                    if ( sample.getHydrogen_evolution_micromol() != null && sample.getOxygen_evolution_micromol() != null ) {
                                        if ( ( sample.getHydrogen_evolution_micromol() >= Double.parseDouble(txtThresholdConditional.getText()) &&
                                                hydrogenCheck.isSelected() ) || ( sample.getOxygen_evolution_micromol() >= Double.parseDouble(txtLeakThreshConditional.getText()) &&
                                                leakCheck.isSelected() ) ) {
                                            checkDuplicate = false;
                                            for (int i = 0; i < samplesToCheckUnique.size(); i++) {
                                                duplicateCount = 0;
                                                for ( int j = 0; j < compoundsFound.size(); j++ ) {
                                                    if (compoundsFound.get(j).getType() == CompoundType.Liquid) {
                                                        if (samplesToCheckUnique.get(i).getLiquidDispenseAmount((Liquid) compoundsFound.get(j))
                                                                .equals(sample.getLiquidDispenseAmount((Liquid) compoundsFound.get(j)))) {
                                                            duplicateCount++;
                                                        }
                                                    } else {
                                                        if (samplesToCheckUnique.get(i).getSolidDispenseAmount((Solid) compoundsFound.get(j))
                                                                .equals(sample.getSolidDispenseAmount((Solid) compoundsFound.get(j)))) {
                                                            duplicateCount++;
                                                        }
                                                    }
                                                }
                                                if (duplicateCount == compoundsFound.size()) {
                                                    checkDuplicate = true;
                                                }
                                            }
                                            if ( !checkDuplicate ) {
                                                samplesToCheckUnique.add(sample);
                                            }
                                        }
                                    }
                                }
                            }

                            DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                            String conditionedFilename = String.format("conditional\\%s-%s.csv",
                                    txtExpName.getText(),df2.format(new Date()));
                            File new_file = new File(conditionedFilename);
                            if (new_file.exists()) {
                                new_file.delete();
                            }
                            new_file.createNewFile();
                            PrintWriter writer = new PrintWriter(new_file);

                            writer.write(startingLine+"\n");
                            for (int i = 0; i < controls.length; i++) {
                                writer.write(controls[i]+"\n");
                            }
                            for ( int k = 0; k < samplesToCheckUnique.size(); k++ ) {
                                if ( k < controls.length ) {
                                    continue;
                                }
                                for ( int j = 0; j < Integer.parseInt(txtRepetitionConditional.getText()); j++ ) {
                                    for (int i = 0; i < compoundsFound.size(); i++) {
                                        if (compoundsFound.get(i).getType() == CompoundType.Liquid) {
                                            writer.write(samplesToCheckUnique.get(k).getLiquidDispenseAmount((Liquid) compoundsFound.get(i)).toString() + ",");
                                        } else {
                                            writer.write(samplesToCheckUnique.get(k).getSolidDispenseAmount((Solid) compoundsFound.get(i)).toString() + ",");
                                        }
                                    }
                                    writer.write("\n");
                                }
                            }
                            writer.flush();
                            writer.close();

                            String[] batchInfo = {txtExpName.getText(), txtControlsPerBatch.getText(),
                                txtCapVials.getText(), txtGCMethod.getText(), txtPhotoTime.getText(),
                                txtPurgeTime.getText(), txtSonicTime.getText(), txtNumExperiments.getText(),
                                txtNumBatches.getText()};

                            boolean controlBatchOnly = false;

                            if ( csvFile != null ) {
                                Batch.makeBatches(workflowSystem.getState().getSolids(), workflowSystem.getState().getLiquids(), new_file, batchInfo, controlBatchOnly);
                            }
                            beginConditionalResampling.setText("Start Conditional Resampling");
                        }
                        catch (Exception ex) {
                            messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, ex);
                            beginConditionalResampling.setText("Start Conditional Resampling");
                        }
                    }
                };

                t.start();
                beginConditionalResampling.setText("Stop Conditional Resampling");
            }
        });

        /*writeControlBatchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String[] batchInfo = {txtExpName.getText(), txtControlsPerBatch.getText(),
                            txtCapVials.getText(), txtGCMethod.getText(), txtPhotoTime.getText(),
                            txtPurgeTime.getText(), txtSonicTime.getText(), txtNumExperiments.getText(),
                            txtNumBatches.getText()};

                    boolean controlBatchOnly = true;

                    if ( csvFile != null ) {
                        Batch.makeBatches(workflowSystem.getState().getSolids(), workflowSystem.getState().getLiquids(), csvFile, batchInfo, controlBatchOnly);
                    }
                }
                catch (Exception ex) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,ex);
                }
            }
        });*/
    }

    private void checkDirectoriesForBatches() throws Exception {

        while (checkDirectory("running\\.", "-run") || checkDirectory("runqueue\\.", "-run") ||
                checkDirectory("running\\.", "-") || checkDirectory("runqueue\\.", "-")) {
            Thread.sleep(10000);
        }

    }

    private boolean checkDirectory(String dirLocation, String extension) throws Exception {
        boolean wait = false;
        String fileExtension = ".run";
        String newFileString = txtExpName.getText();
        String extensionBeforeNumber = "-";
        File directory = new File(dirLocation);
        FilenameFilter fileFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(fileExtension);
            }
        };
        if (directory.isDirectory()) {
            File[] files;
            if ((files = directory.listFiles(fileFilter)).length > 0) {
                for (File batchfile : files) {
                    String stripExtension = batchfile.getName().substring(0, batchfile.getName().length() - fileExtension.length());
                    if (stripExtension.contains(extension)) {
                        String stripExtensionAndPostFix = stripExtension.substring(0, stripExtension.lastIndexOf(extension));
                        if (stripExtensionAndPostFix.equals(newFileString)
                                || (newFileString.contains(extension) && stripExtensionAndPostFix
                                .equals(newFileString.substring(0, newFileString.lastIndexOf(extension))))
                                || newFileString.equals(stripExtensionAndPostFix
                                .substring(0,stripExtensionAndPostFix.lastIndexOf(extensionBeforeNumber) ) ) ) {
                                wait = true;
                        }
                    }
                }
            }
        }
        else {
            throw new Exception("Run queue directory not found!");
        }
        return wait;
    }

}
