package Workflow.GUI.Forms;

import Workflow.Com.TCP.KMRCom;
import Workflow.Configuration.Location;
import Workflow.Exception.DeviceOfflineException;
import Workflow.IMessageHandler;
import Workflow.KMR.KMRRobotStatus;
import Workflow.Scripts.RobustnessTesting;
import Workflow.TCP.TCPRobotServerSuper;
import Workflow.WorkflowSystem;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Benjamin on 3/9/2018.
 */
public class KMRTab extends SuperTab {
    private JButton quantosButton;
    private JButton GCButton;
    private JButton photocatButton;
    private JButton sonicatorButton;
    private JButton liquidStationButton;
    private JPanel mainPanel;
    private JButton startLBRButton;
    private JPanel status_Panel;
    private JLabel lbl_location;
    private JButton drivePosButton;
    private JButton cartridgeRobustnessButton;
    private JPanel lbrPanel;
    private JButton cartridgeButton;
    private JButton quantosRobustnessButton;
    private JButton photocatRobustnessButton;
    private JButton capperRobustnessButton;
    private JButton crossroadsButton;
    private JTextField iterationTextfield;
    private JButton sonicatorRobustnessButton;
    private JButton GCRobustnessMinButton;
    private JButton liquidStationRobustness20MinButton;
    private JButton exxonButton;
    private JButton quantosButton1;
    private JButton quantos2Button;
    private JButton cartridgeButton1;
    private JButton liquidButton;
    private JButton capperButton1;
    private JButton photoButton;
    private JButton GCButton1;
    private JButton dryingStationButton;
    private JButton chargingStationButton;
    private JButton inputStationButton;
    private JButton testCapper16VialsButton;
    private JButton vibratoryPhotolysisButton;
    private JLabel lblPose;
    private JLabel lblBatteryState;
    private JLabel lblCharging;
    private JLabel lblSafety;
    private JButton inputStation2Button;
    private JButton inputStation3Button;
    private JButton lazyStuffButton;
    ScheduledExecutorService executer_service;
    private JButton CubeRobustButton;

    RobotStatusTab robot_status_tab;
    LBRTab lbr_tab;

    public KMRTab() {
//        if(lbr_tab == null)
//            lbr_tab = new LBRTab();
//        if(robot_status_tab == null)
//            robot_status_tab = new RobotStatusTab();

        executer_service = Executors.newSingleThreadScheduledExecutor();
        quantosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Quantos);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        liquidStationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.LiquidHandlingSystem);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        sonicatorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Sonicator);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        photocatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Photocat);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        GCButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.GC);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });
        startLBRButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartLBRAndWait(workflowSystem.getLBRComNoCheck(), Location.GC);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });


        executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    UpdateStatus();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UpdateUIEvent, e1);
                }
            }
        }, 0, 100, TimeUnit.MILLISECONDS);

        drivePosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().ArmToDrivePos();
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction, e1);
                }
            }
        });


        cartridgeRobustnessButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            RobustnessTesting.CartridgeRobustness(workflowSystem, Integer.parseInt(iterationTextfield.getText()));
                        } catch (Exception e1) {
                            messageHandler.HandleException(IMessageHandler.ExceptionSource.RobustnessTest,e1);
                        }
                    }
                };
                t.start();
            }
        });
        cartridgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Cartridge);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });


        quantosRobustnessButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            RobustnessTesting.QuantosSampleRobustness(workflowSystem, Integer.parseInt(iterationTextfield.getText()));
                        } catch (Exception e1) {
                            messageHandler.HandleException(IMessageHandler.ExceptionSource.RobustnessTest,e1);
                        }
                    }
                };
                t.start();
            }
        });

        photocatRobustnessButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            RobustnessTesting.PhotocatRackRobustness(workflowSystem, Integer.parseInt(iterationTextfield.getText()));
                        } catch (Exception e1) {
                            messageHandler.HandleException(IMessageHandler.ExceptionSource.RobustnessTest,e1);
                        }
                    }
                };
                t.start();
            }
        });
        capperRobustnessButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            RobustnessTesting.CapperRobustness(workflowSystem, Integer.parseInt(iterationTextfield.getText()));
                        } catch (Exception e1) {
                            messageHandler.HandleException(IMessageHandler.ExceptionSource.RobustnessTest,e1);
                        }
                    }
                };
                t.start();
            }

        });
        crossroadsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Crossroads);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        sonicatorRobustnessButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    RobustnessTesting.SonicatorRobustness(workflowSystem, Integer.parseInt(iterationTextfield.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.RobustnessTest,e1);
                }
            }
        });
        GCRobustnessMinButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    RobustnessTesting.GCRobustness(workflowSystem, Integer.parseInt(iterationTextfield.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.RobustnessTest,e1);
                }
            }
        });
        CubeRobustButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                    RobustnessTesting.CubeRobustness(workflowSystem, Integer.parseInt(iterationTextfield.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.RobustnessTest,e1);
                }
                    }
                };
                t.start();
            }
        });
        liquidStationRobustness20MinButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    RobustnessTesting.LiquidStationRobustness(workflowSystem, Integer.parseInt(iterationTextfield.getText()));
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.RobustnessTest,e1);
                }
            }
        });

        dryingStationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.DryingStation);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        chargingStationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.ChargingStation);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        inputStationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.InputStation);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });

        vibratoryPhotolysisButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Vibratory_Photocat);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        inputStation2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.InputStation2);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        inputStation3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    workflowSystem.getKMRCom().StartMoveToLocation(Location.InputStation3);
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
        lazyStuffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    for(int i=0;i<=13;i++) {

                        workflowSystem.getLBRCom().GraspRackFromBufferAndWait(3,10);
                        workflowSystem.getLBRCom().PlaceRackInBufferAndWait(3, 10);
                    }
                } catch (Exception e1) {
                    messageHandler.HandleException(IMessageHandler.ExceptionSource.UIAction,e1);
                }
            }
        });
    }

    private void UpdateStatus() throws CloneNotSupportedException, DeviceOfflineException {
        KMRCom kmr_com = null;
        TCPRobotServerSuper server = null;
        if (workflowSystem != null) {
            try {
                kmr_com = workflowSystem.getKMRComNoCheck();
                if (kmr_com != null) {
                    server = kmr_com.GetServer();

                    robot_status_tab.UpdateStatus(true, server);

                } else {
                    robot_status_tab.UpdateStatus(false, (TCPRobotServerSuper) null);
                }
            } catch (DeviceOfflineException e) {
                robot_status_tab.UpdateStatus(false, (TCPRobotServerSuper) null);
            }

        } else {
            robot_status_tab.UpdateStatus(false, (TCPRobotServerSuper) null);
        }

        KMRRobotStatus stat = null;
        if (server != null)
            stat = (KMRRobotStatus) server.GetStatus();
        if (stat != null) {
            lbl_location.setText(stat.getLastCommandedLocation().toString());
            if(stat.getKMR_Position() != null)
                lblPose.setText(String.format("%.2f,%.2f,%.3f",stat.getKMR_Position().getX(),stat.getKMR_Position().getY(),stat.getKMR_Position().getTheta()));
            else
                lblPose.setText("");
            lblBatteryState.setText(String.format("%.2f",stat.getBatteryState()));
            if(stat.isCharging())
                lblCharging.setText("Charging");
            else
                lblCharging.setText("Not Charging");
            lblSafety.setText(stat.getSafetyState().name());
        }
        else {
            lbl_location.setText("Disconnected");
            lblPose.setText("");
            lblCharging.setText("");
            lblBatteryState.setText("");
            lblSafety.setText("");
        }


    }

    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }

    private void createUIComponents() {
//        if(lbr_tab == null)
        lbr_tab = new LBRTab();
//        if(robot_status_tab == null)
        robot_status_tab = new RobotStatusTab();
        status_Panel = robot_status_tab.getMainPanel();
        lbrPanel = lbr_tab.getMainPanel();
    }

    @Override
    public void setWorkflow(WorkflowSystem workflow) {
        super.setWorkflow(workflow);
        lbr_tab.setWorkflow(workflow);

    }

    @Override
    public void setMessageHandler(MessageHandler messageHandler) {
        super.setMessageHandler(messageHandler);
        lbr_tab.setMessageHandler(messageHandler);
    }


}

