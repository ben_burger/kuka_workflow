package Workflow.GUI.Forms;

import Workflow.GUI.Forms.SuperTab;

import javax.swing.*;

public class CSVTab extends SuperTab {
    private JPanel mainPanel;
    private JButton saveButton;
    private JButton openButton;
    private JTable table1;

    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }
}
