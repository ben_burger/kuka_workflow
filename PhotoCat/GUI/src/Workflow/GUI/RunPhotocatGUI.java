package Workflow.GUI;

import Workflow.AlertSystem.Rules.CapStock;
import Workflow.GUI.Forms.Dashboard;
import Workflow.Configuration.Configuration;
import Workflow.WorkflowSystem;


import java.io.FileNotFoundException;

import java.io.File;
import Workflow.Batch.Batch;

/**
 * Created by Benjamin on 3/8/2018.
 */
public class RunPhotocatGUI {
    public static void main(String[] args) throws Exception {

        boolean webcam = false;
        try {
            webcam = args[0].equals("1");
        } catch(Exception e)
        {

        }
        WorkflowSystem workflow = new WorkflowSystem(true,webcam);
        try {Configuration.loadSettings();} catch(FileNotFoundException e) {Configuration.saveUserSettings();}
        Dashboard dashboard = new Dashboard(workflow);
        dashboard.MakeItHappen();
    }
}
