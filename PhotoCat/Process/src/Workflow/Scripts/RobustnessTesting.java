package Workflow.Scripts;

import Workflow.Batch.Batch;
import Workflow.Com.TCP.GCCom;
import Workflow.Com.TCP.KMRCom;
import Workflow.Com.TCP.LBRCom;
import Workflow.Exception.*;
import Workflow.Configuration.Location;
import Workflow.LBR.LBRRobotStatus;
import Workflow.LBR.SerializableFrame;
import Workflow.TCP.RobotStatusEnum;
import Workflow.WorkflowSystem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RobustnessTesting {
    public static void CartridgeRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException {
//        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
//        //Write to CSV
//        try (BufferedWriter bw = new BufferedWriter(new FileWriter("QuantosCartridgeRobustness" + formatter.format(new Date()) + ".csv"))) {
//            StringBuilder sb = new StringBuilder();
//
//            sb.append("#measurement");
//            sb.append(',');
//            sb.append("location");
//            sb.append(',');
//            sb.append("x (mm)");
//            sb.append(',');
//            sb.append("y (mm)");
//            sb.append(',');
//            sb.append("z (mm)");
//            sb.append(',');
//            sb.append("a (rad)");
//            sb.append(',');
//            sb.append("b (rad)");
//            sb.append(',');
//            sb.append("c (rad)");
//            sb.append(',');
//            sb.append("timestamp (s)");
//            sb.append("\r\n");
//            bw.write(sb.toString());
//
//
//            for (int i = 0; i < repetitions; i++) {
//                if(i!= 0) {
//                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Quantos);
//                    workflowSystem.getKMRCom().WaitForTaskCompletion();
//                }
//                workflowSystem.getKMRCom().StartMoveToLocation(Location.Cartridge);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//                workflowSystem.getKMRCom().StartLBR(workflowSystem.getLBRComNoCheck(), Location.Cartridge);
//                Thread.sleep(1000);
//                {
//                    workflowSystem.getLBRCom().SixPointCalibration();
//                    {
//                        LBRRobotStatus status = workflowSystem.getLBRCom().WaitForTaskCompletion();
//                        SerializableFrame cube_coor = status.getCubeOrigin();
//                        sb = new StringBuilder();
//                        sb.append(Integer.toString(i));
//                        sb.append(',');
//                        sb.append("Cartridge");
//                        sb.append(',');
//                        sb.append(cube_coor.X);
//                        sb.append(',');
//                        sb.append(cube_coor.Y);
//                        sb.append(',');
//                        sb.append(cube_coor.Z);
//                        sb.append(',');
//                        sb.append(cube_coor.A);
//                        sb.append(',');
//                        sb.append(cube_coor.B);
//                        sb.append(',');
//                        sb.append(cube_coor.C);
//                        sb.append(',');
//                        sb.append(formatter.format(new Date()));
//                        sb.append("\r\n");
//                        bw.write(sb.toString());
//                        System.out.println(sb.toString());
//                        bw.flush();
//                    }
//
//                    workflowSystem.getLBRCom().StartCartridgeManipulations();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().CartridgeRemoveFromHotel(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().PlaceCartridgeFromQuantos();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//
//                    workflowSystem.getLBRCom().RemoveCartridgeFromQuantos();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    for (int cartridge_index = 1; cartridge_index <= 19; cartridge_index++) {
//
//
//                        workflowSystem.getLBRCom().PlaceCartridgeManipulations(cartridge_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().CartridgeRemoveFromHotel(cartridge_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().PlaceCartridgeFromQuantos();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().RemoveCartridgeFromQuantos();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//                    }
//
//                    workflowSystem.getLBRCom().PlaceCartridgeManipulations(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().FinishCartridgeManipulations();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().FinishArmManipulations();
//
//                }
//                Thread.sleep(2000);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//
//            }
//        } catch (RobotNoHeartBeatException e) {
//            e.printStackTrace();
//        }
    }

    public static void QuantosSampleRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException {
//        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
//        //Write to CSV
//        try (BufferedWriter bw = new BufferedWriter(new FileWriter("QuantosSampleRobustness" + formatter.format(new Date()) + ".csv"))) {
//            StringBuilder sb = new StringBuilder();
//
//            sb.append("#measurement");
//            sb.append(',');
//            sb.append("location");
//            sb.append(',');
//            sb.append("x (mm)");
//            sb.append(',');
//            sb.append("y (mm)");
//            sb.append(',');
//            sb.append("z (mm)");
//            sb.append(',');
//            sb.append("a (rad)");
//            sb.append(',');
//            sb.append("b (rad)");
//            sb.append(',');
//            sb.append("c (rad)");
//            sb.append(',');
//            sb.append("timestamp (s)");
//            sb.append("\r\n");
//            bw.write(sb.toString());
//            System.out.println(sb.toString());
//            bw.flush();
//
//
//            for (int i = 0; i < repetitions; i++) {
//                if(i!= 0) {
//                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Cartridge);
//                    workflowSystem.getKMRCom().WaitForTaskCompletion();
//                }
//
//                workflowSystem.getKMRCom().StartMoveToLocation(Location.Quantos);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//                workflowSystem.getKMRCom().StartLBR(workflowSystem.getLBRComNoCheck(), Location.Quantos);
//                Thread.sleep(1000);
//                {
//                    workflowSystem.getLBRCom().SixPointCalibration();
//                    {
//                        LBRRobotStatus status = workflowSystem.getLBRCom().WaitForTaskCompletion();
//                        SerializableFrame cube_coor = status.getCubeOrigin();
//                        sb = new StringBuilder();
//                        sb.append(Integer.toString(i));
//                        sb.append(',');
//                        sb.append("Quantos");
//                        sb.append(',');
//                        sb.append(cube_coor.X);
//                        sb.append(',');
//                        sb.append(cube_coor.Y);
//                        sb.append(',');
//                        sb.append(cube_coor.Z);
//                        sb.append(',');
//                        sb.append(cube_coor.A);
//                        sb.append(',');
//                        sb.append(cube_coor.B);
//                        sb.append(',');
//                        sb.append(cube_coor.C);
//                        sb.append(',');
//                        sb.append(formatter.format(new Date()));
//                        sb.append("\r\n");
//                        bw.write(sb.toString());
//                        System.out.println(sb.toString());
//                        bw.flush();
//                    }
//
//                    workflowSystem.getLBRCom().PlaceRackInBuffer(0,0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().StartVialHandling();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspVialFromBuffer(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().PlaceSampleInQuantos();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspSampleFromQuantos();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    for (int vial_index = 1; vial_index < 18; vial_index++) {
//                        if(vial_index==8)
//                            {vial_index++;vial_index++;}
//                        workflowSystem.getLBRCom().PlaceVialInBuffer(vial_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().GraspVialFromBuffer(vial_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().PlaceSampleInQuantos();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().GraspSampleFromQuantos();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//                    }
//
//
//                    workflowSystem.getLBRCom().PlaceVialInBuffer(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().StopVialHandling();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspRackFromBuffer(0,0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().FinishArmManipulations();
//
//                }
//                Thread.sleep(2000);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//
//            }
//        } catch (RobotNoHeartBeatException e) {
//            e.printStackTrace();
//        }
    }

    public static void GCnDriteRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        //Write to CSV
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("PhotocatSampleRobustness" + formatter.format(new Date()) + ".csv"))) {
            StringBuilder sb = new StringBuilder();

            sb.append("#run");
            sb.append(',');
            sb.append("timestamp (s)");
            sb.append("\r\n");
            bw.write(sb.toString());
            bw.flush();


            workflowSystem.getGCCom();

            for (int i = 0; i < repetitions; i++) {
                {
                    sb = new StringBuilder();
                    sb.append(Integer.toString(i));
                    sb.append(',');
                    sb.append(formatter.format(new Date()));
                    sb.append("\r\n");
                    bw.write(sb.toString());
                    bw.flush();
                }

                Batch batch_one = Batch.ReadFromFile(workflowSystem.getState().getSolids(), workflowSystem.getState().getLiquids(), new File("batch_one.run"));
                batch_one.setName("batch_one-" + i);
                Batch batch_two = Batch.ReadFromFile(workflowSystem.getState().getSolids(), workflowSystem.getState().getLiquids(), new File("batch_two.run"));
                batch_two.setName("batch_two-" + i);
                Batch batch_three = Batch.ReadFromFile(workflowSystem.getState().getSolids(), workflowSystem.getState().getLiquids(), new File("batch_three.run"));
                batch_three.setName("batch_three-" + i);
                workflowSystem.getGCCom().StartBatch(new Batch[] {batch_one, batch_two,batch_three});
                Thread.sleep(10 * 1000);
                while(workflowSystem.getGCCom().WaitForCommandable().getBatches_running().size() != 0)
                {
                    Thread.sleep(60 * 1000);
                }
                workflowSystem.getGCCom().GetResults(new Batch[] {batch_one, batch_two,batch_three});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void PhotocatRackRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        //Write to CSV
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("PhotocatSampleRobustness" + formatter.format(new Date()) + ".csv"))) {
            StringBuilder sb = new StringBuilder();

            sb.append("#measurement");
            sb.append(',');
            sb.append("location");
            sb.append(',');
            sb.append("x (mm)");
            sb.append(',');
            sb.append("y (mm)");
            sb.append(',');
            sb.append("z (mm)");
            sb.append(',');
            sb.append("a (rad)");
            sb.append(',');
            sb.append("b (rad)");
            sb.append(',');
            sb.append("c (rad)");
            sb.append(',');
            sb.append("timestamp (s)");
            sb.append("\r\n");
            bw.write(sb.toString());
            bw.flush();


            for (int i = 0; i < repetitions; i++) {
                if(i!= 0) {
                    workflowSystem.getKMRCom().StartMoveToLocationAndWait(Location.Cartridge);
                }
                workflowSystem.getKMRCom().TransPortKMRAndStartLBRAndWait(workflowSystem.getLBRComNoCheck(),Location.Vibratory_Photocat);
                    {

                    {
                        LBRRobotStatus status = workflowSystem.getLBRCom().SixPointCalibrationAndWait();
                        SerializableFrame cube_coor = status.getCubeOrigin();
                        sb = new StringBuilder();
                        sb.append(Integer.toString(i));
                        sb.append(',');
                        sb.append("Photocat");
                        sb.append(',');
                        sb.append(cube_coor.X);
                        sb.append(',');
                        sb.append(cube_coor.Y);
                        sb.append(',');
                        sb.append(cube_coor.Z);
                        sb.append(',');
                        sb.append(cube_coor.A);
                        sb.append(',');
                        sb.append(cube_coor.B);
                        sb.append(',');
                        sb.append(cube_coor.C);
                        sb.append(',');
                        sb.append(formatter.format(new Date()));
                        sb.append("\r\n");
                        bw.write(sb.toString());
                        bw.flush();
                    }

                    workflowSystem.getLBRCom().PlaceRackInBufferAndWait(0,0);


                    workflowSystem.getLBRCom().StartVialHandlingAndWait();
                    workflowSystem.getLBRCom().GraspVialFromBufferAndWait(0);
                    for(int sammple_index=0;sammple_index<16;sammple_index++)
                    {
                        int sample_number;
                        if(sammple_index<8)
                            sample_number = sammple_index;
                        else
                            sample_number =sammple_index+2;

                        workflowSystem.getLBRCom().PlaceVialInBufferAndWait(sample_number);
                        workflowSystem.getLBRCom().GraspVialFromBufferAndWait(sample_number);

                        workflowSystem.getLBRCom().MoveToDryPositionAndWait();
                        workflowSystem.getVibratoryCom().StartDry();
                        workflowSystem.getLBRCom().DryProcedureAndWait();
                        workflowSystem.getVibratoryCom().StopDry();
                        workflowSystem.getLBRCom().MoveToNeutralAfterDryAndWait();

                        workflowSystem.getLBRCom().PlaceSampleInPhotolysisStationAndWait(sammple_index);
                        workflowSystem.getLBRCom().GraspSampleFromPhotolysisStationAndWait(sammple_index);


                    }
                    workflowSystem.getLBRCom().PlaceVialInBufferAndWait(0);
                    workflowSystem.getLBRCom().StopVialHandlingAndWait();


                    workflowSystem.getLBRCom().GraspRackFromBufferAndWait(0,0);

                    workflowSystem.getLBRCom().ArmToDrivePosAndWait();
                    workflowSystem.getLBRCom().FinishArmManipulationsAndWait(workflowSystem.getKMRCom());


                }
                workflowSystem.getKMRCom().WaitForCommandable();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void CubeRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException, RobotBusyException, RobotNoHeartBeatException, NotAllowedException {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        //Write to CSV
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("CubeRobustness" + formatter.format(new Date()) + ".csv"))) {

            StringBuilder sb = new StringBuilder();
            sb.append("#measurement");
            sb.append(',');
            sb.append("previous location");
            sb.append(',');
            sb.append("location");
            sb.append(',');
            sb.append("x (mm)");
            sb.append(',');
            sb.append("y (mm)");
            sb.append(',');
            sb.append("z (mm)");
            sb.append(',');
            sb.append("a (rad)");
            sb.append(',');
            sb.append("b (rad)");
            sb.append(',');
            sb.append("c (rad)");
            sb.append(',');
            sb.append("timestamp (s)");
            sb.append("\r\n");
            bw.write(sb.toString());
            bw.flush();

            Location[] locations = Location.values();
            Location previousLocation = null;
            Location nextLocation = Location.Quantos;
            int enumIndex = nextLocation.ordinal();
            int nextIndex = enumIndex + 1;
            workflowSystem.getKMRCom().StartMoveToLocationAndWait(nextLocation);

            for (int i = 0; i < 12; i++) {
                int allEdges[][] = new int[14][14];
                for (int j = 0; j < 90; j++) {
                    previousLocation = nextLocation;



                    while ( ( nextLocation == Location.Photocat || nextLocation == Location.Crossroads ||
                            nextLocation == Location.Unknown || nextLocation == Location.DryingStation ) ||
                            previousLocation == nextLocation || allEdges[previousLocation.ordinal()][nextLocation.ordinal()]==1 ) {
                        nextIndex %= locations.length;
                        nextLocation = locations[nextIndex];
                        nextIndex++;
                    }

                    DisallowCharge(workflowSystem);
                    allEdges[previousLocation.ordinal()][nextLocation.ordinal()] = 1;
                    workflowSystem.getKMRCom().StartMoveToLocationAndWait(nextLocation); // The combined method has a 5 minute time-out
                    SerializableFrame cube_coor = null;
                    if(nextLocation != Location.ChargingStation ) { //cant do 6P @ charging station, but need to use edge nevertheless
                        workflowSystem.getKMRCom().StartLBRAndWait(workflowSystem.getLBRComNoCheck(), nextLocation);
                        LBRRobotStatus status = workflowSystem.getLBRCom().SixPointCalibrationAndWait();
                        cube_coor = status.getCubeOrigin();
                    }
                    sb = new StringBuilder();
                    sb.append(Integer.toString(i*90+j));
                    sb.append(',');
                    sb.append(previousLocation);
                    sb.append(',');
                    sb.append(nextLocation);
                    sb.append(',');

                    if(nextLocation != Location.ChargingStation ) sb.append(cube_coor.X);
                    sb.append(',');
                    if(nextLocation != Location.ChargingStation ) sb.append(cube_coor.Y);
                    sb.append(',');
                    if(nextLocation != Location.ChargingStation ) sb.append(cube_coor.Z);
                    sb.append(',');
                    if(nextLocation != Location.ChargingStation ) sb.append(cube_coor.A);
                    sb.append(',');
                    if(nextLocation != Location.ChargingStation ) sb.append(cube_coor.B);
                    sb.append(',');
                    if(nextLocation != Location.ChargingStation ) sb.append(cube_coor.C);
                    sb.append(',');
                    sb.append(formatter.format(new Date()));
                    sb.append("\r\n");
                    bw.write(sb.toString());
                    bw.flush();

                    if(nextLocation != Location.ChargingStation ) workflowSystem.getLBRCom().ArmToDrivePosAndWait();

                    AllowCharge(workflowSystem);

                    try {
                        if(nextLocation != Location.ChargingStation ) workflowSystem.getLBRCom().FinishArmManipulationsAndWait(workflowSystem.getKMRCom());
                    } catch (Exception e)
                    {

                    }


                    Thread.sleep(5000);
                    workflowSystem.getKMRCom().WaitForCommandable();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    //There is no guarantee that after this method the robot will be active. After this ALWAYS THROW AN ENSURE ROBOTISCALBIRATEDATPOSITION
    protected static void AllowCharge(WorkflowSystem workflowSystem) throws CloneNotSupportedException, IOException, InterruptedException, RobotBusyException, NotAllowedException {
        try {
            LBRCom lbr = workflowSystem.getLBRComNoCheck();
            KMRCom kmr = workflowSystem.getKMRCom();
            if(kmr.GetServer().GetStatus().getStatus().equals(RobotStatusEnum.TaskRunning) && lbr != null && lbr.isAlive())
                workflowSystem.getLBRCom().AllowChargingOfRobot();
            else
                workflowSystem.getKMRCom().AllowChargingOfRobotAndWait();
            //Allow the arm controller to turn off as the robot goes for a charge
            Thread.sleep(3000);
        } catch (RobotErrorStateException e) {
            e.printStackTrace();
        } catch (RobotNoHeartBeatException e) {
            //Robot has gone for a charge immediately. We do not care.
            e.printStackTrace();
        } catch (DeviceOfflineException e) {
            e.printStackTrace();
        }
    }

    //There is no guarantee that after this method the robot will be active. After this ALWAYS THROW AN ENSURE ROBOTISCALBIRATEDATPOSITION
    protected static void DisallowCharge(WorkflowSystem workflowSystem) throws CloneNotSupportedException, IOException, InterruptedException, DeviceOfflineException, RobotErrorStateException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
        boolean worked = false;
        boolean tried_lbr =false;
        try {
            LBRCom lbr = workflowSystem.getLBRComNoCheck();
            KMRCom kmr = workflowSystem.getKMRCom();
            if(kmr.GetServer().GetStatus().getStatus().equals(RobotStatusEnum.TaskRunning) && lbr != null && lbr.isAlive()) {
                //This may fail because in between the robot deciding going for a charge, we may be trying to sent a
                // discharge command, which is then nefer picked up because the lbr goes offline
                // In this case this will never complete, and will throw an error while the robots waits for the task to
                // start
                tried_lbr = true;
                workflowSystem.getLBRCom().DisallowChargingOfRobotAndWait();
            }
            else
            {
                if(kmr.GetServer().GetStatus().getStatus().equals(RobotStatusEnum.TaskRunning))
                    kmr.WaitForCommandable();
                workflowSystem.getKMRCom().DisallowChargingOfRobotAndWait();
            }

            worked = true;
        } catch (RobotErrorStateException e) {
            e.printStackTrace();
        } catch (RobotNoHeartBeatException e) {
            e.printStackTrace();
        } catch (DeviceOfflineException e) {
            e.printStackTrace();
        }

        //Above error may not work if the robot goes for a charge
        //while it is still on, that means that the lbr has shut down
        if(!worked) {
            {
                if(!tried_lbr)
                {
                    System.out.println("KMR FAILED Allow CHARGE UNEXPECTEDLY");
                    System.out.println("Was a command sent to the robot eventhough the KMR jobs are active?!");
                }
                // SO the LBR failed, did not process the command properly, so wait for the KMR to become commendable
                // so let it finish the waitforcontainer method that finishes after the arm finishes (at some pint?)
                workflowSystem.getKMRCom().WaitForCommandable();
                workflowSystem.getKMRCom().DisallowChargingOfRobotAndWait();

            }
        }
    }

    public static void CapperRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException {
//        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
//        //Write to CSV
//        try (BufferedWriter bw = new BufferedWriter(new FileWriter("CapperSampleRobustness" + formatter.format(new Date()) + ".csv"))) {
//            StringBuilder sb = new StringBuilder();
//
//            sb.append("#measurement");
//            sb.append(',');
//            sb.append("location");
//            sb.append(',');
//            sb.append("x (mm)");
//            sb.append(',');
//            sb.append("y (mm)");
//            sb.append(',');
//            sb.append("z (mm)");
//            sb.append(',');
//            sb.append("a (rad)");
//            sb.append(',');
//            sb.append("b (rad)");
//            sb.append(',');
//            sb.append("c (rad)");
//            sb.append(',');
//            sb.append("timestamp (s)");
//            sb.append("\r\n");
//            bw.write(sb.toString());
//            System.out.println(sb.toString());
//            bw.flush();
//
//
//            for (int i = 0; i < repetitions; i++) {
//                if(i!= 0) {
//                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Crossroads);
//                    workflowSystem.getKMRCom().WaitForTaskCompletion();
//                }
//                workflowSystem.getKMRCom().StartMoveToLocation(Location.Capper);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//                workflowSystem.getKMRCom().StartLBR(workflowSystem.getLBRComNoCheck(), Location.Capper);
//                Thread.sleep(1000);
//                {
//                    workflowSystem.getLBRCom().SixPointCalibration();
//                    {
//                        LBRRobotStatus status = workflowSystem.getLBRCom().WaitForTaskCompletion();
//                        SerializableFrame cube_coor = status.getCubeOrigin();
//                        sb = new StringBuilder();
//                        sb.append(Integer.toString(i));
//                        sb.append(',');
//                        sb.append("Capper");
//                        sb.append(',');
//                        sb.append(cube_coor.X);
//                        sb.append(',');
//                        sb.append(cube_coor.Y);
//                        sb.append(',');
//                        sb.append(cube_coor.Z);
//                        sb.append(',');
//                        sb.append(cube_coor.A);
//                        sb.append(',');
//                        sb.append(cube_coor.B);
//                        sb.append(',');
//                        sb.append(cube_coor.C);
//                        sb.append(',');
//                        sb.append(formatter.format(new Date()));
//                        sb.append("\r\n");
//                        bw.write(sb.toString());
//                        System.out.println(sb.toString());
//                        bw.flush();
//                    }
//
//                    workflowSystem.getLBRCom().PlaceRackInBuffer(0,0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().StartVialHandling();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspVialFromBuffer(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().PlaceVialInCapper();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspVialFromCapper();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    for (int vial_index = 1; vial_index < 8; vial_index++) {
//                        if(vial_index==8)
//                        {vial_index++;vial_index++;}
//                        workflowSystem.getLBRCom().PlaceVialInBuffer(vial_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().GraspVialFromBuffer(vial_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().PlaceVialInCapper();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().GraspVialFromCapper();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//                    }
//
//
//                    workflowSystem.getLBRCom().PlaceVialInBuffer(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//
//                    workflowSystem.getLBRCom().GraspVialFromBuffer(10);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().PlaceVialInCapper();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspVialFromCapper();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    for (int vial_index = 11; vial_index < 18; vial_index++) {
//                        if(vial_index==8)
//                        {vial_index++;vial_index++;}
//                        workflowSystem.getLBRCom().PlaceVialInBuffer(vial_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().GraspVialFromBuffer(vial_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().PlaceVialInCapper();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().GraspVialFromCapper();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//                    }
//
//
//                    workflowSystem.getLBRCom().PlaceVialInBuffer(10);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().StopVialHandling();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspRackFromBuffer(0,0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().FinishArmManipulations();
//
//                }
//                Thread.sleep(2000);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//
//            }
//        } catch (RobotNoHeartBeatException e) {
//            e.printStackTrace();
//        }
    }

    public static void SonicatorRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException, RobotBusyException, NotAllowedException {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        //Write to CSV
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("SonicatorSampleRobustness" + formatter.format(new Date()) + ".csv"))) {
            StringBuilder sb = new StringBuilder();

            sb.append("#measurement");
            sb.append(',');
            sb.append("location");
            sb.append(',');
            sb.append("x (mm)");
            sb.append(',');
            sb.append("y (mm)");
            sb.append(',');
            sb.append("z (mm)");
            sb.append(',');
            sb.append("a (rad)");
            sb.append(',');
            sb.append("b (rad)");
            sb.append(',');
            sb.append("c (rad)");
            sb.append(',');
            sb.append("timestamp (s)");
            sb.append("\r\n");
            bw.write(sb.toString());
            System.out.println(sb.toString());
            bw.flush();


            for (int i = 0; i < repetitions; i++) {
                if(i!= 0) {
                    workflowSystem.getKMRCom().StartMoveToLocationAndWait(Location.Crossroads);
                                }
                workflowSystem.getKMRCom().StartMoveToLocationAndWait(Location.Sonicator);
                                workflowSystem.getKMRCom().StartLBR(workflowSystem.getLBRComNoCheck(), Location.Sonicator);
                Thread.sleep(1000);
                {
                    workflowSystem.getLBRCom().SixPointCalibration();
                    {
                        LBRRobotStatus status = workflowSystem.getLBRCom().WaitForCommandable();
                        SerializableFrame cube_coor = status.getCubeOrigin();
                        sb = new StringBuilder();
                        sb.append(Integer.toString(i));
                        sb.append(',');
                        sb.append("Sonicator");
                        sb.append(',');
                        sb.append(cube_coor.X);
                        sb.append(',');
                        sb.append(cube_coor.Y);
                        sb.append(',');
                        sb.append(cube_coor.Z);
                        sb.append(',');
                        sb.append(cube_coor.A);
                        sb.append(',');
                        sb.append(cube_coor.B);
                        sb.append(',');
                        sb.append(cube_coor.C);
                        sb.append(',');
                        sb.append(formatter.format(new Date()));
                        sb.append("\r\n");
                        bw.write(sb.toString());
                        System.out.println(sb.toString());
                        bw.flush();
                    }

                    workflowSystem.getLBRCom().PlaceRackInBufferAndWait(0,0);

                    workflowSystem.getLBRCom().StartVialHandlingAndWait();

                    workflowSystem.getLBRCom().GraspVialFromBufferAndWait(0);

                    workflowSystem.getLBRCom().PlaceVialInSonicatorAndWait(0);

                    workflowSystem.getLBRCom().GraspVialFromSonicatorAndWait(0);

                    for (int vial_index = 1; vial_index < 17; vial_index++) {
                        if(vial_index==8)
                        {vial_index++;vial_index++;}
                        workflowSystem.getLBRCom().PlaceVialInBufferAndWait(vial_index);

                        workflowSystem.getLBRCom().GraspVialFromBufferAndWait(vial_index);

                        workflowSystem.getLBRCom().PlaceVialInSonicatorAndWait(vial_index);

                        workflowSystem.getLBRCom().GraspVialFromSonicatorAndWait(vial_index);

                        workflowSystem.getLBRCom().MoveToDryPositionAndWait();
                        workflowSystem.getLBRCom().DryProcedureAndWait();
                        workflowSystem.getLBRCom().MoveToNeutralAfterDryAndWait();

                    }

                    workflowSystem.getLBRCom().PlaceVialInBufferAndWait(0);

                    workflowSystem.getLBRCom().StopVialHandlingAndWait();

                    workflowSystem.getLBRCom().GraspRackFromBufferAndWait(0,0);

                    workflowSystem.getLBRCom().FinishArmManipulations();

                }
                Thread.sleep(2000);
                workflowSystem.getKMRCom().WaitForCommandable();

            }
        } catch (RobotNoHeartBeatException e) {
            e.printStackTrace();
        }
    }

    public static void GCRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException {
//        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
//        //Write to CSV
//        try (BufferedWriter bw = new BufferedWriter(new FileWriter("GCSampleRobustness" + formatter.format(new Date()) + ".csv"))) {
//            StringBuilder sb = new StringBuilder();
//
//            sb.append("#measurement");
//            sb.append(',');
//            sb.append("location");
//            sb.append(',');
//            sb.append("x (mm)");
//            sb.append(',');
//            sb.append("y (mm)");
//            sb.append(',');
//            sb.append("z (mm)");
//            sb.append(',');
//            sb.append("a (rad)");
//            sb.append(',');
//            sb.append("b (rad)");
//            sb.append(',');
//            sb.append("c (rad)");
//            sb.append(',');
//            sb.append("timestamp (s)");
//            sb.append("\r\n");
//            bw.write(sb.toString());
//            System.out.println(sb.toString());
//            bw.flush();
//
//
//            for (int i = 0; i < repetitions; i++) {
//                if(i!= 0) {
//                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Crossroads);
//                    workflowSystem.getKMRCom().WaitForTaskCompletion();
//                }
//                workflowSystem.getKMRCom().StartMoveToLocation(Location.GC);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//                workflowSystem.getKMRCom().StartLBR(workflowSystem.getLBRComNoCheck(), Location.GC);
//                Thread.sleep(1000);
//                {
//                    workflowSystem.getLBRCom().SixPointCalibration();
//                    {
//                        LBRRobotStatus status = workflowSystem.getLBRCom().WaitForTaskCompletion();
//                        SerializableFrame cube_coor = status.getCubeOrigin();
//                        sb = new StringBuilder();
//                        sb.append(Integer.toString(i));
//                        sb.append(',');
//                        sb.append("GC");
//                        sb.append(',');
//                        sb.append(cube_coor.X);
//                        sb.append(',');
//                        sb.append(cube_coor.Y);
//                        sb.append(',');
//                        sb.append(cube_coor.Z);
//                        sb.append(',');
//                        sb.append(cube_coor.A);
//                        sb.append(',');
//                        sb.append(cube_coor.B);
//                        sb.append(',');
//                        sb.append(cube_coor.C);
//                        sb.append(',');
//                        sb.append(formatter.format(new Date()));
//                        sb.append("\r\n");
//                        bw.write(sb.toString());
//                        System.out.println(sb.toString());
//                        bw.flush();
//                    }
//                    workflowSystem.getLBRCom().PressParkHeadspaceButton();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//                    for(int station_index=0;station_index<3;station_index++) {
//                        workflowSystem.getLBRCom().PlaceRackInBuffer(0, station_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//
//                        workflowSystem.getLBRCom().GraspRackFromBuffer(0, station_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//                    }
//
//                    workflowSystem.getLBRCom().PressParkHeadspaceButton();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//                    workflowSystem.getLBRCom().FinishArmManipulations();
//                }
//                Thread.sleep(2000);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//
//            }
//        } catch (RobotNoHeartBeatException e) {
//            e.printStackTrace();
//        }
    }

    public static void LiquidStationRobustness(WorkflowSystem workflowSystem, int repetitions) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException {
//        Format formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
//        //Write to CSV
//        try (BufferedWriter bw = new BufferedWriter(new FileWriter("LiquidStationSampleRobustness" + formatter.format(new Date()) + ".csv"))) {
//            StringBuilder sb = new StringBuilder();
//
//            sb.append("#measurement");
//            sb.append(',');
//            sb.append("location");
//            sb.append(',');
//            sb.append("x (mm)");
//            sb.append(',');
//            sb.append("y (mm)");
//            sb.append(',');
//            sb.append("z (mm)");
//            sb.append(',');
//            sb.append("a (rad)");
//            sb.append(',');
//            sb.append("b (rad)");
//            sb.append(',');
//            sb.append("c (rad)");
//            sb.append(',');
//            sb.append("timestamp (s)");
//            sb.append("\r\n");
//            bw.write(sb.toString());
//            System.out.println(sb.toString());
//            bw.flush();
//
//
//            for (int i = 0; i < repetitions; i++) {
//                if(i!= 0) {
//                    workflowSystem.getKMRCom().StartMoveToLocation(Location.Crossroads);
//                    workflowSystem.getKMRCom().WaitForTaskCompletion();
//                }
//                workflowSystem.getKMRCom().StartMoveToLocation(Location.LiquidHandlingSystem);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//                workflowSystem.getKMRCom().StartLBRAndWait(workflowSystem.getLBRComNoCheck(),Location.LiquidHandlingSystem);
//                Thread.sleep(1000);
//                {
//                    workflowSystem.getLBRCom().SixPointCalibration();
//                    {
//                        LBRRobotStatus status = workflowSystem.getLBRCom().WaitForTaskCompletion();
//                        SerializableFrame cube_coor = status.getCubeOrigin();
//                        sb = new StringBuilder();
//                        sb.append(Integer.toString(i));
//                        sb.append(',');
//                        sb.append("LiquidStation");
//                        sb.append(',');
//                        sb.append(cube_coor.X);
//                        sb.append(',');
//                        sb.append(cube_coor.Y);
//                        sb.append(',');
//                        sb.append(cube_coor.Z);
//                        sb.append(',');
//                        sb.append(cube_coor.A);
//                        sb.append(',');
//                        sb.append(cube_coor.B);
//                        sb.append(',');
//                        sb.append(cube_coor.C);
//                        sb.append(',');
//                        sb.append(formatter.format(new Date()));
//                        sb.append("\r\n");
//                        bw.write(sb.toString());
//                        System.out.println(sb.toString());
//                        bw.flush();
//                    }
//
//                    workflowSystem.getLBRCom().PlaceRackInBuffer(0,0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().StartVialHandling();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspVialFromBuffer(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().PlaceVialInLiquidStation(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().MoveVialFromStationOneToStationTwo();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspVialFromLiquidStation(1);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    for (int vial_index = 1; vial_index <= 17; vial_index++) {
//                        if(vial_index==8)
//                        {vial_index++;vial_index++;}
//                        workflowSystem.getLBRCom().PlaceVialInBuffer(vial_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().GraspVialFromBuffer(vial_index);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().PlaceVialInLiquidStation(0);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().MoveVialFromStationOneToStationTwo();
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                        workflowSystem.getLBRCom().GraspVialFromLiquidStation(1);
//                        workflowSystem.getLBRCom().WaitForTaskCompletion();
//                    }
//
//
//                    workflowSystem.getLBRCom().PlaceVialInBuffer(0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//
//                    workflowSystem.getLBRCom().StopVialHandling();
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().GraspRackFromBuffer(0,0);
//                    workflowSystem.getLBRCom().WaitForTaskCompletion();
//
//                    workflowSystem.getLBRCom().FinishArmManipulations();
//
//                }
//                Thread.sleep(2000);
//                workflowSystem.getKMRCom().WaitForTaskCompletion();
//
//            }
//        } catch (RobotNoHeartBeatException e) {
//            e.printStackTrace();
//        }
//    }
}
}
