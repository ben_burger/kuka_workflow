package Workflow.WorldState;


import Workflow.Batch.*;
import Workflow.Batch.Batch;
import Workflow.Exception.CompoundNotPResentException;
import Workflow.Com.Serial.Quantos.DispensingAlgorithm;
import Workflow.Exception.CompoundNotPResentException;
import Workflow.Exception.InconsistentStateException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class WorldState {

    private Boolean StateLock = false;
    private Liquid[] Liquids;
    private Solid[] Solids;

    private Map<BatchLocation, Batch> batch_locations = new HashMap();
    private int Minimal_Caps_Present = 0;
    private Date PhotolysisStopTime = null;
    private BatchLocation[] StorageRackHolders = new BatchLocation[]{BatchLocation.InputStation_1, BatchLocation.InputStation_2
            , BatchLocation.InputStation_3 , BatchLocation.InputStation_4, BatchLocation.InputStation_5, BatchLocation.InputStation_6
            , BatchLocation.InputStation_7, BatchLocation.InputStation_8, BatchLocation.InputStation_9
            , BatchLocation.InputStation_10

            , BatchLocation.InputStation_11, BatchLocation.InputStation_12,
            BatchLocation.InputStation_13, BatchLocation.InputStation_14, BatchLocation.InputStation_15,
            //BatchLocation.InputStation_16, Causing problems
            BatchLocation.InputStation_17, BatchLocation.InputStation_18,
            BatchLocation.InputStation_19,BatchLocation.InputStation_20,BatchLocation.InputStation_21
            ,BatchLocation.InputStation_22

            ,BatchLocation.InputStation_23
            //,BatchLocation.InputStation_24 //causing problems
            ,BatchLocation.InputStation_25
            ,BatchLocation.InputStation_26,BatchLocation.InputStation_27
            ,BatchLocation.InputStation_28,BatchLocation.InputStation_29,BatchLocation.InputStation_30
            ,BatchLocation.InputStation_31,BatchLocation.InputStation_32,BatchLocation.InputStation_33
            ,BatchLocation.InputStation_34,BatchLocation.InputStation_35,BatchLocation.InputStation_36
            ,BatchLocation.InputStation_37,BatchLocation.InputStation_38,BatchLocation.InputStation_39
    };

    public WorldState() throws IOException, ParseException, CompoundNotPResentException, InconsistentStateException {
        Liquids = ReadLiquids();
        Solids = ReadSolids();
        for (BatchLocation location : BatchLocation.values()) {
            if (location != BatchLocation.NoRackAssigned)
                batch_locations.put(location, null);
        }


        ReadWorldState();

    }

    public Liquid[] getLiquids() {
        synchronized (StateLock) {
            return Liquids.clone();
        }
    }

    public Solid[] getSolids() {
        synchronized (StateLock) {
            return Solids.clone();
        }
    }

    public List<Batch> getRunsInTheSystem() {
        List<Batch> result = new LinkedList<>();
        synchronized (StateLock) {
            for (BatchLocation location : BatchLocation.values()) {
                if (location != BatchLocation.NoRackAssigned) {
                    Batch batch = getRackAtLocation(location);
                    if (batch != null)
                        result.add(batch);
                }
            }
        }

        return result.stream().sorted(Comparator.comparing(Batch::getBatch_name)).collect(Collectors.toList());
    }

    public List<Batch> ReadRunsInSystem() throws IOException, ParseException, CompoundNotPResentException {
        return ReadRunsInSystem(true,true);
    }
    public List<Batch> ReadRunsInSystem(boolean include_running, boolean include_completed) throws IOException, ParseException, CompoundNotPResentException {
        List<Batch> result = new LinkedList<>();
        File[] files_running, files_completed;
        if(include_running)
        {
            File dir = new File("running\\.");
            files_running = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".run");
                }
            });
            if(files_running!= null)
            for (File batchfile : files_running) {
                result.add(Batch.ReadFromFile(getSolids(), getLiquids(), batchfile));
            }
        }


        if(include_completed)
        {
            File dir = new File("completed\\.");
            files_completed = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".run");
                }
            });
            for (File batchfile : files_completed) {
                result.add(Batch.ReadFromFile(getSolids(), getLiquids(), batchfile));
            }
        }
        return result;
    }

    private void ReadWorldState() throws IOException, ParseException, CompoundNotPResentException, InconsistentStateException {
        Batch[] RunsInSystem = ReadRunsInSystem().toArray(new Batch[0]);
//set locations of racks in both rack instances as well as this class
        FileReader fileReader = new FileReader(new File("world.state"));
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;

        while ((line = bufferedReader.readLine()) != null && !line.equals("")) {
            String[] split = line.split("[:()]");
            String field = split[0];
            if (field.equals("Capper_Caps")) {
                Minimal_Caps_Present = Integer.parseInt(split[1]);
            }
            else {
                BatchLocation location = BatchLocation.valueOf(field);
                Batch batch;
                if (split[1].contains("FreshRack")) {
                    batch = Batch.FreshBatch(split[1]);
                    MoveRackToLocation(location, batch);
                    batch.setLocation(location);
                    if (split.length > 2) {
                        Date originalDate = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSS").parse(split[2]);
                        batch.setTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start, originalDate);
                    } else {
                        batch.setTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start, null);
                    }
                } else if (split[1].length() > 0) {
                    Optional<Batch> test =Arrays.stream(RunsInSystem).filter(x -> x.getBatch_name().equals(split[1])).findFirst();
                    if(!test.isPresent())
                    {
                        System.out.println("can not find batch" + split[1]);
                    }
                    batch =test.get();
                    MoveRackToLocation(location, batch);
                    batch.setLocation(location);
                }

            }
        }
        fileReader.close();
    }

    private void WriteWorldState() {
        File state_file = new File("world.state");

        try {
            File new_file = new File(state_file.getAbsolutePath() + "_new");

            if (new_file.exists()) {
                new_file.delete();
            }
            new_file.createNewFile();
            {
                PrintWriter writer = new PrintWriter(new_file);
                WriteBatchToWriter(writer);
                writer.flush();
                writer.close();
            }

            if (state_file.exists()) {
                boolean a = state_file.delete();

            }

            new_file.renameTo(state_file);
        } catch (IOException e) {
            //eeh?
        }
    }

    private void WriteBatchToWriter(PrintWriter writer) {
        for (BatchLocation location : BatchLocation.values()) {
            Batch batch = getRackAtLocation(location);
            SimpleDateFormat convertToString = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSS");
            if (batch != null)
                if ( batch.getBatch_name().contains("FreshRack") ) {
                    Date added = batch.getTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start);
                    writer.write(location.toString() + ":" + batch.getBatch_name() +
                            (added == null? "" : ("(" + convertToString.format(added) + ")" ) )

                            + "\r\n");
                }
                else {
                    writer.write(location.toString() + ":" + batch.getBatch_name() + "\r\n");
                }
        }

        writer.write("Capper_Caps:" + Minimal_Caps_Present + "\r\n");
    }


    public void MoveRack(BatchLocation source, BatchLocation target, Batch batch) {
        synchronized (StateLock) {
            if (getRackAtLocation(source) == null)
                throw new NotImplementedException();


            MoveRackToLocation(target, batch);
            MoveRackToLocation(source, null);
            batch.setLocation(target);

            WriteWorldState();
        }
    }


    private void MoveRackToLocation(BatchLocation target, Batch batch) {
        synchronized (StateLock) {
            batch_locations.put(target, batch);
        }
    }

    public Batch getRackAtLocation(BatchLocation source) {
        synchronized (StateLock) {
            return batch_locations.get(source);
        }
    }

    public void AddSolidDispesedAmount(Batch batch, Sample sample, Solid solid, float amount) throws IOException {
        synchronized (StateLock) {

            batch.addSolidDispensed(sample, solid, amount);
        }
    }

    public void AddLiquidDispesedAmount(Batch batch, Sample sample, Liquid liquid, float amount) throws IOException {
        synchronized (StateLock) {
            batch.addLiquidDispensed(sample, liquid, amount);
            ReduceVolume(liquid, amount);
        }
    }

    private void ReduceVolume(Liquid liquid, float miliLiters) {
        synchronized (StateLock) {
            liquid.reduceMinimalVolume(miliLiters);
            WriteLiquids();
        }
    }

    public void SetGCWell(Batch batch, Sample sample, int gc_well_index) throws IOException {
        synchronized (StateLock) {
            batch.SetGCWell(sample, gc_well_index);
        }
    }

    public void SetVialCapped(Batch batch, Sample sample) throws IOException {
        synchronized (StateLock) {
            Minimal_Caps_Present--;
            batch.setSampleCapped(sample);
            WriteWorldState();
        }
    }

    public void SetSolidBlocked() {
        synchronized (StateLock) {
            Optional<Solid> solid = Arrays.stream(Solids).filter(x -> x.getHotelIndex() == -1).findFirst();
            if (solid.isPresent()) {
                solid.get().setBlocked(true);
                WriteSolids();
            }
        }
    }

    public void MoveSolid(Solid solid_to_dispense, int oldHotelIndex, int newHotelIndex) {
        synchronized (StateLock) {
            Optional<Solid> solid = Arrays.stream(Solids).filter(x -> x.getName().equals(solid_to_dispense.getName()) && x.getHotelIndex() == oldHotelIndex).findFirst();
            solid.get().setHotelIndex(newHotelIndex);
            WriteSolids();
        }
    }

    private void WriteLiquids() {
        File state_file = new File("liquid.csv");

        try {
            File new_file = new File(state_file.getAbsolutePath() + "_new");

            if (new_file.exists()) {
                new_file.delete();
            }
            new_file.createNewFile();
            {
                PrintWriter writer = new PrintWriter(new_file);
                WriteLiquidstoWriter(writer);
                writer.flush();
                writer.close();
            }

            if (state_file.exists()) {
                boolean a = state_file.delete();

            }

            new_file.renameTo(state_file);
        } catch (IOException e) {
            //eeh?
        }
    }

    private void WriteLiquidstoWriter(PrintWriter writer) {
        DateFormat df2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        writer.write("Name,Density,StationID,Pump,VolumePresent,Min,Max,ExpiryDate" + "\r\n");
        for (Liquid liquid : Liquids) {
            writer.write(liquid.getName());
            writer.write(",");
            writer.write(liquid.getDensity() + "");
            writer.write(",");
            writer.write(liquid.getStationID() + "");
            writer.write(",");
            writer.write(liquid.getPumpID() + "");
            writer.write(",");
            writer.write(liquid.getMinimalVolumePresent() + "");
            writer.write(",");
            writer.write(liquid.getMin() + "");
            writer.write(",");
            writer.write(liquid.getMax()+"");
            writer.write(",");
            writer.write(liquid.getExpiryDate() != null? df2.format(liquid.getExpiryDate()) : "");
            writer.write("\r\n");

        }

    }

    private Solid[] ReadSolids() throws IOException {
        List<Solid> liqs = new LinkedList<Solid>();

        FileReader fileReader = new FileReader(new File("solid.csv"));
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        line = bufferedReader.readLine();
        while ((line = bufferedReader.readLine()) != null) {
            Solid solid;
            String[] data = line.split(",");
            solid = new Solid(data[0], Integer.parseInt(data[1]), "1".equals(data[2]), Integer.parseInt(data[3]), DispensingAlgorithm.valueOf(data[4]).name(), Float.parseFloat(data[5]), Float.parseFloat(data[6]), Integer.parseInt(data[7]));

            liqs.add(solid);
        }
        fileReader.close();
        return (Solid[]) liqs.toArray(new Solid[liqs.size()]);

    }

    private Liquid[] ReadLiquids() throws IOException, ParseException {
        List<Liquid> liqs = new LinkedList<Liquid>();
        DateFormat df2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss"); FileReader fileReader = new FileReader(new File("liquid.csv"));
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        line = bufferedReader.readLine(); //Ignore column names
        while ((line = bufferedReader.readLine()) != null) {
            Liquid liq;
            String[] data = line.split(",",8);
            Date date;

            if(data[7] != null && !data[7].equals(""))
                liq = new Liquid(data[0], Float.parseFloat(data[1]), Integer.parseInt(data[3]), Float.parseFloat(data[4]), Integer.parseInt(data[2]), Float.parseFloat(data[5]), Float.parseFloat(data[6]), df2.parse(data[7]));
            else liq = new Liquid(data[0], Float.parseFloat(data[1]), Integer.parseInt(data[3]), Float.parseFloat(data[4]), Integer.parseInt(data[2]), Float.parseFloat(data[5]), Float.parseFloat(data[6]));

            liqs.add(liq);
        }
        fileReader.close();
        return (Liquid[]) liqs.toArray(new Liquid[liqs.size()]);
    }

    private void WriteSolids() {
        File state_file = new File("solid.csv");

        try {
            File new_file = new File(state_file.getAbsolutePath() + "_new");

            if (new_file.exists()) {
                new_file.delete();
            }
            new_file.createNewFile();
            {
                PrintWriter writer = new PrintWriter(new_file);
                WriteSolidstoWriter(writer);
                writer.flush();
                writer.close();
            }

            if (state_file.exists()) {
                boolean a = state_file.delete();

            }

            new_file.renameTo(state_file);
        } catch (IOException e) {
            //eeh?
        }
    }

    private void WriteSolidstoWriter(PrintWriter writer) {
       writer.write("SolidName,Location,Blocked,tapping,DispensingAlgorithm,Min,Max" + "\r\n");
        for (Solid solid : Solids) {
            writer.write(solid.getName());
            writer.write(",");
            writer.write(solid.getHotelIndex() + "");
            writer.write(",");
            writer.write(solid.getBlocked() ? "1" : "0");
            writer.write(",");
            writer.write(solid.getTapping() + "");
            writer.write(",");
            writer.write(solid.getDispenseAlgorithm());
            writer.write(",");
            writer.write(solid.getMin() + "");
            writer.write(",");
            writer.write(solid.getMax()+"");
            writer.write(",");
            writer.write(solid.getRemainingDosages()+"");
            writer.write("\r\n");
        }

    }

    public int getMinimal_Caps_Present() {
        synchronized (StateLock) {
            return Minimal_Caps_Present;
        }
    }

    public Batch[] getFreshRacks() {
        LinkedList<Batch> result = new LinkedList<>();
        synchronized (StateLock) {

                for (BatchLocation location : StorageRackHolders) {
                    if (location != BatchLocation.NoRackAssigned) {
                        Batch batch = getRackAtLocation(location);
                        if (batch != null && batch.getBatch_name().contains("FreshRack")) {
                            result.add(batch);
                        }
                    }

                }
        }
        Batch[] output = result.stream().sorted((Batch lhs,Batch rhs) ->
        {
            //Returns: a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
            if(lhs.getTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start) != null
                    && rhs.getTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start) != null)
            return
                    Math.toIntExact(lhs.getTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start).getTime() -
                    rhs.getTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start).getTime());
            else if(lhs.getTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start) == null)
                return -1;
            else return 1;
        })
                .toArray(Batch[]::new);
        return output;
    }

    public void AssignFreshRack(Batch batch, Batch fresh) throws IOException {
        batch.setLocation(fresh.getBatchLocation());
        batch.setRackIdentifier(fresh.getRack_identifier());
        MoveRackToLocation(fresh.getBatchLocation(), batch);
        batch.setTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start,
            fresh.getTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start));
        batch.setStatus(BatchStatus.NoRackAssigned);
        WriteWorldState();
    }

    public BatchLocation[] getBufferBatchLocationForStatus(Batch batch, BatchStatus batchStatus) {

        List<BatchLocation> result = new LinkedList<>();


        LinkedList<BatchLocation> locations = new LinkedList<BatchLocation>();
        if (batchStatus.equals(BatchStatus.MoveToStorage)) {
            locations.addAll(Arrays.asList(StorageRackHolders));
        } else {
            if (batch.getBatchLocation().isOnRobot())
                result.add(batch.getBatchLocation());
            locations.addAll(Arrays.asList(new BatchLocation[]{BatchLocation.Robot_Transport_1, BatchLocation.Robot_Transport_2, BatchLocation.Robot_Transport_2, BatchLocation.Robot_Transport_3,
                    BatchLocation.Robot_Transport_4,
                    BatchLocation.Robot_Transport_5, BatchLocation.Robot_Transport_6}));
        }

        for (BatchLocation location : locations) {
            if (getRackAtLocation(location) == null || getRackAtLocation(location) == batch)
                result.add(location);
        }
        return result.toArray(new BatchLocation[0]);
    }

    public void AddSolid(String name, int hotelIndex, float min, float max) {
        Solid solid = new Solid(name, hotelIndex, false, 0, DispensingAlgorithm.Advanced.name(),min,max, 999);
        List<Solid> new_solids = Arrays.stream(Solids).collect(Collectors.toList());
        new_solids.add(solid);
        Solids = new_solids.toArray(new Solid[0]);
        WriteSolids();
    }

    public void SetSolidUnblocked(Solid solid) {
        solid.setBlocked(false);
        WriteSolids();
    }

    public void RemoveSolid(Solid solid) {
        List<Solid> new_solids = Arrays.stream(Solids).collect(Collectors.toList());
        new_solids.remove(solid);
        Solids = new_solids.toArray(new Solid[0]);
        WriteSolids();
    }

    public BatchLocation getFreePositionOnRobot() {

        List<BatchLocation> result = new LinkedList<>();


        LinkedList<BatchLocation> locations = new LinkedList<BatchLocation>();
        locations.addAll(Arrays.asList(new BatchLocation[]{BatchLocation.Robot_Transport_1, BatchLocation.Robot_Transport_2, BatchLocation.Robot_Transport_3,
                BatchLocation.Robot_Transport_4,
                BatchLocation.Robot_Transport_5, BatchLocation.Robot_Transport_6}));

        for (BatchLocation location : locations) {
            if (getRackAtLocation(location) == null)
                result.add(location);
        }
        if(result.size() == 0)
            return null;
        return result.get(0);
    }

    public void addFreshRack(BatchLocation location, int batch_identifier) throws InconsistentStateException, IOException {
        synchronized (StateLock) {
            Batch batch = Batch.FreshBatch("FreshRack" + batch_identifier);
            MoveRackToLocation(location, batch);
            batch.setLocation(location);
            batch.setTimestamp(BatchTimestampEnum.add_fresh_rack, BatchTimestampType.start, new Date());
            WriteWorldState();
        }
    }

    public void RemoveRack(Batch batch_to_remove) {
        synchronized (StateLock) {
            MoveRackToLocation(batch_to_remove.getBatchLocation(), null);
            batch_to_remove.setLocation(BatchLocation.NoRackAssigned);
            WriteWorldState();
        }
    }

    public void AddPackOfCaps() {
        synchronized (StateLock) {
            Minimal_Caps_Present += 100;
            WriteWorldState();
        }
    }

    public void addLiquid(String name, int station, int pump, float density, float stock, float min, float max, Date date) {
//        if(Arrays.stream(Liquids).filter(x-> x.Name.equals(name)).findAny().isPresent())
//            throw new InputMismatchException("can only accept unique nanmes for liquids");

        if(station <0 && station>1)
            throw new InputMismatchException("there is only liquid station 1 (0) and liquid station 2 (1)");

        if(pump <0 && pump>6)
            throw new InputMismatchException("Each liquid station has 7 pumps (0-6)");

        if(Arrays.stream(Liquids).filter(x-> x.getStationID() == station && x.getPumpID() == pump).findAny().isPresent())
            throw new InputMismatchException("There can only be 1 liquid for each pump");

        if(density <=0d)
            throw new InputMismatchException("density can not be negative");

        if(stock <=0d)
            throw new InputMismatchException("stock can not be negative");
        if(max > 5 || max < 0f)
            throw new InputMismatchException("max 0-5mg");
        if(min <0 || min > 5)
            throw new InputMismatchException("min 0-5mg");
        synchronized (StateLock) {
            Liquid new_liquid = new Liquid(name, density, pump, stock, station,min,max, date );

            List<Liquid> new_liquids = Arrays.stream(Liquids).collect(Collectors.toList());
            new_liquids.add(new_liquid);
            Liquids = new_liquids.toArray(new Liquid[0]);
            WriteLiquids();
        }
    }

    public void DeleteLiquid(Liquid liquid) {
        synchronized (StateLock) {
            List<Liquid> new_liquids = Arrays.stream(Liquids).collect(Collectors.toList());
            new_liquids.remove(liquid);
            Liquids = new_liquids.toArray(new Liquid[0]);
            WriteLiquids();
        }
    }

    public void RestockLiquid(Liquid liquid, float new_stock, int days_till_expired) {
        synchronized (StateLock) {
            List<Liquid> liquids = Arrays.stream(Liquids).collect(Collectors.toList());
            Optional<Liquid >liquid_instance = liquids.stream().filter(x -> x.getName().equals(liquid.getName())).findFirst();
            if(liquid_instance.isPresent()) {
                liquid_instance.get().setMinimalVolumePreset(new_stock, days_till_expired);
                if(liquid_instance.get().getStationID() >= 9000)
                    liquid_instance.get().setStationID(liquid_instance.get().getStationID() - 9000);

                if(liquid_instance.get().getPumpID() >= 9000)
                    liquid_instance.get().setPumpID(liquid_instance.get().getPumpID() - 9000);


                WriteLiquids();
            }
        }
    }

    public void SetPhotolysisStopTime(Date date) {
        this.PhotolysisStopTime = date;
    }

    public Date getPhotolysisStopTime() {
        return PhotolysisStopTime;
    }

    public Solid[] getUniqueSolids() {
        Solid[] solids = getSolids();

        List<Solid> result = new LinkedList<>();
        for(Solid solid : solids)
        {
            if(result.stream().filter(x -> x.Name.equals(solid.getName())).findFirst().isPresent())
            {}
            else {
                result.add(solid);
            }
        }
        return result.toArray(new Solid[0]);
    }

    public Liquid[] getUniqueLiquids() {
        Liquid[] liquids = getLiquids();

        List<Liquid> result = new LinkedList<>();
        for(Liquid liquid : liquids)
        {
            if(result.stream().filter(x -> x.Name.equals(liquid.getName())).findFirst().isPresent())
            {}
            else {
                result.add(liquid);
            }
        }
        return result.toArray(new Liquid[0]);
    }

    public void SetRemainingDosages(Solid solid, int remainingDosages) {
          synchronized (StateLock) {
              solid.SetRemainingDosages(remainingDosages);
              WriteSolids();
          }

    }

    public void RemoveLiquid(Liquid liquid) {
        synchronized (StateLock) {
            liquid.setStationID(liquid.getStationID() + 9000);

            WriteLiquids();
        }
    }
}
