package Workflow;

import Workflow.Batch.*;
import Workflow.Configuration.Configuration;
import Workflow.Exception.CompoundNotPResentException;
import Workflow.Exception.InconsistentStateException;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CollectData {

    public static void main(String[] args) throws Exception {
        WorkflowSystem workflowSystem = new WorkflowSystem(false,false);
//        Solid[] p10 = Arrays.stream(workflowSystem.getState().getSolids()).toArray(Solid[]::new);
//        for(Solid p10s : p10)
//            System.out.println(p10s.getName() + workflowSystem.getState().getRackAtLocation(BatchLocation.SolidDispenseStation_Manipulation).NeedToDispenseSolid(p10s));
        CreateOverview(workflowSystem,"completed");
    }

    public static void CreateOverview(WorkflowSystem workflowSystem, String folder) throws IOException, ClassNotFoundException, ParseException, CompoundNotPResentException, InconsistentStateException {
        try {
            Configuration.loadSettings();
        } catch (FileNotFoundException e) {
            Configuration.saveUserSettings();
        }

        Batch[] batches_in_system;


        WorldState state = workflowSystem.getState();
        //List files in //runqueuefolder

        Batch batch = null;
        File dir = new File(folder+"\\.");
        File[] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".run");
            }
        });

        boolean hasBatchesWaiting = false;
        List<Batch> result = new LinkedList<>();
        if(files!=null)
        for (File batchfile : files) {
            //TODO filter based on solids liquids present?
            batch = Batch.ReadFromFile(state.getSolids(), state.getLiquids(), batchfile);
            result.add(batch);
            hasBatchesWaiting = true;
        }

        if (hasBatchesWaiting)
            ContructFile(result,workflowSystem, folder);
    }

    private static void ContructFile(List<Batch> result, WorkflowSystem workflowSystem, String folder) throws IOException, InconsistentStateException {
        File new_file = new File(folder + "\\overview.csv");
        File new_file_solid = new File(folder + "\\overview_solid.csv");
        File new_file_liquid = new File(folder + "\\overview_liquid.csv");

        for(File file : new File[] {new_file, new_file_liquid, new_file_solid}) {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(new_file);
        PrintWriter writer_solids = new PrintWriter(new_file_solid);
        PrintWriter writer_liquids = new PrintWriter(new_file_liquid);

        //Write columns
        String[] columns = new String [] {
//                "batch_name",
        "Name", "SampleIndex", "SampleNumber","comment","vial_capped","gc_well_number",
                "hydrogen_evolution","oxygen_evolution","hydrogen_evolution_micromol","oxygen_evolution_micromol","internal_hydrogen_standard", "weighted_hydrogen_micromol","sample_location_weight","weighted_is_sl_hydrogen_evolution_micromol","number_of_controls_in_batch",
                "sonication_time_min","illumination_time_min","gc_method", "temperature photolysis start","temperature photolysis end","purge_time","cap_vials",
   };

  writer.write("batch_name");
        for(String column : columns) {
            writer.write(",");
            writer.write(column);

        }

        Solid[] solids = workflowSystem.getState().getUniqueSolids();
        Liquid[] liquids = workflowSystem.getState().getUniqueLiquids();

        for(Solid solid : solids) {
            writer.write(",");
            writer.write(solid.getName() + "_to_dispense");

        }

        for(Liquid liquid : liquids) {
            writer.write(",");
            writer.write(liquid.getName() + "_to_dispense");

        }

        for(Solid solid : solids) {
            writer.write(",");
            writer.write(solid.getName() + "_dispensed");

        }

        for(Liquid liquid : liquids) {
            writer.write(",");
            writer.write(liquid.getName() + "_dispensed");

        }
        BatchTimestampEnum[]timestampEnums = BatchTimestampEnum.values();
        BatchTimestampType[]timestampTypes = BatchTimestampType.values();
        for( BatchTimestampEnum timestampEnum : timestampEnums)
            for(BatchTimestampType batchTimestampType : timestampTypes)
            {
                writer.write(",");
                writer.write(String.format("%s_%s",timestampEnum.name(), batchTimestampType.name()));

            }
        writer.write("\r\n");

        for(Batch batch : result)
        {
            if(batch.getBatch_name().equals("hypothesis-0221"))
            {
                System.out.println("");
            }
            int number_of_controls = 0;
            try {
                number_of_controls = batch.CalculateWeightedHydrogen();
            } catch (Exception exception)
            {}
            for (Sample sample : batch.getSamples())
            {

                writer.write(batch.getBatch_name());
                writer.write(",");
                writer.write(sample.getName());
                writer.write(",");
                writer.write(sample.getSampleIndex()+"");
                writer.write(",");
                writer.write(sample.getSampleNumber()+"");
                writer.write(",");
                writer.write("\"" + batch.getComment() + "\"");
                writer.write(",");
                writer.write(sample.getCapped()?"1":"0");
                writer.write(",");
                writer.write(sample.getGC_well_number()!=null?sample.getGC_well_number()+"":"");
                writer.write(",");
                writer.write(sample.getHydrogen_evolution()!=null?sample.getHydrogen_evolution()+"":"");
                writer.write(",");
                writer.write(sample.getOxygen_evolution()!=null?sample.getOxygen_evolution()+"":"");
                writer.write(",");
                writer.write(sample.getHydrogen_evolution_micromol()!=null?sample.getHydrogen_evolution_micromol()+"":"");
                writer.write(",");
                writer.write(sample.getOxygen_evolution_micromol()!=null?sample.getOxygen_evolution_micromol()+"":"");
                writer.write(",");
                writer.write(sample.getInternalHydrogenStandard()!=null?sample.getInternalHydrogenStandard()+"":"");
                writer.write(",");
                writer.write(sample.getWeightedHydrogenMicromol()!=null?sample.getWeightedHydrogenMicromol()+"":"");
                writer.write(",");
                writer.write(sample.getSampleLocationWeight()!=null?sample.getSampleLocationWeight()+"":"");
                writer.write(",");
                writer.write(sample.getWeightedISSLHydrogenMicromol()!=null?sample.getWeightedISSLHydrogenMicromol()+"":"");
                writer.write(",");
                writer.write(number_of_controls + "");
                writer.write(",");


                writer.write(batch.getSonication_time_min()+"");
                writer.write(",");
                writer.write(batch.getIllumination_time_min()+"");
                writer.write(",");
                writer.write(batch.getGc_method());
                writer.write(",");
                writer.write(batch.getTemperature_Photolysis_end()!=null?batch.getTemperature_Photolysis_end()+"":"");
                writer.write(",");
                writer.write(batch.getTemperature_Photolysis_start()!=null?batch.getTemperature_Photolysis_start()+"":"");
                writer.write(",");

                writer.write(batch.getPurge_time_sec()+"");
                writer.write(",");
                writer.write(batch.getCap_vials()?"1":"o");

                for(Solid solid : solids) {
                    writer.write(",");
                    writer.write(sample.getSolidDispenseAmount(solid)!=null?sample.getSolidDispenseAmount(solid)+"":"0");
                }

                for(Liquid liquid : liquids) {
                    writer.write(",");
                    writer.write(sample.getLiquidDispenseAmount(liquid)!=null?sample.getLiquidDispenseAmount(liquid)+"":"0");
                }

                for(Solid solid : solids) {
                    writer.write(",");
                    writer.write(sample.getSolidDispensed(solid)!=null?sample.getSolidDispensed(solid)+"":"0");
                }

                for(Liquid liquid : liquids) {
                    writer.write(",");
                    writer.write(sample.getLiquidDispensed(liquid)!=null?sample.getLiquidDispensed(liquid)+"":"0");
                }

                for(Solid solid : solids) {
                    if(sample.getSolidDispenseAmount(solid)!=0 && sample.getSolidDispenseAmount(solid)!=0) {
                        writer_solids.write(sample.getSolidDispenseAmount(solid) != null ? sample.getSolidDispenseAmount(solid) + "" : "0");
                        writer_solids.write(",");
                        writer_solids.write(sample.getSolidDispensed(solid) != null ? sample.getSolidDispensed(solid) + "" : "0");
                        writer_solids.write("\r\n");
                        writer_solids.flush();
                    }

                }

                for(Liquid liquid : liquids) {
                    if(sample.getLiquidDispenseAmount(liquid)!=0 && sample.getLiquidDispensed(liquid)!=0) {
                        writer_liquids.write(sample.getLiquidDispenseAmount(liquid) != null ? sample.getLiquidDispenseAmount(liquid) + "" : "0");
                        writer_liquids.write(",");
                        writer_liquids.write(sample.getLiquidDispensed(liquid) != null ? sample.getLiquidDispensed(liquid) + "" : "0");
                        writer_liquids.write("\r\n");
                        writer_liquids.flush();
                    }

                }


                for( BatchTimestampEnum timestampEnum : timestampEnums)
                    for(BatchTimestampType batchTimestampType : timestampTypes)
                    {
                        writer.write(",");
                        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        writer.write(batch.getTimestamp(timestampEnum,batchTimestampType)!=null?df2.format(batch.getTimestamp(timestampEnum,batchTimestampType)): "");
                    }

                writer.write("\r\n");
                writer.flush();


            }
        }

        writer.close();
        writer_liquids.close();
        writer_solids.close();

    }
}
