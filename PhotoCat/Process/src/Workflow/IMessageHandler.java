package Workflow;

/**
 * Created by Benjamin on 9/6/2018.
 */
public interface IMessageHandler {
    public void HandleException(ExceptionSource source, Exception e);

    void WriteMessage(String s);

    enum ExceptionSource {
        UIAction,
        UpdateUIEvent,
        RobustnessTest, //Special type of UI event, one that has its own thread....

        GenericRobotProcess,    //We should only see this incase we forget to set it somewhere
        GenericProcess,         //We should only see this incase we forget to set it somewhere

        //These are shared between ExceptionHandler
        PickUpNewRacksProcess,
        SolidDispensingProcess,
        LiquidDispenseProcess,
        CappingProcess,
        SonicatorProcess,
        PhotolysisProcess,
        GCProcess,
        AlertProcess,
        Logger
    }
}
