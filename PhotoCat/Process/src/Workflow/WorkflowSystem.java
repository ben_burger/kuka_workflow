package Workflow;

import Workflow.AlertSystem.AlertSystem;
import Workflow.Batch.Batch;
import Workflow.Batch.Sample;
import Workflow.Exception.CompoundNotPResentException;
import Workflow.Com.ComSuper;
import Workflow.Com.Serial.*;
import Workflow.Com.TCP.KMRCom;
import Workflow.Com.Serial.Quantos.QuantosCom;
import Workflow.Com.TCP.LBRCom;
import Workflow.Com.TCP.GCCom;
import Workflow.Configuration.ComStation;
import Workflow.Configuration.Configuration;
import Workflow.Exception.DeviceOfflineException;
import Workflow.Exception.InconsistentStateException;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.ProcessManagement.Process.ScheduleHeuristic.ScheduleAlgorithm;
import Workflow.WorldState.WorldState;
import com.github.sarxos.webcam.Webcam;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.imageio.ImageIO;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Benjamin on 3/8/2018.
 */
public class WorkflowSystem {
    private VibratoryCom VibratoryCom;
    private QuantosCom QuantosCom;
    private KernScaleCom ScaleCom;
    private AdruinoPump LiquidCom;
    private ArdruinoCom PhotoCom;
    private ArdruinoCom SonicatorCom;
    private CapperCom CapperCom;
    private KMRCom KMRCom;
    private KernScaleCom ScaleCom2;
    private AdruinoPump LiquidCom2;
    private Workflow.Com.TCP.LBRCom LBRCom;
    private GCCom GCCom;
    private IMessageHandler MessageHandler;
    private WorldState State;
    private ProcessManager ProcessManager;
    private Workflow.AlertSystem.AlertSystem AlertSystem;
    public Webcam[] webcam;
    private TemperatureProbe ProbeCom;

    public TemperatureProbe getProbeCom() throws DeviceOfflineException {
        if(ProbeCom != null && ProbeCom.isAlive())
        {
            return ProbeCom;
        }
        else
        {
            throw new DeviceOfflineException("ProbeCom");
        }
    }

    public GCCom getGCCom() throws DeviceOfflineException {
        if(GCCom != null && GCCom.isAlive())
        {
            return GCCom;
        }
        else
        {
            throw new DeviceOfflineException("GCCom");
        }
    }

    private KernScaleCom getScaleCom2() throws DeviceOfflineException {
        if(ScaleCom2 != null && ScaleCom2.isAlive())
        {
            return ScaleCom2;
        }
        else
        {
            throw new DeviceOfflineException("ScaleCom2");
        }
    }

    public void ConnectScale2() throws Exception {
        if (ScaleCom2 != null)
            ScaleCom2.Destruct();
        ScaleCom2 = new KernScaleCom(Configuration.getScalePort2());
    }

    private AdruinoPump getLiquidCom2() throws DeviceOfflineException {
        if(LiquidCom2 != null && LiquidCom2.isAlive())
        {
            return LiquidCom2;
        }
        else
        {
            throw new DeviceOfflineException("LiquidCom2");
        }
    }

    public void ConnectLiquid2() throws Exception {
        if (LiquidCom2 != null)
            LiquidCom2.Destruct();
        LiquidCom2 = new AdruinoPump(Configuration.getLiquidPumpPort2());
    }

public WorkflowSystem() throws IOException, ParseException, CompoundNotPResentException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, InconsistentStateException
    { this(true,true);

}

    public WorkflowSystem(boolean log, boolean webcams) throws IOException, ParseException, CompoundNotPResentException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, InconsistentStateException {
        if (log) {
            DealWithLog();
            CheckFolderExistAndCreate("log\\");
            CheckFolderExistAndCreate("completed\\");
            CheckFolderExistAndCreate("running\\");
            CheckFolderExistAndCreate("runqueue\\");
            CheckFolderExistAndCreate("pictures\\");
            CheckFolderExistAndCreate("conditional\\");
        }
        if(webcams)
        {
            //(new Thread(new Runnable(){
              //  public void run(){
                    webcam = new Webcam[2];
                    for(int i=0;i<2;i++)
                    {
                        webcam[i] = Webcam.getWebcams().get(i);
                        webcam[i].open();
                    }
            //    }
            //})).start();
        }
        State = new WorldState();
        if (log)
        {
            ProcessManager = new ProcessManager(this, ScheduleAlgorithm.SingleBatch);
        AlertSystem = new AlertSystem(this);
        }
    }

    private void DealWithLog() throws IOException {
        Path path = Paths.get("log\\");
        if (!Files.exists(path)) {

            Files.createDirectory(path);
        } else
        {
            //Create new log folder
            path = Paths.get("log\\log\\");
            if (!Files.exists(path)) {

                Files.createDirectory(path);
            }
            DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
            String old_log_folder = String.format("log\\log\\%s\\",df2.format(new Date()));
            path = Paths.get(old_log_folder);
            if (!Files.exists(path)) {

                Files.createDirectory(path);
            }
            //MoveOldLogFiles

            File dis = new File("log\\");
            File[] files = dis.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".log");
                }
            });

            for (File batchfile : files) {
                Path temp = Files.move
                        (Paths.get(batchfile.getAbsolutePath()),
                                Paths.get(old_log_folder + batchfile.getName()));
            }

        }

        CheckFolderExistAndCreate("log\\");
    }

    private void CheckFolderExistAndCreate(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        if (!Files.exists(path)) {

            Files.createDirectory(path);
        }
    }


    public QuantosCom getQuantosCom () throws DeviceOfflineException {
        if(QuantosCom != null && QuantosCom.isAlive())
            return QuantosCom;
        else
            throw new DeviceOfflineException("Quantos");
    }

    public CapperCom getCapperCom () throws DeviceOfflineException {
        if(CapperCom != null && CapperCom.isAlive())
        {
            return CapperCom;
        }
        else
        {
            throw new DeviceOfflineException("Capper");
        }
    }
    public KernScaleCom getScaleCom(int stationId ) throws DeviceOfflineException {
        if(stationId==0)
            return getScaleCom();
        else return getScaleCom2();
    }

        private KernScaleCom getScaleCom() throws DeviceOfflineException {
        if(ScaleCom != null && ScaleCom.isAlive())
            return ScaleCom;
        else
            throw new DeviceOfflineException("Scale");
    }

    private AdruinoPump getLiquidCom() throws DeviceOfflineException {
        if(LiquidCom != null && LiquidCom.isAlive())
            return LiquidCom;
        else
            throw new DeviceOfflineException("LiquidStation");
    }

    public AdruinoPump getLiquidCom(int stationId) throws DeviceOfflineException {
        if(stationId==0)
            return getLiquidCom();
        else return getLiquidCom2();
    }

    public ArdruinoCom getPhotoCom() throws DeviceOfflineException {
        if(PhotoCom != null && PhotoCom.isAlive())
            return PhotoCom;
        else
            throw new DeviceOfflineException("Photocat");

    }

    public ArdruinoCom getSonicatorCom() throws DeviceOfflineException {
        if(SonicatorCom != null && SonicatorCom.isAlive())
            return SonicatorCom;
        else
            throw new DeviceOfflineException("Sonicator");
    }

    public void ConnectSonicator() throws Exception {
        if (SonicatorCom != null)
            SonicatorCom.Destruct();
        SonicatorCom = new ArdruinoCom(Configuration.getSonicatorPort(),"sonicator");
    }

    public void ConnectPhoto() throws Exception {
        if (PhotoCom != null)
            PhotoCom.Destruct();
        PhotoCom = new ArdruinoCom(Configuration.getPhotoCatPort(),"magnetic-photocat");
    }

    public void ConnectCapper() throws Exception {
        if (CapperCom != null)
            CapperCom.Destruct();
        CapperCom = new CapperCom(Configuration.getCapperPort());
    }

    public void ConnectScale() throws Exception {
        if (ScaleCom != null)
            ScaleCom.Destruct();
        ScaleCom = new KernScaleCom(Configuration.getScalePort());
    }

    public void ConnectLiquid() throws Exception {
        if (LiquidCom != null)
            LiquidCom.Destruct();
        LiquidCom = new AdruinoPump(Configuration.getLiquidPumpPort());
    }

    public void ConnectQuantos() throws Exception {
        if (QuantosCom != null)
            QuantosCom.Destruct();
        QuantosCom = new QuantosCom(Configuration.getQuantosPort());
    }

    public void ConnectKMR() throws Exception {
        if (KMRCom != null) {
            KMRCom.Destruct();
        }

        if(LBRCom != null)
        {
            LBRCom.Destruct();
        }

        KMRCom = new KMRCom();

        LBRCom = new LBRCom();
    }
    public void ConnectGC() throws Exception {
        if (GCCom != null) {
            GCCom.Destruct();
        }

        if(GCCom != null)
        {
            GCCom.Destruct();
        }

        GCCom = new GCCom();
    }


      public KMRCom getKMRCom() throws DeviceOfflineException {
          if (KMRCom != null && KMRCom.isAlive())
              return KMRCom;
          else {
              int tries = 0;
              while (tries < 15) {
                  try {
                      Thread.sleep(tries * 2000 + 5000);
                  } catch (Exception e) {

                  }
                  if (KMRCom.isAlive())
                      return KMRCom;
                  tries++;
              }
              throw new DeviceOfflineException("KMRCom");
          }
      }

    @Deprecated
    public KMRCom getKMRComNoCheck() throws DeviceOfflineException {
            return KMRCom;
      }


    public LBRCom getLBRCom() throws DeviceOfflineException {

        if(LBRCom != null && LBRCom.isAlive())
            return LBRCom;
        else
            throw new DeviceOfflineException("LBRCom");
    }
    public VibratoryCom getVibratoryCom() throws DeviceOfflineException {

        if(VibratoryCom != null && VibratoryCom.isAlive())
            return VibratoryCom;
        else
            throw new DeviceOfflineException("VibratoryCom");
    }


    @Deprecated
    public LBRCom getLBRComNoCheck() throws DeviceOfflineException {

        return LBRCom;
    }

    public WorldState getState() {
        return State;
    }

    public void SetMessageHandler(IMessageHandler messageHandler) {
        MessageHandler = messageHandler;
    }

    public IMessageHandler getMessageHandler() {
        return MessageHandler;
    }

    public ComSuper getComNoCheck(ComStation station) throws DeviceOfflineException {
        ComSuper COM;
        switch (station) {
            case VibratoryPhotolysis:
                COM = VibratoryCom;
                break;
            case Capper:
                COM = CapperCom;
                break;
            case Quantos:
                COM = QuantosCom;
                break;
            case Scale:
                COM = ScaleCom;
                break;
            case Liquid:
                COM = LiquidCom;
                break;
            case Photo:
                COM = PhotoCom;
                break;
            case KMR:
                COM = KMRCom;
                break;
            case Scale2:
                COM = ScaleCom2;
                break;
            case Liquid2:
                COM = LiquidCom2;
                break;
            case Sonicator:
                COM = SonicatorCom;
                break;
            case GC:
                COM = GCCom;
                break;
            default:
                throw new NotImplementedException();
        }
        return COM;
    }

    public Workflow.ProcessManagement.ProcessManager getProcessManager() {
        return ProcessManager;
    }

    public void ConnectVibratory() throws Exception {
        if(VibratoryCom != null)
        {
            VibratoryCom.Destruct();
        }

        VibratoryCom = new VibratoryCom(Configuration.getVibratoryPhotoCatPort());
    }

    public Workflow.AlertSystem.AlertSystem getAlertSystem() {
        return AlertSystem;
    }

    public void ConnectProbe() throws IOException, InterruptedException {
        if(ProbeCom != null)
        {
            ProbeCom.Destruct();
        }

        ProbeCom = new TemperatureProbe(Configuration.getProbePort());
    }

    public void TakePicture(int i,Batch batch, Sample sample, String s) throws IOException {
        synchronized (webcam[i]) {
            File batch_folder = new File(String.format("pictures\\%s\\", batch.getBatch_name()));
            batch_folder.mkdirs();
            File f = new File(String.format("pictures\\%s\\%s-%s.png", batch.getBatch_name(), sample.getName(), s));

            ImageIO.write(webcam[i].getImage(), "PNG", f);
        }
    }

    public boolean CheckCameraStatus(int i)
    {
        try {
            synchronized (webcam[i]) {
                return webcam[i].getDevice().isOpen();
            }
        } catch (Exception e)
        {
            return false;
        }
    }
}
