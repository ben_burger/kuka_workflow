package Workflow;
import Workflow.Configuration.Configuration;
import Workflow.Exception.DeviceOfflineException;
import Workflow.Batch.Liquid;
import Workflow.Util.LinearRegression;
import Workflow.Util.Tuple;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by taurnist on 01/08/2017.
 */
public class LiquidDispensingProcessManager {
    private final WorkflowSystem workflowSystem;
    List<Tuple<String,Tuple<Float,Float>>> Fit_values = new LinkedList<Tuple<String,Tuple<Float,Float>>>();
    int stationID;

    public LiquidDispensingProcessManager(WorkflowSystem workflowSystem, int stationID) {
        super();
        this.workflowSystem = workflowSystem;
        this.stationID = stationID;
    }

    //returns amount dispensed (mL)
    public Tuple<Float,Float> DispenseLiquidNoScale(Liquid liquid, float amount_ml) throws InterruptedException, DeviceOfflineException {
        //Retrieve water.fit
        //save values to some property
        // use said values to dispense:
        List<Tuple<String,Tuple<Float,Float>>> fits = Fit_values.stream().filter(tuple -> ((String)tuple.item1).equals(liquid.Name)).collect(Collectors.toList());

        if(fits.isEmpty())
        {
            //Read Liquid.fit
            //save to tuple


            FileReader fileReader = null;
            try {
                fileReader = new FileReader(liquid.Name + ".fit");
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String[] line = bufferedReader.readLine().split(",");
                Fit_values.add(
                        new Tuple<String, Tuple<Float,Float>>(
                                liquid.Name,
                                new Tuple<Float, Float>(
                                        Float.parseFloat(line[0]),
                                        Float.parseFloat(line[1])
                                )
                        )
                );



            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            fits = Fit_values.stream().filter(tuple -> ((String)tuple.item1).equals(liquid.Name)).collect(Collectors.toList());
        }
        float slope, intersect;
        slope = fits.get(0).item2.item1;
        intersect = fits.get(0).item2.item2;
        return new Tuple(DispenseLiquidTimeAndMeasure(1,Math.round(amount_ml * slope + intersect)),amount_ml * slope + intersect) ;
    }

    public void DispenseLiquidTime(int pumpId, int sleep_mili) throws DeviceOfflineException, InterruptedException {
        workflowSystem.getLiquidCom(stationID).SetRate(200);
        workflowSystem.getLiquidCom(stationID).Start(pumpId);
        Thread.sleep(sleep_mili);
        workflowSystem.getLiquidCom(stationID).SetRate(0);
        workflowSystem.getLiquidCom(stationID).Stop();
    }

    public float DispenseLiquidTimeAndMeasure(int pumpId, int sleep_mili) throws InterruptedException, DeviceOfflineException {
        float weight_start = workflowSystem.getScaleCom(stationID).getStableWeight();
        DispenseLiquidTime(pumpId, sleep_mili);

        Thread.sleep(5000);

        return workflowSystem.getScaleCom(stationID).getStableWeight() - weight_start;
    }

    public float DispenseLiquidPController(int pumpId, float target) throws InterruptedException, DeviceOfflineException {






        float weight_start = workflowSystem.getScaleCom(stationID).getStableWeight();

        //because we reverse the liquid a little bit, just prime it a bit for small volumes
        workflowSystem.getLiquidCom(stationID).Forward();
        Thread.sleep(100);
        workflowSystem.getLiquidCom(stationID).SetRate(40);
        Thread.sleep(100);
        workflowSystem.getLiquidCom(stationID).Start(pumpId);
        Thread.sleep(1500 );
        workflowSystem.getLiquidCom(stationID).Stop();
        Thread.sleep(100);

        workflowSystem.getLiquidCom(stationID).SetRate(0);
        // Turns out a P controller is enough for a fisher scale (Fast Response)
        // But for the cheaper scale from RS, which responds slower, we need an I term
        float konstant_P = workflowSystem.getLiquidCom(stationID).GetP();
        float konstant_I = workflowSystem.getLiquidCom(stationID).GetI();
        float konstant_D = workflowSystem.getLiquidCom(stationID).GetD();

        int number_of_points =workflowSystem.getLiquidCom(stationID).GetNumberOfPoints();
        int index_points = 0;

        float[] error = new float[number_of_points];
        float current_error = target - (workflowSystem.getScaleCom(stationID).getWeight()- weight_start);
        float sum = 0;
        boolean cont = true;
        workflowSystem.getLiquidCom(stationID).Forward();

        Thread.sleep(100);
        while(cont) {
            workflowSystem.getLiquidCom(stationID).Start(pumpId);
            long stamp = System.currentTimeMillis();

            sum = sum - error[index_points];
            error[index_points] = current_error;
            sum = sum + current_error;
            index_points = (index_points +1) % number_of_points;


            int rate =Math.round ( konstant_P * current_error
                    + konstant_I * sum
                    + konstant_D * (current_error - error[(index_points +(number_of_points-1)) % number_of_points])
            );

            rate = Math.min(100,Math.max(12, rate));

            workflowSystem.getLiquidCom(stationID).SetRate(rate);
            waitUntil(stamp + 200);
            current_error = target - (workflowSystem.getScaleCom(stationID).getWeight()- weight_start);
            if(!NotWithinRange(current_error)) {
                workflowSystem.getLiquidCom(stationID).Stop();
//                current_error = target - (ScaleCom.getWeight()- weight_start); //TEMP CHANGE IN ABSENCE OF DAC
                current_error = target - (workflowSystem.getScaleCom(stationID).getStableWeight()- weight_start);
                cont = NotWithinRange(current_error);
            }
        }
        workflowSystem.getLiquidCom(stationID).Stop();

        //Now reverse the liquid a little bit.
        workflowSystem.getLiquidCom(stationID).Reverse();
        Thread.sleep(100);
        workflowSystem.getLiquidCom(stationID).SetRate(40);
        Thread.sleep(100);
        workflowSystem.getLiquidCom(stationID).Start(pumpId);
        Thread.sleep(2000);
        workflowSystem.getLiquidCom(stationID).Stop();

        return (workflowSystem.getScaleCom(stationID).getStableWeight() - weight_start);



    }

    public static void waitUntil(long timestamp) {
        long millis = timestamp - System.currentTimeMillis();
        // return immediately if time is already in the past
        if (millis <= 0)
            return;
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private boolean NotWithinRange(float error) {
        return error>0.05;
    }






    private static double calculateAverage(double[] marks) {
        Double sum = 0d;
        if(marks.length != 0) {
            for (Double mark : marks) {
                sum += mark;
            }
            return sum / marks.length;
        }
        return sum;
    }
    public static double sd (double[]  table) {
        // Step 1:
        double mean = calculateAverage(table);
        double temp = 0;

        for (int i = 0; i < table.length; i++)
        {
            double val = table[i];

            // Step 2:
            double squrDiffToMean = Math.pow(val - mean, 2);

            // Step 3:
            temp += squrDiffToMean;
        }

        // Step 4:
        double meanOfDiffs = (double) temp / (double) (table.length);

        // Step 5:
        return Math.sqrt(meanOfDiffs);
    }


    private static double calculateAveragea(List <Double> marks) {
        Double sum = 0d;
        if(!marks.isEmpty()) {
            for (Double mark : marks) {
                sum += mark;
            }
            return sum / marks.size();
        }
        return sum;
    }
    private static double sda(List<Double> table)  {
        // Step 1:
        double mean = calculateAveragea(table);
        double temp = 0;
        Object[] tab =  table.toArray();

        for (int i = 0; i < table.size(); i++)
        {
            double val = (Double) tab[i];

            // Step 2:
            double squrDiffToMean = Math.pow(val - mean, 2);

            // Step 3:
            temp += squrDiffToMean;
        }

        // Step 4:
        double meanOfDiffs = (double) temp / (double) (table.size());

        // Step 5:
        return Math.sqrt(meanOfDiffs);
    }

    public void LiquidHandlingSystemNoScaleTest(Liquid liquid, int samples, String location) throws DeviceOfflineException, IOException, InterruptedException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(location +"-"+ liquid.Name + "-" + samples +  "-AutomatedMeasurementNoScale.csv"))) {
            StringBuilder sb = new StringBuilder();
            sb.append("amount dispensed");
            sb.append(',');
            sb.append("average (mL)");
            sb.append(',');
            sb.append("st dev. (mL)");
            sb.append("\r\n");
            float [] amounts = new float [] {0.5f,1.0f,2.5f,5.0f};
            for (float amount: amounts) {

                double [] results = DispenseLiquidHandlingSystemNoScaleTest(amount , samples,liquid,location);
                sb.append(String.format("%.02f",amount));
                sb.append(',');
                sb.append(String.format("%.02f",calculateAverage(results)));
                sb.append(',');
                sb.append(String.format("%.02f",sd(results)));
                sb.append("\r\n");
            }
            bw.write(sb.toString());
            bw.flush();
            bw.close();
        }
    }
    private double[] DispenseLiquidHandlingSystemNoScaleTest(float  amount_ml, int samples, Liquid liquid, String location) throws InterruptedException, IOException, DeviceOfflineException {
        double[] results = new double[samples];
        double time = 0d;

        for(int i=0;i<samples;i++)
        {
            Tuple<Float,Float> data = DispenseLiquidNoScale(liquid,amount_ml);
            results[i] = data.item1;
            time = data.item2;
        }

        //Write to CSV
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(location +"-"+ liquid.Name + "-" + amount_ml + "-" + samples +  "-AutomatedMeasurementNoScale.csv")))
        {
            StringBuilder sb = new StringBuilder();

            sb.append("measurement ID");
            sb.append(',');
            sb.append("Dispense aim");
            sb.append(',');
            sb.append("Dispensed amount");
            sb.append(',');
            sb.append("Dispense time");
            sb.append("\r\n");
            bw.write(sb.toString());
            for(int i=0;i<samples;i++)
            {
                StringBuilder sb2 = new StringBuilder();

                sb2.append(i);
                sb2.append(',');
                sb2.append(String.format("%.02f",amount_ml));
                sb2.append(',');
                sb2.append(String.format("%.02f",results[i]/liquid.Density));
                sb2.append(',');
                sb2.append(time);
                sb2.append("\r\n");

                bw.write(sb2.toString());
            }

            bw.flush();
            bw.close();
        }
        return results;
    }

    public void LiquidHandlingSystemLeastSquares(Liquid liquid) throws IOException {
        String name = liquid.Name;
        StringBuilder data_set_overview = new StringBuilder();
        List<Double> x = new LinkedList<Double>();
        List<Double> y = new LinkedList<Double>();
        File dir = new File(".");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.getName().contains("FitData.csv") && child.getName().contains(name)) {

                    FileReader fileReader = new FileReader(child);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);

                    List<Double> x_now = new LinkedList<Double>();
                    List<Double> y_now = new LinkedList<Double>();
                    String line;
                    line = bufferedReader.readLine();
                    while ((line = bufferedReader.readLine()) != null) {
                        String[] data = line.split(",");
                        x_now.add(Double.parseDouble(data[2])); //how much to dispense
                        y_now.add(Double.parseDouble(data[1])); //time to dispense
                    }


                    String[] split = child.getName().split("-");
                    int time = Integer.parseInt(split[2]);

                    data_set_overview.append(time);
                    data_set_overview.append(",");

                    double average = calculateAveragea(x_now);
                    data_set_overview.append(average);
                    data_set_overview.append(",");
                    double sd = sda(x_now);
                    data_set_overview.append(sd);
                    data_set_overview.append("\r\n");

                    if(average > 0.4)
                    {
                        x.add(calculateAveragea(x_now));
                        y.add(Double.parseDouble(time+""));

                    }



                }
            }
        }

        LinearRegression model = new LinearRegression(x.stream().mapToDouble(f -> f != null ? f : Double.NaN).toArray(),
                y.stream().mapToDouble(f -> f != null ? (f/600d) : Double.NaN).toArray());

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(name + ".fit")))
        {
            bw.write(model.slope()*600d + ", " + model.intercept()*600d);
            bw.write("\r\n");
            bw.write("Intersect, slope");
            bw.write("\r\n");

            bw.write("");bw.write("\r\n");
            bw.write("");bw.write("\r\n");
            bw.write("dispense time, average mL dispensed, st dev. mL dispensed");
            bw.write("\r\n");
            bw.write(data_set_overview.toString());
            bw.flush();
            bw.close();
        }
    }

    public void LiquidHandlingSystemFitLineData(Liquid liquid, String location, Integer[] amounts ) throws DeviceOfflineException, IOException, InterruptedException {
        workflowSystem.getScaleCom(stationID).getWeight();

        int samples = 25;


        for (int amount :
                amounts) {
            DispenseLiquidHandlingSystemFitLineData(liquid,amount, samples, location);
        }


    }
    private void DispenseLiquidHandlingSystemFitLineData(Liquid mixture, int sleep_mili , int samples, String location) throws InterruptedException, IOException, DeviceOfflineException {
        double[] results = new double[samples];
        for(int i=0;i<samples;i++)
        {
            results[i] = DispenseLiquidTimeAndMeasure(mixture.getPumpID(),sleep_mili );
        }

        //Write to CSV
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(location +"-"+ mixture.Name + "-" + sleep_mili + "-" + samples + "-FitData.csv")))
        {
            StringBuilder sb = new StringBuilder();

            sb.append("measurement ID");
            sb.append(',');
            sb.append("Dispense aim");
            sb.append(',');
            sb.append("Dispensed amount mL");
            sb.append("\r\n");
            bw.write(sb.toString());
            for(int i=0;i<samples;i++)
            {
                StringBuilder sb2 = new StringBuilder();

                sb2.append(i);
                sb2.append(',');
                sb2.append(sleep_mili);
                sb2.append(',');
                sb2.append(String.format("%.02f",results[i]/mixture.Density));
                sb2.append("\r\n");

                bw.write(sb2.toString());
            }

            bw.flush();
            bw.close();
        }
    }

    public void LiquidHandlingSystemAutomatedTest(Liquid liquid,int samples,String location , float[] amounts) throws IOException, DeviceOfflineException, InterruptedException {

        String mixture = liquid.Name;

        float density = liquid.Density;


        try (BufferedWriter bw = new BufferedWriter(new FileWriter(location +"-"+ mixture + "-" + samples +  "-AutomatedMeasurement.csv"))) {
            StringBuilder sb = new StringBuilder();
            sb.append("amount dispensed");
            sb.append(',');
            sb.append("average (mL)");
            sb.append(',');
            sb.append("st dev. (mL)");
            sb.append("\r\n");

            for (float amount: amounts) {

                double [] results = DispenseLiquidHandlingSystemAutomatedTest(liquid.getPumpID(), mixture, amount , samples,density,location);
                sb.append(String.format("%.02f",amount));
                sb.append(',');
                sb.append(String.format("%.02f",calculateAverage(results)));
                sb.append(',');
                sb.append(String.format("%.02f",sd(results)));
                sb.append("\r\n");
            }
            bw.write(sb.toString());
            bw.flush();
            bw.close();
        }
    }
    private  double[] DispenseLiquidHandlingSystemAutomatedTest(int pumpId, String mixture, float  amount, int samples, float density, String location) throws InterruptedException, IOException, DeviceOfflineException {
        double[] results = new double[samples];


        for(int i=0;i<samples;i++)
        {
            results[i] = DispenseLiquidPController(pumpId, amount * density);
        }

        //Write to CSV
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(location +"-"+ mixture + "-" + amount + "-" + samples +  "-AutomatedMeasurement.csv")))
        {
            StringBuilder sb = new StringBuilder();

            sb.append("measurement ID");
            sb.append(',');
            sb.append("Dispense aim");
            sb.append(',');
            sb.append("Dispensed amount");
            sb.append("\r\n");
            bw.write(sb.toString());
            for(int i=0;i<samples;i++)
            {
                StringBuilder sb2 = new StringBuilder();

                sb2.append(i);
                sb2.append(',');
                sb2.append(String.format("%.02f",amount));
                sb2.append(',');
                sb2.append(String.format("%.02f",results[i]/density));
                sb2.append("\r\n");

                bw.write(sb2.toString());
            }


            bw.flush();
            bw.close();



        }
        return results;
    }



}

