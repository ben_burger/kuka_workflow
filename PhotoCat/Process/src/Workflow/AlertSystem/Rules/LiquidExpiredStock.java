package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Batch.Liquid;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

public class LiquidExpiredStock extends RuleSuper {

    LinkedList<Liquid> expired_stock;
    public LiquidExpiredStock() {
        super();
    }

    @Override
        protected boolean RunRule(WorkflowSystem WorkflowSystem) {
            WorldState state = WorkflowSystem.getState();
            LinkedList<Liquid> result = new LinkedList();

        for(Liquid liquid : state.getLiquids())
        {
            if(liquid.getExpiryDate().getTime() < (new Date().getTime())
                    && liquid.getStationID() < 9000) //We need to keep liquid somewhere that are not being used anymore
                result.add(liquid);
        }
        if(result.size() > 0)
        {
            expired_stock = result;
            return true;
        }
        return false;

    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        StringBuilder sb = new StringBuilder();
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        for(Liquid liq : expired_stock)
        {
            sb.append(String.format("Liquid Expired for liquid: %s (%s)\r\n",liq.Name, df2.format(liq.getExpiryDate())));
        }
        return new Alert(AlertType.StockAlert, sb.toString(),this);
    }
}
