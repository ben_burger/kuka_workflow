package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.WorkflowSystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CapStock extends RuleSuper {

    public CapStock() {
        super();
    }

    @Override
    protected boolean RunRule(WorkflowSystem WorkflowSystem) {
        return WorkflowSystem.getState().getMinimal_Caps_Present() < 50;
    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return new Alert(AlertType.StockAlert, String.format("Low stock level: caps (%d)",WorkflowSystem.getState().getMinimal_Caps_Present()),this);
    }
}
