//package Workflow.AlertSystem.Rules;
//
//import Workflow.AlertSystem.Alert;
//import Workflow.AlertSystem.AlertType;
//import Workflow.AlertSystem.RuleSuper;
//import Workflow.Batch.Batch;
//import Workflow.Batch.BatchStatus;
//import Workflow.Batch.BatchTimestampEnum;
//import Workflow.Batch.BatchTimestampType;
//import Workflow.Configuration.Configuration;
//import Workflow.Exception.CompoundNotPResentException;
//import Workflow.Exception.InconsistentStateException;
//import Workflow.TCP.RobotStatusEnum;
//import Workflow.TCP.TaskSuper;
//import Workflow.WorkflowSystem;
//import Workflow.WorldState.WorldState;
//import com.sun.javafx.image.BytePixelSetter;
//
//import java.io.File;
//import java.io.FilenameFilter;
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.LinkedList;
//import java.util.List;
//
//public class EmptyQueue extends RuleSuper {
//    int Maximal_Seconds_without_work = 10;
//    @Override
//    protected boolean RunRule(WorkflowSystem WorkflowSystem) throws Exception {
//        try {
//            if (!HasBatchesWaiting(WorkflowSystem)) {
//                Date start_waiting = new Date();
//                while ((start_waiting.getTime() + Maximal_Seconds_without_work * 1000 > (new Date()).getTime()) &&
//                        !HasBatchesWaiting(WorkflowSystem)) {
//                    if (HasBatchesWaiting(WorkflowSystem))
//                        break;
//                    Thread.sleep(100);
//                }
//                if (!HasBatchesWaiting(WorkflowSystem)) {
//                    return true;
//                }
//            }
//            return false;
//        } catch (CompoundNotPResentException e)
//        {
//            Exception  = e;
//            return true;
//        }
//    }
//
//    @Override
//    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
//        if(Exception == null)
//            return new Alert(AlertType.StockAlert, "Tantalus out of work. Is the optimiser not running?", this);
//        else
//            return new Alert(AlertType.StockAlert, Exception.getMessage(), this);
//    }
//
//    private boolean HasBatchesWaiting(WorkflowSystem workflowSystem) throws ParseException, IOException, CompoundNotPResentException, InconsistentStateException {
//
//        WorldState state = workflowSystem.getState();
//
//        Batch batch = null;
//        File dir = new File("runqueue\\.");
//        File[] files = dir.listFiles(new FilenameFilter() {
//            @Override
//            public boolean accept(File dir, String name) {
//                return name.endsWith(".run");
//            }
//        });
//
//        boolean hasBatchesWaiting = false;
//        List<Batch> result = new LinkedList<>();
//        for (File batchfile : files) {
//            //TODO filter based on solids liquids present?
//            batch = Batch.ReadFromFile(state.getSolids(), state.getLiquids(), batchfile);
//            if(batch.getGc_method() != null && !batch.getGc_method().equals(""))
//                if(!batch.getCap_vials())
//                    throw new InconsistentStateException("Something or someone is trying to measure GC without capping vials!!");
//            if(batch.getIllumination_time_min() != 0)
//                if(!batch.getCap_vials())
//                    throw new InconsistentStateException("Something or someone is trying to photolyse without capping vials!!");
//            if(batch.getSonication_time_min() != 0)
//                if(!batch.getCap_vials())
//                    throw new InconsistentStateException("Something or someone is trying to sonicate without capping vials!!");
//
//            result.add(batch);
//            hasBatchesWaiting = true;
//        }
//        return hasBatchesWaiting;
//    }
//}