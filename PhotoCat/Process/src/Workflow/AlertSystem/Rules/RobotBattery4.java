package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Exception.DeviceOfflineException;
import Workflow.WorkflowSystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RobotBattery4 extends RuleSuper {
    Double RobotCharge;
    final double minimalCharge = 0.01d;

    public RobotBattery4() {
        super();
    }

    @Override
    protected
    boolean RunRule(WorkflowSystem WorkflowSystem) throws Exception {
    try {
        Double charge = WorkflowSystem.getKMRCom().GetStatus().getBatteryState();

            if (charge < minimalCharge) {
                RobotCharge = charge;
                return true;
            }
    } catch (DeviceOfflineException e)
    {
        //Job for another rule!!!!
        return false;
    }

        return false;
    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {

        return new Alert(AlertType.RobotAlert, String.format("Tantalus charge below limit (%.2f) (%.2f)",minimalCharge,RobotCharge),this);
    }
}
