package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Com.TCP.GCCom;
import Workflow.Exception.DeviceOfflineException;
import Workflow.Exception.InconsistentStateException;
import Workflow.Exception.RobotErrorStateException;
import Workflow.WorkflowSystem;
import workflow.GC.GCStationStatus;

import java.util.*;

public class GCCheck extends RuleSuper {

    //This date represents either the moment that the current set of batches on GC was submitted
    //or this data represents the moment that the last batch on GC was completed
    Date date;
    int number_of_batches_completed;
    public GCCheck() {
        super();
    }

    @Override
    protected boolean RunRule(WorkflowSystem WorkflowSystem) throws DeviceOfflineException, InconsistentStateException, InterruptedException, CloneNotSupportedException, RobotErrorStateException {
        GCCom gc = WorkflowSystem.getGCCom();
        GCStationStatus status = gc.WaitForCommandable();

        //case 1 - no batches are submitted
        if(date == null && status.getBatches_running().size() == 0 )
            return false;


        //Case 2 GC running, but date is null - first time new batch is running
        if(date == null && status.getBatches_running().size() > 0 )
        {
            date = new Date();
            number_of_batches_completed = status.getBatches_completed().size();
            return false;
        }
        //Case 3 GC running, but date is not null - we have noticed batches were submitted, and we are now waiting for 210 minutes
        // new batch completed
        if(date != null && status.getBatches_completed().size() > number_of_batches_completed)
        {
            number_of_batches_completed = status.getBatches_completed().size();
            if(status.getBatches_running().size() == 0)
            {
                date = null;
            } else {
                date = new Date();
            }
            return false;
        }

        // Case 4 - no batches have been completed, but its been less than 210 since the lsat was completed
        if(date != null && status.getBatches_completed().size() == number_of_batches_completed
                && date.getTime() + 3.5*60*60*1000 >= (new Date()).getTime())
        {
            return false;
        }

        // Case 5 - no batches have been completed in last 210 minutes -> sent alert
        if(date != null && status.getBatches_completed().size() == number_of_batches_completed
        && date.getTime() + 3.5*60*60*1000 < (new Date()).getTime())
        {
            date = null;
            return true;
        }

        throw new InconsistentStateException("something is wrong with GC");
    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        return new Alert(AlertType.StockAlert, String.format("GC is busy for too long on 1 batch, did the ndrite crash?"),this);
    }
    @Override
    public void Reset()
    {
        super.Reset();
        date =  null;
    }

}
