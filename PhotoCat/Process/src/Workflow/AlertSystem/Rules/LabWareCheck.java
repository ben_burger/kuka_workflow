package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Com.TCP.GCCom;
import Workflow.Exception.DeviceOfflineException;
import Workflow.Exception.InconsistentStateException;
import Workflow.Exception.RobotErrorStateException;
import Workflow.WorkflowSystem;
import com.sun.corba.se.spi.orbutil.threadpool.Work;
import workflow.GC.GCStationStatus;

import java.util.Date;

    public class LabWareCheck extends RuleSuper {

        Exception DeviceOfflineException;
public LabWareCheck() {
            super();
        }

        @Override
        protected boolean RunRule(WorkflowSystem WorkflowSystem) throws DeviceOfflineException, InconsistentStateException, InterruptedException, CloneNotSupportedException, RobotErrorStateException {

            try {
                WorkflowSystem.getGCCom();
                WorkflowSystem.getQuantosCom();
                WorkflowSystem.getLiquidCom(0);
                WorkflowSystem.getLiquidCom(1);
                WorkflowSystem.getScaleCom(0);
                WorkflowSystem.getScaleCom(1);
                WorkflowSystem.getCapperCom();
                WorkflowSystem.getSonicatorCom();
                WorkflowSystem.getVibratoryCom();
                WorkflowSystem.CheckCameraStatus(0);
                WorkflowSystem.CheckCameraStatus(1);
            } catch (Exception e)
            {
                DeviceOfflineException  = e;
                return true;

            }
            return false;
        }

        @Override
        protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
            return new Alert(AlertType.ProcessAlert, DeviceOfflineException,this);
        }
    }

