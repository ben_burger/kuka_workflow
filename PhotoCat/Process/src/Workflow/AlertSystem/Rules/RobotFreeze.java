package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Com.TCP.LBRCom;
import Workflow.LBR.Messages.LBRTask;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TaskSuper;
import Workflow.WorkflowSystem;
import com.sun.corba.se.spi.orbutil.threadpool.Work;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RobotFreeze extends RuleSuper {
    int Maximal_Minutes_Single_Job = 5;
    @Override
    protected boolean RunRule(WorkflowSystem WorkflowSystem) throws Exception {
        try {
            if (WorkflowSystem.getLBRComNoCheck().isAlive())
                if (WorkflowSystem.getLBRCom().GetStatus().getStatus().equals(RobotStatusEnum.TaskRunning)) {
                LBRTask task = (LBRTask) WorkflowSystem.getLBRCom().GetStatus().getTask();
                Date start_waiting = new Date();
                boolean still_running = true;
                {
                        LBRCom lbr = WorkflowSystem.getLBRCom();
                    while (still_running
                    && (start_waiting.getTime() + Maximal_Minutes_Single_Job * 60 * 1000 )> (new Date()).getTime()) {
                        if (!lbr.GetStatus().getStatus().equals(RobotStatusEnum.TaskRunning)
                        || !task.getType().equals(((LBRTask) lbr.GetStatus().getTask()).getType())) {

                            still_running = false;
                            break;
                        }
                        Thread.sleep(5);
                    }
                    if (still_running) {
                        return true;
                    }
                }
            }
        } catch (NullPointerException e)
        {

        }
        return false;
    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        try {
            return new Alert(AlertType.RobotAlert,"Tantalus seems to have frozen, please help tantalus " + ((LBRTask) WorkflowSystem.getLBRCom().GetStatus().getTask()).getType().name(), this);
        } catch (Exception e)
        {
            return new Alert(AlertType.RobotAlert,"Tantalus seems to have frozen, and Tantalus went offline.", this);
        }
    }
}
