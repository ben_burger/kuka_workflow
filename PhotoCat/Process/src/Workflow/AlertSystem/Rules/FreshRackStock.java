package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Batch.Batch;
import Workflow.Batch.BatchLocation;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FreshRackStock extends RuleSuper {
    public FreshRackStock() {
        super();
    }

    @Override
    protected boolean RunRule(WorkflowSystem WorkflowSystem) {
        return NumberOfFreshRacks(WorkflowSystem) < 6;
    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        return new Alert(AlertType.StockAlert, String.format("Low stock level: fresh racks (%d)",NumberOfFreshRacks(WorkflowSystem)),this);
    }

    private int NumberOfFreshRacks(WorkflowSystem WorkflowSystem)
    {
        WorldState state = WorkflowSystem.getState();
        int result =0;
        for(BatchLocation location : BatchLocation.values())
        {
            Batch rack = state.getRackAtLocation(location);
            if(rack != null && rack.isFreshRack())
            {
                result++;
            }

        }

        return result;
    }
}
