package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.ProcessManagement.ProcessTypeEnum;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

public class ProcessAndStepsActive extends RuleSuper {
    LinkedList<WorkflowStepEnum> StepsDisabled;
    LinkedList<ProcessTypeEnum> ProcessesDisabled;

    public ProcessAndStepsActive() {

        super();
    }

    @Override
    protected boolean RunRule(WorkflowSystem WorkflowSystem) {
        LinkedList<ProcessTypeEnum> process_disabled = new LinkedList();
        for (ProcessTypeEnum process : ProcessTypeEnum.values()) {
            if (!WorkflowSystem.getProcessManager().getProcessEnabled(process)) {
                process_disabled.add(process);
            }
        }

        LinkedList<WorkflowStepEnum> steps_disabled = new LinkedList();
        for (WorkflowStepEnum workflowStep : WorkflowStepEnum.values()) {
            if (!WorkflowSystem.getProcessManager().GetStepEnabled(workflowStep)) {
                steps_disabled.add(workflowStep);
            }
        }

        if (steps_disabled.size() > 0 || process_disabled.size() > 0) {
            StepsDisabled = steps_disabled;
            ProcessesDisabled = process_disabled;
            return true;
        }

        return false;
    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        StringBuilder sb = new StringBuilder();
        if(ProcessesDisabled.size() > 0) {
            sb.append(String.format("Processes disabled: %s",String.join(",", ProcessesDisabled.stream().map(x -> x.name()).toArray(String[]::new))));
        }
        if(StepsDisabled.size() > 0) {
            if(ProcessesDisabled.size() > 0) sb.append("\r\n");
            sb.append(String.format("Steps disabled: %s",String.join(",",StepsDisabled.stream().map(x -> x.name()).toArray(String[]::new))));
        }
        return new Alert(AlertType.ProcessAlert, sb.toString(),this);
    }
}
