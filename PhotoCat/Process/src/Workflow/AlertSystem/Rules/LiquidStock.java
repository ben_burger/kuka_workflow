package Workflow.AlertSystem.Rules;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Batch.Liquid;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;

public class LiquidStock extends RuleSuper {

    LinkedList<Liquid> low_stock;
    public LiquidStock() {
        super();
    }

    @Override
    protected boolean RunRule(WorkflowSystem WorkflowSystem) {
        LinkedList<Liquid> result = new LinkedList();

        Liquid[] liquids_in_system = WorkflowSystem.getState().getLiquids();
        String[] unique_liquids = Arrays.stream(liquids_in_system).map(Liquid::getName).distinct().toArray(String[]::new);

        //sum how much is left for each liquid (above 55 mL)

        double[] volume_left = new double[unique_liquids.length];

        for (int i = 0; i < unique_liquids.length; i++) {
            volume_left[i] = 0;

            for (Liquid liquid : liquids_in_system) {
                if (liquid.Name.equals(unique_liquids[i])
                        && liquid.getMinimalVolumePresent() > 55.0
                    && liquid.getStationID() < 9000)
                    //we kind of hack this in by setting their station ID over 9000.
                {
                    volume_left[i] += (liquid.getMinimalVolumePresent() - 50.0);

                }
            }
        }
        for(int i=0;i<unique_liquids.length;i++)
        {
            if(volume_left[i] < 100) //We use this hack
            {
                String liquid_name= unique_liquids[i];
                Liquid liq = Arrays.stream(liquids_in_system).filter(x -> x.getName().equals(liquid_name)).findFirst().get();
                if(liq.getStationID() < 9000) //We need to keep liquid somewhere that are not being used anymore
                    result.add(liq);
            }
        }

        if(result.size() > 0)
        {
            low_stock = result;
            return true;
        }
        return false;
    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        StringBuilder sb = new StringBuilder();
        for(Liquid liq : low_stock)
        {
            sb.append(String.format("Low stock level for liquid: %s (%.2f)\r",liq.Name, liq.getMinimalVolumePresent()));
        }
        return new Alert(AlertType.StockAlert, sb.toString(),this);
    }
}
