package Workflow.AlertSystem.Rules;

import java.util.Date;
import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.AlertSystem.RuleSuper;
import Workflow.Com.TCP.LBRCom;
import Workflow.Exception.DeviceOfflineException;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import com.kuka.nav.robot.SafetyState;
import com.kuka.roboticsAPI.controllerModel.sunrise.ISafetyState;
import com.kuka.roboticsAPI.controllerModel.sunrise.SunriseSafetyState;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class RobotSafety extends RuleSuper {
    SafetyState SafetyKMR;
    SunriseSafetyState.SafetyStopType SafetyLBR;
    DeviceOfflineException DeviceOfflineException;
    public RobotSafety() {
        super();
    }

    @Override
    protected boolean RunRule(WorkflowSystem WorkflowSystem) throws Exception {
        SafetyKMR = null;
        SafetyLBR = null;
        DeviceOfflineException = null;

        try {
            SafetyState safety_kmr = WorkflowSystem.getKMRCom().GetStatus().getSafetyState();
            LBRCom lbr = WorkflowSystem.getLBRComNoCheck();

            SunriseSafetyState.SafetyStopType safety_lbr = null;
            if (lbr.isAlive()) {
                try {
                    safety_lbr = lbr.GetStatus().getSafetyStopType();
                } catch(NullPointerException e)
                {
                    //sometimes the LBR goes off after we checked whether it was alive... - starting job
                }
            }

            if (safety_kmr.equals(SafetyState.PROTECTIVE_STOP) ||
                    (safety_lbr != null && (!safety_lbr.equals(SunriseSafetyState.SafetyStopType.NOSTOP)))) {

                boolean still_unsafe = true;
                Date started = new Date();
                while (still_unsafe && started.getTime() + 5 * 60 * 1000 > (new Date()).getTime()) {
                    safety_kmr = WorkflowSystem.getKMRCom().GetStatus().getSafetyState();
                    lbr = WorkflowSystem.getLBRComNoCheck();

                    safety_lbr = null;
                    if (lbr.isAlive()) {
                        try {
                            safety_lbr = lbr.GetStatus().getSafetyStopType();
                        } catch(NullPointerException e)
                        {
                            //sometimes the LBR goes off after we checked whether it was alive... - starting job
                        }
                    }
                    if ( !(   safety_kmr.equals(SafetyState.PROTECTIVE_STOP) ||
                            (safety_lbr != null && (!safety_lbr.equals(SunriseSafetyState.SafetyStopType.NOSTOP))
                            ))) {
                        still_unsafe = false;
                    }
                    if (still_unsafe)
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                        }
                }

                if (still_unsafe) {
                    SafetyKMR = safety_kmr;
                    SafetyLBR = safety_lbr;
                    return true;
                }
            }
        } catch(DeviceOfflineException e)
        {
            DeviceOfflineException = e;
            return true;
        }
        return false;
    }

    @Override
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        StringBuilder sb = new StringBuilder();

        if(SafetyKMR != null &&  !SafetyKMR.equals(SafetyState.SAFE))
        {
            sb.append(String.format("Safety stop on KMR: %s", SafetyKMR.name()));
        }


        if((SafetyLBR != null && (!SafetyLBR.equals(SunriseSafetyState.SafetyStopType.NOSTOP))))
        {
            sb.append(String.format("Safety stop on LBR: %s", SafetyLBR.name()));
        }


        if(SafetyKMR == null && SafetyLBR == null)
        {
            if(DeviceOfflineException != null) {
                sb.append(String.format("Device Offline: DeviceOfflineException: %s", DeviceOfflineException.getMessage()));
                return new Alert(AlertType.RobotAlert, sb.toString(), this);
            }
        }
        return super.ConstructAlert(WorkflowSystem);
    }
}
