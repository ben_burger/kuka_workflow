package Workflow.AlertSystem;

public enum AlertType {
    RobotAlert,
    ProcessAlert,
    StockAlert
}
