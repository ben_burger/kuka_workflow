package Workflow.AlertSystem;

import Workflow.IMessageHandler;
import Workflow.Util.SimpleLogger;
import Workflow.WorkflowSystem;
import org.reflections.Reflections;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import sun.util.calendar.LocalGregorianCalendar;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.*;

public class AlertSystem {
    private boolean SentAlerts = false;
    private SimpleLogger logger;
    private LinkedBlockingQueue<Alert> Alerts;
    private LinkedBlockingQueue<Alert> UnprocessedAlerts;
    private RuleSuper[] Rules;
    private WorkflowSystem WorkflowSystem;
    private boolean SentTexts = false;
    private boolean SentEmails = true;

    private static String USER_NAME = "uolfuturelab2";  // GMail user name (just the part before "@gmail.com")
    private static String PASSWORD = "stommegoogle"; // GMail password



    public AlertSystem(WorkflowSystem workflowSystem) throws IOException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ReadUsers();
        WorkflowSystem = workflowSystem;
        logger = new SimpleLogger("alertsystem");
        CreateRules();
        ClearAlerts();
    }

    private void CreateRules() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //Some reflection black magic performed at runtime :)
        Reflections reflections = new Reflections("Workflow.AlertSystem.Rules");
        Set<Class<? extends RuleSuper>> allClasses =
                reflections.getSubTypesOf(RuleSuper.class);
        Rules = new RuleSuper[allClasses.size()];
        int index = 0;
        for (Class class_type : allClasses) {
            Constructor<?> constructor = class_type.getConstructor();
            Rules[index] = (RuleSuper) constructor.newInstance();
            index++;
        }
    }

    public void ClearAlerts() {
        Alerts = new LinkedBlockingQueue();
        UnprocessedAlerts = new LinkedBlockingQueue();
        for (RuleSuper rule : Rules)
            rule.Reset();
    }

    private void ReadUsers() {

    }

    public void HandleProcessException(IMessageHandler.ExceptionSource exceptionMessageSource, Exception e) {
        Alert alert;
        switch (exceptionMessageSource) {

            case GenericRobotProcess:
            case GenericProcess:
            case PickUpNewRacksProcess:
            case SolidDispensingProcess:
            case LiquidDispenseProcess:
            case CappingProcess:
            case SonicatorProcess:
            case PhotolysisProcess:
            case GCProcess:
                alert = new Alert(AlertType.ProcessAlert, e, "Process crashed: " + exceptionMessageSource.name());
                break;
            case UIAction:
            case UpdateUIEvent:
            case RobustnessTest:
            default:
                throw new NotImplementedException();
        }
        AddAlert(alert);
    }

    public void RunRules() throws Exception {
        for (RuleSuper rule : Rules) {
            if (!rule.RuleActivated()) {
                if (rule.Run(WorkflowSystem))
                    AddAlert(rule.ConstructAlert(WorkflowSystem));
            }
        }
    }

    public void AddAlert(Alert alert) {
        Alerts.add(alert);
        UnprocessedAlerts.add(alert);
    }


    public void DealWithAlert() throws InterruptedException {
        if(UnprocessedAlerts.peek() != null) {
            DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            StringBuilder sb = new StringBuilder();
            sb.append("Current alerts:\r\n");

            while (UnprocessedAlerts.peek() != null) {
                Alert alert = UnprocessedAlerts.take();
                if(alert.getRule() != null)
                    sb.append(String.format("rule: %s\r\n",alert.getRule().getClass().getSimpleName()));
                sb.append(String.format("type:%s\r\n", alert.getType().name()));
                sb.append(String.format("date-time:%s\r\n", df2.format(alert.getTimeStamp())));
                sb.append(String.format("%s\r\n", alert.getMessage()));
                sb.append("\r\n");
            }

            String message = sb.toString();
            if(SentEmails)
                SentEmails(message);
        }
    }

    private void SentEmails(String message) {
        String [] receiver = new String[] {
                "uolfuturelab@gmail.com"
                ,"taurnist@liverpool.ac.uk" //BB
                ,"Nicola.Rankin@liverpool.ac.uk"//Nicola
                ,"kuka.photolysis.operator@gmail.com" //BB
                ,"kuka.operator.2@gmail.com" //Nicola
                , "bharris1@uwalumni.com" //Brandon
                , "aicuser@liv.ac.uk" //Rob
                , "A.P.Coogan@liverpool.ac.uk"//Angus

        };
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        sb.append("Emails sent:\r\n:datetime");
        sb.append(df2.format(new Date()));
        sb.append("\r\nmessage:");
        sb.append(message);
        logger.LogEvent(sb.toString());
        sendFromGMail(USER_NAME,PASSWORD,receiver, "Tantalus - Alert" ,message);
    }


    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for( int i = 0; i < to.length; i++ ) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (AddressException ae) {
            ae.printStackTrace();
        }
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }

    public RuleSuper[] GetRules() {
        return Rules;
    }

    public Alert[] getAlerts() {
        return Alerts.toArray(new Alert[0]);
    }

    public boolean getSentTexts() {
        return SentTexts;
    }

    public boolean getSentEmails() {
        return SentEmails;
    }

    public void SendEmails(boolean b) {
        SentEmails = b;
    }

    public void SendTexts(boolean b) {
        SentTexts = b;
    }
}
