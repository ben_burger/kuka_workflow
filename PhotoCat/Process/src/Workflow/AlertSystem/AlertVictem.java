package Workflow.AlertSystem;

import java.util.Date;
import java.util.HashMap;

public class AlertVictem {
    private String Name;
    private String EmailAdress;
    private HashMap<AlertType, Boolean> ActiveAlerts;


    public AlertVictem(String name, String emailAdress)
    {
        this.Name = name;
        this.EmailAdress = emailAdress;
        this.ActiveAlerts = new HashMap();

        for(AlertType alertType : AlertType.values())
            ActiveAlerts.put(alertType,true);
    }

    public void EnableAlert(AlertType type)
    {
        ActiveAlerts.put(type, true);
    }
    public void DisableAlert(AlertType type)
    {
        ActiveAlerts.put(type, false);
    }

    public boolean AlertEnabled(AlertType type)
    {
        if(ActiveAlerts.containsKey(type))
            return ActiveAlerts.get(type);
        return false;
    }

    public void AlertUser(Alert[] alerts)
    {

    }


}
