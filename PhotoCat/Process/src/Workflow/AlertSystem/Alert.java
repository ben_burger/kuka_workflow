package Workflow.AlertSystem;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Alert implements Serializable {
    private  RuleSuper Rule;
    private AlertType Type;
    private Exception e;
    private String Message;
    private Date Created;

    public Alert(AlertType type, Exception e, RuleSuper rule) {
        Type = type;
        SetMessageBasedOnException(e, null);
        Created = new Date();
        Rule = rule;
    }

    public Alert(AlertType type, String message, RuleSuper rule) {
        Type = type;
        Message = message;
        Created = new Date();
        Rule = rule;
    }

    public Alert(AlertType type, Exception e) {
        Created = new Date();
        Type = type;
        SetMessageBasedOnException(e,null);
    }

    public Alert(AlertType type, Exception e, String message) {
        Created = new Date();
        Type = type;
        this.e = e;
        SetMessageBasedOnException(e, message);
    }

    public Alert(AlertType type, String message) {
        Type = type;
        Message = message;
        Created = new Date();
    }
    private void SetMessageBasedOnException(Exception e, String message) {
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            if(message == null || message.equals(""))
                Message = String.format("Exception Type: %s\r\nException message: %s\r\n StackTrace:%s\r\n",e.getClass().getName(),e.getMessage(), sw.toString());
            else
                Message = String.format("Exception Type: %s\r\nMessage: %s \r\n Exception message: %s\r\n StackTrace: %s\r\n",e.getClass().getName(), message,e.getMessage(), sw.toString());
            sw.close();
            sw = null;
            pw.close();
            pw = null;
        } catch (Exception ex) {
            if (Message == null && e != null)
                if(message == null || message.equals(""))
                    Message = String.format("Exception Type: %s\r\nException message: %s\r\n",e.getClass().getName(), e.getMessage());
                else
                    Message = String.format("Exception Type: %s\r\nMessage: %s\r\n Exception message: %s\r\n",e.getClass().getName(), message, e.getMessage());
            if(e == null)
                Message = String.format("Alert was generated, but the nature of error is unclear");
            if (sw != null)
                try {
                    sw.close();
                    sw = null;
                } catch (Exception ex1) {
                }
            if (pw != null)
                try {
                    pw.close();
                    pw = null;
                } catch (Exception ex1) {
                }
        }
    }

    public AlertType getType() {
        return Type;
    }

    public String getMessage() {
        return Message;
    }

    public Date getTimeStamp() {
        return Created;
    }

    public RuleSuper getRule() {
        return Rule;
    }
}
