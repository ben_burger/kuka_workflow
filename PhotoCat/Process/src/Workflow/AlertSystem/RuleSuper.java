package Workflow.AlertSystem;

import Workflow.WorkflowSystem;

import java.io.Serializable;
import java.util.Date;

public abstract class RuleSuper implements Serializable{
    private Date LastActivation;
    protected Exception Exception;

    public RuleSuper()
    {
        LastActivation = null;
    }

    public boolean Run(WorkflowSystem WorkflowSystem) throws Exception
    {
        if(LastActivation == null)
        {
            try {
                if (RunRule(WorkflowSystem)) {
                    LastActivation = new Date();
                    return true;
                }
            } catch (Exception e)
            {
                LastActivation = new Date();
                e = Exception;
                return true;
            }
            return false;
        }
        return RuleActivated();
    }

    public void Reset()
    {
        LastActivation = null;
    }

    public boolean RuleActivated()
    {
        return LastActivation != null;
    }


    protected abstract boolean RunRule(WorkflowSystem WorkflowSystem) throws Exception;
    protected Alert ConstructAlert(WorkflowSystem WorkflowSystem) {
        return new Alert(AlertType.ProcessAlert, Exception,this);
    }
    public Date GetLastActivation() {
        return LastActivation;
    }


}
