package Workflow.ProcessManagement;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class ProcessStatus implements Cloneable {
    boolean Enabled = true;
    boolean Running = false;
    String LastError;

    public ProcessStatus() {
    }

    public boolean isEnabled() {
        return Enabled;
    }

    public void setEnabled(boolean enabled) {
        Enabled = enabled;
    }

    public boolean isRunning() {
        return Running;
    }

    public void setRunning(boolean running) {
        Running = running;
    }

    public String getLastError() {
        return LastError;
    }

    public void setLastError(String lastError) {
        LastError = lastError;
    }

    public ProcessStatus getClone() throws CloneNotSupportedException {
        return (ProcessStatus) this.clone();
    }
}
