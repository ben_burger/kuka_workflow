package Workflow.ProcessManagement;

import Workflow.IMessageHandler;
import Workflow.WorkflowSystem;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Benjamin on 12/16/2018.
 */
public abstract class ProcessSuper {
    protected final ProcessManager Manager;
    protected WorkflowSystem workflowSystem;
    ProcessStatus Status;
    Boolean StatusLock = new Boolean(true);
    ScheduledExecutorService thread;

    boolean running = false;
    protected IMessageHandler.ExceptionSource exceptionMessageSource = IMessageHandler.ExceptionSource.GenericProcess;

    public ProcessSuper(WorkflowSystem workflowSystem, ProcessManager manager) {

        Status = new ProcessStatus();
        this.workflowSystem = workflowSystem;
        this.Manager = manager;
    }

    public ProcessStatus getStatus() throws CloneNotSupportedException {
        synchronized (StatusLock)
        {
            return Status.getClone();
        }
    }

    protected abstract void Run()  throws Exception;

    public void SetEnabled(boolean enable) {
        synchronized (StatusLock) {
            Status.setEnabled(enable);
        }
    }

    public boolean isRunning() {
        synchronized (StatusLock) {
            return Status.isRunning();
        }
    }

    public void StartRunning() {
        thread = Executors.newSingleThreadScheduledExecutor();
        thread.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    if (Manager.isProcesses_enabled() && Status.isEnabled()) {
                        synchronized (StatusLock) {
                            Status.setRunning(true);
                        }
                        Run(); //Checks if cartridge is in there, and dispenses. Scheduler should automatically start once this guy is done writing finihs time, wich should be reset if another round of dispensing is required.
                        //This one is responsible for all door moving (so close door open door before writing finish timestamp)
                        synchronized (StatusLock) {
                            Status.setRunning(false);
                        }
                    }
                } catch (Exception e) {

                    synchronized (StatusLock) {
                        Status.setEnabled(false);
                        Status.setRunning(false);
                    }

                    ProcessExceptionForAlertAndUI(e);
                }
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);
    }

    protected void ProcessExceptionForAlertAndUI(Exception e)
    {
        workflowSystem.getMessageHandler().HandleException(exceptionMessageSource,e);
        workflowSystem.getAlertSystem().HandleProcessException(exceptionMessageSource,e);
    }

    public boolean GetEnabled() {
        synchronized (StatusLock)
        {
            return Status.isEnabled();
        }
    }
}
