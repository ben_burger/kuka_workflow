package Workflow.ProcessManagement;

public enum WorkflowStepEnum {
    Storage,
    SolidDispensing,
    LiquidDispensing,
    Sonication,
    GCAnalysis,
 Photolysis ;
}
