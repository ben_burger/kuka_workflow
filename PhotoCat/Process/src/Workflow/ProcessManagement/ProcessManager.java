package Workflow.ProcessManagement;

import Workflow.ProcessManagement.Process.ScheduleHeuristic.IScheduler;
import Workflow.ProcessManagement.Process.ScheduleHeuristic.ScheduleAlgorithm;
import Workflow.ProcessManagement.Process.*;
import Workflow.ProcessManagement.Process.ScheduleHeuristic.WorkOnFarAndOld;
import Workflow.Util.SimpleLogger;
import Workflow.WorkflowSystem;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class ProcessManager {
    private WorkflowSystem WorkflowSystem;
    private IScheduler ScheduleAlgorithm;
    boolean processes_enabled = false;
    Map<ProcessTypeEnum, ProcessSuper> Processes;

    HashMap<WorkflowStepEnum,Boolean> WorkflowStepEnabled = new HashMap();
    private SimpleLogger logger;


    public ProcessManager(WorkflowSystem workflowSystem, ScheduleAlgorithm scheduleAlgorithm) throws IOException {
        this.WorkflowSystem = workflowSystem;
        this.ScheduleAlgorithm = SetupScheduler(scheduleAlgorithm);
        CreateProcesses(this.ScheduleAlgorithm);
        for(WorkflowStepEnum step : WorkflowStepEnum.values())
        {
            WorkflowStepEnabled.put(step,true);
        }

        logger = new SimpleLogger("process-manager");


    }

    public void EnableStep(WorkflowStepEnum step, boolean enable) {
        StringBuilder sb  = new StringBuilder();
        sb.append("Operator ");
        sb.append(enable?"enabling":"disabling");
        sb.append(" robot step ");
        try {
            sb.append(step.name());
        } catch (Exception e)
        {
            sb.append("step.name() failed for unclear reasons");
            sb.append(e.getMessage());
            sb.append(e.getStackTrace());
        }
        logger.LogEvent(sb.toString());
        WorkflowStepEnabled.put(step, enable);

    }
    public boolean GetStepEnabled(WorkflowStepEnum step)
    {
        return WorkflowStepEnabled.get(step);
    }


    private IScheduler SetupScheduler(ScheduleAlgorithm scheduleAlgorithm) {
        switch (scheduleAlgorithm) {
            case SingleBatch:
                return new WorkOnFarAndOld();
            default:
                throw new NotImplementedException();
        }
    }

    private void CreateProcesses(IScheduler Scheduler) throws IOException {
        Processes = new HashMap<>();

        for (ProcessTypeEnum processType : ProcessTypeEnum.values()) {
            ProcessSuper process;
            switch (processType) {
                case Quantos:
                    process = new SolidDispensingProcess(WorkflowSystem, this);
                    break;
                case Sonicator:
                    process = new SonicatorProcess(WorkflowSystem, this);
                    break;
                case Photocat:
                    process = new PhotolysisProcess(WorkflowSystem, this);
                    break;
                case GC:
                    process = new GasChromatograpyProcess(WorkflowSystem, this);
                    break;
                case Tantalus:
                    process = new RobotJobProcess(WorkflowSystem, this, Scheduler);
                    break;
                case PickUpRacks:
                    process = new PickUpNewRacksProcess(WorkflowSystem, this);
                    break;
                case AlertProcess:
                    process = new AlertProcess(WorkflowSystem, this);
                    break;
                case LabLogger:
                    process = new LabLogger(WorkflowSystem, this);
                    break;
                default:
                    throw new NotImplementedException();
            }
            Processes.put(processType, process);

            process.StartRunning();
        }
    }

    public void EnableProcess(ProcessTypeEnum type, boolean enable) {
        StringBuilder sb  = new StringBuilder();
        sb.append("Operator ");
        sb.append(enable?"enabling":"disabling");
        sb.append(" workflow process ");
        sb.append(type.name());
        logger.LogEvent(sb.toString());

        ProcessSuper process = Processes.get(type);
        process.SetEnabled(enable);
    }

    public boolean isProcesses_enabled() {
        return processes_enabled;
    }

    public void setProcesses_enabled(boolean processes_enabled) {
        StringBuilder sb  = new StringBuilder();
        sb.append("Operator ");
        sb.append(processes_enabled?"enabling":"disabling");
        sb.append(" workflow collective ");
        logger.LogEvent(sb.toString());

        this.processes_enabled = processes_enabled;
    }

    public ProcessStatus GetStatus(ProcessTypeEnum type) throws CloneNotSupportedException {
        ProcessSuper process = Processes.get(type);
        return process.getStatus();
    }

    public boolean getProcessEnabled(ProcessTypeEnum processTypeEnum) {
        ProcessSuper process = Processes.get(processTypeEnum);
        return process.GetEnabled();
    }
}
