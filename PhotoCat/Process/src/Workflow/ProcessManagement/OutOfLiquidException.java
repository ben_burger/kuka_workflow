package Workflow.ProcessManagement;

import Workflow.Batch.Liquid;

public class OutOfLiquidException extends Exception{
    Liquid liquid;

    public OutOfLiquidException(Liquid liquid)
    {
        super();
        this.liquid = liquid;
    }

}
