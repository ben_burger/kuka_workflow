package Workflow.ProcessManagement.Process;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.Batch.*;
import Workflow.Com.Serial.Quantos.*;
import Workflow.Exception.InconsistentStateException;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.Process.RobotJob.Exception.QuantosInverventionException;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.ProcessManagement.ProcessSuper;
import Workflow.ProcessManagement.ProcessTypeEnum;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class SolidDispensingProcess extends ProcessSuper {


    public SolidDispensingProcess(WorkflowSystem workflowSystem, ProcessManager manager) {
        super(workflowSystem, manager);
        exceptionMessageSource = IMessageHandler.ExceptionSource.SolidDispensingProcess;

    }

    @Override
    protected void Run() throws Exception {
        String message = "";
//Only responsible for checking completion of runs and setting time stamps such
        // that scheduler cna pick them up
        //responsible for turning off and setting time stamps
        if (true){
            QuantosCom quantos = workflowSystem.getQuantosCom();
            Batch batch = workflowSystem.getState().getRackAtLocation(BatchLocation.SolidDispenseStation_Manipulation);
            if (batch != null && batch.getStatus() == BatchStatus.DispenseSolid && batch.getProgress() == Progress.Waiting) {
                batch.setProgress(Progress.Running);
                quantos.LockDosingHeadPin(true);
                quantos.MoveDoor(false);
                quantos.MoveAutoSampler(0);
                Solid[] solids = batch.getSolids();
                for (int i = 0; i < solids.length; i++) {
                    Solid batch_solid = solids[i];
                    Optional<Solid> solid = Arrays.stream(workflowSystem.getState().getSolids())
                            .filter(x -> x.getHotelIndex() == -1 && x.getName().equals(batch_solid.getName()))
                            .findFirst();

                    if (solid.isPresent()) {
                        QuantosHeadInfo headdata = quantos.GetHeadData();
                        if(!solid.get().Name.equals(headdata.Substance) && (!headdata.Substance.equals("??")))
                            throw new InconsistentStateException("wrong head mounted, expecting " + solid.get().Name + " but found " + headdata.Substance);
                        if(headdata.Substance.equals("??")) {
                            message = String.format("WARNING: dispensing solid %s , but cartridge is not labeled (??)", solid.get().Name);
                            batch.AppendComment(message);
                            workflowSystem.getMessageHandler().WriteMessage(message);
                        }
                        workflowSystem.getState().SetRemainingDosages(solid.get(), headdata.RemainingDosages);

                        Date finish = batch.getSolid_dispensing_finish_date_time(i);
                        if (batch.NeedToDispenseSolid(solid.get())) {

                            //Set settings
                            quantos.SetTargetTolerance(1);
                            quantos.SetDispensingAlgorithm(DispensingAlgorithm.valueOf(batch_solid.getDispenseAlgorithm()));
                            quantos.SetTappingBeforeDosing(batch_solid.getTapping() > 9);
                            quantos.SetTappingWhileDosing(batch_solid.getTapping() > 9);
                            if (batch_solid.getTapping() > 9)
                                quantos.SetTappingIntensity(batch_solid.getTapping() > 100 ? 100 : batch_solid.getTapping());


                            Sample[] samples = Arrays.stream(batch.getSamples()).sorted((e1, e2) -> Integer.compare(e1.getSampleIndex(), e2.getSampleIndex())).toArray(Sample[]::new);
                            for (Sample sample : samples) {
                                float target = sample.getSolidDispenseAmount(solid.get()) - sample.getSolidDispensed(solid.get());
                                if (Math.abs(target) > sample.getSolidDispenseAmount(solid.get()) * 0.1 && target > 0.5 ) {
                                    quantos.SetTargetValue(target);
                                    quantos.MoveAutoSampler(sample.getSampleNumber());
                                    QuantosResponse response = quantos.DosePowder();
                                    boolean stopDispensing = false;
                                    boolean throwInterventionException = false;
                                    boolean retrieve_weight = false;
                                    switch (response) {
                                        case Success:
                                            retrieve_weight = true;
                                            break;
                                        case AnotherJob:
                                            //We should have single point of control,
                                            // but if we do not, we have an operator playing with the machine?...
                                            // or the Quantos started to do the internal adjustments...
                                            throw new NotImplementedException();
                                        case NotAllowedNow:
                                            //Stop Dispensing, and retry SOON!
                                            stopDispensing = true;
                                            batch.setProgress(Progress.Waiting);
                                            break;
                                        case HeadLimitReached:
                                            workflowSystem.getMessageHandler().WriteMessage("HeadLimitReached for cartridge " + solid.get().getName());
                                            workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert,new QuantosInverventionException(response),"HeadLimitReached for cartridge " + solid.get().getName()));
                                            workflowSystem.getState().SetSolidBlocked();
                                            stopDispensing = true;
                                            batch.setStatus(BatchStatus.CartridgeChangeQuantos);
                                            break;
                                        case PowderFlowError:
                                            message = "Powderflow error for cartridge " + solid.get().getName();
                                            workflowSystem.getMessageHandler().WriteMessage(message);
                                            workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert,new QuantosInverventionException(response),message));
                                            workflowSystem.getState().SetSolidBlocked();
                                            stopDispensing = true;
                                            retrieve_weight = true;
                                            batch.setStatus(BatchStatus.CartridgeChangeQuantos);
                                            break;
                                        case HeadExpirydateReached:
                                            message = "Cartridge expiry date error for cartridge " + solid.get().getName();
                                            workflowSystem.getMessageHandler().WriteMessage(message);
                                            workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert,new QuantosInverventionException(response),message)); workflowSystem.getState().SetSolidBlocked();
                                            stopDispensing = true;
                                            batch.setStatus(BatchStatus.CartridgeChangeQuantos);
                                            break;
                                        case HeadNotAllowed:
                                            //Stop Dispensing
                                            workflowSystem.getState().SetSolidBlocked();
                                            stopDispensing = true;
                                            batch.setStatus(BatchStatus.CartridgeChangeQuantos);
                                            break;
                                        case Timeout:
                                            retrieve_weight = true;
                                        case WeightInstable:
                                        case NotSelected:
                                        case SafeposError:
                                        case SamplerBlocked:
                                             stopDispensing = true;
                                             throwInterventionException = true;
                                             break;
                                        case NotMounted: //STATE ERROR
                                            throw new InconsistentStateException("Expecting cartridge to be mounted.");

                                    }
                                    quantos.MoveDoor(false);
                                    QuantosDispenseInformation sampleData = null;
                                    if(retrieve_weight) {
                                        try {
                                            if (sampleData == null)
                                                sampleData = quantos.GetSampleData();
                                        } catch (Exception e) {
                                            Thread.sleep(15000);
                                        }

                                        try {
                                            if (sampleData == null)
                                                sampleData = quantos.GetSampleData();
                                        } catch (Exception e) {
                                            Thread.sleep(15000);
                                        }

                                        try {
                                            if (sampleData == null)
                                                sampleData = quantos.GetSampleData();
                                        } catch (Exception e) {
                                            Thread.sleep(15000);
                                        }

                                        if (sampleData != null)
                                            workflowSystem.getState().AddSolidDispesedAmount(batch, sample, solid.get(), sampleData.Content);
                                        else {
                                            Float weight = null;

                                            for (int tries = 0; tries < 3 && weight == null; i++) {
                                                try {
                                                    weight = quantos.GetStableWeight();
                                                } catch (Exception e) {
                                                    Thread.sleep(15000);
                                                }
                                            }
                                            if (weight != null)
                                                workflowSystem.getState().AddSolidDispesedAmount(batch, sample, solid.get(), 1000 * weight);
                                            else {

                                                message = "Could not retrieve weight after error for cartridge " + solid.get().getName() + " and samplenumber " + sample.getSampleNumber();
                                                batch.AppendComment(message);
                                                workflowSystem.getMessageHandler().WriteMessage(message);
                                                workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert, new QuantosInverventionException(response), message));
                                                throw new QuantosInverventionException(response);
                                            }
                                        }
                                    }

                                    if(throwInterventionException)
                                        throw new QuantosInverventionException(response);

                                    if (stopDispensing || (!workflowSystem.getProcessManager().getProcessEnabled(ProcessTypeEnum.Quantos)))
                                    {
                                        batch.setProgress(Progress.Waiting);
                                        return;
                                    }
                                }
                            }
                            batch.setSolid_dispensing_finish_date_time(i, new Date());

                        }
                        headdata = quantos.GetHeadData();

                        workflowSystem.getState().SetRemainingDosages(solid.get(), headdata.RemainingDosages);
                        break;
                    }

                }

                boolean done = true;
                for (int i = 0; i < batch.getSolids().length && done; i++) {
                    done  = (!batch.NeedToDispenseSolid(batch.getSolids()[i]));
                }

                batch.setProgress(Progress.Waiting);
                if (done)
                    batch.setStatus(BatchStatus.UnloadQuantos);
                else
                    batch.setStatus(BatchStatus.CartridgeChangeQuantos);


            }
        }
    }
}
