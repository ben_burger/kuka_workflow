package Workflow.ProcessManagement.Process;

import Workflow.Com.Serial.Quantos.DispensingAlgorithm;
import Workflow.Com.TCP.KMRCom;
import Workflow.Com.TCP.LBRCom;
import Workflow.Exception.*;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.Process.RobotJob.DispenseLiquids;
import Workflow.ProcessManagement.Process.RobotJob.JobSuper;
import Workflow.ProcessManagement.Process.ScheduleHeuristic.IScheduler;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.TCP.RobotStatusEnum;
import Workflow.Util.SimpleLogger;
import Workflow.WorkflowSystem;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class RobotJobProcess extends Workflow.ProcessManagement.ProcessSuper {
    IScheduler Scheduler;
    SimpleLogger RobotLog;

    public RobotJobProcess(WorkflowSystem workflowSystem, ProcessManager manager, IScheduler Scheduler) throws IOException {
        super(workflowSystem, manager);
        this.Scheduler = Scheduler;
        exceptionMessageSource = IMessageHandler.ExceptionSource.GenericRobotProcess;
        RobotLog = new SimpleLogger("RobotJobs");
    }


    @Override
    protected void Run() throws Exception {
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if(!workflowSystem.getKMRCom().GetStatus().GetCharging());
        {
            JobSuper newJob = Scheduler.GetNewJob(workflowSystem);
            if (newJob != null) {
                if (!newJob.getExceptionMessageSource().equals(null))
                    exceptionMessageSource = newJob.getExceptionMessageSource();
                try {
                    DisallowCharge(workflowSystem);
                    newJob = Scheduler.GetNewJob(workflowSystem);
                    RobotLog.LogEvent(String.format("%s Starting job %s for batch %s", df2.format(new java.util.Date()), newJob.getClass().getName(), newJob.getBatch().getBatch_name()));
                    newJob.Execute(workflowSystem);
                    RobotLog.LogEvent(String.format("%s Finished job %s for batch %s", df2.format(new java.util.Date()), newJob.getClass().getName(), newJob.getBatch().getBatch_name()));
                    //BB HACK on AIC request to always load the photolysis station straight after capping
                    if(!(newJob instanceof DispenseLiquids))
                        AllowCharge(workflowSystem);
                } catch(Exception e)
                {
                    workflowSystem.getProcessManager().EnableStep(newJob.getWorkflowStep(),false);
                    RobotLog.LogEvent(String.format("%s Failed job %s for batch %s", df2.format(new java.util.Date()), newJob.getClass().getName(), newJob.getBatch().getBatch_name()));
                    throw e;
                }
            }
        }
    }

    //There is no guarantee that after this method the robot will be active. After this ALWAYS THROW AN ENSURE ROBOTISCALBIRATEDATPOSITION
    protected void AllowCharge(WorkflowSystem workflowSystem) throws CloneNotSupportedException, IOException, InterruptedException, RobotBusyException, NotAllowedException {
        try {
            LBRCom lbr = workflowSystem.getLBRComNoCheck();
            KMRCom kmr = workflowSystem.getKMRCom();
            if(kmr.GetServer().GetStatus().getStatus().equals(RobotStatusEnum.TaskRunning) && lbr != null && lbr.isAlive())
                workflowSystem.getLBRCom().AllowChargingOfRobot();
            else
                workflowSystem.getKMRCom().AllowChargingOfRobotAndWait();
            //Allow the arm controller to turn off as the robot goes for a charge
            Thread.sleep(3000);
        } catch (RobotErrorStateException e) {
            e.printStackTrace();
        } catch (RobotNoHeartBeatException e) {
            //Robot has gone for a charge immediately. We do not care.
            e.printStackTrace();
        } catch (DeviceOfflineException e) {
            e.printStackTrace();
        }
    }

    //There is no guarantee that after this method the robot will be active. After this ALWAYS THROW AN ENSURE ROBOTISCALBIRATEDATPOSITION
    protected void DisallowCharge(WorkflowSystem workflowSystem) throws CloneNotSupportedException, IOException, InterruptedException, DeviceOfflineException, RobotErrorStateException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
        boolean worked = false;
        boolean tried_lbr =false;
        try {
            LBRCom lbr = workflowSystem.getLBRComNoCheck();
            KMRCom kmr = workflowSystem.getKMRCom();
            if(kmr.GetServer().GetStatus().getStatus().equals(RobotStatusEnum.TaskRunning) && lbr != null && lbr.isAlive()) {
                //This may fail because in between the robot deciding going for a charge, we may be trying to sent a
                // discharge command, which is then nefer picked up because the lbr goes offline
                // In this case this will never complete, and will throw an error while the robots waits for the task to
                // start
                tried_lbr = true;
                workflowSystem.getLBRCom().DisallowChargingOfRobotAndWait();
            }
            else
            {
                if(kmr.GetServer().GetStatus().getStatus().equals(RobotStatusEnum.TaskRunning))
                    kmr.WaitForCommandable();
                workflowSystem.getKMRCom().DisallowChargingOfRobotAndWait();
            }

            worked = true;
        } catch (RobotErrorStateException e) {
            e.printStackTrace();
        } catch (RobotNoHeartBeatException e) {
            e.printStackTrace();
        } catch (DeviceOfflineException e) {
            e.printStackTrace();
        }

        //Above error may not work if the robot goes for a charge
        //while it is still on, that means that the lbr has shut down
        if(!worked) {
            {
                if(!tried_lbr)
                {
                    System.out.println("KMR FAILED Allow CHARGE UNEXPECTEDLY");
                    System.out.println("Was a command sent to the robot eventhough the KMR jobs are active?!");
                }
                // SO the LBR failed, did not process the command properly, so wait for the KMR to become commendable
                // so let it finish the waitforcontainer method that finishes after the arm finishes (at some pint?)
                workflowSystem.getKMRCom().WaitForCommandable();
                workflowSystem.getKMRCom().DisallowChargingOfRobotAndWait();

            }
        }
    }

}
