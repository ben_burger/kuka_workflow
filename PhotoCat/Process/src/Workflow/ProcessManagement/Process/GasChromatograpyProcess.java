package Workflow.ProcessManagement.Process;

import Workflow.Batch.*;
import Workflow.CollectData;
import Workflow.Com.TCP.GCCom;
import Workflow.Exception.DeviceOfflineException;
import Workflow.Exception.InconsistentStateException;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.ProcessManagement.ProcessTypeEnum;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import workflow.GC.GCStationStatus;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class GasChromatograpyProcess extends Workflow.ProcessManagement.ProcessSuper {


    public GasChromatograpyProcess(WorkflowSystem workflowSystem, ProcessManager manager) {
        super(workflowSystem, manager);
        exceptionMessageSource = IMessageHandler.ExceptionSource.GCProcess;
    }

    @Override
    protected void Run() throws Exception {
        GCCom gcCom = workflowSystem.getGCCom();
        if(true) {
            if (workflowSystem.getGCCom().WaitForCommandable().getBatches_running().size() == 0) {
                LinkedList<Batch> batches_in_gc = new LinkedList<Batch>();

                for (BatchLocation location : new BatchLocation[]{BatchLocation.GC_Manipulation_1, BatchLocation.GC_Manipulation_2, BatchLocation.GC_Manipulation_3}) {
                    Batch batch = workflowSystem.getState().getRackAtLocation(location);

                    if (batch != null) {
                        batches_in_gc.add(batch);
                    }
                }

                //Check if batches have the right status:
                if (batches_in_gc.size() != batches_in_gc.stream().filter(x -> x.getStatus().equals(BatchStatus.GCAnalysis)).toArray().length) {
                    if (batches_in_gc.size() != batches_in_gc.stream().filter(x -> x.getStatus().equals(BatchStatus.GCAnalysis)
                            || x.getStatus().equals(BatchStatus.LoadGC)
                            || x.getStatus().equals(BatchStatus.UnloadGC)).toArray().length)
                        //Not all batches have the right statu
                        throw new InconsistentStateException("Not all batches in GC have the status photolyss, nor are the remaining being loaded");

                    // now at this point not all batches have been submitted
                    // , or they are being unloaded
                    //So then all is ok?...
                    return;
                }

//            workflowSystem.getMessageHandler().LogMessage(BatchTimestampEnum.gc_analysis, MessagePriority.INFO,"Calling GC.GetResults");
                if (batches_in_gc.size() != 0 && !gcCom.GetResults(batches_in_gc.toArray(new Batch[0]))) {
                    throw new InconsistentStateException("Can not retrieve all results for photolysis, eventhough everything is DONE - logic error ?");
                }
//            workflowSystem.getMessageHandler().LogMessage(BatchTimestampEnum.gc_analysis, MessagePriority.INFO,"Successfully called GC.GetResults");

                if(batches_in_gc != null && batches_in_gc.size() != 0) {
                    for (Batch batch : batches_in_gc) {
                        batch.CalculateWeightedHydrogen();
                    }


                    for (Batch batch : batches_in_gc) {

                        Files.move(Paths.get(batch.getBatch_file().getAbsolutePath()), Paths.get("completed\\" + batch.getBatch_name() + ".run"));
                        batch.updateFile(new File("completed\\" + batch.getBatch_name() + ".run"));

                        batch.setTimestamp(BatchTimestampEnum.gc_analysis, BatchTimestampType.finish, new Date());
                        batch.setStatus(BatchStatus.UnloadGC);
                    }

                    //Calculate new file
                    try {
                        CollectData.CreateOverview(workflowSystem, "completed");
                        Files.copy((new File("completed\\overview.csv")).toPath(), (new File("D:\\benjamin\\Dropbox\\KUKA\\Experiments\\live output\\overview.csv")).toPath(), StandardCopyOption.REPLACE_EXISTING);
                    } catch (Exception e) {
                        workflowSystem.getMessageHandler().HandleException(exceptionMessageSource, e);
                    }

                    //HACK This was implemented to clear the queue once something gets generated to ensure i-2 by forcing the optimiser to generate
//                    File file = new File("runqueue");
//                    String[] myFiles;
//                    if (file.isDirectory()) {
//                        myFiles = file.list();
//                        for (int i = 0; i < myFiles.length; i++) {
//                            File myFile = new File(file, myFiles[i]);
//                            myFile.delete();
//                        }
//                    }
                }
            }
        }
    }
}

