package Workflow.ProcessManagement.Process;

import Workflow.Batch.*;
import Workflow.Configuration.Configuration;
import Workflow.Configuration.PhotolysisStation;
import Workflow.Exception.InconsistentStateException;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.ProcessManagement.ProcessTypeEnum;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Date;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class PhotolysisProcess extends Workflow.ProcessManagement.ProcessSuper {


    public PhotolysisProcess(WorkflowSystem workflowSystem, ProcessManager manager) {
        super(workflowSystem, manager);
        exceptionMessageSource = IMessageHandler.ExceptionSource.PhotolysisProcess;
    }

    @Override
    protected void Run() throws Exception {
        //responsible for turning off and setting time stamps
        if(true)
            {
                Batch batch;

                if (Configuration.getPhotolysisStation().equals(PhotolysisStation.Magnetic)) {
                    batch = workflowSystem.getState().getRackAtLocation(BatchLocation.Photocat_Manipulation);
                } else if (Configuration.getPhotolysisStation().equals(PhotolysisStation.Vibratory)) {
                    batch = workflowSystem.getState().getRackAtLocation(BatchLocation.Vibratory_Photocat_Manipulation);
                } else {
                    throw new NotImplementedException();
                }

                if (batch != null && batch.getStatus() == BatchStatus.StartPhotolysis) {
                    //case 1: sonicator off son_time = 0 - old situation
                    //case 2: equilibriate in sonicator son_time != 0 - equilibriate situation
                    if (  ( //batch.getSonication_time_min() == 0 &&
                            (workflowSystem.getState().getPhotolysisStopTime() == null || workflowSystem.getState().getPhotolysisStopTime().getTime() + 15 * 60 * 1000 < (new Date()).getTime()))
//                            ||
//                            (batch.getSonication_time_min() != 0 &&  (batch.getTimestamp(BatchTimestampEnum.load_photocat, BatchTimestampType.finish).getTime() + 2 * 60 * 60 * 1000 < (new Date()).getTime()))
                    ) {
                        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.start, new Date());
                        if (Configuration.getPhotolysisStation().equals(PhotolysisStation.Magnetic)) {
                            workflowSystem.getPhotoCom().Start();
                        } else if (Configuration.getPhotolysisStation().equals(PhotolysisStation.Vibratory)) {
                            workflowSystem.getVibratoryCom().Start();
                        } else {
                            throw new NotImplementedException();
                        }
                        batch.setStatus(BatchStatus.Photolysis);
                        batch.setTemperature_Photolysis_start(workflowSystem.getProbeCom().getTemperature());
                    }
                } else if (batch != null && batch.getStatus() == BatchStatus.Photolysis) {
                    Date start = batch.getTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.start);
                    if (start != null) {
                        if (batch.getIllumination_time_min() * 60 * 1000 < (new Date()).getTime() - start.getTime()) {
                            workflowSystem.getState().SetPhotolysisStopTime(new Date());
                            if (Configuration.getPhotolysisStation().equals(PhotolysisStation.Magnetic)) {
                                workflowSystem.getPhotoCom().Stop();
                            } else if (Configuration.getPhotolysisStation().equals(PhotolysisStation.Vibratory)) {
                                workflowSystem.getVibratoryCom().Stop();
                            } else {
                                throw new NotImplementedException();
                            }
                            batch.setTemperature_Photolysis_end(workflowSystem.getProbeCom().getTemperature());
                            batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.finish, new Date());
                            batch.setStatus(BatchStatus.UnloadPhotocat);
                        }
                    } else {
                        throw new InconsistentStateException("Batch on photolysis, but has no start photolysis time");
                    }
                }
            }
        }
    }

