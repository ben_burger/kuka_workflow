package Workflow.ProcessManagement.Process;

import Workflow.Batch.*;
import Workflow.Exception.DeviceOfflineException;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class SonicatorProcess extends Workflow.ProcessManagement.ProcessSuper {


    public SonicatorProcess(WorkflowSystem workflowSystem, ProcessManager manager) {
        super(workflowSystem, manager);
        exceptionMessageSource = IMessageHandler.ExceptionSource.SonicatorProcess;

    }

    @Override
    protected void Run() throws DeviceOfflineException, IOException {
        if (true) {
            workflowSystem.getSonicatorCom();
        //responsible for turning off and setting time stamps
        Batch batch = workflowSystem.getState().getRackAtLocation(BatchLocation.SonicationStation_Manipulation);
        if (batch != null) {
            Date start = batch.getTimestamp(BatchTimestampEnum.sonication, BatchTimestampType.start);
            if (start != null) {

                if (batch.getSonication_time_min() * 60 * 1000 < (new Date()).getTime() - start.getTime()
                        //&& batch.getTimestamp(BatchTimestampEnum.sonication, BatchTimestampType.finish) == null
                        ) {
                    workflowSystem.getSonicatorCom().Stop();
                    batch.setTimestamp(BatchTimestampEnum.sonication, BatchTimestampType.finish, new Date());
                    batch.setStatus(BatchStatus.UnLoadSonicator);
                }
            }
        }
        }
    }
}
