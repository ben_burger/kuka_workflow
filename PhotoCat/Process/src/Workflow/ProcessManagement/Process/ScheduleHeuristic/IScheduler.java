package Workflow.ProcessManagement.Process.ScheduleHeuristic;

import Workflow.Exception.DeviceOfflineException;
import Workflow.Exception.RobotErrorStateException;
import Workflow.Exception.RobotNoHeartBeatException;
import Workflow.ProcessManagement.Process.RobotJob.JobSuper;
import Workflow.WorkflowSystem;

import java.io.IOException;
import java.text.ParseException;

/**
 * Created by Benjamin on 9/6/2018.
 */
public interface IScheduler {
    JobSuper GetNewJob(WorkflowSystem workflowSystem) throws IOException, ParseException, DeviceOfflineException, InterruptedException, RobotNoHeartBeatException, RobotErrorStateException, CloneNotSupportedException;

    int getNumberOfRacksInSystem();
}
