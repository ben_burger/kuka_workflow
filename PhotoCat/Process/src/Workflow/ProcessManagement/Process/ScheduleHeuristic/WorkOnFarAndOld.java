package Workflow.ProcessManagement.Process.ScheduleHeuristic;

import Workflow.Batch.Batch;
import Workflow.Batch.BatchLocation;
import Workflow.Batch.BatchStatus;
import Workflow.Batch.Progress;
import Workflow.Configuration.Configuration;
import Workflow.Configuration.Location;
import Workflow.Configuration.PhotolysisStation;
import Workflow.Exception.DeviceOfflineException;
import Workflow.Exception.RobotErrorStateException;
import Workflow.Exception.RobotNoHeartBeatException;
import Workflow.ProcessManagement.Process.RobotJob.*;
import Workflow.ProcessManagement.Process.RobotJob.JobSuper;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;

import java.util.*;

/**
 * Created by Benjamin on 9/6/2018.
 * This scheduler will favor racks that have a high status-step, and after that their run - start or name.
 * Will also favor jobs for which the robot does not have to drive - lazyness+1
 */
public class WorkOnFarAndOld implements IScheduler {
    @Override
    public JobSuper GetNewJob(WorkflowSystem workflowSystem) throws DeviceOfflineException, InterruptedException, RobotNoHeartBeatException, RobotErrorStateException, CloneNotSupportedException {
        Batch[] batches_in_system = workflowSystem.getState().getRunsInTheSystem().stream()
                .filter(x -> !x.getBatch_name().contains("FreshRack") && x.getStatus() != BatchStatus.DONE)
                .sorted()
                .toArray(Batch[]::new);
        Location current_location = workflowSystem.getKMRCom().GetStatus().getLastCommandedLocation();

        JobSuper result = null;


        for (int index = batches_in_system.length-1; index >=0; index--) {
            if (batches_in_system[index].getProgress() == Progress.Waiting && ( batches_in_system[index].getStatus() == BatchStatus.LoadPhotocat  || batches_in_system[index].getStatus() == BatchStatus.UnloadPhotocat)){
                //Just try it
                result = scheduleJob(workflowSystem, batches_in_system[index],null);
            }

            //if we failed, we can try again..
            if (result != null)
                return result;
        }

        Batch[] priority = new Batch[0];
        switch (current_location) {
            case Quantos:
                priority = Arrays.stream(batches_in_system)
                        .filter(x -> x.getStatus().equals(BatchStatus.LoadQuantos) || x.getStatus().equals(BatchStatus.UnloadQuantos))
                        .sorted()
                        .toArray(Batch[]::new);
                break;
            case Vibratory_Photocat:
                priority = Arrays.stream(batches_in_system)
                        .filter(x -> x.getStatus().equals(BatchStatus.LoadPhotocat) || x.getStatus().equals(BatchStatus.UnloadPhotocat))
                        .sorted()
                        .toArray(Batch[]::new);

                break;
            case GC:
                priority = Arrays.stream(batches_in_system)
                        .filter(x -> x.getStatus().equals(BatchStatus.LoadGC) || x.getStatus().equals(BatchStatus.UnloadGC))
                        .sorted()
                        .toArray(Batch[]::new);
                break;
            case Sonicator:
                priority = Arrays.stream(batches_in_system)
                        .filter(x -> x.getStatus().equals(BatchStatus.LoadSonicator) || x.getStatus().equals(BatchStatus.UnLoadSonicator))
                        .sorted()
                        .toArray(Batch[]::new);
                break;
            case Cartridge:
                priority = Arrays.stream(batches_in_system)
                        .filter(x -> x.getStatus().equals(BatchStatus.CartridgeChangeQuantos))
                        .sorted()
                        .toArray(Batch[]::new);
                break;
            case InputStation2:
            case InputStation:
            case InputStation3:
                priority = Arrays.stream(batches_in_system)
                        .filter(x -> (x.getStatus().equals(BatchStatus.PickUp) && current_location.equals(x.getBatchLocation().getLocation()))
                                || (x.getStatus().equals(BatchStatus.MoveToStorage) && null == x.getBatchLocation().getLocation())
                        )
                        .sorted()
                        .toArray(Batch[]::new);
            break;
        }


        //oldest first
        for (int index = priority.length-1; index >=0; index--) {
            Batch batch = priority[index];
            if (batch.getProgress() == Progress.Waiting) {
                //Just try it
                result = scheduleJob(workflowSystem, batch, current_location);
            }

            //if we failed, we can try again..
            if (result != null)
                return result;
        }
        //try and change CARTRIDGE!!
        for (int index = batches_in_system.length-1; index >=0; index--) {
            if (batches_in_system[index].getProgress() == Progress.Waiting && batches_in_system[index].getStatus() == BatchStatus.CartridgeChangeQuantos) {
                //Just try it
                result = scheduleJob(workflowSystem, batches_in_system[index],null);
            }

            //if we failed, we can try again..
            if (result != null)
                return result;
        }

        //oldest first
        for (int index = batches_in_system.length-1; index >=0; index--) {
            if (batches_in_system[index].getProgress() == Progress.Waiting) {
                //Just try it
                result = scheduleJob(workflowSystem, batches_in_system[index],null);
            }

            //if we failed, we can try again..
            if (result != null)
                return result;
        }

        return null;
    }

    @Override
    public int getNumberOfRacksInSystem() {
        return Configuration.getScheduler().getNumberOfRacks();
    }

    private JobSuper scheduleJob(WorkflowSystem workflowSystem, Batch batch, Location force_location) {
        if (batch.getProgress() == Progress.Waiting) {
            switch (batch.getStatus()) {
                case PickUp:
/*right location?*/  if(force_location == null || batch.getBatchLocation().getLocation().equals(force_location))
/*Step enabled*/        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.Storage))
/*Can we do it?*/           if(workflowSystem.getState().getFreePositionOnRobot() != null)
                                return new PickUpBatchFromInput(batch);
                    break;
                case LoadQuantos:
                    if(force_location == null || Location.Quantos.equals(force_location))
                        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.SolidDispensing) && workflowSystem.getState().getRackAtLocation(BatchLocation.SolidDispenseStation_Manipulation) == null)
                            return new LoadRackQuantos(batch);
                    break;
                case DispenseSolid:
                    //Automated Quantos Event
                    break;
                case CartridgeChangeQuantos:
                    if(force_location == null || Location.Cartridge.equals(force_location))
                        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.SolidDispensing))
                            return new CartridgeQuantos(batch);
                    break;
                case UnloadQuantos:
                    if(force_location == null || Location.Quantos.equals(force_location))
                        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.SolidDispensing))
                            return new UnloadRackQuantos(batch);
                    break;
                case DispenseLiquid:
                    if(force_location == null || Location.LiquidHandlingSystem.equals(force_location))
                        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.LiquidDispensing)
                                && workflowSystem.getState().getRackAtLocation(BatchLocation.LiquidDispenseStation_Manipulation) == null
                                && workflowSystem.getState().getRackAtLocation(BatchLocation.Vibratory_Photocat_Manipulation) == null
                        )

                        return new DispenseLiquids(batch);
                    break;
                case LoadSonicator:
                    if(force_location == null || Location.Sonicator.equals(force_location))
                        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.Sonication) && workflowSystem.getState().getRackAtLocation(BatchLocation.SonicationStation_Manipulation) == null)
                        return new LoadRackSonicator(batch);
                    break;
                case Sonicate:
                    //*Automated Event
                    break;
                case UnLoadSonicator: {
                    if(force_location == null || Location.Sonicator.equals(force_location))
                        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.Sonication))
                        return new UnloadRackSonicator(batch);
                }
                    break;
                case LoadPhotocat:
                    if(force_location == null || Location.Vibratory_Photocat.equals(force_location))
                        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.Photolysis))
                        if ((Configuration.getPhotolysisStation().equals(PhotolysisStation.Magnetic) && workflowSystem.getState().getRackAtLocation(BatchLocation.Photocat_Manipulation) == null)
                                || (Configuration.getPhotolysisStation().equals(PhotolysisStation.Vibratory) && workflowSystem.getState().getRackAtLocation(BatchLocation.Vibratory_Photocat_Manipulation) == null)) {
                            return new LoadRackPhotocat(batch);
                        }
                    break;
                case Photolysis:
                    //*Automated Event
                    break;
                case UnloadPhotocat: {
                    if(force_location == null || Location.Vibratory_Photocat.equals(force_location))
                        if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.Photolysis))
                        return new UnloadRackPhotocat(batch);
                }
                    break;
                case LoadGC:
                    if(force_location == null || Location.GC.equals(force_location))
                    {

                        boolean canLoadGC = true;
                        if (!workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.GCAnalysis)) {
                            canLoadGC = false;
                        }
                        for (BatchLocation location : new BatchLocation[]{BatchLocation.GC_Manipulation_1, BatchLocation.GC_Manipulation_2, BatchLocation.GC_Manipulation_3}) {
                            if (workflowSystem.getState().getRackAtLocation(location) != null) {
                                canLoadGC = canLoadGC && false;
                                break;
                            }
                        }

                        Batch[] batches_in_system = workflowSystem.getState().getRunsInTheSystem().stream()
                                .filter(x -> x.getStatus().equals(BatchStatus.LoadGC) && !x.getBatch_name().contains("FreshRack") && x.getProgress().equals(Progress.Waiting))
                                .sorted(Comparator.reverseOrder()) //Limit takes the first 3, so reverse order.
                                .limit(3)
                                .toArray(Batch[]::new);

                        if (canLoadGC) {
                            return new LoadRackGC(batches_in_system);
                        }
                        break;
                    }
                    break;
                case GCAnalysis:
                //*Automated EventHi TR
                    break;
                case UnloadGC: {
                    if(force_location == null || Location.GC.equals(force_location))
                    {

                        boolean canUnloadGC = true;
                        if (!workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.GCAnalysis)) {
                            canUnloadGC = false;
                        }
                        for (BatchLocation location : new BatchLocation[]{BatchLocation.GC_Manipulation_1, BatchLocation.GC_Manipulation_2, BatchLocation.GC_Manipulation_3}) {
                            Batch b = workflowSystem.getState().getRackAtLocation(location);

                            if (b != null && !b.getStatus().equals(BatchStatus.UnloadGC)) {
                                canUnloadGC = canUnloadGC && false;
                                break;
                            }
                        }

                        Batch[] batches_in_system = workflowSystem.getState().getRunsInTheSystem().stream()
                                .filter(x -> x.getStatus().equals(BatchStatus.UnloadGC) && !x.getBatch_name().contains("FreshRack") && x.getProgress().equals(Progress.Waiting))
                                .sorted()
                                .toArray(Batch[]::new);
                        if (canUnloadGC && batches_in_system.length <= 3 && batches_in_system.length > 0) {
                            return new UnloadRackGC(batches_in_system);
                        }

                        break;
                    }
                }
                break;
                case MoveToStorage: {
                    if (workflowSystem.getProcessManager().GetStepEnabled(WorkflowStepEnum.Storage)) {
                        BatchLocation[] buffer_locations = workflowSystem.getState().getBufferBatchLocationForStatus(batch, BatchStatus.MoveToStorage);
                        if(force_location == null)
                            if (buffer_locations != null && buffer_locations.length > 0)
                                return new MoveToStorage(batch, buffer_locations[0]);
                        else {
                                Optional<BatchLocation> batch_location_at_forced_location = Arrays.stream(buffer_locations).filter(x->force_location.equals(x.getLocation())).findFirst();
                            if(batch_location_at_forced_location.isPresent())
                                return new MoveToStorage(batch, batch_location_at_forced_location.get());
                            }
                    }

                    break;
                }
                case DONE:
                default:
            }
        }

        return null;
    }
}
