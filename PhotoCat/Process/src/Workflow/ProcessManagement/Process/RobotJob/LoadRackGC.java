package Workflow.ProcessManagement.Process.RobotJob;


import Workflow.Batch.*;
import Workflow.Com.TCP.GCCom;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import Workflow.Configuration.Location;
import Workflow.WorldState.WorldState;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * Transports the rack from a buffer to manipulation rack holder of quantos
 * Loads the Quantos + sets up first cartridge
 * Expects the manipulation holder to be free
 */
public class LoadRackGC extends JobSuper {
    Batch[] batches;
    public LoadRackGC(Batch[] batch) {
        super(batch[0], WorkflowStepEnum.GCAnalysis);
        batches=batch;
        exceptionMessageSource = IMessageHandler.ExceptionSource.GCProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws Exception {
        for(Batch array_batch : batches) {
            array_batch.setProgress(Progress.Running);
            array_batch.setTimestamp(BatchTimestampEnum.load_gc, BatchTimestampType.start, new Date());
        }
        WorldState state = workflowSystem.getState();
        GCCom gcCom = workflowSystem.getGCCom();

        for(Batch array_batch:batches) {
            BatchLocation batchLocation = array_batch.getBatchLocation();

            Location location = batchLocation.getLocation();

            if (location != null) {
                //batch is not on the robot
                EnsureRobotIsCalibratedAtLocation(workflowSystem, batchLocation.getLocation());
                BatchLocation new_robot_location = state.getFreePositionOnRobot();
                workflowSystem.getLBRCom().GraspRackFromBufferAndWait(new_robot_location.GetStationIndex(), batchLocation.GetStationIndex());
                state.MoveRack(batchLocation, new_robot_location, array_batch);
            }
         }
            EnsureRobotIsCalibratedAtLocation(workflowSystem, Location.GC);

            workflowSystem.getLBRCom().PressParkHeadspaceButtonAndWait();
            BatchLocation[] gc_locations = new BatchLocation[] {BatchLocation.GC_Manipulation_1,BatchLocation.GC_Manipulation_2,BatchLocation.GC_Manipulation_3};
            int batch_location_index = 0;
            for(Batch array_batch:batches) {
                BatchLocation batchLocation = array_batch.getBatchLocation();

                workflowSystem.getLBRCom().PlaceRackInBufferAndWait(array_batch.getBatchLocation().GetStationIndex(), gc_locations[batch_location_index].GetStationIndex());
                state.MoveRack(array_batch.getBatchLocation(), gc_locations[batch_location_index], array_batch);
                array_batch.setTimestamp(BatchTimestampEnum.gc_analysis, BatchTimestampType.start, new Date());
                array_batch.setTimestamp(BatchTimestampEnum.load_gc, BatchTimestampType.finish, new Date());

                SetGCWellNumbers(workflowSystem, array_batch, gc_locations[batch_location_index]);
                batch_location_index++;
            }
            workflowSystem.getLBRCom().PressParkHeadspaceButtonAndWait();
        gcCom.StartBatch(batches);
        for(Batch array_batch:batches) {
            array_batch.setStatusAndWaiting(BatchStatus.GCAnalysis);
        }
    }

    private void SetGCWellNumbers(WorkflowSystem workflowSystem, Batch batch, BatchLocation gc_batch_location) throws IOException {
        int index;
        switch (gc_batch_location) {
            case GC_Manipulation_1:
                index=0;
                break;
            case GC_Manipulation_2:
                index=1;
                break;
            case GC_Manipulation_3:
                index=2;
                break;
            default:
                throw new NotImplementedException();
        }
        for(Sample sample : batch.getSamples())
        {
            int sample_rack_index = sample.getRackIndex();
            int result = 12*((int)Math.floor(sample_rack_index/6)) + index * 36 + (sample_rack_index % 6) + 1;
            batch.setGCWell(sample, result);

        }

    }
}
