package Workflow.ProcessManagement.Process.RobotJob;

import Workflow.Batch.Batch;
import Workflow.Batch.Sample;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import com.github.sarxos.webcam.Webcam;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_highgui;
import org.opencv.core.Core;
import org.opencv.highgui.VideoCapture;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;


public abstract class webcamSupportJob extends JobSuper{
    public webcamSupportJob(Batch batch, WorkflowStepEnum workflowstep) {
        super(batch, workflowstep);
    }

    protected void TakePicture(WorkflowSystem workflowSystem, int i,Batch batch, Sample sample, String s) {
        try {
            workflowSystem.TakePicture(i, batch, sample, s);
        } catch (Exception e)
        {}
    }

    protected void DisposeOfWebcam() {
    }
}
