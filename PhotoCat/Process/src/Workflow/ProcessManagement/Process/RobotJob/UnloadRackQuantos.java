package Workflow.ProcessManagement.Process.RobotJob;


import Workflow.Batch.*;
import Workflow.Exception.DeviceOfflineException;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.Process.RobotJob.Exception.LiquidSystemCanNotDispenseException;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * unloads the quantos to manipulation rack
 * moves manipulation rack to a buffer
 */
public class UnloadRackQuantos extends JobSuper {
    Thread thread_station;
    Exception exception_thread_station;

    public UnloadRackQuantos(Batch batch) {
        super(batch, WorkflowStepEnum.SolidDispensing);
        exceptionMessageSource = IMessageHandler.ExceptionSource.SolidDispensingProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws Exception {
        batch.setProgress(Progress.Running);
        batch.setTimestamp(BatchTimestampEnum.unload_quantos, BatchTimestampType.start, new Date());
        workflowSystem.getQuantosCom().MoveAutoSampler(0);
    EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, BatchLocation.SolidDispenseStation_Manipulation);

        Sample[] samples = Arrays.stream(batch.getSamples()).sorted((e1, e2) -> Integer.compare(e1.getSampleIndex(),e2.getSampleIndex())).toArray(Sample[]::new);
        MoveAutoSampler(workflowSystem,samples[0]);

        workflowSystem.getLBRCom().StartVialHandlingAndWait();

        for(int i=0; i<samples.length; i++)
        {
            WaitForFinish();
            workflowSystem.getLBRCom().GraspSampleFromQuantosAndWait();
            if(i+1 < samples.length)
                MoveAutoSampler(workflowSystem, samples[i+1]);
            workflowSystem.getLBRCom().PlaceVialInBufferAndWait(samples[i].getRackIndex());
        }

        workflowSystem.getLBRCom().StopVialHandlingAndWait();
        BatchLocation location_on_robot = workflowSystem.getState().getFreePositionOnRobot();
        MoveManipulationBatchToBuffer(workflowSystem,BatchLocation.SolidDispenseStation_Manipulation,location_on_robot);
        batch.setTimestamp(BatchTimestampEnum.unload_quantos, BatchTimestampType.finish, new Date());
        batch.setStatusAndWaiting(NextStep(batch));

    }
    private void WaitForFinish() throws Exception {
        thread_station.join();
        if(exception_thread_station != null)
            throw exception_thread_station;
    }
    private void MoveAutoSampler(WorkflowSystem workflowSystem, Sample sample) throws LiquidSystemCanNotDispenseException, InterruptedException, DeviceOfflineException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    workflowSystem.getQuantosCom().MoveAutoSampler((16 + sample.getSampleIndex()) % 31);
                } catch (Exception e) {
                    exception_thread_station = e;
                }
            }
        });
        thread_station = thread;
        exception_thread_station = null;

        thread.start();
    }
}
