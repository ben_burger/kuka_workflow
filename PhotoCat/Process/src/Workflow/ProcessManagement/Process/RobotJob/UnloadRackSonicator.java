package Workflow.ProcessManagement.Process.RobotJob;

 
import Workflow.Batch.*;
import Workflow.Com.TCP.LBRCom;
import Workflow.Exception.*;
import Workflow.Configuration.Location;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * unloads the quantos to manipulation rack
 * moves manipulation rack to a buffer
 */
public class UnloadRackSonicator extends webcamSupportJob {


    public UnloadRackSonicator(Batch batch) {
        super(batch, WorkflowStepEnum.Sonication);
        exceptionMessageSource = IMessageHandler.ExceptionSource.SonicatorProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws InterruptedException, RobotErrorStateException, CloneNotSupportedException, IOException, DeviceOfflineException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
            batch.setProgress(Progress.Running);
            batch.setTimestamp(BatchTimestampEnum.unload_sonicator, BatchTimestampType.start, new Date());

            EnsureRobotIsCalibratedAtLocation(workflowSystem, Location.Sonicator);

            LBRCom lbr = workflowSystem.getLBRCom();
            workflowSystem.getVibratoryCom();

            lbr.StartVialHandlingAndWait();
            Sample[] samples = Arrays.stream(batch.getSamples()).sorted((e1, e2) -> Integer.compare(e1.getSampleIndex(), e2.getSampleIndex())).toArray(Sample[]::new);
            for (Sample sample : samples) {
                lbr.GraspVialFromSonicatorAndWait(sample.getSampleNumber());

                workflowSystem.getLBRCom().MoveToDryPositionAndWait();
                workflowSystem.getVibratoryCom().StartDry();
                workflowSystem.getLBRCom().DryProcedureAndWait();
                workflowSystem.getVibratoryCom().StopDry();

                TakePicture(workflowSystem,1,batch, sample,"after-sonication");

                workflowSystem.getLBRCom().MoveToNeutralAfterDryAndWait();

                lbr.PlaceVialInBufferAndWait(sample.getRackIndex());
            }
            lbr.StopVialHandlingAndWait();
            BatchLocation location_on_robot = workflowSystem.getState().getFreePositionOnRobot();
            MoveManipulationBatchToBuffer(workflowSystem, BatchLocation.SonicationStation_Manipulation, location_on_robot);
            batch.setTimestamp(BatchTimestampEnum.unload_sonicator, BatchTimestampType.finish, new Date());

            batch.setStatusAndWaiting(NextStep(batch));

            DisposeOfWebcam();
    }


}
