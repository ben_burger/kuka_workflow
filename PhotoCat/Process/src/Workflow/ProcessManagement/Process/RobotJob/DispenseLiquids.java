package Workflow.ProcessManagement.Process.RobotJob;


import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.Batch.*;
import Workflow.Com.Serial.CapperCom;
import Workflow.Exception.*;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.OutOfLiquidException;
import Workflow.ProcessManagement.Process.RobotJob.Exception.LiquidSystemCanNotDispenseException;
import Workflow.LiquidDispensingProcessManager;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Benjamin on 9/6/2018.
 * This class is reponsible for
 * Transporting a rack into manipulation rack of liquid station
 * Dispense liquids in each vial + robot manipulation
 * Transport rack to a buffer
 */
public class DispenseLiquids extends JobSuper {
    Thread dispense_thread_station0;
    Exception exception_dispense_thread_station0;
    Date dispense_thread_station0_started;

    Thread dispense_thread_station1;
    Exception exception_dispense_thread_station1;
    Date dispense_thread_station1_started;

    Thread cap_thread;
    Exception exception_cap_thread_station;
    Date cap_thread_started;

    public DispenseLiquids(Batch batch) {
        super(batch, WorkflowStepEnum.LiquidDispensing);
        exceptionMessageSource = IMessageHandler.ExceptionSource.LiquidDispenseProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws Exception {
        batch.setProgress(Progress.Running);
        batch.setTimestamp(BatchTimestampEnum.dispense_liquid, BatchTimestampType.start, new Date());

        PerformCheckCanDispenseAllLiquid(workflowSystem);


        WorldState state = workflowSystem.getState();
        EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, BatchLocation.LiquidDispenseStation_Manipulation);


        //Sometimes once is not enough, so....
        workflowSystem.getCapperCom().SetParameter(CapperCom.CapperParameter.PurgeTime, batch.getPurge_time_sec() + "");
        workflowSystem.getCapperCom().SetParameter(CapperCom.CapperParameter.PurgeTime, batch.getPurge_time_sec() + "");
        workflowSystem.getCapperCom().SetParameter(CapperCom.CapperParameter.PurgeTime, batch.getPurge_time_sec() + "");
        workflowSystem.getCapperCom().SetParameter(CapperCom.CapperParameter.PurgeTime, batch.getPurge_time_sec() + "");
        workflowSystem.getCapperCom().SetParameter(CapperCom.CapperParameter.PurgeTime, batch.getPurge_time_sec() + "");

        workflowSystem.getLBRCom().StartVialHandlingAndWait();
        Sample[] samples = Arrays.stream(batch.getSamples()).sorted(Comparator.comparingInt(Sample::getSampleIndex)).filter(x -> x.hasLiquidsToDispense() || (batch.getCap_vials() && !x.getCapped())).toArray(Sample[]::new);
        int n = samples.length;
        boolean perform_recovery = false;
        for (int i = 0; i < samples.length + 3; i++) {
            if (batch.getCap_vials()) {

                if (i >= 3 && i < n + 3) {
                    Sample sample = samples[i - 3];
                    if (WaitForFinishCapper(workflowSystem)) {
                        perform_recovery = true;
                        workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert, new CapperDidNotFinishInTime(),"Robot resetting, and disabling step."));
                        break;
                    }

                    workflowSystem.getLBRCom().GraspVialFromCapperAndWait();
                    workflowSystem.getLBRCom().PlaceVialInBufferAndWait(sample.getRackIndex());
                }

                if (i >= 2 && i < n + 2) {
                    Sample sample = samples[i - 2];
                    if (WaitForFinishDispense(workflowSystem, 1)) {
                        perform_recovery = true;
                        workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert, new LiquidDispenseDidNotFinishInTime(1),"Robot resetting, and disabling step."));
                        break;
                    }
                    workflowSystem.getLBRCom().GraspVialFromLiquidStationAndWait(1);

                    PerformCheckAllLiquidsDispensed(batch, sample);

                    workflowSystem.getLBRCom().PlaceVialInCapperAndWait();
                    StartCapping(workflowSystem, sample);
                }
            } else {
                if (i >= 2 && i < n + 2) {
                    Sample sample = samples[i - 2];
                    if (WaitForFinishDispense(workflowSystem, 1)) {
                        workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert, new LiquidDispenseDidNotFinishInTime(1),"Robot resetting, and disabling step."));
                        perform_recovery = true;
                        break;
                    }
                    workflowSystem.getLBRCom().GraspVialFromLiquidStationAndWait(1);
                    workflowSystem.getLBRCom().PlaceVialInBufferAndWait(sample.getRackIndex());

                }
            }

            if (i >= 1 && i < n + 1) {
                Sample sample = samples[i - 1];
                if (WaitForFinishDispense(workflowSystem, 0)) {
                    workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert, new LiquidDispenseDidNotFinishInTime(0),"Robot resetting, and disabling step."));
                    perform_recovery = true;
                    break;
                }
                float weight = workflowSystem.getScaleCom(1).getStableWeight();
                workflowSystem.getLBRCom().MoveVialFromStationOneToStationTwoAndWait();
                if (workflowSystem.getScaleCom(1).getStableWeight() - weight < 5) {
                    workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert, new InconsistentStateException("Vial got lost, detected in liquid station 1."), "Robot resetting, and disabling step."));
                    perform_recovery = true;
                    break;
                }
                StartDispense(workflowSystem, sample, 1);
            }

            if (i >= 0 && i < n) {
                Sample sample = samples[i];
                workflowSystem.getLBRCom().GraspVialFromBufferAndWait(sample.getRackIndex());
                float weight = workflowSystem.getScaleCom(0).getStableWeight();
                workflowSystem.getLBRCom().PlaceVialInLiquidStationAndWait(0);
                if (workflowSystem.getScaleCom(0).getStableWeight() - weight < 5) {
                    workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert, new InconsistentStateException("Vial got lost, detected in liquid station 0."), "Robot resetting, and disabling step."));
                    perform_recovery = true;
                    break;
                }
                StartDispense(workflowSystem, sample, 0);
            }
        }
        if (perform_recovery) {
            workflowSystem.getProcessManager().EnableStep(WorkflowStepEnum.LiquidDispensing, false);
            workflowSystem.getProcessManager().EnableStep(WorkflowStepEnum.SolidDispensing, false);
            workflowSystem.getLBRCom().StopVialHandlingAndWait();

            //@HACK
            workflowSystem.getLBRCom().AllowChargingOfRobotAndWait();
            try {
                WaitForFinishCapper(workflowSystem);
            } catch (Exception e) {
            }
            try {
                WaitForFinishDispense(workflowSystem, 0);
            } catch (Exception e) {
            }
            try {
                WaitForFinishDispense(workflowSystem, 1);
            } catch (Exception e) {
            }
        } else {
            workflowSystem.getLBRCom().StopVialHandlingAndWait();
            BatchLocation location_after_job = state.getFreePositionOnRobot();
            MoveManipulationBatchToBuffer(workflowSystem, BatchLocation.LiquidDispenseStation_Manipulation, location_after_job);


            batch.setTimestamp(BatchTimestampEnum.dispense_liquid, BatchTimestampType.finish, new Date());
            batch.setStatusAndWaiting(NextStep(batch));
        }
    }

    private void PerformCheckCanDispenseAllLiquid(WorkflowSystem workflowSystem) throws OutOfLiquidException {
        Liquid[] liquids_in_system = workflowSystem.getState().getLiquids();
        String[] unique_liquids = Arrays.stream(liquids_in_system).map(Liquid::getName).distinct().toArray(String[]::new);


        double[] volume_left = new double[unique_liquids.length];

        for (int i = 0; i < unique_liquids.length; i++) {
            volume_left[i] = 0;

            for (Liquid liquid : liquids_in_system) {
                if (liquid.Name.equals(unique_liquids[i])
                        && (liquid.getMinimalVolumePresent() > 55.0)
                        && (!liquid.Expired())
                ) {
                    volume_left[i] += (liquid.getMinimalVolumePresent() - 50.0);
                }
            }
        }

        //sum how much this batch will use
        double[] new_batch_volume_needed = new double[unique_liquids.length];

        for (int i = 0; i < unique_liquids.length; i++) {
            new_batch_volume_needed[i] = 0;
            String name = unique_liquids[i];

            Liquid liq = Arrays.stream(liquids_in_system)
                    .filter(x -> x.getName().equals(name))
                    .findFirst().get();
            if (Arrays.stream(batch.getLiquids()).filter(x -> x.getName().equals(name)).findFirst().isPresent())
                for (Sample sample : batch.getSamples()) {
                    double amount_needed = sample.getLiquidDispenseAmount(liq) - sample.getLiquidDispensed(liq);
                    if (amount_needed > 0.1f)
                        new_batch_volume_needed[i] += amount_needed;
                }
        }

        //Compare the sums...
        for (int i = 0; i < unique_liquids.length; i++) {
            String name = unique_liquids[i];

            if (volume_left[i] < new_batch_volume_needed[i]) {
                Liquid liq = Arrays.stream(liquids_in_system).filter(x -> x.getName().equals(name)).findFirst().get();
                throw new OutOfLiquidException(liq);
            }
        }
    }

    private void PerformCheckAllLiquidsDispensed(Batch batch, Sample sample) throws NotAllLiquidsDispensedException {
        for (Liquid liq : batch.getLiquids()) {
            //Maybe we should just sent an alert?!
//            if(0.1f<(sample.getLiquidDispensed(liq)-sample.getLiquidDispenseAmount(liq) ))
//                throw new NotAllLiquidsDispensedException();
        }
    }

    private void StartCapping(WorkflowSystem workflowSystem, Sample sample) {
        cap_thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (workflowSystem.getState().getMinimal_Caps_Present() > 0) {
                        workflowSystem.getCapperCom().StartAndWait();
                        workflowSystem.getState().SetVialCapped(batch, sample);

                    } else

                        throw new OutOfCapsException();
                } catch (Exception e) {
                    exception_cap_thread_station = e;
                }
            }
        });
        exception_cap_thread_station = null;
        cap_thread.start();
        cap_thread_started = new Date();
    }

    //returns true if action failed
    private boolean WaitForFinishCapper(WorkflowSystem workflowSystem) throws Exception {
        while (cap_thread.isAlive() &&
                (((new Date()).getTime() - cap_thread_started.getTime()) < (batch.getPurge_time_sec() + 180) * 1000)
        )
            Thread.sleep(1000);

        if (cap_thread.isAlive()) {
            return true;
        }

        if (exception_cap_thread_station != null)
            throw exception_cap_thread_station;
        return false;
    }

    //returns true if action failed
    private boolean WaitForFinishDispense(WorkflowSystem workflowSystem, int i) throws Exception {

        if (i == 0) {
            while (dispense_thread_station0.isAlive() &&
                    (((new Date()).getTime() - dispense_thread_station0_started.getTime()) < (300 * 1000))
            )
                Thread.sleep(1000);

            if (dispense_thread_station0.isAlive()) {
                workflowSystem.getLiquidCom(0).Stop();
                return true;
            }
        } else if (i == 1) {
            while (dispense_thread_station1.isAlive() &&
                    (((new Date()).getTime() - dispense_thread_station1_started.getTime()) < (300 * 1000))
            )
                Thread.sleep(1000);

            if (dispense_thread_station1.isAlive()) {
                workflowSystem.getLiquidCom(1).Stop();
                return true;
            }
        }

        if (i == 0 && exception_dispense_thread_station0 != null)
            throw exception_dispense_thread_station0;
        else if (i == 1 && exception_dispense_thread_station1 != null)
            throw exception_dispense_thread_station1;

        return false;

    }

    private void StartDispense(WorkflowSystem workflowSystem, Sample sample, int stationID) throws LiquidSystemCanNotDispenseException, InterruptedException, DeviceOfflineException {
        Thread dispense_thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Liquid[] liquids_sample = Arrays.stream(batch.getLiquids())
                            .filter(x -> sample.getLiquidDispenseAmount(x) - sample.getLiquidDispensed(x) > 0.1f)

                            .toArray(Liquid[]::new);
                    WorldState state = workflowSystem.getState();
                    for (Liquid liquid_sample : liquids_sample) {
                        float target = (sample.getLiquidDispenseAmount(liquid_sample) - sample.getLiquidDispensed(liquid_sample));
                        if ((target > 0.1f)) {
                            Optional<Liquid> liquid_ = Arrays.stream(state.getLiquids())
                                    .filter(x -> x.getStationID() == stationID)
                                    .filter(x -> x.getName().equals(liquid_sample.getName()))
                                    .filter(x -> x.getMinimalVolumePresent() > (target + 50.0f))
                                    .filter(x -> !x.Expired())
                                    .sorted(Comparator.comparing(Liquid::getExpiryDate))
                                    .findAny();
                            if (liquid_.isPresent()) {
                                Liquid liquid = liquid_.get();
                                LiquidDispensingProcessManager liquid_station = new LiquidDispensingProcessManager(workflowSystem, stationID);

                                float volume_dispensed = 0;

                                volume_dispensed = liquid_station.DispenseLiquidPController(liquid.getPumpID(), target * liquid.Density) / liquid.Density;

                                state.AddLiquidDispesedAmount(batch, sample, liquid, volume_dispensed);


                            }
                        }
                    }
                } catch (Exception e) {
                    if (stationID == 0) {

                        exception_dispense_thread_station0 = e;
                    } else {
                        exception_dispense_thread_station1 = e;
                    }

                }
            }
        });
        if (stationID == 0) {
            dispense_thread_station0 = dispense_thread;
            exception_dispense_thread_station0 = null;
            dispense_thread_station0_started = new Date();
        } else {
            dispense_thread_station1 = dispense_thread;
            exception_dispense_thread_station1 = null;
            dispense_thread_station1_started = new Date();
        }
        dispense_thread.start();


    }


}
