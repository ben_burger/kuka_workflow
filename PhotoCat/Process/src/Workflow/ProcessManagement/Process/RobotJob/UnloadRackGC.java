package Workflow.ProcessManagement.Process.RobotJob;

import Workflow.Batch.*;
import Workflow.Exception.*;
import Workflow.Configuration.Location;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * Transports the rack from a buffer to manipulation rack holder of quantos
 * Loads the Quantos + sets up first cartridge
 * Expects the manipulation holder to be free
 */
public class UnloadRackGC extends JobSuper {
    Batch[] batches;

    public UnloadRackGC(Batch[] batches) {
        super(batches[0], WorkflowStepEnum.GCAnalysis);
        this.batches = batches;
        exceptionMessageSource = IMessageHandler.ExceptionSource.GCProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws InterruptedException, RobotErrorStateException, CloneNotSupportedException, IOException, ParseException, DeviceOfflineException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
        for (Batch array_batch : batches) {
            array_batch.setProgress(Progress.Running);
            batch.setTimestamp(BatchTimestampEnum.unload_gc, BatchTimestampType.start, new Date());
        }


        EnsureRobotIsCalibratedAtLocation(workflowSystem, Location.GC);
        workflowSystem.getLBRCom().PressParkHeadspaceButtonAndWait();
        WorldState state = workflowSystem.getState();
        for (Batch array_batch : batches) {
            BatchLocation location = state.getFreePositionOnRobot();
            workflowSystem.getLBRCom().GraspRackFromBufferAndWait(location.GetStationIndex(), array_batch.getBatchLocation().GetStationIndex());
            workflowSystem.getState().MoveRack(array_batch.getBatchLocation(), location, array_batch);

            batch.setTimestamp(BatchTimestampEnum.unload_gc, BatchTimestampType.finish, new Date());
            array_batch.setStatusAndWaiting(NextStep(array_batch));
        }
        workflowSystem.getLBRCom().PressParkHeadspaceButtonAndWait();

    }
}
