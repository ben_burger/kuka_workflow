package Workflow.ProcessManagement.Process.RobotJob;

import Workflow.Batch.*;
import Workflow.Exception.*;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.util.Date;
import Workflow.CollectData;
public class MoveToStorage extends JobSuper {
    BatchLocation BatchLocationAfterJobFinishes;

    public MoveToStorage(Batch batch, BatchLocation batchLocationAfterJobFinishes) {
        super(batch, WorkflowStepEnum.Storage);
        BatchLocationAfterJobFinishes = batchLocationAfterJobFinishes;
        exceptionMessageSource = IMessageHandler.ExceptionSource.GenericRobotProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws InterruptedException, RobotErrorStateException, CloneNotSupportedException, IOException, ParseException, DeviceOfflineException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
        batch.setProgress(Progress.Running);
        batch.setTimestamp(BatchTimestampEnum.move_to_storage,BatchTimestampType.start,new Date());


        EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem,BatchLocationAfterJobFinishes);



        Files.move(Paths.get(batch.getBatch_file().getAbsolutePath()),Paths.get("completed\\"+batch.getBatch_name()+".run"));
        batch.updateFile(new File("completed\\"+batch.getBatch_name()+".run"));


        batch.setStatusAndWaiting(BatchStatus.DONE);
        batch.setTimestamp(BatchTimestampEnum.move_to_storage,BatchTimestampType.finish,new Date());



    }

}
