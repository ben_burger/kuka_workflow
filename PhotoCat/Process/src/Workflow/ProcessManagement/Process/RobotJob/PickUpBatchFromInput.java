package Workflow.ProcessManagement.Process.RobotJob;


import Workflow.Batch.*;
import Workflow.Configuration.Configuration;
import Workflow.Configuration.PhotolysisStation;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * ASSUMES ROBOT 0 + Solid Manipulation is Empty
 *
 * Transports the rack from a buffer to manipulation rack holder of quantos
 * Loads the Quantos + sets up first cartridge
 * Expects the manipulation holder to be free
 */
public class PickUpBatchFromInput extends JobSuper {
    public PickUpBatchFromInput(Batch batch) {
        super(batch, WorkflowStepEnum.Photolysis);
        exceptionMessageSource = IMessageHandler.ExceptionSource.PickUpNewRacksProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws Exception {
        batch.setProgress(Progress.Running);
        batch.setTimestamp(BatchTimestampEnum.PickUp,BatchTimestampType.start,new Date());
        if(batch.getBatchLocation().getLocation() != null) {
            BatchLocation location = workflowSystem.getState().getFreePositionOnRobot();
            EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, location);
        }
        batch.setTimestamp(BatchTimestampEnum.PickUp,BatchTimestampType.finish,new Date());
        batch.setStatusAndWaiting(NextStep(batch));
    }
}
