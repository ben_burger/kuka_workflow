package Workflow.ProcessManagement.Process.RobotJob;


import Workflow.Batch.*;
import Workflow.Configuration.Configuration;
import Workflow.Configuration.Location;
import Workflow.Configuration.PhotolysisStation;
import Workflow.Exception.*;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import com.sun.corba.se.spi.orbutil.threadpool.Work;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * ASSUMES ROBOT 0 + Solid Manipulation is Empty
 *
 * Transports the rack from a buffer to manipulation rack holder of quantos
 * Loads the Quantos + sets up first cartridge
 * Expects the manipulation holder to be free
 */
public class LoadRackPhotocat extends webcamSupportJob {
    public LoadRackPhotocat(Batch batch) {
        super(batch, WorkflowStepEnum.Photolysis);
        exceptionMessageSource = IMessageHandler.ExceptionSource.PhotolysisProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws Exception {
        if(batch.getIllumination_time_min() >= 9000 && batch.getIllumination_time_min() <= 9999 )
        {
            //Kinetics
            int minutes = batch.getIllumination_time_min() - 9000;
            if(!batch.getBatchLocation().isOnRobot())
            {
                throw new InconsistentStateException("Kinetics experiment without rack on robot error");
            }
            batch.setProgress(Progress.Running);
            batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.start, new Date());
            KineticsExperiment(workflowSystem,batch.getBatchLocation().GetStationIndex(), minutes);
            batch.setStatusAndWaiting(BatchStatus.LoadGC);
        } else if(batch.getIllumination_time_min() >= 8000 && batch.getIllumination_time_min() <= 8999 )
        {
            //Kinetics
            int minutes = batch.getIllumination_time_min() - 8000;
            if(!batch.getBatchLocation().isOnRobot())
            {
                throw new InconsistentStateException("stagger experiment without rack on robot error");
            }
            batch.setProgress(Progress.Running);
            batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.start, new Date());
            StaggerExperiment(workflowSystem,batch.getBatchLocation().GetStationIndex(), minutes);
            batch.setStatusAndWaiting(BatchStatus.LoadGC);
        } else {


            batch.setProgress(Progress.Running);
            batch.setTimestamp(BatchTimestampEnum.load_photocat, BatchTimestampType.start, new Date());

            if (Configuration.getPhotolysisStation().equals(PhotolysisStation.Magnetic)) {
                EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, BatchLocation.Photocat_Manipulation);


                workflowSystem.getPhotoCom().Start();
            } else if (Configuration.getPhotolysisStation().equals(PhotolysisStation.Vibratory)) {
                EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, BatchLocation.Vibratory_Photocat_Manipulation);
                workflowSystem.getLBRCom().StartVialHandlingAndWait();
                for (Sample sample : batch.getSamples()) {
                    //if(sample.getSampleNumber() != 1) {
                    if(true) {
                        workflowSystem.getLBRCom().GraspVialFromBufferAndWait(sample.getRackIndex());

                        TakePicture(workflowSystem, 0, batch, sample, "before-photolysis");

                        workflowSystem.getLBRCom().PlaceSampleInPhotolysisStationAndWait(sample.getSampleIndex());
                    }
                }
                workflowSystem.getLBRCom().StopVialHandlingAndWait();
            } else {
                throw new NotImplementedException();
            }
            batch.setTimestamp(BatchTimestampEnum.load_photocat, BatchTimestampType.finish, new Date());

            batch.setStatusAndWaiting(BatchStatus.StartPhotolysis);
            DisposeOfWebcam();
        }
    }

    private void StaggerExperiment(WorkflowSystem workflowSystem , int rackIndex, int minutes_in_between) throws IOException, DeviceOfflineException, CloneNotSupportedException, InterruptedException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        EnsureRobotIsCalibratedAtLocation(workflowSystem, Location.Vibratory_Photocat);

        Batch batch  = workflowSystem.getState().getRackAtLocation(BatchLocation.valueOf("Robot_Transport_" + (rackIndex +1)));
        if(batch == null)
        {
            workflowSystem.getMessageHandler().WriteMessage("Rack index " + rackIndex + " on robot is empty");
            return;
        }

        if(workflowSystem.getState().getRackAtLocation(BatchLocation.Vibratory_Photocat_Manipulation) != null)
        {
            workflowSystem.getMessageHandler().WriteMessage("there is a rack in vibratory station -> can not run stagger experiment");
            return;
        }

        batch.AppendComment("Running stagger experiment with minutes in between samples: " + minutes_in_between);

        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.start, new Date());

        workflowSystem.getLBRCom().PlaceRackInBufferAndWait(rackIndex, 0);

        workflowSystem.getLBRCom().StartVialHandlingAndWait();
        Date start = new Date();
        workflowSystem.getVibratoryCom().Start();
        for(int i=1;i<=22;i++)
        {

            if(i<=16)
            {
                int vial_index = i-1;
                if (vial_index >= 8) {
                    vial_index++;
                    vial_index++;
                }
                workflowSystem.getLBRCom().GraspVialFromBufferAndWait(vial_index);
                workflowSystem.getLBRCom().PlaceSampleInPhotolysisStationAndWait(i-1);
            } else {
                //estimated time of a placement
                Thread.sleep(13640);
            }

            if((i - 6) >= 1)
            {
                int rack_index = i-7;
                if (rack_index >= 8) {
                    rack_index++;
                    rack_index++;
                }
                workflowSystem.getLBRCom().GraspSampleFromPhotolysisStationAndWait(i-7);
                workflowSystem.getLBRCom().PlaceVialInBufferAndWait(rack_index);
            }


            long sleep = (start.getTime() + i * minutes_in_between * /*1 min*/60 * 1000 ) - new Date().getTime();
            if(sleep > 0)
                Thread.sleep(sleep);
        }
        workflowSystem.getVibratoryCom().Stop();
        workflowSystem.getLBRCom().StopVialHandlingAndWait();
        workflowSystem.getLBRCom().GraspRackFromBufferAndWait(rackIndex, 0);

        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.finish, new Date());
    }

    private void KineticsExperiment(WorkflowSystem workflowSystem , int rackIndex, int minutes_in_between) throws IOException, DeviceOfflineException, CloneNotSupportedException, InterruptedException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException, RobotErrorStateException {
        EnsureRobotIsCalibratedAtLocation(workflowSystem, Location.Vibratory_Photocat);
        Batch batch  = workflowSystem.getState().getRackAtLocation(BatchLocation.valueOf("Robot_Transport_" + (rackIndex +1)));
        if(batch == null)
        {
            workflowSystem.getMessageHandler().WriteMessage("Rack index " + rackIndex + " on robot is empty");
            return;
        }

        if(workflowSystem.getState().getRackAtLocation(BatchLocation.Vibratory_Photocat_Manipulation) != null)
        {
            workflowSystem.getMessageHandler().WriteMessage("there is a rack in vibratory station -> can not run stagger experiment");
            return;
        }

        batch.AppendComment("Running kinetics experiment with step (min)" + minutes_in_between);

        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.start, new Date());

        workflowSystem.getLBRCom().PlaceRackInBufferAndWait(rackIndex, 0);

        workflowSystem.getLBRCom().StartVialHandlingAndWait();
        for(int i=2;i<=16;i++)
        {
            int vial_index = i-1;
            if (vial_index >= 8) {
                vial_index++;
                vial_index++;
            }
            workflowSystem.getLBRCom().GraspVialFromBufferAndWait(vial_index);
            workflowSystem.getLBRCom().PlaceSampleInPhotolysisStationAndWait(i-1);
        }
        workflowSystem.getVibratoryCom().Start();
        Date start = new Date();
        for(int i=2;i<=16;i++)
        {
            long sleep = (start.getTime() + (i-1) * minutes_in_between * /*1 min*/60 * 1000 ) - new Date().getTime();
            if(sleep > 0)
                Thread.sleep(sleep);

            int vial_index = i-1;
            if (vial_index >= 8) {
                vial_index++;
                vial_index++;
            }
            workflowSystem.getLBRCom().GraspSampleFromPhotolysisStationAndWait(i-1);
            workflowSystem.getLBRCom().PlaceVialInBufferAndWait(vial_index);


        }
        workflowSystem.getVibratoryCom().Stop();
        workflowSystem.getLBRCom().StopVialHandlingAndWait();
        workflowSystem.getLBRCom().GraspRackFromBufferAndWait(rackIndex, 0);

        batch.setTimestamp(BatchTimestampEnum.photolysis, BatchTimestampType.finish, new Date());

    }

}
