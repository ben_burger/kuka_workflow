package Workflow.ProcessManagement.Process.RobotJob;

import Workflow.AlertSystem.Alert;
import Workflow.AlertSystem.AlertType;
import Workflow.Batch.Batch;
import Workflow.Batch.BatchStatus;
import Workflow.Batch.Progress;
import Workflow.Batch.Solid;
import Workflow.Com.Serial.Quantos.QuantosResponse;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.Process.RobotJob.Exception.QuantosInverventionException;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;
import Workflow.Configuration.Location;
import Workflow.WorldState.WorldState;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Benjamin on 9/6/2018.
 * Checks if there can be dispensed with the current cartridge
 * If not then  it will Optionally - Removes cartridge from Quantos
 *              And it will place the right cartridge in the Quantos
 *
 * Loads new cartridge in Quantos
 */
public class CartridgeQuantos extends JobSuper {

    public CartridgeQuantos(Batch batch) {
        super(batch, WorkflowStepEnum.SolidDispensing);
        exceptionMessageSource = IMessageHandler.ExceptionSource.SolidDispensingProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws Exception {
        batch.setProgress(Progress.Running);
        WorldState state = workflowSystem.getState();

        Solid[] solids_in_system = state.getSolids();
        Solid[] solids_in_batch = batch.getSolids();

        Optional<Solid> cartridge_in_hotel = Arrays.stream(solids_in_system).filter(x -> x.getHotelIndex() == -1).findFirst();
        if(cartridge_in_hotel.isPresent() && (!cartridge_in_hotel.get().getBlocked()) && Arrays.stream(solids_in_batch).filter(x -> x.getName().equals(cartridge_in_hotel.get().getName())).findFirst().isPresent())
        {
            //there is one, but is it already done?
            int i=0;
            for(;i<solids_in_batch.length;i++)
            {
                if(solids_in_batch[i].getName().equals(cartridge_in_hotel.get().getName()))
                {
                    if(batch.NeedToDispenseSolid(batch.getSolids()[i])) {//We are golden, cartridge can be used
                        batch.setStatusAndWaiting(BatchStatus.DispenseSolid);
                        return;
                    } else
                        break; //Well we cant just start dispensing, so lets remove it and place another
                }
            }

        }

        //So now Determin which cartridge to place in hotel
        Solid solid_to_place_in_hotel = null;
        for(int i=0;i<solids_in_batch.length;i++)
        {
            if(batch.NeedToDispenseSolid(batch.getSolids()[i]))
            {
                Solid solid_in_batch = solids_in_batch[i];
                Optional<Solid> solid_to_dispense =
                        Arrays.stream(solids_in_system).filter(x ->
                                (!x.getBlocked()) && x.getName().equals(solid_in_batch.getName())
                                && 0 <= x.getHotelIndex() && 20 > x.getHotelIndex()
                        ).findFirst();

                if (solid_to_dispense.isPresent()) {
                    solid_to_place_in_hotel = solid_to_dispense.get();
                    break;
                }
            }
        }

        if(solid_to_place_in_hotel == null)
        {
            boolean allDone = true;
            for(int i=0;i<solids_in_batch.length && allDone;i++)
            {
                allDone = (!batch.NeedToDispenseSolid(batch.getSolids()[i]));
            }

            if(allDone)
            {
                batch.setStatusAndWaiting(BatchStatus.UnloadQuantos);
                return;

            } else {
                batch.setStatusAndWaiting(BatchStatus.CartridgeChangeQuantos);
                workflowSystem.getMessageHandler().WriteMessage("no cartridges available");
                workflowSystem.getAlertSystem().AddAlert(new Alert(AlertType.ProcessAlert,new QuantosInverventionException(QuantosResponse.ExternalAction),"no cartridges available"));
                workflowSystem.getProcessManager().EnableStep(WorkflowStepEnum.SolidDispensing,false);
                return;
            }
        }


        //Now it seems the only case is left where we optionally take cartridge out, and place another one in
        EnsureRobotIsCalibratedAtLocation(workflowSystem,Location.Cartridge);

        //Ensure cartridge is graspable door, look
        workflowSystem.getQuantosCom().MoveDoor(true);
        workflowSystem.getQuantosCom().MoveAutoSampler(0);

        workflowSystem.getLBRCom().StartCartridgeManipulationsAndWait();


        if(cartridge_in_hotel.isPresent())
        {
            //Grasp Cartridge
            //And stick it in the hotel
            workflowSystem.getQuantosCom().LockDosingHeadPin(false);
            int hotel_index = GetFreeHotelIndex(workflowSystem);
            workflowSystem.getLBRCom().RemoveCartridgeFromQuantosAndWait();
            workflowSystem.getLBRCom().PlaceCartridgeManipulationsAndWait(hotel_index);
            state.MoveSolid(cartridge_in_hotel.get(), -1, hotel_index);
        }




        int hotel_index = solid_to_place_in_hotel.getHotelIndex();
        //Place the cartridge in the Quantos
        workflowSystem.getLBRCom().CartridgeRemoveFromHotelAndWait(hotel_index);
        workflowSystem.getLBRCom().PlaceCartridgeFromQuantosAndWait();
        state.MoveSolid(solid_to_place_in_hotel, hotel_index,-1);


        workflowSystem.getLBRCom().FinishCartridgeManipulationsAndWait();

        workflowSystem.getQuantosCom().MoveDoor(false);
        workflowSystem.getQuantosCom().LockDosingHeadPin(true);
        batch.setStatusAndWaiting(BatchStatus.DispenseSolid);
    }

    private int GetFreeHotelIndex(WorkflowSystem workflowSystem) throws Exception {
        Solid[] solids = workflowSystem.getState().getSolids();
        for(int i=0;i<20;i++)
        {
            int a = i;//Becasue Java needs ?!
            Optional<Solid> solid = Arrays.stream(solids).filter(x->x.getHotelIndex() == a).findFirst();
            if(!solid.isPresent())
            {
                return i;
            }
        }
        throw new Exception("no space in the hotel");
    }
}

