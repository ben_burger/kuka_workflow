package Workflow.ProcessManagement.Process.RobotJob;


import Workflow.Batch.*;
import Workflow.Exception.DeviceOfflineException;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.Process.RobotJob.Exception.LiquidSystemCanNotDispenseException;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * ASSUMES ROBOT 0 + Solid Manipulation is Empty
 *
 * Transports the rack from a buffer to manipulation rack holder of quantos
 * Loads the Quantos + sets up first cartridge
 * Expects the manipulation holder to be free
 */
public class LoadRackQuantos extends JobSuper {
    public LoadRackQuantos(Batch batch) {
        super(batch, WorkflowStepEnum.SolidDispensing);
        exceptionMessageSource = IMessageHandler.ExceptionSource.SolidDispensingProcess;
    }
    Thread thread_station;
    Exception exception_thread_station;
    @Override
    public void Execute(WorkflowSystem workflowSystem) throws Exception {
        batch.setProgress(Progress.Running);
        batch.setTimestamp(BatchTimestampEnum.load_quantos, BatchTimestampType.start, new Date());
        workflowSystem.getQuantosCom().MoveAutoSampler(0);
        EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, BatchLocation.SolidDispenseStation_Manipulation);

        workflowSystem.getLBRCom().StartVialHandlingAndWait();
        Sample[] samples = Arrays.stream(batch.getSamples()).sorted((e1, e2) -> Integer.compare(e1.getSampleIndex(),e2.getSampleIndex())).toArray(Sample[]::new);
        MoveAutoSampler(workflowSystem,samples[0]);
        for(Sample sample : samples)
        {
            MoveAutoSampler(workflowSystem,sample);
            //workflowSystem.getQuantosCom().MoveAutoSampler((16 + sample.getSampleIndex()) % 31);
            workflowSystem.getLBRCom().GraspVialFromBufferAndWait(sample.getRackIndex());
            WaitForFinish();
            workflowSystem.getLBRCom().PlaceSampleInQuantosAndWait();
        }
        workflowSystem.getLBRCom().StopVialHandlingAndWait();

        batch.setTimestamp(BatchTimestampEnum.load_quantos, BatchTimestampType.finish, new Date());
        batch.setStatusAndWaiting(BatchStatus.CartridgeChangeQuantos);
    }

    private void WaitForFinish() throws Exception {
        // some suspicious error handling...
        thread_station.join();
        if(exception_thread_station != null)
            throw exception_thread_station;
    }
    private void MoveAutoSampler(WorkflowSystem workflowSystem, Sample sample) throws LiquidSystemCanNotDispenseException, InterruptedException, DeviceOfflineException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    workflowSystem.getQuantosCom().MoveAutoSampler((16 + sample.getSampleIndex()) % 31);
                } catch (Exception e) {
                    exception_thread_station = e;
                }
            }
        });
        thread_station = thread;
        exception_thread_station = null;

        thread.start();
    }
}
