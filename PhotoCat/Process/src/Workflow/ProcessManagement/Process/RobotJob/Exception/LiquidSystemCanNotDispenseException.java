package Workflow.ProcessManagement.Process.RobotJob.Exception;

import Workflow.Batch.Liquid;

/**
 * Created by Benjamin on 9/9/2018.
 */
public class LiquidSystemCanNotDispenseException extends  Exception{
    Liquid liquid;

    public LiquidSystemCanNotDispenseException(Liquid liquid) {
        super("Liquid station can not dispense " + liquid.getName());
        this.liquid = liquid;
    }
}
