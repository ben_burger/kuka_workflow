package Workflow.ProcessManagement.Process.RobotJob.Exception;

import Workflow.Com.Serial.Quantos.QuantosResponse;

/**
 * Created by Benjamin on 9/10/2018.
 */
public class QuantosInverventionException extends Exception {
    QuantosResponse response;

    public QuantosInverventionException(QuantosResponse resp) {
        super("Quantos needs help: " + resp.toString());
        this.response = resp;
    }
}
