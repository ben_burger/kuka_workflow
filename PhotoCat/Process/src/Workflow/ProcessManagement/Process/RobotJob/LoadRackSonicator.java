package Workflow.ProcessManagement.Process.RobotJob;


import Workflow.Batch.*;
import Workflow.Exception.*;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * Transports the rack from a buffer to manipulation rack holder of quantos
 * Loads the Quantos + sets up first cartridge
 * Expects the manipulation holder to be free
 */
public class LoadRackSonicator extends webcamSupportJob {
    public LoadRackSonicator(Batch batch) {
        super(batch, WorkflowStepEnum.Sonication);
        exceptionMessageSource = IMessageHandler.ExceptionSource.SonicatorProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws InterruptedException, RobotErrorStateException, CloneNotSupportedException, IOException, DeviceOfflineException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
        batch.setProgress(Progress.Running);
        batch.setTimestamp(BatchTimestampEnum.load_sonicator, BatchTimestampType.start, new Date());
        EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, BatchLocation.SonicationStation_Manipulation);

        workflowSystem.getLBRCom().StartVialHandlingAndWait();
        Sample[] samples = Arrays.stream(batch.getSamples()).sorted((e1, e2) -> Integer.compare(e1.getSampleIndex(),e2.getSampleIndex())).toArray(Sample[]::new);
        for(Sample sample : samples)
        {
            workflowSystem.getLBRCom().GraspVialFromBufferAndWait(sample.getRackIndex());
            workflowSystem.getLBRCom().MoveToDryPositionAndWait();
            TakePicture(workflowSystem,1,batch, sample,"before-sonication");
            workflowSystem.getLBRCom().MoveToNeutralAfterDryAndWait();
            workflowSystem.getLBRCom().PlaceVialInSonicatorAndWait(sample.getSampleNumber());
        }
        workflowSystem.getLBRCom().StopVialHandlingAndWait();


        workflowSystem.getSonicatorCom().Start();
        batch.setTimestamp(BatchTimestampEnum.load_sonicator, BatchTimestampType.finish, new Date());
        batch.setTimestamp(BatchTimestampEnum.sonication, BatchTimestampType.start, new Date());
        batch.setStatusAndWaiting(BatchStatus.Sonicate);
        DisposeOfWebcam();

    }
}
