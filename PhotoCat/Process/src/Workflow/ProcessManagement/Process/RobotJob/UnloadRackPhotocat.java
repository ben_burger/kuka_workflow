package Workflow.ProcessManagement.Process.RobotJob;


import Workflow.Batch.*;
import Workflow.Configuration.Configuration;
import Workflow.Configuration.PhotolysisStation;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.WorkflowSystem;

import java.util.Date;

/**
 * Created by Benjamin on 9/6/2018.
 * unloads the quantos to manipulation rack
 * moves manipulation rack to a buffer
 */
public class UnloadRackPhotocat extends webcamSupportJob {

    public UnloadRackPhotocat(Batch batch) {
        super(batch, WorkflowStepEnum.Photolysis);
        exceptionMessageSource = IMessageHandler.ExceptionSource.PhotolysisProcess;
    }

    @Override
    public void Execute(WorkflowSystem workflowSystem) throws Exception {
        batch.setProgress(Progress.Running);
        batch.setTimestamp(BatchTimestampEnum.unload_photocat, BatchTimestampType.start, new Date());
        if(Configuration.getPhotolysisStation().equals(PhotolysisStation.Magnetic)) {

            EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, BatchLocation.Photocat_Manipulation);
            BatchLocation location_on_robot = workflowSystem.getState().getFreePositionOnRobot();
            MoveManipulationBatchToBuffer(workflowSystem, BatchLocation.Photocat_Manipulation, location_on_robot);
        } else if(Configuration.getPhotolysisStation().equals(PhotolysisStation.Vibratory)) {
            EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(workflowSystem, BatchLocation.Vibratory_Photocat_Manipulation);


            workflowSystem.getLBRCom().StartVialHandlingAndWait();
            for(Sample sample : batch.getSamples())
            {
                if(true) {
//                    if(sample.getSampleNumber() != 1) {

                    workflowSystem.getLBRCom().GraspSampleFromPhotolysisStationAndWait(sample.getSampleIndex());
                    TakePicture(workflowSystem, 0, batch, sample, "after-photolysis");
                    workflowSystem.getLBRCom().PlaceVialInBufferAndWait(sample.getRackIndex());
                }
            }
            workflowSystem.getLBRCom().StopVialHandlingAndWait();


            BatchLocation location_on_robot = workflowSystem.getState().getFreePositionOnRobot();
            MoveManipulationBatchToBuffer(workflowSystem, BatchLocation.Vibratory_Photocat_Manipulation, location_on_robot);
        }
        batch.setTimestamp(BatchTimestampEnum.unload_photocat, BatchTimestampType.finish, new Date());

        batch.setStatusAndWaiting(NextStep(batch));

        DisposeOfWebcam();
    }
}
