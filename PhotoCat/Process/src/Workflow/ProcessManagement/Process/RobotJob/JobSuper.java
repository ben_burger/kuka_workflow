package Workflow.ProcessManagement.Process.RobotJob;

import Workflow.Batch.*;
import Workflow.Com.TCP.KMRCom;
import Workflow.Com.TCP.LBRCom;
import Workflow.Exception.*;
import Workflow.Configuration.Location;
import Workflow.IMessageHandler;
import Workflow.KMR.KMRRobotStatus;
import Workflow.LBR.LBRRobotStatus;
import Workflow.ProcessManagement.WorkflowStepEnum;
import Workflow.TCP.RobotStatusEnum;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;
import jdk.management.resource.NotifyingMeter;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Benjamin on 9/6/2018.
 */
public abstract class JobSuper {
    protected Batch batch;
    protected IMessageHandler.ExceptionSource exceptionMessageSource;
    protected WorkflowStepEnum workflowstep;
    public JobSuper(Batch batch, WorkflowStepEnum workflowstep) {
        this.batch = batch;
    }

    public abstract void Execute(WorkflowSystem workflowSystem) throws Exception;

    protected void MoveManipulationBatchToBuffer(WorkflowSystem workflowSystem, BatchLocation manipulation_source, BatchLocation target) throws IOException, InterruptedException, RobotErrorStateException, CloneNotSupportedException, DeviceOfflineException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
        if (target.getLocation() == null) {
            //If we need to place it on the robot
            EnsureRobotIsCalibratedAtLocation(workflowSystem, manipulation_source.getLocation());
            workflowSystem.getLBRCom().GraspRackFromBufferAndWait(target.GetStationIndex(), manipulation_source.GetStationIndex());
            workflowSystem.getState().MoveRack(manipulation_source, target, batch);
            return;
        } else {
            //if we do  not need to place on the robot
            if (manipulation_source.getLocation() != null) {
                //make sure rack is on robot
                EnsureRobotIsCalibratedAtLocation(workflowSystem, manipulation_source.getLocation());
                BatchLocation location_on_robot = workflowSystem.getState().getFreePositionOnRobot();
                workflowSystem.getLBRCom().GraspRackFromBufferAndWait(location_on_robot.GetStationIndex(), manipulation_source.GetStationIndex());
                workflowSystem.getState().MoveRack(manipulation_source, location_on_robot, batch);
            }

        }
        EnsureRobotIsCalibratedAtLocation(workflowSystem, target.getLocation());
        workflowSystem.getLBRCom().PlaceRackInBuffer(batch.getBatchLocation().GetStationIndex(), target.GetStationIndex());
        workflowSystem.getState().MoveRack(batch.getBatchLocation(), target, batch);
    }

    protected void EnsureBatchAtLocationAndEnsureRobotIsCalibratedAtLocation(WorkflowSystem workflowSystem, BatchLocation target) throws CloneNotSupportedException, InterruptedException, RobotErrorStateException, IOException, DeviceOfflineException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
        if (batch.getBatchLocation() != target) {

            //Ensure Rack is on the robot
            WorldState state = workflowSystem.getState();
            BatchLocation batchLocation = batch.getBatchLocation();
            Location location = batchLocation.getLocation();
            if (location != null) {
                //batch is not on the robot
                EnsureRobotIsCalibratedAtLocation(workflowSystem, batchLocation.getLocation());
                BatchLocation location_on_robot = target.getLocation() == null ? target : state.getFreePositionOnRobot();

                workflowSystem.getLBRCom().GraspRackFromBufferAndWait(location_on_robot.GetStationIndex(), batchLocation.GetStationIndex());
                state.MoveRack(batchLocation, location_on_robot, batch);
            }

            if (target.getLocation() != null) {
                //Place Rack from Robot
                EnsureRobotIsCalibratedAtLocation(workflowSystem, target.getLocation());
                LBRRobotStatus status = workflowSystem.getLBRCom().PlaceRackInBufferAndWait(batch.getBatchLocation().GetStationIndex(), target.GetStationIndex());
                state.MoveRack(batch.getBatchLocation(), target, batch);
            }
        } else {
            EnsureRobotIsCalibratedAtLocation(workflowSystem, target.getLocation());
        }
    }

    protected void EnsureRobotIsCalibratedAtLocation(WorkflowSystem workflowSystem, Location location) throws CloneNotSupportedException, InterruptedException, RobotErrorStateException, IOException, DeviceOfflineException, RobotNoHeartBeatException, RobotBusyException, NotAllowedException {
        if(location == null)
            throw new NotImplementedException();

        KMRRobotStatus kmrStatus = (KMRRobotStatus) workflowSystem.getKMRCom().GetServer().GetStatus();
        boolean kmr_right_place = kmrStatus.getLastCommandedLocation() == location;
        LBRRobotStatus lbrStatus = (LBRRobotStatus) workflowSystem.getLBRComNoCheck().GetServer().GetStatus();
        boolean lbr_active = lbrStatus != null && kmrStatus.getStatus() == RobotStatusEnum.TaskRunning && lbrStatus.getStatus() == RobotStatusEnum.Commandable;
        //TODO CHECK IF LBR IS IN ERROR
        //if //4: lbr is active at right location
        if (kmr_right_place && lbr_active) {
            if (lbrStatus.getCubeOrigin() == null) {
                //4-2: arm need calibvratio
                //do 6P
                workflowSystem.getLBRCom().SixPointCalibrationAndWait();
            }
            //4-1:Arm calibrated
            //easy done

        } else {
            if (!kmr_right_place) {
                //if kmr at wrong location:
                if (lbr_active) {
                    //1: lbr is active at wrong location
                    //Stoplbr, continue with 2:
                    workflowSystem.getLBRCom().ArmToDrivePosAndWait();
                    workflowSystem.getLBRCom().FinishArmManipulations();
                    Thread.sleep(3000);

                }
                workflowSystem.getKMRCom().WaitForCommandable();
                //2: lbr is inactive at wrong location
                //drive to location. continue with 3:
                workflowSystem.getKMRCom().TransPortKMRAndStartLBRAndWait(workflowSystem.getLBRComNoCheck(), location);
                workflowSystem.getLBRCom().SixPointCalibrationAndWait();
            } else {
                //3: lbr is inactive at  the right location location
                //active lbr at right location with 6P
                workflowSystem.getKMRCom().StartLBRAndWait(workflowSystem.getLBRComNoCheck(), location);
                workflowSystem.getLBRCom().SixPointCalibrationAndWait();
            }
        }
        //Done thing always calibrated at the right location posibly with doing nothing at all.
    }

    public static BatchStatus NextStep(Batch batch) {
        BatchStatus status = batch.getStatus();

        if (status.getStep() < BatchStatus.PickUp.getStep()) {
            return BatchStatus.PickUp;
        }

        if (status.getStep() < BatchStatus.LoadQuantos.getStep()) {
            if (!SkipSolidDispensing(batch))
                return BatchStatus.LoadQuantos;
        }

        if (status.getStep() < BatchStatus.DispenseLiquid.getStep()) {
            if (!SkipLiquidDispensing(batch))
                return BatchStatus.DispenseLiquid;
        }

        if (status.getStep() < BatchStatus.LoadSonicator.getStep()) {
            if (!SkipSampleDispersion(batch))
                return BatchStatus.LoadSonicator;
        }

        if (status.getStep() < BatchStatus.LoadPhotocat.getStep()) {
            if (!SkipPhotolysis(batch))
                return BatchStatus.LoadPhotocat;
        }

        if (status.getStep() < BatchStatus.LoadGC.getStep()) {
            if (!SkipGCAnalysis(batch))
                return BatchStatus.LoadGC;
        }

        return BatchStatus.MoveToStorage;
    }

    private static boolean SkipGCAnalysis(Batch batch) {
        if (batch.getGc_method().equals("") || batch.getGc_method() == null)
            return true;
        return false;
    }

    private static  boolean SkipPhotolysis(Batch batch) {
        if (batch.getIllumination_time_min() == 0)
            return true;
        return false;
    }

    private static  boolean SkipSampleDispersion(Batch batch) {
        if (batch.getSonication_time_min() == 0)
            return true;
        return false;
    }

    private static  boolean SkipCapVials(Batch batch) {
        if (!batch.getCap_vials())
            return true;

        for (Sample sample : batch.getSamples())
            if (!sample.getCapped())
                return false;
        return true;
    }

    private static boolean SkipLiquidDispensing(Batch batch) {
        if(batch.getCap_vials())
        {
            if(Arrays.stream(batch.getSamples()).filter(x->!x.getCapped()).findAny().isPresent())
            {
                return false;
            }
        }

        if (batch.getLiquids().length == 0)
            return true;

        if (batch.getSamples().length == 0)
            return true;

        for (Liquid liquid : batch.getLiquids())
            for (Sample sample : batch.getSamples()) {


                if (0.9 * sample.getLiquidDispenseAmount(liquid) > sample.getLiquidDispensed(liquid))
                    return false;
            }
        return true;
    }

    private static boolean SkipSolidDispensing(Batch batch) {
        if (batch.getSolids().length == 0)
            return true;

        if (batch.getSamples().length == 0)
            return true;

        for (Solid solid : batch.getSolids())
            for (Sample sample : batch.getSamples()) {
                if (0.9 * sample.getSolidDispenseAmount(solid) > sample.getSolidDispensed(solid))
                    return false;
            }
        return true;
    }

    public IMessageHandler.ExceptionSource getExceptionMessageSource() {
        return exceptionMessageSource;
    }


    public WorkflowStepEnum getWorkflowStep() {
        return workflowstep;
    }

    public Batch getBatch() {
        return batch;
    }
}
