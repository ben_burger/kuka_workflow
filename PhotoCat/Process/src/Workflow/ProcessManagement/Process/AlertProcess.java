package Workflow.ProcessManagement.Process;

import Workflow.Batch.*;
import Workflow.Com.Serial.Quantos.DispensingAlgorithm;
import Workflow.Com.Serial.Quantos.QuantosCom;
import Workflow.Com.Serial.Quantos.QuantosDispenseInformation;
import Workflow.Com.Serial.Quantos.QuantosResponse;
import Workflow.Exception.InconsistentStateException;
import Workflow.IMessageHandler;
import Workflow.ProcessManagement.Process.RobotJob.Exception.QuantosInverventionException;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.ProcessManagement.ProcessSuper;
import Workflow.WorkflowSystem;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class AlertProcess extends ProcessSuper {


    public AlertProcess(WorkflowSystem workflowSystem, ProcessManager manager) {
        super(workflowSystem, manager);
        exceptionMessageSource = IMessageHandler.ExceptionSource.AlertProcess;

    }

    @Override
    protected void Run() throws Exception {
        workflowSystem.getAlertSystem().RunRules();
        workflowSystem.getAlertSystem().DealWithAlert();
    }
}
