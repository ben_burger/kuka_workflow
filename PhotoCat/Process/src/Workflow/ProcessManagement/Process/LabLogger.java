package Workflow.ProcessManagement.Process;

import Workflow.IMessageHandler;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.Util.SimpleLogger;

import java.io.IOException;
import java.util.Date;
import Workflow.WorkflowSystem;

public class LabLogger  extends Workflow.ProcessManagement.ProcessSuper {

    private final SimpleLogger TempeatureLog;
    private Date lastTemperatureLog;

    public LabLogger(WorkflowSystem workflowSystem, ProcessManager manager) throws IOException {
        super(workflowSystem, manager);
        TempeatureLog = new SimpleLogger("LabTemperature");
        exceptionMessageSource = IMessageHandler.ExceptionSource.Logger;
    }

    @Override
    protected void Run() throws Exception {
        if(lastTemperatureLog == null ||
                (lastTemperatureLog.getTime()  + 60 * 1000) > ((new Date().getTime()))
        ) {
            TempeatureLog.LogEvent("T=" + workflowSystem.getProbeCom().getTemperature());
            lastTemperatureLog = new Date();
        }
    }
}
