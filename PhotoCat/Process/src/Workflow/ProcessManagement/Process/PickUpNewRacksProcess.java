package Workflow.ProcessManagement.Process;

import Workflow.Batch.*;
import Workflow.Configuration.Configuration;
import Workflow.Exception.OutOfCapsException;
import Workflow.Exception.OutOfFreshRacks;
import Workflow.ProcessManagement.OutOfLiquidException;
import Workflow.ProcessManagement.Process.RobotJob.JobSuper;
import Workflow.ProcessManagement.ProcessManager;
import Workflow.ProcessManagement.ProcessSuper;
import Workflow.WorkflowSystem;
import Workflow.WorldState.WorldState;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Benjamin on 12/16/2018.
 */
public class PickUpNewRacksProcess extends ProcessSuper {

    public PickUpNewRacksProcess(WorkflowSystem workflowSystem, ProcessManager manager) {
        super(workflowSystem, manager);
    }

    @Override
    protected void Run() throws Exception {
        Batch[] batches_in_workingon = workflowSystem.getState().getRunsInTheSystem().stream()
                .filter(x -> !x.getBatch_name().contains("FreshRack")
                        && x.getStatus().getStep() <= BatchStatus.LoadGC.getStep()
                        && x.getStatus().getStep() > BatchStatus.NoRackAssigned.getStep())

                .toArray(Batch[]::new);
        Batch[] batches_in_system = workflowSystem.getState().getRunsInTheSystem().stream()
                .filter(x -> !x.getBatch_name().contains("FreshRack")
                        && x.getStatus().getStep() < BatchStatus.DONE.getStep())

                .toArray(Batch[]::new);
        //Now we need to retrieve a new job from the \\runqueue
        //Give it that sweet new rack, and continue running..
        //Getting run from the queue if any
        WorldState state = workflowSystem.getState();
        //List files in //runqueuefolder

        if (batches_in_system.length < Configuration.getScheduler().getNumberOfRacks() && batches_in_workingon.length < Configuration.getScheduler().getNumberOfRacksToWorkOn()) {


            Batch batch = null;
            File dir = new File("runqueue\\.");
            File[] files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".run");
                }
            });

            boolean hasBatchesWaiting = false;
            List<Batch> result = new LinkedList<>();
            for (File batchfile : files) {
                //TODO filter based on solids liquids present?
                batch = Batch.ReadFromFile(state.getSolids(), state.getLiquids(), batchfile);
                Date submit_start = batch.getTimestamp(BatchTimestampEnum.submit, BatchTimestampType.start);
                batch.ResetTimestamps();
                if(submit_start != null)
                    batch.setTimestamp(BatchTimestampEnum.submit, BatchTimestampType.start, submit_start);
                result.add(batch);
                hasBatchesWaiting = true;
            }

            if (!hasBatchesWaiting)
                return;

            PerformStockChecks(batch);


            batch = result.stream().sorted((e1, e2) -> e1.getBatch_name().compareTo(e2.getBatch_name())).toArray(Batch[]::new)[0];
            Batch fresh = workflowSystem.getState().getFreshRacks()[0];


            if (batch.getStatus() == BatchStatus.NoRackAssigned && batch.getProgress() == Progress.Waiting)
                batch.setProgress(Progress.Running);


            Files.move(Paths.get("runqueue\\" + batch.getBatch_name() + ".run"), Paths.get("running\\" + batch.getBatch_name() + ".run"));
            batch.updateFile(new File("running\\" + batch.getBatch_name() + ".run"));

            batch.setTimestamp(BatchTimestampEnum.run, BatchTimestampType.start, new Date());
            state.AssignFreshRack(batch, fresh);

            batch.setStatusAndWaiting(JobSuper.NextStep(batch));
        }
    }

    private void PerformStockChecks(Batch new_batch) throws OutOfCapsException, OutOfFreshRacks, OutOfLiquidException {
        PerformCapStockCheck(new_batch);
        PerformLiquidStockCheck(new_batch);
        PerformFreshRackCheck();
    }

    private void PerformFreshRackCheck() throws OutOfFreshRacks {
        if (workflowSystem.getState().getFreshRacks().length == 0) {
            throw new OutOfFreshRacks();
        }
    }

    private void PerformLiquidStockCheck(Batch new_batch) throws OutOfLiquidException {

        //Determin list of unique liquid names
        Liquid[] liquids_in_system = workflowSystem.getState().getLiquids();
        String[] unique_liquids = Arrays.stream(liquids_in_system).map(Liquid::getName).distinct().toArray(String[]::new);

        //sum how much is left for each liquid (above 55 mL)

        double[] volume_left = new double[unique_liquids.length];

        for (int i = 0; i < unique_liquids.length; i++) {
            volume_left[i] = 0;

            for (Liquid liquid : liquids_in_system) {
                if (liquid.Name.equals(unique_liquids[i]) && liquid.getMinimalVolumePresent() > 55.0) {
                    volume_left[i] += (liquid.getMinimalVolumePresent() - 50.0);
                }
            }
        }

        //sum how much per liquid the remaining batches will use
        double[] volume_needed = new double[unique_liquids.length];

        for (int i = 0; i < unique_liquids.length; i++) {
            volume_needed[i] = 0;
                String name = unique_liquids[i];
            Liquid liq = Arrays.stream(liquids_in_system).filter(x -> x.getName().equals(name)).findFirst().get();

            for (Batch batch : workflowSystem.getState().getRunsInTheSystem().stream().filter(x -> x.getStatus().getStep() >= BatchStatus.PickUp.getStep() && x.getStatus().getStep() < BatchStatus.LoadSonicator.getStep()).collect(Collectors.toList())) {
                if(Arrays.stream(batch.getLiquids()).filter(x->x.getName().equals(name)).findFirst().isPresent())
                    for (Sample sample : batch.getSamples()) {
                    double amount_needed = sample.getLiquidDispenseAmount(liq) - sample.getLiquidDispensed(liq);
                    if (amount_needed > 0.1f)
                        volume_needed[i] += amount_needed;
                }
            }
        }

        //sum how much this batch will use
        double[] new_batch_volume_needed = new double[unique_liquids.length];

        for (int i = 0; i < unique_liquids.length; i++) {
            new_batch_volume_needed[i] = 0;
            String name = unique_liquids[i];

            Liquid liq = Arrays.stream(liquids_in_system).filter(x -> x.getName().equals(name)).findFirst().get();
            if(Arrays.stream(new_batch.getLiquids()).filter(x->x.getName().equals(name)).findFirst().isPresent())
                for (Sample sample : new_batch.getSamples()) {
                    double amount_needed = sample.getLiquidDispenseAmount(liq) - sample.getLiquidDispensed(liq);
                    if (amount_needed > 0.1f)
                        new_batch_volume_needed[i] += amount_needed;
                }
        }

        //Compare the sums...
        for (int i = 0; i < unique_liquids.length; i++) {
            String name = unique_liquids[i];

            if(volume_left[i] < volume_needed[i] + new_batch_volume_needed[i]) {
                Liquid liq = Arrays.stream(liquids_in_system).filter(x -> x.getName().equals(name)).findFirst().get();
                throw new OutOfLiquidException(liq);
            }
        }
    }


    private void PerformCapStockCheck(Batch new_batch) throws OutOfCapsException {
        int NumberOfCapsNeeded = 0;
        Integer[] caps = workflowSystem.getState().getRunsInTheSystem().stream().filter(x -> x.getCap_vials()).map(x -> Arrays.stream(x.getSamples()).filter(y -> !y.getCapped()).toArray().length).toArray(Integer[]::new);
        for (int i = 0; i < caps.length; i++) {
            NumberOfCapsNeeded += caps[i];
        }

        if (workflowSystem.getState().getMinimal_Caps_Present() < NumberOfCapsNeeded + Arrays.stream(new_batch.getSamples()).filter(x -> (!x.getCapped()) && new_batch.getCap_vials()).toArray().length) {
            throw new OutOfCapsException();
        }
    }
}
