package Workflow.TCP;

/**
 * Created by Benjamin on 3/10/2018.
 */
public enum RobotStatusEnum {TaskRunning("TaskRunning"), Commandable("Commandable"), Error("Error"), Disconnected("Disconnected");
private String text;
    RobotStatusEnum(String toString)
{
    text = toString;
}
public String toString(){
    return text;
}}
