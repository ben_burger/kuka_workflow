package Workflow.TCP;

/**
 * Created by Benjamin on 3/11/2018.
 */
public enum MessageType {
    Heartbeat, //heartbeat client
    Response,
    TaskRequest,
    ServerHeartBeat, //heartbeat server
    TaskSpecific


}
