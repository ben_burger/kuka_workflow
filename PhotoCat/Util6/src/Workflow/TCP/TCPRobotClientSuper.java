package Workflow.TCP;

import Workflow.Configuration.Configuration;
import Workflow.TCP.Messages.*;
import com.kuka.task.ITaskLogger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.net.Socket;
import java.util.Date;
import java.util.concurrent.*;

/**
 * Created by Benjamin on 3/11/2018.
 */
public class TCPRobotClientSuper<T extends RobotStatus> {
    public Boolean isConnected() {
        synchronized (Connected) {
            return Connected;
        }
    }
    private boolean DEBUG = false;
    private int Port;
    private String IP;

    private Boolean Connected;
    protected T Status;
    Socket clientSocket;
    ExecutorService Connection_Executer_service;

    protected ObjectInputStream inFromServer;
    protected ObjectOutputStream outToServer;
    Date lastHeartBeatSent;
    Date lastHeartBeatReceived;
    private Boolean SentMessageLock = true;
    public ITaskLogger LOG;

    ScheduledExecutorService reconnect_Executer_service;

    public TCPRobotClientSuper(T status, String IP, int Port, ITaskLogger logger) {
        Status = status;
        this.IP = IP;
        this.Port = Port;
        Connected = false;
        LOG = logger;
        reconnect_Executer_service = Executors.newSingleThreadScheduledExecutor();
        reconnect_Executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                boolean connect;
                synchronized (Connected) {
                    connect = Connected;
                }
                if (!connect)
                    Connect();
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);

    }

    public boolean Connect() {
        if(DEBUG)
            LOG.info("Try to Connect");
        if (Connected)
            throw new NotImplementedException(); //Implies multiple Servers....
        try {
            clientSocket = new Socket(IP, Port);
            if(LOG != null)
                LOG.info("Client connected");
//            clientSocket.setSoTimeout(500);
            lastHeartBeatReceived = new Date();


            outToServer = new ObjectOutputStream(clientSocket.getOutputStream());
            inFromServer = new ObjectInputStream((clientSocket.getInputStream()));
            if (Connection_Executer_service != null)
                Connection_Executer_service.shutdown();
            Connection_Executer_service = Executors.newFixedThreadPool(3);
            synchronized (Connected) {
                Connected = true;
            }
            Connection_Executer_service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = isConnected();
                    while (connected) {
                        try {
                            LisenForMessages(inFromServer, outToServer);
                            if(DEBUG)
                                LOG.info("LisenForMessages");
                            Thread.sleep(100);
                        } catch (IOException e) {
                            if (LOG != null)
                                LOG.error("TCP-Fail in Lisen");
                            else e.printStackTrace();
                            Disconnected(e);
                        } catch (ClassNotFoundException e) {
                            LOG.error("Emergency Stop (TCP-Fail) in Sent:CloneNotSupportedException");
                            Disconnected(e);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (CloneNotSupportedException e) {
                            LOG.error("Emergency Stop (TCP-Fail) in Sent:CloneNotSupportedException");
                            Disconnected(e);
                        }
                        connected = isConnected();
                    }
                }
            });
            Connection_Executer_service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = isConnected();
                    while (connected) {
                        try {
                            SentHeartBeat(inFromServer, outToServer);
                            if(DEBUG)
                                LOG.info("Sending HeartBeat");
                            Thread.sleep(100);
                        } catch (IOException e) {
                            if (LOG == null)
                                e.printStackTrace();
                            else
                                LOG.error("Emergency Stop (TCP-Fail) in Sent");
                            Disconnected(e);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (CloneNotSupportedException e) {
                            LOG.error("Emergency Stop (TCP-Fail) in Sent:CloneNotSupportedException");
                            Disconnected(e);
                        }
                        connected = isConnected();
                    }
                }
            });
            Connection_Executer_service.submit(new Runnable() {
                @Override
                public void run() {

                    boolean connected = isConnected();
                    while (connected) {
                        try {
                            CheckServerAlive();
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        connected = isConnected();
                    }

                }
            });

        } catch (IOException e) {
            synchronized (Connected) {
                Connected = false;
            }
            if (Connection_Executer_service != null)
                Connection_Executer_service.shutdown();
        }

        return Connected;
    }

    private void ProcessHeartBeat(ServerHeartBeat message) {
        synchronized (lastHeartBeatReceived) {
            lastHeartBeatReceived = new Date();

        }
        if (message.EmergencyStop) {
            if (LOG != null)
                LOG.error("Emergency Stop (TCP-heartbeat)");
            Disconnected(new Exception("Emergency stop commanded"));
        }
        if(DEBUG)
            LOG.info("Receiving HeartBeat: " + message.getTimestamp().toString());
    }

    private void CheckServerAlive() {
        synchronized (lastHeartBeatReceived) {
            long diffInMillies = (new Date()).getTime() - lastHeartBeatReceived.getTime();
            if (diffInMillies > Configuration.getTimeoutTCP()) {
                if (LOG != null)
                    LOG.error("Emergency Stop (TCP-Timeout)");
                Disconnected(new TimeoutException());
            }
        }
    }

    protected void SentHeartBeat(ObjectInputStream inFromServer, ObjectOutputStream outToServer) throws IOException, CloneNotSupportedException {
        T stat;
        synchronized (Status) {
            stat = (T) Status.clone();
        }
        synchronized (SentMessageLock) {
            if (LOG != null && DEBUG)
                LOG.info(stat.toString());
            outToServer.writeObject(new HeartBeat(new Date(), new Date(),(T) stat.clone()));
            outToServer.flush();
            lastHeartBeatSent = new Date();

        }
    }

    public TaskSuper getNewTask() {
        synchronized (Connected) {
            if (!Connected)
                return null;
        }
        synchronized(Status) {
            if (Status.getStatus() == RobotStatusEnum.TaskRunning)
                return Status.getTask();
        }
        return null;

    }


    private void LisenForMessages(ObjectInputStream inFromClient, ObjectOutputStream outToClient) throws IOException, ClassNotFoundException, CloneNotSupportedException {
        Object input;

        synchronized (inFromClient) {
            input = inFromClient.readObject();
        }
        TCPMessageSuper message = (TCPMessageSuper) input;
        while (message != null) {
            switch (message.getType()) {
                case ServerHeartBeat:
                    ProcessHeartBeat((ServerHeartBeat) message);
                    break;
                default:
                    TaskSuper task = ((TaskRequest) message).getTask();
                    boolean status_commandable;
                    synchronized (Status) {
                        status_commandable = Status.getStatus() == RobotStatusEnum.Commandable;
                    }
                    boolean allowed = AcceptTask(task);
                    if (!allowed) {
                        synchronized (SentMessageLock) {
                            try {
                                outToServer.writeObject(new TaskResponse(new Date(), TaskResponseEnum.NotAllowed, "", task));
                                outToServer.flush();
                                lastHeartBeatSent = new Date();
                                if(DEBUG)
                                    LOG.info("Response sent");
                            } catch (IOException e) {
                                Disconnected(e);
                            }
                        }
                        break;
                    } else  if (status_commandable) {
                        setTask(task);

                        synchronized (SentMessageLock) {
                            try {
                                outToServer.writeObject(new TaskResponse(new Date(), TaskResponseEnum.Accepted, "", task));
                                outToServer.flush();
                                lastHeartBeatSent = new Date();
                                if(DEBUG)
                                    LOG.info("Response sent");
                            } catch (IOException e) {
                                Disconnected(e);
                            }
                        }
                    } else {
                        synchronized (SentMessageLock) {
                            try {
                                outToServer.writeObject(new TaskResponse(new Date(), TaskResponseEnum.Busy, "", task));
                                outToServer.flush();
                                lastHeartBeatSent = new Date();
                                if(DEBUG)
                                    LOG.info("Response sent");
                            } catch (IOException e) {
                                Disconnected(e);
                            }
                        }
                    }
            }
            synchronized (inFromClient) {
                message = (TCPMessageSuper) inFromClient.readObject();
            }
        }


    }

    public boolean AcceptTask(TaskSuper task) {
        return true;
    }

     private void Disconnected(Exception e) {
        synchronized (Connected) {
            Connected = false;
        }
        if (Connection_Executer_service != null)
            Connection_Executer_service.shutdown();
        if (clientSocket != null)
            try {
                clientSocket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        if (e != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            if(DEBUG)
                LOG.info("Disconnected: " + sw.toString());
        }
    }


    public void Destruct() {
        if (reconnect_Executer_service != null) {
            reconnect_Executer_service.shutdown();
            reconnect_Executer_service = null;
        }
        Disconnected(null);

    }


    private void setTask(TaskSuper task) throws IOException, CloneNotSupportedException {
        synchronized (Status) {

            Status.setStatus(RobotStatusEnum.TaskRunning);
            Status.setTask(task);
        }

        SentHeartBeat(inFromServer, outToServer);
    }

    public void updateStatus(boolean taskResult, String error) {
        synchronized (Status) {

            if(error != null && (!error.equals(""))) {
                Status.setError(error);
                LOG.error(error);
            }

             if (!taskResult) {
                Status.setStatus(RobotStatusEnum.Error);
            } else {
                Status.setStatus(RobotStatusEnum.Commandable);
            }
        }
    }
}