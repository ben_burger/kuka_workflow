package Workflow.TCP.Messages;

import Workflow.TCP.MessageType;
import Workflow.TCP.TaskResponseEnum;
import Workflow.TCP.TaskSuper;

import java.util.Date;

/**
 * Created by Benjamin on 3/11/2018.
 */
public class TaskResponse extends TCPMessageSuper {
    private static final long serialVersionUID = 234242343243L;
    private TaskResponseEnum Response;
    private String Error;
    private TaskSuper Task;

    public TaskResponseEnum getResponse() {
        return Response;
    }

    public String getError() {
        return Error;
    }

    public TaskResponse(Date timestamp, TaskResponseEnum response, String error, TaskSuper task) {
        super(timestamp, MessageType.Response);
        Response = response;
        Error = error;
        Task = task;
    }

    public String toString()
    {
        StringBuilder bw = new StringBuilder();
        bw.append("Response from Robot: ");
        bw.append("\r\n");
        bw.append("Response: " + Response.toString());
        bw.append("\r\n");
        bw.append("Error: " + Error);
        bw.append("\r\n");
        return bw.toString();

    }

    public TaskSuper getTask()
    {
        return Task;
    }
}
