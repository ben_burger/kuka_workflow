package Workflow.TCP.Messages;


import Workflow.TCP.MessageType;
import Workflow.TCP.TaskSuper;

import java.util.Date;

/**
 * Created by Benjamin on 3/10/2018.
 */
public class TaskRequest extends TCPMessageSuper {
    private static final long serialVersionUID = 3265476376554L;
    TaskSuper Task;

    public TaskRequest(Date timestamp, TaskSuper task) {
        super(timestamp, MessageType.TaskRequest);
        Task = task;
    }

    public TaskSuper getTask() {
        return Task;
    }
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("TaskRequest:\r\n" + super.toString());
        sb.append("Task: " + Task.toString());
        sb.append("\r\n");
        return sb.toString();
    }

}
