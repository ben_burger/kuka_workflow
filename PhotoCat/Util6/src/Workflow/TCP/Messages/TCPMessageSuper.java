package Workflow.TCP.Messages;

import Workflow.TCP.MessageType;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Benjamin on 3/10/2018.
 */
public class TCPMessageSuper implements Serializable {
    private static final long serialVersionUID = 90432874328763297L;
    Date Timestamp;

    public MessageType getType() {
        return Type;
    }

    MessageType Type ;

    public TCPMessageSuper(Date timestamp, MessageType type) {
        Timestamp = timestamp;
        Type = type;
    }

    public Date getTimestamp() {
        return Timestamp;
    }
}
