package Workflow.TCP.Messages;

import Workflow.TCP.MessageType;
import Workflow.TCP.RobotStatus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Benjamin on 3/10/2018.
 */
public class HeartBeat<T extends RobotStatus> extends TCPMessageSuper {
    private static final long serialVersionUID = 904328743287632943L;

    Date StartTask;
    T Status;

    public RobotStatus getStatus() {
        return Status;
    }


    public Date getStartTask() {
        return StartTask;
    }

    public HeartBeat(Date timestamp, Date startTask) {
        super(timestamp, MessageType.Heartbeat);
        StartTask = startTask;
    }
    public HeartBeat(Date timestamp, Date startTask, T status) {
        super(timestamp, MessageType.Heartbeat);
        StartTask = startTask;
        Status = status;
    }

    @Override
    public String toString()
    {  DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        StringBuilder sb = new StringBuilder();
        sb.append("HeartBeat\r\n");
        sb.append("StartTask: " +StartTask==null ? "" : df2.format(StartTask));
        sb.append("\r\n");
        sb.append("Status: " + ((Status == null) ? "" : Status.toString()));
        sb.append("\r\n");

        return sb.toString();
    }


}
