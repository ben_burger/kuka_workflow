package Workflow.TCP.Messages;

import Workflow.TCP.MessageType;
import Workflow.TCP.RobotStatus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Benjamin on 3/10/2018.
 */
public class ServerHeartBeat<T extends RobotStatus>  extends TCPMessageSuper {
    private static final long serialVersionUID = 21312321231L;
    public boolean EmergencyStop;
    T Status;
    public boolean isEmergencyStop() {
        return EmergencyStop;
    }

    public RobotStatus getStatus() {
        return Status;
    }

    public ServerHeartBeat(Date timestamp, boolean emergencyStop) {
        super(timestamp, MessageType.ServerHeartBeat);
        EmergencyStop = emergencyStop;
    }
    public ServerHeartBeat(Date timestamp, boolean emergencyStop, T Status) {
        super(timestamp, MessageType.ServerHeartBeat);
        EmergencyStop = emergencyStop;
        this.Status = Status;
    }

    @Override
    public String toString()
    {  DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        StringBuilder sb = new StringBuilder();
        sb.append("HeartBeat\r\n");
        sb.append("EmergencyStop: " + (EmergencyStop ? "1" : "0"));
        sb.append("\r\n");
        sb.append("Status: " + ((Status == null) ? "" : Status.toString()));
        sb.append("\r\n");

        return sb.toString();
    }

}
