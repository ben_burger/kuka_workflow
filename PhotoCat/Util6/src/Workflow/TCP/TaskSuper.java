package Workflow.TCP;

import java.io.Serializable;

/**
 * Created by Benjamin on 3/11/2018.
 */
public class TaskSuper  implements Serializable {

    private TaskStatusEnum Status;

    public TaskSuper() {
        Status = TaskStatusEnum.NotStarted;
    }

    public TaskStatusEnum getStatus() {
        return Status;
    }

    public void setStatus(TaskStatusEnum status) {
        Status = status;
    }


    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Status: " + Status.toString());
        sb.append("\r\n");
        return sb.toString();
    }


}
