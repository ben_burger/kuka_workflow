package Workflow.TCP;

/**
 * Created by Benjamin on 3/11/2018.
 */
public enum TaskStatusEnum {Running, Error, Completed, NotStarted
}
