package Workflow.TCP;


import java.io.*;
import java.util.Date;

/**
 * Created by Benjamin on 3/11/2018.
 */
public class RobotStatus implements Serializable, Cloneable {
    private RobotStatusEnum Status;
    private Date TaskStarted;
    private TaskSuper Task;
    private String Error;

    public void setStatus(RobotStatusEnum status) {
        Status = status;
    }

    public Date getTaskStarted() {
        return TaskStarted;
    }

    public void setTaskStarted(Date taskStarted) {
        TaskStarted = taskStarted;
    }

    public void setTask(TaskSuper task) {
        Task = task;
    }

    public void setError(String error) {
        Error = error;
    }

    public RobotStatusEnum getStatus() {
        return Status;
    }

    public TaskSuper getTask() {
        return Task;
    }

    public String getError() {
        return Error;
    }

    public RobotStatus(Date timestamp, RobotStatusEnum status, TaskSuper task, String error, Date taskStarted) {
        Status = status;
        Task = task;
        Error = error;
        TaskStarted = taskStarted;
    }

    public String toString()
    {
        StringBuilder bw = new StringBuilder();
        if(TaskStarted != null) {
            bw.append("TaskStarted: " + TaskStarted.toString());
            bw.append("\r\n");
        }
        bw.append("RobotStatus: " + Status.toString());
        bw.append("\r\n");
        if(Error != null) {
            bw.append("Error: " + Error);
            bw.append("\r\n");
        }
        if(Task != null) {
            bw.append(Task.toString());
        }
        return bw.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        ObjectOutputStream oos = null;
//        try {
//            oos = new ObjectOutputStream(bos);
//            oos.writeObject(this);
//            oos.flush();
//            oos.close();
//            bos.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        byte[] byteData = bos.toByteArray();
//        ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
//        ObjectInputStream ois;
//        Object res=null;
//        try {
//            ois = new ObjectInputStream(bais);
//            res  = (Object) ois.readObject();
//            ois.close();
//            bais.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        return res;
        return super.clone();
    }
}

