package Workflow.TCP;

import Workflow.Configuration.Configuration;
import Workflow.Util.SimpleLogger;
import Workflow.TCP.Messages.HeartBeat;
import Workflow.TCP.Messages.ServerHeartBeat;
import Workflow.TCP.Messages.TCPMessageSuper;

import java.io.*;
import java.net.ServerSocket;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by Benjamin on 3/11/2018.
 */
public abstract class TCPRobotServerSuper<T extends RobotStatus>{
    protected SimpleLogger logger;
    public int Port = 666;
    private boolean DEBUG = false;

    ServerSocket Socket;
    Boolean ClientConnected = false;
    Boolean WaitingForClient = false;
    //KMR RobotStatus from heartbeat
    protected Boolean SendingMessageLock = true;

    Date lastHeartBeatSent;
    protected Date lastHeartBeatReceived;
    ExecutorService Connection_Executer_Service;
    protected Queue<TCPMessageSuper> UnprocessedMessages;
    ObjectInputStream inFromClient;
    protected ObjectOutputStream outToClient;
    ScheduledExecutorService reconnect_Executer_service;
    protected T Status; //USED TO LOCK STATUS
    protected Boolean Status_lock = true;
    private Date connectionActiveWithoutReset = null;


    public Date getLastHeartBeatReceived() {
        Date date = null;
        synchronized (Status_lock) {
            if(lastHeartBeatReceived != null)
                date = (Date) lastHeartBeatReceived.clone();
        }
        return date;
    }

    public TCPRobotServerSuper(T status, int port, String logIdentifier) throws IOException {
        logger = new SimpleLogger(logIdentifier);
        Port = port;
        Socket = new ServerSocket(Port);
        ClientConnected = false;
        WaitingForClient = false;
        Status =status;
        reconnect_Executer_service = Executors.newSingleThreadScheduledExecutor();
        reconnect_Executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                boolean connect, waitclient;
                synchronized (ClientConnected) {
                    connect = ClientConnected;
                }
                synchronized (WaitingForClient) {
                    waitclient = WaitingForClient;
                }

                if (!connect && !waitclient) {
                        logger.LogEvent("Reconnect");
                    synchronized (WaitingForClient) {
                        WaitingForClient = true;
                    }
                    StartServer();
                }
            }
        }, 0, 1000, MILLISECONDS);
    }

    public T GetStatus() throws CloneNotSupportedException {
        T status;
        synchronized (Status_lock)
        {
            status = (T) Status.clone();
        }
        return status;
    }

    protected void ProcessHeartBeat(HeartBeat<T> message) {
        synchronized (Status_lock) {
            Status = (T) message.getStatus();
            logger.LogMessage(true,message.toString());
            lastHeartBeatReceived = new Date();
        }
    }
    public void StartServer() {
        try {
            logger.LogEvent("Server Waiting for Client");
            java.net.Socket connectionSocket = Socket.accept();
            logger.LogEvent("Client Connected");
            connectionActiveWithoutReset = new Date();


            SetClientConnected();
            WaitingForClient = false;

            inFromClient =
                    new ObjectInputStream((connectionSocket.getInputStream()));
            outToClient = new ObjectOutputStream(connectionSocket.getOutputStream());
            UnprocessedMessages = new LinkedList<TCPMessageSuper>();
            if (Connection_Executer_Service != null)
                Connection_Executer_Service.shutdown();
            Connection_Executer_Service = Executors.newFixedThreadPool(3
            );
            Connection_Executer_Service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = getClientConnected();
                    while (connected) {
                        try {
                            LisenForMessages(inFromClient, outToClient);
                            Thread.sleep(100);
                        } catch (IOException e) {
                            SetClientDisconnected();
                        } catch (ClassNotFoundException e) {
                            SetClientDisconnected();
                        } catch (InterruptedException e) {
                            SetClientDisconnected();
                        }
                        connected = getClientConnected();
                    }
                }
            });

            Connection_Executer_Service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = getClientConnected();
                    while (connected) {
                        try {
                            SentHeartBeat();
                            Thread.sleep(100);
                        } catch (IOException e) {
                            SetClientDisconnected();
                        } catch (InterruptedException e) {
                            SetClientDisconnected();
                        }
                        connected = getClientConnected();
                    }

                }
            });

            Connection_Executer_Service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = getClientConnected();
                    while (connected) {
                        try {
                            CheckConnectionAlive();
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                        connected = getClientConnected();
                    }
                }
            });


        } catch (IOException e1) {
            SetClientDisconnected();
            lastHeartBeatSent = null;
            lastHeartBeatReceived = null;
        }
    }



    private void CheckConnectionAlive() {
        synchronized (Status_lock) {
            long diffInMillies = (new Date()).getTime() - lastHeartBeatReceived.getTime();
            if (diffInMillies > Configuration.getTimeoutTCP())
                SetClientDisconnected();
        }
    }

    private void SentHeartBeat() throws IOException {
        ServerHeartBeat heartbeat = new ServerHeartBeat(new Date(), Configuration.getEmergencyStop());
        if(DEBUG)
            logger.LogMessage(false,heartbeat.toString());
        synchronized (SendingMessageLock) {
            outToClient.writeObject(heartbeat);
            outToClient.flush();
            lastHeartBeatSent = new Date();
        }
    }

    private void SetClientDisconnected() {
        logger.LogEvent("Disconnected");
        synchronized (ClientConnected) {
            ClientConnected = false;
        }
        synchronized (Status_lock) {
            Status.setStatus(RobotStatusEnum.Disconnected);
        }
    }

    private void SetClientConnected() {
        synchronized (Status_lock) {
            lastHeartBeatReceived = new Date();
        }
        synchronized (ClientConnected) {
            ClientConnected = true;
        }

    }

    private void LisenForMessages(ObjectInputStream inFromClient, ObjectOutputStream outToClient) throws IOException, ClassNotFoundException {
        TCPMessageSuper message;
        synchronized (inFromClient) {
            message = (TCPMessageSuper) inFromClient.readObject();
        }
        if(DEBUG)
        logger.LogMessage(true,message.toString());
        while (message != null) {
            switch (message.getType()) {
                case Heartbeat:
                    ProcessHeartBeat((HeartBeat) message);

                    break;
                default:
                    synchronized (UnprocessedMessages) {
                        UnprocessedMessages.add(message);
                    }
            }
            synchronized (inFromClient) {
                message = (TCPMessageSuper) inFromClient.readObject();
            }
        }

        if(connectionActiveWithoutReset.getTime() + 1 * 60*60*1000 < (new Date()).getTime())
        {
            synchronized (SendingMessageLock) {
                outToClient.reset();
            }
            inFromClient.reset();
        }
    }

    public Boolean getClientConnected() {
        Date last_heart  = null;
        synchronized (Status_lock)
        {
            if(lastHeartBeatReceived != null)
                last_heart = (Date) lastHeartBeatReceived.clone();
            else
                return false;
        }
        synchronized (ClientConnected) {

            boolean heartbeat_alive =   (new Date()).getTime() - last_heart.getTime() < Configuration.getTimeoutTCP();
            return heartbeat_alive && ClientConnected;
        }
//        synchronized (ClientConnected) {
//
//             return  ClientConnected;
//        }
    }

    public static void main(String argv[]) throws Exception {

//        KMRTCPServer server = new KMRTCPServer();
//        Thread.sleep(5000);
//        KMRTask task = new KMRTask(Location.Photocat, KMRTaskTypeEnum.TransportKMR);
//        System.out.println("Sending KMRTask");
//        System.out.println(server.SentTask(task));
    }



    public void Destruct() throws IOException {
        if (Connection_Executer_Service != null)
            Connection_Executer_Service.shutdown();
        if (reconnect_Executer_service != null)
            reconnect_Executer_service.shutdown();
        SetClientDisconnected();
        try {
            Socket.close();
        } catch (IOException e) {
            //This is okay, if it cant be closed, it will be closed.
        }
        Socket = null;
        logger.Destruct();
    }
}