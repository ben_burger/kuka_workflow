package Workflow.Configuration;

import java.io.*;

/**
 * Created by taurnist on 02/08/2017.
 */
public class Configuration  implements Externalizable {
    private static final long serialVersionUID = 90432874328763297L;


    private static Boolean EmergencyStop = false;


    public static Boolean getEmergencyStop() {
        return EmergencyStop;
    }
    public static void setEmergencyStop(Boolean emergencyStop) {
        EmergencyStop = emergencyStop;
    }

    public static String getScalePort2() {
        return ScalePort2;
    }

    public static String getVibratoryPhotoCatPort() {
        return VibratoryPhotoCatPort;
    }

    public static void setScale2Port(String scalePort2) {
        ScalePort2 = scalePort2;
    }

    public static String getLiquidPumpPort2() {
        return LiquidPumpPort2;
    }

    public static void setLiquidPump2Port(String liquidPumpPort2) {
        LiquidPumpPort2 = liquidPumpPort2;
    }

    public Configuration()
    {}

    //COM Connection Port Info
    private static String QuantosPort = "COM66";
    private static String PhotoCatPort = "COM48";
    private static String VibratoryPhotoCatPort = "COM69";
    private static String LiquidPumpPort = "COM56";
    private static String ScalePort = "COM58";
    private static String SonicatorPort = "COM5";
    private static String CapperPort = "COM54";
    private static String ScalePort2 = "COM42";
    private static String LiquidPumpPort2 = "COM60 ";
    private static String ProbePort = "COM10";

    private static int ServerKMRPort = 666;
    private static int sideLBRPort = 667;

    public static void setVibratoryPhotoCatPort(String vibratoryPhotoCatPort) {
        VibratoryPhotoCatPort = vibratoryPhotoCatPort;
    }

    public static void setQuantosPort(String quantosPort) {     QuantosPort = quantosPort;    }
    public static void setPhotoCatPort(String photoCatPort) {
        PhotoCatPort = photoCatPort;
    }
    public static void setLiquidPumpPort(String liquidPumpPort) {
        LiquidPumpPort = liquidPumpPort;
    }
    public static void setScalePort(String scalePort) {
        ScalePort = scalePort;
    }
    public static void setCapperPort(String capperPort) {
        CapperPort = capperPort;
    }
    public static void setSonicatorPort(String sonicatorPort) {
        SonicatorPort = sonicatorPort;
    }
    public static void setKMRPort(int serverSideKMRPort) {
        ServerKMRPort = serverSideKMRPort;
    }
    public static void setLBRPort(int sideLBRPort) {
        Configuration.sideLBRPort = sideLBRPort;
    }

    public static String getSonicatorPort() {
        return SonicatorPort;
    }
    public static String getQuantosPort() {
        return QuantosPort;
    }
    public static String getPhotoCatPort() {
        return PhotoCatPort;
    }
    public static String getLiquidPumpPort() {
        return LiquidPumpPort;
    }
    public static String getCapperPort() {
        return CapperPort;
    }
    public static String getScalePort() {
        return ScalePort;
    }
    public static int getSideKMRPort() {
        return ServerKMRPort;
    }
    public static int getSideLBRPort() {
        return sideLBRPort;
    }

    //Connection Timeouts
    private static int TimeoutQuantos = 180000;
    private static int TimeoutAdruino = 10000;
    private static int timeoutScale = 10000;
    private static int timeoutTCP = 10000;
    private static int TimeoutCapper = 10000;

    public static int getTimeoutQuantos() {
        return TimeoutQuantos;
    }
    public static int getTimeoutAdruino() {
        return TimeoutAdruino;
    }
    public static int getTimeoutCapper() {
        return TimeoutCapper;
    }
    public static long getTimeoutScale() { return timeoutScale; }
    public static int getTimeoutTCP() {
        return timeoutTCP;
    }

    public static void setTimeoutCapper(int timeoutCapper) {
        TimeoutCapper = timeoutCapper;
    }
    public static void setTimeoutQuantos(int timeoutQuantos) {
        TimeoutQuantos = timeoutQuantos;
    }
    public static void setTimeoutAdruino(int timeoutAdruino) {
        TimeoutAdruino = timeoutAdruino;
    }
    public static void setTimeoutScale(int timeoutScale) {
        Configuration.timeoutScale = timeoutScale;
    }
    public static void setTimeoutTCP(int timeoutTCP) {
        Configuration.timeoutTCP = timeoutTCP;
    }



    private static int HeartBeatFrequencyQuantos = 5000;
    private static int HeartBeatFrequencyAdruino = 5000;
    private static int HeartBeatFrequencyScale = 5000;
    private static int HeartBeatFrequencyTCP = 5000;
    private static int HeartBeatFrequencyCapper = 5000;

    public static int getHeartBeatFrequencyQuantos() {
        return HeartBeatFrequencyQuantos;
    }
    public static int getHeartBeatFrequencyAdruino() {
        return HeartBeatFrequencyAdruino;
    }
    public static int getHeartBeatFrequencyScale() {
        return HeartBeatFrequencyScale;
    }
    public static int getHeartBeatFrequencyTCP() {
        return HeartBeatFrequencyTCP;
    }
    public static int getHeartBeatFrequencyCapper() {
        return HeartBeatFrequencyCapper;
    }

    public static void setHeartBeatFrequencyQuantos(int heartBeatFrequencyQuantos) {
        HeartBeatFrequencyQuantos = heartBeatFrequencyQuantos;
    }
    public static void setHeartBeatFrequencyAdruino(int heartBeatFrequencyAdruino) {
        HeartBeatFrequencyAdruino = heartBeatFrequencyAdruino;
    }
    public static void setHeartBeatFrequencyScale(int heartBeatFrequencyScale) {
        HeartBeatFrequencyScale = heartBeatFrequencyScale;
    }
    public static void setHeartBeatFrequencyTCP(int heartBeatFrequencyTCP) {
        HeartBeatFrequencyTCP = heartBeatFrequencyTCP;
    }
    public static void setHeartBeatFrequencyCapper(int heartBeatFrequencyCapper) {
        HeartBeatFrequencyCapper = heartBeatFrequencyCapper;
    }

    //ProcessManagement Managers
    private static boolean DEBUGLiquidmodule = true;

    public static boolean DEBUGLiquidmodule() {
        return DEBUGLiquidmodule;
    }


    public static String getGCIP() {
        return GCIP;
    }

    public static int getGCPort() {
        return GCPort;
    }

    private static String GCIP = "172.31.1.71";
    private static int GCPort = 665;

    public static void setGCIP(String GCIP) {
        Configuration.GCIP = GCIP;
    }

    public static void setGCPort(int GCPort) {
        Configuration.GCPort = GCPort;
    }

    private static PhotolysisStation PhotolysisStation =Workflow.Configuration.PhotolysisStation.Vibratory;
    private static SchedulingStyle Scheduler = SchedulingStyle.SingleBatch;


    public static Workflow.Configuration.PhotolysisStation getPhotolysisStation() {
        return PhotolysisStation;
    }

    public static void setPhotolysisStation(Workflow.Configuration.PhotolysisStation photolysisStation) {
        PhotolysisStation = photolysisStation;
    }

    public static SchedulingStyle getScheduler() {
        return Scheduler;
    }

    public static void setScheduler(SchedulingStyle scheduler) {
        Scheduler = scheduler;
    }

    public static String getProbePort() {
        return ProbePort;
    }

    public static void setProbePort(String probePort) {
        ProbePort = probePort;
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        //COM Ports
        PhotolysisStation = PhotolysisStation.fromValue(in.readUTF());
        Scheduler = SchedulingStyle.fromValue(in.readUTF());
           VibratoryPhotoCatPort = in.readUTF();
        QuantosPort = in.readUTF();
        PhotoCatPort = in.readUTF();
        LiquidPumpPort = in.readUTF();
        ScalePort = in.readUTF();
        SonicatorPort = in.readUTF();
        CapperPort = in.readUTF();
        LiquidPumpPort2 = in.readUTF();
        ProbePort = in.readUTF();
        ScalePort2 = in.readUTF();


        ServerKMRPort = in.readInt();
        sideLBRPort = in.readInt();


        //COM Timeout
        TimeoutQuantos = in.readInt();
        TimeoutAdruino = in.readInt();
        timeoutScale = in.readInt();
        timeoutTCP = in.readInt();
        TimeoutCapper = in.readInt();

//        //HeartBeats frequencies
        HeartBeatFrequencyQuantos = in.readInt();
        HeartBeatFrequencyAdruino = in.readInt();
        HeartBeatFrequencyScale = in.readInt();
        HeartBeatFrequencyTCP = in.readInt();
        HeartBeatFrequencyCapper = in.readInt();

        //Scripts
        DEBUGLiquidmodule = in.readBoolean();

        //Ports
        GCPort = in.readInt();
        GCIP = in.readUTF();
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        //COM Ports
        out.writeUTF(PhotolysisStation.toValue());
        out.writeUTF(Scheduler.toValue());

        out.writeUTF(VibratoryPhotoCatPort);
        out.writeUTF(QuantosPort);
        out.writeUTF(PhotoCatPort);
        out.writeUTF(LiquidPumpPort);
        out.writeUTF(ScalePort);
        out.writeUTF(SonicatorPort);
        out.writeUTF(CapperPort);
        out.writeUTF(LiquidPumpPort2);
        out.writeUTF(ProbePort);
        out.writeUTF(ScalePort2);
        out.writeInt(ServerKMRPort);
        out.writeInt(sideLBRPort);

        //COM Timeout
        out.writeInt(TimeoutQuantos);
        out.writeInt(TimeoutAdruino);
        out.writeInt(timeoutScale);
        out.writeInt(timeoutTCP);
        out.writeInt(TimeoutCapper);

        //HeartBeats frequencies
        out.writeInt(HeartBeatFrequencyQuantos);
        out.writeInt(HeartBeatFrequencyAdruino);
        out.writeInt(HeartBeatFrequencyScale);
        out.writeInt(HeartBeatFrequencyTCP);
        out.writeInt(HeartBeatFrequencyCapper);

        //Scripts
        out.writeBoolean(DEBUGLiquidmodule);

        //Ports
        out.writeInt(GCPort);
        out.writeUTF(GCIP);
    }

    public static void loadSettings() throws IOException, ClassNotFoundException {
            FileInputStream fis = new FileInputStream("photo.config");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Configuration settings =  (Configuration) ois.readObject();
            ois.close();
    }

    public static void saveUserSettings() throws IOException {
            FileOutputStream fos = new FileOutputStream("photo.config");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(new Configuration());
            oos.close();
    }

}
