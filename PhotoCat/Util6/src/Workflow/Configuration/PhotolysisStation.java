package Workflow.Configuration;

public enum PhotolysisStation {
    Magnetic("Magnetic"), Vibratory("Vibratory");

    private final String value;

    PhotolysisStation(String value) {
        this.value = value;
    }

    public static PhotolysisStation fromValue(String value) {
        if (value != null) {
            for (PhotolysisStation station : values()) {
                if (station.value.equals(value)) {
                    return station;
                }
            }
        }

       throw new IllegalArgumentException("Invalid color: " + value);
    }

    public String toValue() {
        return value;
    }
}
