package Workflow.Configuration;


public enum SchedulingStyle {
    SingleBatch("SingleBatch", 2, 1),MultiBatch("MultiBatch", 6,6);

    private final String value;
    private final int NumberOfRacksInSystem;
    private final int numberOfRacksToWorkOn;

    SchedulingStyle(String value, int numberOfRacksInSystem, int numberOfRacksToWorkOn) {
        this.value = value; this.NumberOfRacksInSystem = numberOfRacksInSystem;
        this.numberOfRacksToWorkOn = numberOfRacksToWorkOn;
    }

    public static SchedulingStyle fromValue(String value) {
        if (value != null) {
            for (SchedulingStyle enum_value : values()) {
                if (enum_value.value.equals(value)) {
                    return enum_value;
                }
            }
        }

        throw new IllegalArgumentException("Invalid SchedulingStyle: " + value);
    }
    public String toValue() {
        return value;
    }

    public int getNumberOfRacks() {
        return NumberOfRacksInSystem;
    }

    public int getNumberOfRacksToWorkOn() {
        return numberOfRacksToWorkOn;
    }
}
