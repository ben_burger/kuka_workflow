package Workflow.Configuration;

public enum Location {
    Quantos("Quantos"),
    Photocat("Photocat"),
    Vibratory_Photocat("Vibratory_Photocat"),
    LiquidHandlingSystem("LiquidHandlingSystem"),
    GC("GC"),
    Crossroads("Crossroads"),
    Sonicator("Sonicator"),
    Cartridge("Cartridge"),
    ChargingStation(""),
    Unknown("Unknown"),
    DryingStation("DryingStation"), //Essentially a stub for no location known, not travelable
    InputStation("InputStation"),
    InputStation2("InputStation2"),
    InputStation3("InputStation3");
    public String getPointSetName() {
        return PointSetName;
    }

    private String PointSetName;

    Location(String name)
    {

        PointSetName = name;
    }
}

