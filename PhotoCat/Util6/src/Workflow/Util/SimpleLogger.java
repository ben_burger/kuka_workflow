package Workflow.Util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleLogger {
    private PrintWriter writer;
    private String logIdentifier;

    public SimpleLogger(String identifier) throws IOException {
        logIdentifier = identifier;
        InitializeSerialIO();
    }

    protected void InitializeSerialIO() throws IOException {
        writer = new PrintWriter(new File("log\\" + logIdentifier + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date(System.currentTimeMillis())) + ".log"));
    }

    public void Destruct() throws IOException {

        if (writer != null)
            writer.close();
    }

    public void LogMessage(boolean received, String data)  {
        if(data != null) {
            StringBuffer sb = new StringBuffer();
            sb.append(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date(System.currentTimeMillis())));
            sb.append(" ");
            sb.append(logIdentifier);
            if (received)
                sb.append(" ------->");
            else
                sb.append(" <-------");
            sb.append(": ");
            sb.append(data.trim());
            sb.append("\r\n");
            writer.append(sb.toString());
            writer.flush();
        }
    }
    public void LogEvent(String data)  {
        if(data != null) {
            StringBuffer sb = new StringBuffer();
            sb.append(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date(System.currentTimeMillis())));
            sb.append(logIdentifier);
            sb.append(": ");
            sb.append(data.trim());
            sb.append("\r\n");
            writer.append(sb.toString());
            writer.flush();
        }
    }

}
