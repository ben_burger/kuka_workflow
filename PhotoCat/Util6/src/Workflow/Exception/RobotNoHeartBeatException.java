package Workflow.Exception;

public class RobotNoHeartBeatException extends Exception {
    public RobotNoHeartBeatException(String station) {
        super(station);
    }
}
