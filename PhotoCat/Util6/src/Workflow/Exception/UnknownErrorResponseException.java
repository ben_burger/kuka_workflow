package Workflow.Exception;

/**
 * Created by Benjamin on 3/9/2018.
 */
public class UnknownErrorResponseException extends Exception {
    public  UnknownErrorResponseException(String module)
    {
        super(module + " has returned an unknown error");
    }
}
