package Workflow.Exception;

/**
 * Created by Benjamin on 3/9/2018.
 */
public class UnknownResponseException extends Exception{
    public  UnknownResponseException(String module)
    {
        super(module + " has returned an unknown response");
    }
}
