package Workflow.Exception;

public class LiquidDispenseDidNotFinishInTime extends Exception {
    public LiquidDispenseDidNotFinishInTime(int stationId)
    {
        super("Station " + stationId + " not dispensing. Robot resetting and disabling step.");
    }
}
