package Workflow.Exception;

public class TimeOutException extends Exception
{
    public TimeOutException(String module)
    {
        super(module + " has timed out.");
    }
}
