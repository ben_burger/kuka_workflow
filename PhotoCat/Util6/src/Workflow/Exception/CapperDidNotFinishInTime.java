package Workflow.Exception;

public class CapperDidNotFinishInTime extends Exception {
    public CapperDidNotFinishInTime()
    {
        super("Capper did not finish task in time, suspected crash");
    }

}
