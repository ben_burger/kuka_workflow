package Workflow.Exception;

public class DeviceOfflineException extends Exception {
    String Station;

    public DeviceOfflineException(String station) {
        super("Station is offline: "+ station);
        Station = station;
    }
}
