package Workflow.Exception;

public class RobotErrorStateException extends Exception {

    public RobotErrorStateException(String message)
    {
        super(message);
    }

    public RobotErrorStateException() {

    }
}
