package Workflow.Exception;

public class CompoundNotPResentException extends Exception
{
    public CompoundNotPResentException(String compound, String batch_name) {
        super(String.format("Compound %s is not present in batch %s",compound,batch_name));
    }
}
