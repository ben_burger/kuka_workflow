package Workflow.Exception;

/**
 * Created by Benjamin on 3/10/2018.
 */
public class QuantosInputException extends Exception {
    public QuantosInputException(String message)
    {super(message);}
}
