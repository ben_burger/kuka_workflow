package Workflow.Exception;

/**
 * Created by Benjamin on 3/10/2018.
 */
public class TooManySamplesInRackException extends Exception{

    public TooManySamplesInRackException()
    {
        super("A rack may only contain between 0 and 18 samples.");
    }
}
