package Workflow.Exception;

/**
 * Created by Benjamin on 3/10/2018.
 */
public class QuantosException extends Exception {

    public QuantosException(String error)
    {
        super("Quantos returned the following error: "+ error);
    }
}
