package Workflow.Exception;

public class InconsistentStateException extends Exception {
    public InconsistentStateException(String message) {
        super(message);
    }
}
