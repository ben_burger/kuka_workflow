package workflow.GC;

import Workflow.Batch.Batch;
import Workflow.Batch.BatchTimestampEnum;
import Workflow.Batch.BatchTimestampType;
import Workflow.Configuration.Configuration;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TCPStationServerSuper;
import Workflow.TCP.TaskSuper;
import workflow.GC.Messages.GCResults;
import workflow.GC.Messages.GCTask;
import workflow.GC.Messages.GCTaskTypeEnum;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by Benjamin on 12/11/2018.
 */
public class GCTCPServer extends TCPStationServerSuper<GCStationStatus> {
    public GCTCPServer() throws IOException {
        super(new GCStationStatus(null, RobotStatusEnum.Disconnected, null, null, null, false), Configuration.getGCPort());
        getNewTask();
    }

    public void AddBatchesToSubmittedQueue(Batch[] batchs)
    {
        synchronized(Status) {
            Status.AddBatchesToSubmittedQueue(batchs);
        }
    }

    public void SetBatchCompleted(Batch batch) throws IOException {
        synchronized(Status) {
            batch.setTimestamp(BatchTimestampEnum.gc_analysis, BatchTimestampType.finish, new Date());
            Status.SetBatchCompleted(batch);
        }
    }

    public void SetMeasurementInProgress(boolean measurementInProgress) {
        synchronized(Status) {
            Status.setStation_running(measurementInProgress);
        }
    }

    public void SentResults(Batch[] results) throws IOException {
        synchronized(Status) {
            GCResults result = new GCResults(new Date(), results);
            SentMessage(result);
        }

    }
    @Override
    public boolean AcceptTask(TaskSuper task) {
        synchronized(Status) {
            if (((GCTask) task).getType() == GCTaskTypeEnum.RunBatch && Status.getBatches_running().size() > 0)
                return false;

            return true;
        }
    }

    public LinkedList<Batch> getBatchesRunning() {
        synchronized(Status) {
            return Status.getBatches_running();
        }
    }

    public LinkedList<Batch> getBatchesSubmitted() {
        synchronized (Status) {
            return Status.getBatches_submitted();
        }
    }
        public LinkedList<Batch> getBatchesCompleted() {
        synchronized(Status) {
            return Status.getBatches_completed();
    }


}

    public void  StartRunningBatch(Batch batch) throws IOException {
        synchronized (Status) {
            batch.setTimestamp(BatchTimestampEnum.gc_analysis, BatchTimestampType.start, new Date());
            Status.MoveBatchToRunning(batch);
        }
    }

    public void ResetStatus() {
        synchronized (Status) {
            Status.Reset();
        }
    }
}
