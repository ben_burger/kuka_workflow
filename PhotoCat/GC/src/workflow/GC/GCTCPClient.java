package workflow.GC;


import Workflow.Batch.Batch;
import Workflow.Configuration.Configuration;
import Workflow.TCP.MessageType;
import Workflow.TCP.Messages.TCPMessageSuper;
import Workflow.TCP.Messages.TaskRequest;
import Workflow.TCP.Messages.TaskResponse;
import Workflow.TCP.TCPStationClientSuper;
import Workflow.Util.SimpleLogger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import workflow.GC.Messages.GCResults;
import workflow.GC.Messages.GCTask;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Benjamin on 12/11/2018.
 */
public class GCTCPClient extends TCPStationClientSuper<GCStationStatus> {
    public GCTCPClient(GCStationStatus status) throws IOException {
        super(status, Configuration.getGCIP(), Configuration.getGCPort());
        logger = new SimpleLogger("GC-TCP");

    }


    public TaskResponse SentTask(GCTask task) throws IOException, InterruptedException {


        TaskResponse result = null;
        TaskRequest request = new TaskRequest(new Date(), task);
        logger.LogMessage(false,request.toString());
        synchronized (SentMessageLock) {
            outToServer.writeObject(request);
            outToServer.flush();
        }

        TCPMessageSuper message = null;
        while (message == null) {

            synchronized (UnprocessedMessages) {
                if (UnprocessedMessages.size() > 0)
                    message = UnprocessedMessages.remove();
            }

            if (message == null)
                Thread.sleep(100);

        }
        switch (message.getType()) {
            case Response:
                result = (TaskResponse) message;
                break;
            default:
                throw new NotImplementedException();
        }


        return result;
    }

    public void RemoveUnprocessedResults()
    {
        while (UnprocessedMessages.size() > 0)
                    UnprocessedMessages.remove();
    }

    public Batch[] GetResultsFromUnprocessedMessages() throws InterruptedException {
        TCPMessageSuper message = null;

        while (message == null) {
            synchronized (UnprocessedMessages) {
                if (UnprocessedMessages.size() > 0)
                    message = UnprocessedMessages.remove();
                else {
                    message = null;
                }
            }

            if (message == null)
                Thread.sleep(100);
        }

        if(message.getType().equals(MessageType.TaskSpecific))
        {
            GCResults results = (GCResults) message;
            return results.getResults();
        } else
        {

        }
        return null;
    }
}
