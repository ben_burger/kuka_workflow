package workflow.GC;

import Workflow.Batch.Batch;
import Workflow.TCP.RobotStatus;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TaskSuper;

import java.util.Date;
import java.util.LinkedList;
import java.util.Optional;

/**
 * Created by Benjamin on 12/11/2018.
 */
public class GCStationStatus extends RobotStatus {
    private boolean Station_running = true;
    private LinkedList<Batch> batches_submitted = new LinkedList<Batch>();
    private LinkedList<Batch> batches_running = new LinkedList<Batch>();
    private LinkedList<Batch> batches_completed = new LinkedList<Batch>();


    public LinkedList<Batch> getBatches_running() {
        return (LinkedList<Batch>) batches_running.clone();
    }

    public LinkedList<Batch> getBatches_completed() {
        return (LinkedList<Batch>) batches_completed.clone();
    }

    public LinkedList<Batch> getBatches_submitted() {
        return (LinkedList<Batch>) batches_submitted.clone();
    }

    public GCStationStatus(Date timestamp, RobotStatusEnum status, TaskSuper task, String error, Date taskStarted, boolean station_running) {
        super(timestamp, status, task, error, taskStarted);
        Station_running = station_running;
    }

    public void AddBatchesToSubmittedQueue(Batch[] batchs) {
        batches_submitted = new LinkedList<Batch>();
        for (Batch batch : batchs)
            batches_submitted.add(batch);
        batches_running = new LinkedList<Batch>();
        batches_completed = new LinkedList<Batch>();
    }

    public void SetBatchCompleted(Batch batch) {
        // sanity check
        //TODO check if batch in running
        //todo move to completed
            batches_running.remove(batch);
            batches_completed.add(batch);
    }

    public void setStation_running(boolean station_running) {
        Station_running = station_running;
    }

    public boolean getStation_running() {
        return Station_running;
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        GCStationStatus result = (GCStationStatus) super.clone();
        result.batches_running = new LinkedList(batches_running);
        result.batches_completed = new LinkedList(batches_completed);
        result.batches_submitted = new LinkedList(batches_submitted);
        return result;
    }

    public void MoveBatchToRunning(Batch batch) {
        // sanity check
        //TODO check if batch in submitted (sanity check)
        //todo move to running
        batches_submitted.remove(batch);
        batches_running.add(batch);
    }

    public void Reset() {

            Station_running = false;
            batches_submitted = new LinkedList<Batch>();
            batches_running = new LinkedList<Batch>();
            batches_completed = new LinkedList<Batch>();

        super.setStatus(RobotStatusEnum.Commandable);
        super.setTaskStarted(null);
        super.setTask(null);
        super.setError("");
    }
}
