package workflow.GC.Messages;

/**
 * Created by Benjamin on 3/10/2018.
 */
public enum GCTaskTypeEnum {
    RunBatch, GetResults, Reset
}

