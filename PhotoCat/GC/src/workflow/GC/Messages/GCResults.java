package workflow.GC.Messages;

import Workflow.Batch.Batch;
import Workflow.TCP.MessageType;
import Workflow.TCP.Messages.TCPMessageSuper;

import java.util.Date;

/**
 * Created by Benjamin on 12/11/2018.
 */
public class GCResults extends TCPMessageSuper {
    private static final long serialVersionUID = 904328743287632943L;
    private final Batch[] results;

    public GCResults(Date timestamp, Batch[] results) {
        super(timestamp, MessageType.TaskSpecific);
        this.results =results;
    }

    public Batch[] getResults() {
        return results;
    }
}
