package workflow.GC.Messages; /**
 * Created by Benjamin on 3/10/2018.
 */

import Workflow.Batch.Batch;
import Workflow.TCP.TaskSuper;

import java.io.Serializable;

public class GCTask extends TaskSuper implements Serializable {
    private static final long serialVersionUID = 2132132121L;
    Batch[] Batches;

    public GCTaskTypeEnum getType() {
        return Type;
    }
    private GCTaskTypeEnum Type;

    public GCTask(GCTaskTypeEnum type, Batch[] batches) {
        super();
        Type = type;
        Batches = batches;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("GCTask\r\n" + super.toString());
        sb.append("Type: " + Type.toString());
        sb.append("\r\n");

        return sb.toString();
    }

    public Batch[] getBatches() {
        return Batches;
    }
}
