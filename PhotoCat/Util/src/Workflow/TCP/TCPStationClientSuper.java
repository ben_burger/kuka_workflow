package Workflow.TCP;

import Workflow.Configuration.Configuration;
import Workflow.TCP.Messages.*;
import Workflow.Util.SimpleLogger;
import com.kuka.task.ITaskLogger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.*;

/**
 * Created by Benjamin on 3/11/2018.
 */
public class TCPStationClientSuper<T extends RobotStatus> {
    protected SimpleLogger logger;

    public Boolean isConnected() {
        synchronized (Connected) {
            return Connected;
        }
    }

    private boolean DEBUG = false;
    private int Port;
    private String IP;

    private Boolean Connected;
    protected T Status;
    Socket clientSocket;
    ExecutorService Connection_Executer_service;

    protected ObjectInputStream inFromServer;
    protected ObjectOutputStream outToServer;
    Date lastHeartBeatSent;
    Date lastHeartBeatReceived;
    protected Boolean SentMessageLock = true;

    ScheduledExecutorService reconnect_Executer_service;


    protected Queue<TCPMessageSuper> UnprocessedMessages;

    public TCPStationClientSuper(T status, String IP, int Port) {
        Status = status;
        this.IP = IP;
        this.Port = Port;
        Connected = false;
        reconnect_Executer_service = Executors.newSingleThreadScheduledExecutor();
        reconnect_Executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                boolean connect;
                synchronized (Connected) {
                    connect = Connected;
                }
                if (DEBUG)
                    logger.LogEvent("Check for Reconnect");
                if (!connect)
                    Connect();
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);

    }

    public boolean Connect() {
        if (DEBUG)
            logger.LogEvent("Try to Connect");
        if (Connected)
            throw new NotImplementedException(); //Implies multiple Servers....
        try {
            clientSocket = new Socket(IP, Port);
//            clientSocket.setSoTimeout(500);
            lastHeartBeatReceived = new Date();
            UnprocessedMessages = new LinkedList<TCPMessageSuper>();

            outToServer = new ObjectOutputStream(clientSocket.getOutputStream());
            inFromServer = new ObjectInputStream((clientSocket.getInputStream()));
            if (Connection_Executer_service != null)
                Connection_Executer_service.shutdown();
            Connection_Executer_service = Executors.newFixedThreadPool(3);
            synchronized (Connected) {
                Connected = true;
            }
            Connection_Executer_service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = isConnected();
                    while (connected) {
                        try {
                            LisenForMessages(inFromServer, outToServer);
                            if (DEBUG)
                                logger.LogEvent("LisenForMessages");
                            Thread.sleep(10);
                        } catch (IOException e) {
                            logger.LogEvent("Emergency Stop (TCP-Fail) in Lisen");
                            Disconnected(e);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        connected = isConnected();
                    }
                }
            });
            Connection_Executer_service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = isConnected();
                    while (connected) {
                        try {
                            SentHeartBeat(inFromServer, outToServer);
                            if (DEBUG)
                                logger.LogEvent("Sending HeartBeat");
                            Thread.sleep(100);
                        } catch (IOException e) {
                            e.printStackTrace();
                            logger.LogEvent("Stop (TCP-Fail) in Sent");
                            Disconnected(e);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (CloneNotSupportedException e) {
                            e.printStackTrace();
                        }
                        connected = isConnected();
                    }
                }
            });
            Connection_Executer_service.submit(new Runnable() {
                @Override
                public void run() {

                    boolean connected = isConnected();
                    while (connected) {
                        try {
                            CheckServerAlive();
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        connected = isConnected();
                    }

                }
            });

        } catch (IOException e) {
            synchronized (Connected) {
                Connected = false;
            }
            if (Connection_Executer_service != null)
                Connection_Executer_service.shutdown();
        }
        return Connected;
    }

    private void ProcessHeartBeat(ServerHeartBeat message) {
        synchronized (lastHeartBeatReceived) {
            Status = (T) message.getStatus();
            lastHeartBeatReceived = new Date();

        }
        if (DEBUG)
            logger.LogEvent("Receiving HeartBeat: " + message.getTimestamp().toString());
    }

    private void CheckServerAlive() {
        synchronized (lastHeartBeatReceived) {
            long diffInMillies = (new Date()).getTime() - lastHeartBeatReceived.getTime();
            if (diffInMillies > Configuration.getTimeoutTCP()) {
                logger.LogEvent("Stop (TCP-Timeout)");
                Disconnected(new TimeoutException());
            }
        }
    }

    private void SentHeartBeat(ObjectInputStream inFromServer, ObjectOutputStream outToServer) throws IOException, CloneNotSupportedException {
        T stat;
        synchronized (Status) {
            stat = (T) Status.clone();
        }
        synchronized (SentMessageLock) {
            if (DEBUG)
                logger.LogEvent(stat.toString());
            HeartBeat heartbeat = new HeartBeat(new Date(), new Date(), (T) stat.clone());
            if(DEBUG)
                logger.LogMessage(false, heartbeat.toString());
            outToServer.writeUnshared(heartbeat);

            outToServer.flush();
            lastHeartBeatSent = new Date();

        }
    }

    public T GetStatus() throws CloneNotSupportedException {
        T status;
        synchronized (Status) {
            status = (T) Status.clone();
        }
        return status;
    }


    private void LisenForMessages(ObjectInputStream inFromClient, ObjectOutputStream outToClient) throws IOException, ClassNotFoundException {
        Object input;

        synchronized (inFromClient) {
            input = inFromClient.readObject();
        }
        TCPMessageSuper message = (TCPMessageSuper) input;
        if(DEBUG)
            logger.LogMessage(true, message.toString());
        while (message != null) {
            switch (message.getType()) {
                case ServerHeartBeat:
                    ProcessHeartBeat((ServerHeartBeat) message);
                    break;
                default:
                    synchronized (UnprocessedMessages) {
                        UnprocessedMessages.add(message);
                    }
            }
            synchronized (inFromClient) {
                input = inFromClient.readObject();
            }
            message = (TCPMessageSuper) input;
        }
    }

    private void Disconnected(Exception e) {
        synchronized (Connected) {
            Connected = false;
        }
        Status.setStatus(RobotStatusEnum.Disconnected);
        if (Connection_Executer_service != null)
            Connection_Executer_service.shutdown();
        if (clientSocket != null)
            try {
                clientSocket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        if (e != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            if (DEBUG)
                logger.LogEvent("Disconnected: " + sw.toString());
        }
    }


    public void Destruct() throws IOException {
        if (reconnect_Executer_service != null) {
            reconnect_Executer_service.shutdown();
            reconnect_Executer_service = null;
        }
        Disconnected(null);
        logger.Destruct();
    }


    public Date getLastHeartBeatReceived() {
        return lastHeartBeatReceived;
    }
}