package Workflow.TCP;

import Workflow.Batch.Batch;
import Workflow.Batch.BatchTimestampEnum;
import Workflow.Batch.BatchTimestampType;
import Workflow.Configuration.Configuration;
import Workflow.TCP.Messages.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Created by Benjamin on 3/11/2018.
 */
public abstract class TCPStationServerSuper<T extends RobotStatus> {

    private boolean DEBUG = false;
    public int Port = 666;


    ServerSocket Socket;
    Boolean ClientConnected = false;
    Boolean WaitingForClient = false;
    //KMR RobotStatus from heartbeat
    protected Boolean SendingMessageLock = true;

    Date lastHeartBeatSent;
    protected Date lastHeartBeatReceived;
    ExecutorService Connection_Executer_Service;

    ObjectInputStream inFromClient;
    protected ObjectOutputStream outToClient;
    ScheduledExecutorService reconnect_Executer_service;
    protected T Status; //USED TO LOCK STATUS
    protected Boolean Status_lock = true;

    private Date connectionActiveWithoutReset = null;

    public Date getLastHeartBeatReceived() {
        Date date = null;
        synchronized (Status_lock) {
            if (lastHeartBeatReceived != null)
                date = (Date) lastHeartBeatReceived.clone();
        }
        return date;
    }


    public TCPStationServerSuper(T status, int port) throws IOException {
        Port = port;
        //System.out.println("Port: "+port);
        Socket = new ServerSocket(Port);
        ClientConnected = false;
        WaitingForClient = false;
        Status = status;
        reconnect_Executer_service = Executors.newSingleThreadScheduledExecutor();
        reconnect_Executer_service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                boolean connect, waitclient;
                synchronized (ClientConnected) {
                    connect = ClientConnected;
                }
                synchronized (WaitingForClient) {
                    waitclient = WaitingForClient;
                }
                if (!connect && !waitclient)
                    StartServer();
            }
        }, 2000, 5000, MILLISECONDS);
    }


    protected void ProcessHeartBeat(HeartBeat<T> message) {
        synchronized (Status_lock) {
            lastHeartBeatReceived = new Date();
        }

    }

    public void StartServer() {
        try {
            if (WaitingForClient)
                throw new NotImplementedException(); //Implies multiple KMRs...
            WaitingForClient = true;
            //System.out.println("HERE");
            java.net.Socket connectionSocket = Socket.accept();
            //System.out.println("HERE2");
            connectionActiveWithoutReset = new Date();

            SetClientConnected();
            WaitingForClient = false;

            inFromClient =
                    new ObjectInputStream((connectionSocket.getInputStream()));
            outToClient = new ObjectOutputStream(connectionSocket.getOutputStream());

            if (Connection_Executer_Service != null)
                Connection_Executer_Service.shutdown();
            Connection_Executer_Service = Executors.newFixedThreadPool(3
            );

            Connection_Executer_Service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = getClientConnected();
                    while (connected) {
                        try {
                            LisenForMessages(inFromClient, outToClient);
                            Thread.sleep(10);
                        } catch (IOException e) {
                            SetClientDisconnected();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            SetClientDisconnected();
                        }
                        connected = getClientConnected();
                    }
                }
            });

            Connection_Executer_Service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = getClientConnected();
                    while (connected) {
                        try {
                            SentHeartBeat();
                            Thread.sleep(100);
                        } catch (IOException e) {
                            SetClientDisconnected();
                        } catch (InterruptedException e) {
                            SetClientDisconnected();
                        } catch (CloneNotSupportedException e) {
                            SetClientDisconnected();
                        }
                        connected = getClientConnected();
                    }

                }
            });

            Connection_Executer_Service.submit(new Runnable() {
                @Override
                public void run() {
                    boolean connected = getClientConnected();
                    while (connected) {
                        try {
                            CheckConnectionAlive();
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                        }
                        connected = getClientConnected();
                    }
                }
            });

        } catch (IOException e1) {
            SetClientDisconnected();
            lastHeartBeatSent = null;
            lastHeartBeatReceived = null;
        }
    }


    private void CheckConnectionAlive() {
        synchronized (Status_lock) {
            long diffInMillies = (new Date()).getTime() - lastHeartBeatReceived.getTime();
            if (diffInMillies > Configuration.getTimeoutTCP())
                SetClientDisconnected();
        }
    }

    private void SentHeartBeat() throws IOException, CloneNotSupportedException {

    synchronized(Status_lock){
        synchronized (SendingMessageLock) {

            outToClient.writeUnshared(new ServerHeartBeat(new Date(), Configuration.getEmergencyStop(), Status));
            outToClient.flush();
           outToClient.reset();

            lastHeartBeatSent = new Date();
        }

    }}

    private void SetClientDisconnected() {
        synchronized (ClientConnected) {
            ClientConnected = false;
        }

        Status.setStatus(RobotStatusEnum.Disconnected);
    }

    private void SetClientConnected() {

        synchronized (ClientConnected) {
            ClientConnected = true;
        }
        synchronized (Status_lock) {
            lastHeartBeatReceived = new Date();
            Status.setStatus(RobotStatusEnum.Commandable);
        }
    }

    private void LisenForMessages(ObjectInputStream inFromClient, ObjectOutputStream outToClient) throws IOException, ClassNotFoundException {
        TCPMessageSuper message;
        synchronized (inFromClient) {
            message = (TCPMessageSuper) inFromClient.readUnshared();
//            message = () obj;
        }
        while (message != null) {

            switch (message.getType()) {
                case Heartbeat:

                    ProcessHeartBeat((HeartBeat) message);

                    break;
                case TaskRequest:
                    TaskSuper task = ((TaskRequest) message).getTask();
                    boolean status_commandable;
                    synchronized (Status_lock) {
                        status_commandable = Status.getStatus() == RobotStatusEnum.Commandable;
                    }
                    boolean allowed = AcceptTask(task);
                    if (!allowed) {
                        synchronized (SendingMessageLock) {
                            try {
                                outToClient.writeUnshared(new TaskResponse(new Date(), TaskResponseEnum.NotAllowed, "", task));
                                outToClient.flush();
                                lastHeartBeatSent = new Date();
                            } catch (IOException e) {
                                SetClientDisconnected();
                            }
                        }
                        break;
                    } else if (status_commandable) {
                        setTask(task);
                        synchronized (SendingMessageLock) {
                            try {
                                outToClient.writeUnshared(new TaskResponse(new Date(), TaskResponseEnum.Accepted, "", task));
                                outToClient.flush();
                                lastHeartBeatSent = new Date();
                            } catch (IOException e) {
                                SetClientDisconnected();
                            }
                        }
                    } else {
                        synchronized (SendingMessageLock) {
                            try {
                                outToClient.writeUnshared(new TaskResponse(new Date(), TaskResponseEnum.Busy, "", task));
                                outToClient.flush();
                                lastHeartBeatSent = new Date();
                            } catch (IOException e) {
                                SetClientDisconnected();
                            }
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            synchronized (inFromClient) {
                message = (TCPMessageSuper) inFromClient.readUnshared();
            }
        }

        if(connectionActiveWithoutReset.getTime() + 2*60*1000 < (new Date()).getTime())
        {
            synchronized (SendingMessageLock) {
//                outToClient.reset();
            }
//            inFromClient.reset();
            connectionActiveWithoutReset = new Date();
        }

    }

    public Boolean getClientConnected() {
        Date last_heart = null;
        synchronized (Status_lock) {
            if (lastHeartBeatReceived != null)
                last_heart = (Date) lastHeartBeatReceived.clone();
            else
                return false;
        }
        synchronized (ClientConnected) {
            long diff = ((new Date()).getTime() - last_heart.getTime());
            boolean heartbeat_alive = diff < Configuration.getTimeoutTCP();
            return heartbeat_alive && ClientConnected;
        }

    }


    public boolean AcceptTask(TaskSuper task) {
        return true;
    }

    private void setTask(TaskSuper task) {
        synchronized (Status_lock) {

            Status.setStatus(RobotStatusEnum.TaskRunning);
            Status.setTask(task);
        }

    }

    public TaskSuper getNewTask() {
        synchronized (ClientConnected) {
            if (!ClientConnected)
                return null;
        }
        synchronized (Status_lock) {
            if (Status.getStatus() == RobotStatusEnum.TaskRunning)
                return Status.getTask();
        }
        return null;

    }


    public void Destruct() {
        if (Connection_Executer_Service != null)
            Connection_Executer_Service.shutdown();
        if (reconnect_Executer_service != null)
            reconnect_Executer_service.shutdown();
        SetClientDisconnected();
        try {
            Socket.close();
        } catch (IOException e) {
            //This is okay, if it cant be closed, it will be closed.
        }
        Socket = null;
    }


    public void updateStatus(boolean taskResult, String error) {

        synchronized (Status_lock) {

            if (error != null && (!error.equals(""))) {
                Status.setError(error);
            }
            if (!taskResult) {
                Status.setStatus(RobotStatusEnum.Error);
            } else {
                Status.setStatus(RobotStatusEnum.Commandable);
            }
        }
    }

    public boolean SentMessage(TCPMessageSuper message ) throws IOException {
        synchronized (SendingMessageLock) {
            outToClient.writeUnshared(message);
            outToClient.flush();
        }
        return true;
    }



}
