package Workflow.Batch;

import Workflow.Configuration.Location;


/**
 * Created by Benjamin on 9/4/2018.
 */
public enum BatchLocation {
    NoRackAssigned(null,0),

    Robot_Transport_1(null,0),
    Robot_Transport_2(null,1),
    Robot_Transport_3(null,2),
    Robot_Transport_4(null,3),
    Robot_Transport_5(null,4),
    Robot_Transport_6(null,5),

    SolidDispenseStation_Manipulation(Location.Quantos,0),

    LiquidDispenseStation_Manipulation(Location.LiquidHandlingSystem,0),

    SonicationStation_Manipulation(Location.Sonicator,0),

    Photocat_Manipulation(Location.Photocat,0),
    Vibratory_Photocat_Manipulation(Location.Vibratory_Photocat,0),

    GC_Manipulation_1(Location.GC,0),
    GC_Manipulation_2(Location.GC,1),
    GC_Manipulation_3(Location.GC,2),

    DryingStation_1(Location.DryingStation, 0),
    DryingStation_2(Location.DryingStation, 1),
    DryingStation_3(Location.DryingStation, 2),
    DryingStation_4(Location.DryingStation, 3),
    DryingStation_5(Location.DryingStation, 4),
    DryingStation_6(Location.DryingStation, 5),

    InputStation_1(Location.InputStation, 0),
    InputStation_2(Location.InputStation, 1),
    InputStation_3(Location.InputStation, 2),
    InputStation_4(Location.InputStation, 3),
    InputStation_5(Location.InputStation, 4),
    InputStation_6(Location.InputStation, 5),
    InputStation_7(Location.InputStation, 6),
    InputStation_8(Location.InputStation, 7),
    InputStation_9(Location.InputStation, 8),
    InputStation_10(Location.InputStation, 9),

    InputStation_11(Location.InputStation2, 0),
    InputStation_12(Location.InputStation2, 1),
    InputStation_13(Location.InputStation2, 2),
    InputStation_14(Location.InputStation2, 3),
    InputStation_15(Location.InputStation2, 4),
    InputStation_16(Location.InputStation2, 5),
    InputStation_17(Location.InputStation2, 6),
    InputStation_18(Location.InputStation2, 7),
    InputStation_19(Location.InputStation2, 8),
    InputStation_20(Location.InputStation2, 9),
    InputStation_21(Location.InputStation2, 10),
    InputStation_22(Location.InputStation2, 11),


    InputStation_23(Location.InputStation3, 0),
    InputStation_24(Location.InputStation3, 1),
    InputStation_25(Location.InputStation3, 2),
    InputStation_26(Location.InputStation3, 3),
    InputStation_27(Location.InputStation3, 4),
    InputStation_28(Location.InputStation3, 5),
    InputStation_29(Location.InputStation3, 6),
    InputStation_30(Location.InputStation3, 7),
    InputStation_31(Location.InputStation3, 8),
    InputStation_32(Location.InputStation3, 9),
    InputStation_33(Location.InputStation3, 10),
    InputStation_34(Location.InputStation3, 11),
    InputStation_35(Location.InputStation3, 12),
    InputStation_36(Location.InputStation3, 13),
    InputStation_37(Location.InputStation3, 15),
    InputStation_38(Location.InputStation3, 16),
    InputStation_39(Location.InputStation3, 17);




    private Location location;
    private int StationIndex;

    BatchLocation(Location location, int stationIndex)
    {
        this.location = location;this.StationIndex = stationIndex;
    }


    public Location getLocation() {
        return this.location;
    }

    public int GetStationIndex() {
        return StationIndex;
    }

    public boolean isOnRobot() {
        return this.location == null;
    }
}
