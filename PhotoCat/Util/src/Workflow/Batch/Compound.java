package Workflow.Batch;

import java.io.Serializable;

public class Compound implements Serializable{
    private CompoundType Type;
    public String Name;
    private float Min;
    private float Max;

    public Compound(CompoundType type, String name, float min, float max) {
        Type = type;
        Name = name;
        this.Min = min;
        this.Max = max;
    }

    public float getMin() {
        return Min;
    }

    public void setMin(float min) {
        Min = min;
    }

    public float getMax() {
        return Max;
    }

    public void setMax(float max) {
        Max = max;
    }

    public CompoundType getType() {
        return Type;
    }
    public String getName() {
        return Name;
    }

}
