package Workflow.Batch;

import java.io.Serializable;

public class CompoundDispense  implements Serializable {
    private Compound compound;
    private Float amount_to_dispense;
    private Float amount_dispensed = 0f;

    public void setAmount_dispensed(float amount_dispensed) {
        this.amount_dispensed = amount_dispensed;
    }

    public CompoundDispense(Compound compound, float amount_to_dispense)
    {
        this.compound = compound;
        this.amount_to_dispense = amount_to_dispense;
    }

    public Compound getCompound() {
        return compound;
    }

    public Float getAmount_to_dispense() {
        return amount_to_dispense;
    }

    public Float getAmount_dispensed() {
        return amount_dispensed;
    }
}
