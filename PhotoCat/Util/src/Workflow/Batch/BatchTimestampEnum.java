package Workflow.Batch;

public enum BatchTimestampEnum {
//    run_start,
//    solid_dispensing_start,
//    solid_dispensing_finish,
//    liquid_dispensing_start,
//    liquid_dispensing_finish,
//    capper_start,
//    capper_finish,
//    sonicator_start,
//    sonicator_finish,
//    photocat_start,
//    photocat_finish,
//    gc_submit_job,
//    gc_job_finish;
    add_fresh_rack,
    submit,
    run,
    PickUp,
    load_quantos,
    unload_quantos,
    dispense_liquid,
    load_sonicator,
    sonication,
    unload_sonicator,
    load_photocat,
    photolysis,
    unload_photocat,
    load_gc,
    gc_analysis,
    unload_gc,
     move_to_storage
}

