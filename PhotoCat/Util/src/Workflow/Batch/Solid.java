package Workflow.Batch;

import java.util.UUID;

public class Solid  extends Compound{
    private int HotelIndex;
    private boolean Blocked;
    private int tapping;
    private String DispenseAlgorithm;
    private int RemainingDosages;

    public Solid(String name, int hotelIndex, boolean blocked, int tapping, String dispenseAlgorithm, float min, float max, int remainingDosages) {
        super(CompoundType.Solid, name,min,max);
        this.HotelIndex = hotelIndex;
        this.Blocked = blocked;
        this.tapping = tapping;
        this.DispenseAlgorithm = dispenseAlgorithm;
        this.RemainingDosages = remainingDosages;
    }

public String getDispenseAlgorithm() {
    return DispenseAlgorithm;
}
    public int getHotelIndex() {
        return HotelIndex;
    }

    public boolean getBlocked()
    {
        return this.Blocked;
    }
    @Override
    public boolean equals(Object o)  {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof Solid)) {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        Solid c = (Solid) o;

        // Compare the data members and return accordingly
        return Name.equals(c.getName());
    }

    public void setBlocked(boolean blocked) {
        this.Blocked = blocked;
    }

    public void setHotelIndex(int hotelIndex) {
        this.HotelIndex = hotelIndex;
    }

    public int getTapping() {
        return tapping;
    }

    public void SetRemainingDosages(int remainingDosages) {

        this.RemainingDosages = remainingDosages;
    }

    public int getRemainingDosages() {
        return this.RemainingDosages;
    }
}
