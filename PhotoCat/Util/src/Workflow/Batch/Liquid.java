package Workflow.Batch;



/**
 * Created by Benjamin on 2/19/2018.
 */
import java.util.*;
public class Liquid extends Compound {
    private int PumpID;
    //Water("Water", 1), MixtureTEAMeOHWater("Mixture", 0.899f), MeOH("MeOH",0.792f), SiliconOil("SiliconOil",0.971f);




    public float Density;
    private float MinimalVolumePresent;
    private int stationID;
    private Date ExpiryDate;

    public Liquid(String name, float density, int pumpID, float MinimalVolumePresent, int stationID, float min, float max, Date ExpiryDate) {
        super(CompoundType.Liquid, name,min,max);
        this.Density = density;
        this.PumpID = pumpID;
        this.MinimalVolumePresent = MinimalVolumePresent;
        this.stationID = stationID;
        this.ExpiryDate = ExpiryDate;
    }

    public Liquid(String name, float density, int pumpID, float MinimalVolumePresent, int stationID, float min, float max) {
        this(name,density,pumpID,MinimalVolumePresent,stationID,min,max,null);

    }
    public int getPumpID() {
        return PumpID;
    }

    public float getDensity() {
        return Density;
    }

    public float getMinimalVolumePresent() {
        return MinimalVolumePresent;
    }

    @Override
    public String toString()
    {
        return Name + " (" + Density + " g/mL,"+ MinimalVolumePresent + " mL, ID="+PumpID+",stationID="+stationID+")";
    }

    public void reduceMinimalVolume(float amount) {
        MinimalVolumePresent = MinimalVolumePresent-amount;
    }

    public int getStationID() {
        return stationID;
    }


    public void setStationID(int stationID) {
        this.stationID = stationID;
    }

    public void setMinimalVolumePreset(float minimalVolumePreset, int days_till_expired) {

        this.ExpiryDate = new Date();
        this.ExpiryDate.setTime(this.ExpiryDate.getTime() + (days_till_expired * 24 * 60* 60* 1000));
        this.MinimalVolumePresent = minimalVolumePreset;
    }

    public Date getExpiryDate() {
        return ExpiryDate;
    }

    public boolean Expired() {
        return (new Date()).getTime() > ExpiryDate.getTime();
    }

    public void RemoveLiquidFromSystem() {

    }

    public void setPumpID(int i) {
        this.PumpID = i;
    }
}
