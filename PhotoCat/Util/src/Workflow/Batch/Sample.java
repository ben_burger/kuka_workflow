package Workflow.Batch;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public class Sample  implements Serializable, Cloneable {
    private int SampleIndex;
    private int SampleNumber; //Mostly used for chemist's simplicity
    private String Name;
    private boolean capped = false;
    private Integer GC_well_number;
    private Integer Hydrogen_evolution;
    private Integer Oxygen_evolution;
    private Double Hydrogen_evolution_micromol;
    private Double Oxygen_evolution_micromol;

    private Double Weighted_Hydrogen_evolution_micromol;
    private Double Internal_Hydrogen_Standard;

    private Double Sample_location_Weight;
    private Double Weighted_IS_SL_Hydrogen_evolution_micromol;

    private List<CompoundDispense> SolidDispenses;
    private List<CompoundDispense> LiquidDispenses;


    public Sample() {
    }

    public void setSolidDispenses(List<CompoundDispense> solidDispenses) {
        SolidDispenses = solidDispenses;
    }

    public void setLiquidDispenses(List<CompoundDispense> liquidDispenses) {
        LiquidDispenses = liquidDispenses;
    }

    public int getSampleIndex() {
        return SampleIndex;
    }

    public void setSampleIndex(int sampleIndex) {
        SampleIndex = sampleIndex;
        SampleNumber = sampleIndex + 1;
    }

    public int getSampleNumber() {
        return SampleNumber;
    }

    public void setSampleNumber(int sampleNumber) {
        SampleNumber = sampleNumber;
        SampleIndex = sampleNumber-1;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean getCapped() {
        return capped;
    }

    public void setCapped(boolean capped) {
        this.capped = capped;
    }

    public Integer getGC_well_number() {
        return GC_well_number;
    }

    public void setGC_well_number(int GC_well_number) {
        this.GC_well_number = GC_well_number;
    }

    public void unsetGC_well_number() {
        this.GC_well_number = null;
    }

    public Integer getHydrogen_evolution() {
        return Hydrogen_evolution;
    }

    public void calculate_weighted_hydrogen(double internal_standard, double sample_location_Weight, double internal_Hydrogen_Standard_weighted_location) {

        Internal_Hydrogen_Standard = internal_standard;
        Weighted_Hydrogen_evolution_micromol = Hydrogen_evolution_micromol/ internal_standard;
        Sample_location_Weight = sample_location_Weight;
        Weighted_IS_SL_Hydrogen_evolution_micromol = Hydrogen_evolution_micromol /(internal_Hydrogen_Standard_weighted_location*sample_location_Weight)  ;


    }

    public void setHydrogen_evolution(Integer hydrogen_evolution) {

        Hydrogen_evolution = hydrogen_evolution;
        CalculateHydrogenEvolution(hydrogen_evolution);
    }

    public void unsetHydrogen_evolution() {
        Hydrogen_evolution = null;
        Hydrogen_evolution_micromol = null;
        Weighted_Hydrogen_evolution_micromol = null;
        Internal_Hydrogen_Standard = null;
        Weighted_IS_SL_Hydrogen_evolution_micromol = null;
    }

    public void unsetSample_location_Weight() {
        Sample_location_Weight = null;
    }

    private void CalculateHydrogenEvolution(int hydrogen_evolution) {
        if(hydrogen_evolution > 1000) {
            Hydrogen_evolution_micromol = Math.max(0,9.16645d * Math.pow(10d,-12d) * hydrogen_evolution * hydrogen_evolution + 3.40111d * Math.pow(10d , -6d) * hydrogen_evolution + 1.45358d / 10.0d);
        } else
            Hydrogen_evolution_micromol = 0d;
    }

    public Integer getOxygen_evolution() {
        return Oxygen_evolution;
    }

    public void setOxygen_evolution(Integer oxygen_evolution)
    {
        Oxygen_evolution = oxygen_evolution;
        CalculateOxygenEvolution(oxygen_evolution);
    }

    public void unsetOxygen_evolution() {
        Oxygen_evolution = null;
        Oxygen_evolution_micromol = null;
    }

    private void CalculateOxygenEvolution(int oxygen_evolution) {
        if(oxygen_evolution > 100000)
            Oxygen_evolution_micromol = Math.max(0,5.70607*Math.pow(10d,-6d)*oxygen_evolution - 9.36256d/10d);
        else
            Oxygen_evolution_micromol = 0d;
    }

    public Float getSolidDispenseAmount(Solid solid)
    {
        Optional<CompoundDispense> solid_dispense = SolidDispenses.stream().filter(x -> ((Solid) x.getCompound()).getName().equals(solid.getName())).findFirst();

        if(!solid_dispense.isPresent())
            return 0f;

        return solid_dispense.get().getAmount_to_dispense();
    }
    public Float getLiquidDispenseAmount(Liquid liquid)
    {
        Optional<CompoundDispense>  liquid_dispense = LiquidDispenses.stream().filter(x -> ((Liquid) x.getCompound()).getName().equals(liquid.Name)).findFirst();

        if(!liquid_dispense.isPresent())
            return 0f;

        return liquid_dispense.get().getAmount_to_dispense();
    }

    public void SetSolidDispensedAmount(Solid solid, Float amount)
    {
        CompoundDispense solid_dispense = SolidDispenses.stream().filter(x -> ((Solid) x.getCompound()).getName().equals(solid.getName())).findFirst().get();
        solid_dispense.setAmount_dispensed(amount);
    }

    public void SetLiquidDispensedAmount(Liquid liquid, Float amount)
    {
        CompoundDispense  liquid_dispense = LiquidDispenses.stream().filter(x -> ((Liquid) x.getCompound()).getName().equals(liquid.Name)).findFirst().get();
        liquid_dispense.setAmount_dispensed(amount);
    }

    public Float getSolidDispensed(Solid solid)
    {
        Optional<CompoundDispense> solid_dispense = SolidDispenses.stream().filter(x -> ((Solid) x.getCompound()).getName().equals(solid.getName())).findFirst();

        if(!solid_dispense.isPresent())
            return 0f;

        return solid_dispense.get().getAmount_dispensed();
    }
    public Float getLiquidDispensed(Liquid liquid)
    {
        Optional<CompoundDispense>  liquid_dispense = LiquidDispenses.stream().filter(x -> ((Liquid) x.getCompound()).getName().equals(liquid.Name)).findFirst();

        if(!liquid_dispense.isPresent())
            return 0f;

        return liquid_dispense.get().getAmount_dispensed();
    }

    public void addLiquidDispensed(Liquid liquid, float amount) {
        CompoundDispense  liquid_dispense = LiquidDispenses.stream().filter(x -> ((Liquid) x.getCompound()).getName().equals(liquid.Name)).findFirst().get();
        if(liquid_dispense == null)
            throw new NotImplementedException();

        Float already_dispensed = liquid_dispense.getAmount_dispensed();
        if(already_dispensed == null)
            already_dispensed = 0f;
        liquid_dispense.setAmount_dispensed(already_dispensed + amount);

    }

    public void addSolidDispensed(Solid solid, float amount) {
        CompoundDispense  solid_dispense = SolidDispenses.stream().filter(x -> ((Solid) x.getCompound()).getName().equals(solid.Name)).findFirst().get();
        if(solid_dispense == null)
            throw new NotImplementedException();

        Float already_dispensed = solid_dispense.getAmount_dispensed();
        if(already_dispensed == null)
            already_dispensed = 0f;
        solid_dispense.setAmount_dispensed(already_dispensed + amount);
    }


    public int getRackIndex() {
        if(SampleIndex < 8)
            return SampleIndex;
        else {
            return SampleIndex+2;
        }
    }

    public boolean hasLiquidsToDispense() {
        for(CompoundDispense liquid : LiquidDispenses)
            if(liquid.getAmount_to_dispense() - liquid.getAmount_dispensed() > 0.1f)
                return true;
        return false;
    }

    public Double getOxygen_evolution_micromol() {
        return Oxygen_evolution_micromol;
    }

    public Double getHydrogen_evolution_micromol() {
        return Hydrogen_evolution_micromol;
    }

    public boolean hasGCResults() {
        return Hydrogen_evolution_micromol != null;
    }

    public Double getInternalHydrogenStandard() {
        return Internal_Hydrogen_Standard;
    }

    public Double getWeightedHydrogenMicromol() {
        return Weighted_Hydrogen_evolution_micromol;
    }

    public void SetWeightedHydrogenStandard(double weighted_hydrogen_standard) {
        Weighted_Hydrogen_evolution_micromol = weighted_hydrogen_standard;
    }

    public void SetInternalHydrogenStandard(double internal_hydrogen_standard) {
        Internal_Hydrogen_Standard= internal_hydrogen_standard;
    }

    public Double getSampleLocationWeight() {
        return Sample_location_Weight;
    }

    public Double getWeightedISSLHydrogenMicromol() {
        return Weighted_IS_SL_Hydrogen_evolution_micromol;
    }

    public void setSampleLocationWeight(double sample_location_weight) {
        Sample_location_Weight = sample_location_weight;
    }

    public void setWeightedISSLHydrogenEvolution(double weighted_IS_SL_Hydrogen_evolution_micromol) {
        Weighted_IS_SL_Hydrogen_evolution_micromol = weighted_IS_SL_Hydrogen_evolution_micromol;
    }

    public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }
}
