package Workflow.Batch;

/**
 * Created by Benjamin on 9/6/2018.
 */
public enum Progress {
    Waiting, Running;
}
