package Workflow.Batch;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Benjamin on 9/6/2018.
 */
public enum BatchStatus {
    NoRackAssigned(0,"NoRackAssigned","NR"),
    PickUp(5,"PickUp","PU"),
    LoadQuantos(10,"LoadQuantos","LQ"),
    DispenseSolid(20,"DispenseSolid","DS"),
    CartridgeChangeQuantos(30,"CartridgeChangeQuantos","CC"),
    UnloadQuantos(50,"UnloadQuantos","UQ"),
    DispenseLiquid(60,"DispenseLiquid","DL"),
    LoadSonicator(80,"LoadSonicator","LS"),
    Sonicate(90,"Sonicate","SO"),
    UnLoadSonicator(100,"UnLoadSonicator","US"),
    LoadPhotocat(110,"LoadPhotocat","LP"),
    StartPhotolysis(115, "StartPhotolysis" ,"SP"),
    Photolysis(120,"Photolysis","PH"),
    UnloadPhotocat(130,"UnloadPhotocat","UP"),
    LoadGC(140, "LoadGC","LG"),
    GCAnalysis(150,"GCAnalysis","GC"),
    UnloadGC(160,"UnloadGC","UG"),
    MoveToStorage(180,"MoveToStorage","MS"),
    DONE(200,"DONE","DO");


    private final String Name;
    private final int Step;
    private String short_name;

    BatchStatus(int step, String name, String short_name) {
        this.Step = step;
        this.Name = name;
        this.short_name = short_name;
    }

    public static BatchStatus fromInt(int i) {
        Optional<BatchStatus> batch = Arrays.stream(BatchStatus.values()).filter(x -> x.Step == i).findFirst();
        if(batch.isPresent())
            return batch.get();

        throw new NotImplementedException();
    }

    public String getName()
    {
        return Name;
    }

    public int getStep() {
        return Step;
    }

    public String getShortName() {
        return short_name;
    }
}
