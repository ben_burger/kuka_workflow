package Workflow.Batch;

import Workflow.Exception.CompoundNotPResentException;
import Workflow.Exception.InconsistentStateException;
import javafx.util.Pair;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.swing.*;
import java.io.*;
import java.rmi.UnexpectedException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Batch implements Serializable, Comparable<Batch> {

    private Boolean BatchLock = false;
    private Boolean SavedToFile = true;

    private File batch_file;
    private String rack_identifier = "";
    private BatchStatus status = BatchStatus.NoRackAssigned;
    private BatchLocation Location = BatchLocation.NoRackAssigned;
    private Progress progress = Progress.Waiting;

    private Sample[] Samples;
    private String batch_name = "";
    private Solid[] Solids;
    private Liquid[] Liquids;

    private int sonication_time_min = 10;
    private int illumination_time_min = 120;
    private String gc_method = "";
    private int purge_time_sec = 60;
    private Boolean cap_vials = true;
    private Date[] solid_dispensing_finish_date_time;

    private Double Temperature_Photolysis_start;
    private Double Temperature_Photolysis_end;

    private HashMap<Pair<BatchTimestampEnum, BatchTimestampType>, Date> time_stamps = new HashMap();
    private String comment;

    public Double getTemperature_Photolysis_start() {
        synchronized (BatchLock) {
            return Temperature_Photolysis_start;
        }
    }

    public void setTemperature_Photolysis_start(Double temperature_Photolysis_start) {
        synchronized (BatchLock) {
            Temperature_Photolysis_start = temperature_Photolysis_start;
        }
    }

    public Double getTemperature_Photolysis_end() {
        synchronized (BatchLock) {
            return Temperature_Photolysis_end;
        }
    }

    public void setTemperature_Photolysis_end(Double temperature_Photolysis_end) {
        synchronized (BatchLock) {
            Temperature_Photolysis_end = temperature_Photolysis_end;
        }
    }

    public Progress getProgress() {
        synchronized (BatchLock) {
            return progress;
        }
    }

    public void removeSample(int sampleIdx) throws IOException {
        synchronized (BatchLock) {
            List<Sample> tempSamples = new ArrayList<Sample>(Arrays.asList(Samples));
            Optional<Sample> sample_option = Arrays.stream(Samples).filter(x -> x.getSampleIndex() == sampleIdx).findFirst();
            if (sample_option.isPresent()) {
                tempSamples.remove(sample_option.get());
                Samples = tempSamples.toArray(new Sample[tempSamples.size()]);
                AppendComment(String.format("Vial manually removed by operator, sample %s", sample_option.get().getName()));
                WriteToFile();
            }
            else {
                throw new UnexpectedException("Sample Not Found");
            }
        }
    }

    public void resubmitBatch() throws IOException, ParseException, CompoundNotPResentException {
        synchronized (BatchLock) {
            Batch newBatch = Batch.ReadFromFile(getSolids(), getLiquids(), getBatch_file());
            Integer runNumber = 0;
            String newFileString = newBatch.getBatch_name();
            Integer tempRunNumber = 0;

            String newPostfix = "-run";
            runNumber = checkRunNumberAllDirectories(runNumber, newFileString, newPostfix);
            if (newFileString.contains(newPostfix)) {
                String afterPostFix = newFileString.substring(newFileString.lastIndexOf(newPostfix) + newPostfix.length());
                if ( afterPostFix.matches("\\d+") ) {
                    tempRunNumber = Integer.parseInt(afterPostFix);
                    newFileString = newFileString.substring(0, newFileString.length() - String.valueOf(tempRunNumber).length() - newPostfix.length());
                }
            }

            newPostfix += runNumber+1;
            newBatch.batch_file = new File("runqueue\\" + newFileString + newPostfix + ".run");
            newBatch.batch_name = newFileString+newPostfix;
            newBatch.solid_dispensing_finish_date_time = new Date[Solids.length];
            newBatch.time_stamps = new HashMap<>();
            newBatch.setTimestamp(BatchTimestampEnum.submit, BatchTimestampType.start, new Date());
            newBatch.setLocation(BatchLocation.NoRackAssigned);
            newBatch.setProgress(Progress.Waiting);
            newBatch.setStatus(BatchStatus.NoRackAssigned);
            newBatch.rack_identifier = null;
            newBatch.Temperature_Photolysis_start = null;
            newBatch.Temperature_Photolysis_end = null;

            for ( Sample sample : newBatch.Samples ) {
                for ( Solid solid: newBatch.Solids ) {
                    sample.SetSolidDispensedAmount(solid, 0f);
                }
                for ( Liquid liquid: newBatch.Liquids ) {
                    sample.SetLiquidDispensedAmount(liquid, 0f);
                }
                sample.setCapped(false);
                sample.setName(newBatch.getBatch_name()+"_"+sample.getSampleNumber());
                sample.unsetGC_well_number();
                sample.unsetHydrogen_evolution();
                sample.unsetOxygen_evolution();
                sample.unsetSample_location_Weight();
            }
            newBatch.WriteToFile();
        }
    }

    private Integer checkRunNumber(String directoryName, Integer runNumber, String newFileString, String extension) {
        Integer tempRunNumber = 0;
        File dir = new File(directoryName+"\\.");
        String fileExtension = ".run";
        File[] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(fileExtension);
            }
        });
        for (File batchfile : files) {
            String stripExtension = batchfile.getName().substring(0,batchfile.getName().length()-fileExtension.length());
            if (stripExtension.contains(extension) ) {
                String stripExtensionAndPostFix = stripExtension.substring(0, stripExtension.lastIndexOf(extension));
                if ( stripExtensionAndPostFix.equals(newFileString)
                        || ( newFileString.contains(extension) && stripExtensionAndPostFix
                        .equals(newFileString.substring(0, newFileString.lastIndexOf(extension)))) ) {
                    String potentialRunNumber = stripExtension.substring(stripExtension.lastIndexOf(extension) + extension.length());
                    if (potentialRunNumber.matches("\\d+")) {
                        tempRunNumber = Integer.parseInt(potentialRunNumber);
                        if (tempRunNumber > runNumber) {
                            runNumber = tempRunNumber;
                        }
                    }

                }
            }
        }
        return runNumber;
    }

    private Integer checkRunNumberAllDirectories(Integer runNumber, String newFileString, String extension) {
        runNumber = checkRunNumber("running", runNumber, newFileString, extension);
        runNumber = checkRunNumber("runqueue", runNumber, newFileString, extension);
        runNumber = checkRunNumber("completed", runNumber, newFileString, extension);
        return runNumber;
    }

    public void editDispensed(int sampleIdx, Compound solidOrLiquid, Float dispensedAmount) throws IOException {
        synchronized (BatchLock) {
            Optional<Sample> sample_option = Arrays.stream(Samples).filter(x -> x.getSampleIndex() == sampleIdx).findFirst();
            if (sample_option.isPresent()) {
                if (solidOrLiquid.getClass() == Solid.class) {
                    sample_option.get().SetSolidDispensedAmount((Solid) solidOrLiquid, dispensedAmount); // solids
                } else if (solidOrLiquid.getClass() == Liquid.class) {
                    sample_option.get().SetLiquidDispensedAmount((Liquid) solidOrLiquid, dispensedAmount); // liquids
                } else {
                    throw new UnexpectedException("Dispensed Type Incorrect");
                }
                AppendComment(String.format("Dispensed amount changed for sample %s and compound %s to %.2f", sample_option.get().getName(), solidOrLiquid.getName(), dispensedAmount));
                WriteToFile();
            }
            else {
                throw new UnexpectedException("Sample Not Found");
            }
        }
    }
    private Batch() {
        this(0);
        SavedToFile = true;

    }
    public Batch(int samples) {
        Solids = new Solid[0];
        Liquids = new Liquid[0];
        Samples = new Sample[samples];
        SavedToFile = false;
    }

    public void GenerateGCInputFile(String inputFolder) throws IOException {

        if (!batch_name.contains("FreshRack")) {
            File new_file = new File(inputFolder + batch_name + "-beforeGC.run");

            if (new_file.exists()) {
                new_file.delete();
            }
            new_file.createNewFile();
            {
                PrintWriter writer = new PrintWriter(new_file);
                DateFormat df2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

                writer.write("batch_name:" + batch_name + "\r\n");
                writer.write("sonication_time_min:10\r\n" +
                        "illumination_time_min:120\r\n" +
                        "gc_method:constant-HS rc\r\n" +
                        "purge_time:60\r\n" +
                        "run_start_date_time:2019.02.26.14.23.10\r\n" +
                        "solid_dispensing_start_date_time:2019.02.26.18.59.23\r\n" +
                        "liquid_dispensing_start_date_time:1970.01.01.01.00.00\r\n" +
                        "capper_start_date_time:1970.01.01.01.00.00\r\n" +
                        "sonicator_start_date_time:2019.02.28.12.37.33\r\n" +
                        "capper_start_date_time:1970.01.01.01.00.00\r\n" +
                        "photocat_start_date_time:2019.02.28.16.45.05\r\n" +
                        "gc_submit_job_date_time:2019.03.01.08.38.05\r\n" +
                        "gc_job_finish_date_time:\r\n");

                writer.write("\r\n");

                writer.write("SampleIndex,SampleNumber,Name,gc_well_number,hydrogen_evolution,oxygen_evolution");
                writer.write("\r\n");

                for (int i = 0; i < Samples.length; i++) {
                    Sample sample = Samples[i];
                    writer.write("" + sample.getSampleIndex());
                    writer.write(',');

                    writer.write("" + sample.getSampleNumber());
                    writer.write(',');

                    writer.write("" + sample.getName());
                    writer.write(',');

                    Integer GC_well = sample.getGC_well_number();
                    writer.write(GC_well != null ? GC_well.toString() : "");
                    writer.write(',');

                    Integer Hydrogen_evolutioon = sample.getHydrogen_evolution();
                    writer.write(Hydrogen_evolutioon != null ? Hydrogen_evolutioon.toString() : "");
                    writer.write(',');

                    Integer Oxygen_evolution = sample.getOxygen_evolution();
                    writer.write(Oxygen_evolution != null ? Oxygen_evolution.toString() : "");

                    writer.write("\r\n");
                }
                writer.flush();
                writer.close();
            }
        }
    }

    public boolean hasAllGCResults() {
        for (Sample sample : Samples)
            if (sample.getHydrogen_evolution() != null && sample.getOxygen_evolution() != null)
                return true;
        return false;
    }

    public File getBatch_file() {
        return batch_file;
    }

    public Date getSolid_dispensing_finish_date_time(int i) {
        return this.solid_dispensing_finish_date_time[i];
    }

    public void setSolid_dispensing_finish_date_time(int i, Date date) throws IOException {
        synchronized (BatchLock) {
            this.solid_dispensing_finish_date_time[i] = date;
            WriteToFile();
        }
    }

    public boolean isFreshRack() {
        return batch_name.contains("FreshRack");
    }

    public void setComment(String comment) throws IOException {
        synchronized (BatchLock) {
            this.comment = comment;
            WriteToFile();
        }
    }

    public String getComment() {
        return comment;
    }

    public void AppendComment(String message) throws IOException {
        synchronized (BatchLock) {
            if(comment.equals("") || comment.equals(null))
                this.comment = message;
            else
                this.comment += "\r\n" + message;
            WriteToFile();
        }

    }

    public void StopSavingToDisk() {
        SavedToFile = false;
    }


    enum SampleColumns {SampleIndex, SampleNumber, Name, gc_well_number, hydrogen_evolution, oxygen_evolution, vial_capped, compound, ErrorPeaks, hydrogen_evolution_micromol, oxygen_evolution_micromol, internal_hydrogen_standard_micromol, weighted_hydrogen_micromol, sample_location_weight, weighted_is_sl_hydrogen_evolution_micromol, compounddispense}

    public static Batch ReadFromFile(Solid[] solids_in_workflow, Liquid[] liquids_in_workflow, File file) throws IOException, ParseException, CompoundNotPResentException {
        Batch batch = new Batch();
        batch.batch_file = file;
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        fileReader = new FileReader(file);
        bufferedReader = new BufferedReader(fileReader);

        String line;

        while ((line = bufferedReader.readLine()) != null && !line.equals("")) {
            String[] split = line.split(" |:");
            String field = split[0];
            SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

            if (split.length > 1)
                if (field.equals("batch_name")) {
                    String batch_name = split[1];
                    batch.batch_name = batch_name;
                } else if (field.equals("status")) {
                    int status = Integer.parseInt(split[1]);
                    batch.status = BatchStatus.fromInt(Integer.parseInt(split[1]));
                } else if (field.equals("sonication_time_min")) {
                    int sonication_time_min = Integer.parseInt(split[1]);
                    batch.sonication_time_min = sonication_time_min;
                } else if (field.equals("illumination_time_min")) {
                    int illumination_time_min = Integer.parseInt(split[1]);
                    batch.illumination_time_min = (illumination_time_min);
                } else if (field.equals("gc_method")) {
                    String gc_method = line.substring(10);
                    batch.gc_method = (gc_method);
                } else if (field.equals("purge_time")) {
                    int purge_time_sec = Integer.parseInt(split[1]);
                    batch.purge_time_sec = purge_time_sec;
                } else if (field.equals("cap_vials")) {
                    boolean cap_vials = split[1].equals("1");
                    batch.cap_vials = cap_vials;
                } else if (field.equals("rack_identifier")) {
                    batch.rack_identifier = (split[1]);
                } else if (field.equals("Temperature_Photolysis_start")) {
                    batch.Temperature_Photolysis_start = Double.parseDouble(split[1]);
                } else if (field.equals("Temperature_Photolysis_end")) {
                    batch.Temperature_Photolysis_end = Double.parseDouble(split[1]);
                } else if (field.equals("solid_dispensing_finish_date_time")) {
                    batch.solid_dispensing_finish_date_time = Arrays.stream(split[1].split(",", -1)).map(x -> {
                        try {
                            return format.parse(x);
                        } catch (ParseException e) {
                            return null;
                        }
                    }).toArray(Date[]::new);
                } else {

                    throw new UnexpectedException("error in run.csv");
                }

        }

        boolean timestamps_were_present = false;
        line = bufferedReader.readLine();

        while (line != null && line.equals(""))
            line = bufferedReader.readLine();


        while (line != null && (!line.contains("SampleNumber")) && (!line.equals(""))) {
            String[] split = line.split(" |:");
            String field = split[0];
            SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

            String timetamp_enum;
            BatchTimestampType timestamp_type;

            if (field.contains("start")) {
                timetamp_enum = field.substring(0, field.indexOf("start") - 1);
                timestamp_type = BatchTimestampType.start;
            } else if (field.contains("finish")) {
                timetamp_enum = field.substring(0, field.indexOf("finish") - 1);
                timestamp_type = BatchTimestampType.finish;
            } else {
                throw new UnexpectedException("error in run.csv");
            }
            batch.time_stamps.put(new Pair(BatchTimestampEnum.valueOf(timetamp_enum), timestamp_type), format.parse(split[1]));

            timestamps_were_present = true;
            line = bufferedReader.readLine();
        }


        List<Solid> solids_used = new LinkedList();
        List<Liquid> liquids_used = new LinkedList();
        List<Compound> compounds = new LinkedList();
        List<SampleColumns> columns = new LinkedList();
        String[] columnNamesSplit;
        //Column stuff
        {
            //SampleIndex,SampleNumber,Name,vial_capped,gc_well_number,hydrogen_evolution,oxygen_evolution,
            if (timestamps_were_present)
                line = bufferedReader.readLine();
            while (line.equals(""))
                line = bufferedReader.readLine();
            columnNamesSplit = line.split(",");
            for (int column = 0; column < columnNamesSplit.length; column++) {
                String columnname = columnNamesSplit[column];

                switch (columnname) {
                    case "SampleIndex":
                        columns.add(SampleColumns.SampleIndex);
                        break;
                    case "SampleNumber":
                        columns.add(SampleColumns.SampleNumber);
                        break;
                    case "Name":
                        columns.add(SampleColumns.Name);
                        break;
                    case "gc_well_number":
                        columns.add(SampleColumns.gc_well_number);
                        break;
                    case "hydrogen_evolution":
                        columns.add(SampleColumns.hydrogen_evolution);
                        break;
                    case "oxygen_evolution":
                        columns.add(SampleColumns.oxygen_evolution);
                        break;
                    case "hydrogen_evolution_micromol":
                        columns.add(SampleColumns.hydrogen_evolution_micromol);
                        break;
                    case "oxygen_evolution_micromol":
                        columns.add(SampleColumns.oxygen_evolution_micromol);
                        break;
                    case "vial_capped":
                        columns.add(SampleColumns.vial_capped);
                        break;
                    case "internal_hydrogen_standard_micromol":
                        columns.add(SampleColumns.internal_hydrogen_standard_micromol);
                        break;
                    case "weighted_hydrogen_micromol":
                        columns.add(SampleColumns.weighted_hydrogen_micromol);
                        break;
                    case "sample_location_weight":
                        columns.add(SampleColumns.sample_location_weight);
                        break;
                    case "weighted_is_sl_hydrogen_evolution_micromol":
                        columns.add(SampleColumns.weighted_is_sl_hydrogen_evolution_micromol);
                        break;
                    default:
                        if (!columnname.contains("dispensed")) {
                            columns.add(SampleColumns.compound);
                            Optional<Solid> sol = Arrays.stream(solids_in_workflow).filter(x -> x.getName().equals(columnname)).findFirst();
                            Optional<Liquid> liq = Arrays.stream(liquids_in_workflow).filter(x -> x.Name.equals(columnname)).findFirst();

                            if (sol.isPresent()) {
                                solids_used.add(sol.get());
                                compounds.add(sol.get());
                            } else if (liq.isPresent()) {
                                liquids_used.add(liq.get());
                                compounds.add(liq.get());
                            } else {
                                throw new CompoundNotPResentException(columnname, batch.batch_name);
                            }
                        } else {
                            columns.add(SampleColumns.compounddispense);
                        }
                        break;
                }
            }
        }
        Date[] solid_timestamps = batch.solid_dispensing_finish_date_time;
        batch.setSolids((Solid[]) solids_used.toArray(new Solid[0]));
        batch.setLiquids((Liquid[]) liquids_used.toArray(new Liquid[0]));
        if (solid_timestamps != null)
            batch.solid_dispensing_finish_date_time = solid_timestamps;


        Compound[] column_compounds = compounds.toArray(new Compound[0]);
        List<Sample> samples = new LinkedList();
        int numbersamples = 0;
        while ((line = bufferedReader.readLine()) != null && !line.equals("")) {
            Sample sample = new Sample();
            sample.setSampleIndex(numbersamples);
            sample.setName(batch.getBatch_name() + "_" + sample.getSampleNumber());
            numbersamples++;

            String[] split = line.split(",");

            List<CompoundDispense> sampleSolidDispenses = new LinkedList();
            List<CompoundDispense> sampleLiquidDispenses = new LinkedList();

            for (int column = 0; column < split.length; column++) {
                try {
                    switch (columns.get(column)) {
                        case SampleIndex:
                            sample.setSampleIndex(Integer.parseInt(split[column]));
                            break;
                        case SampleNumber:
                            sample.setSampleNumber(Integer.parseInt(split[column]));
                            break;
                        case Name:
                            sample.setName((split[column]));
                            break;
                        case gc_well_number:
                            sample.setGC_well_number(Integer.parseInt(split[column]));
                            break;
                        case hydrogen_evolution:
                            sample.setHydrogen_evolution(Integer.parseInt(split[column]));
                            break;
                        case oxygen_evolution:
                            sample.setOxygen_evolution(Integer.parseInt(split[column]));
                            break;
                        case hydrogen_evolution_micromol:
                        case oxygen_evolution_micromol:
                            break;
                        case vial_capped:
                            sample.setCapped(split[column].equals("1"));
                            break;
                        case compound: {
                            final int temp_column = column;
                            Compound com = Arrays.stream(column_compounds)
                                    .filter(x -> x.getName().equals(columnNamesSplit[temp_column]))
                                    .findFirst().get();
                            Float amount = Float.parseFloat(split[column]);

                            switch (com.getType()) {
                                case Solid:
                                    sampleSolidDispenses.add(new CompoundDispense(com, amount));
                                    break;

                                case Liquid:
                                    sampleLiquidDispenses.add(new CompoundDispense(com, amount));
                                    break;

                                default:
                                    throw new CompoundNotPResentException(columnNamesSplit[temp_column], batch.batch_name);

                            }
                            break;
                        }
                        case internal_hydrogen_standard_micromol:
                            sample.SetInternalHydrogenStandard(Double.parseDouble(split[column]));
                            break;
                        case weighted_hydrogen_micromol:
                            sample.SetWeightedHydrogenStandard(Double.parseDouble(split[column]));
                            break;
                        case sample_location_weight:
                            sample.setSampleLocationWeight(Double.parseDouble(split[column]));
                            break;
                        case weighted_is_sl_hydrogen_evolution_micromol:
                            sample.setWeightedISSLHydrogenEvolution(Double.parseDouble(split[column]));
                            break;
                        case compounddispense: {
                            final int temp_column = column;
                            final String columnName = columnNamesSplit[temp_column].substring(0, columnNamesSplit[temp_column].length() - 10);
                            Float amount = Float.parseFloat(split[column]);
                            Optional<Compound> com = Arrays.stream(column_compounds).filter(x -> x.getName().equals(columnName)).findFirst();
                            switch (com.get().getType()) {
                                case Solid:
                                    sampleSolidDispenses.stream().filter(x -> x.getCompound().getName().equals(columnName)).findFirst().get().setAmount_dispensed(amount);
                                    break;

                                case Liquid:
                                    sampleLiquidDispenses.stream().filter(x -> x.getCompound().getName().equals(columnName)).findFirst().get().setAmount_dispensed(amount);
                                    break;

                                default:
                                    throw new CompoundNotPResentException(columnName, batch.batch_name);

                            }
                            break;
                        }
                    }
                } catch (NumberFormatException e) {
                }

            }
            sample.setSolidDispenses(sampleSolidDispenses);
            sample.setLiquidDispenses(sampleLiquidDispenses);

            samples.add(sample);


        }

        batch.setSamples(samples.toArray(new Sample[0]));
        StringBuilder comment_sb = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            if (!line.equals(""))
                comment_sb.append(line);
            comment_sb.append("\r\n");
        }

        batch.comment = comment_sb.toString();


        try {
            fileReader.close();
            fileReader = null;
            bufferedReader.close();
            fileReader = null;
        } catch (Exception e) {

        }
        return batch;
    }

    public static Batch ReadFromGCFile(Solid[] solids_in_workflow, Liquid[] liquids_in_workflow, File file) throws IOException, ParseException {
        Batch batch = new Batch();
        batch.batch_file = file;

        FileReader fileReader = null;
        BufferedReader bufferedReader = null;

        try {
            String line;
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null && !line.equals("")) {
                String[] split = line.split(" |:");
                String field = split[0];
                SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

                if (split.length > 1)
                    if (field.equals("batch_name")) {
                        String batch_name = split[1];
                        batch.batch_name = batch_name;
                    } else if (field.equals("status")) {
                        int status = Integer.parseInt(split[1]);
                        batch.status = BatchStatus.fromInt(Integer.parseInt(split[1]));
                    } else if (field.equals("sonication_time_min")) {
                        int sonication_time_min = Integer.parseInt(split[1]);
                        batch.sonication_time_min = sonication_time_min;
                    } else if (field.equals("illumination_time_min")) {
                        int illumination_time_min = Integer.parseInt(split[1]);
                        batch.illumination_time_min = (illumination_time_min);
                    } else if (field.equals("gc_method")) {
                        String gc_method = line.substring(10);
                        batch.gc_method = (gc_method);
                    } else if (field.equals("purge_time")) {
                        int purge_time_sec = Integer.parseInt(split[1]);
                        batch.purge_time_sec = purge_time_sec;
                    } else if (field.equals("cap_vials")) {
                        boolean cap_vials = split[1].equals("1");
                        batch.cap_vials = cap_vials;

                    } else if (field.equals("run_start_date_time")) {
                    } else if (field.equals("solid_dispensing_start_date_time")) {
                    } else if (field.equals("solid_dispensing_finish_date_time")) {
                    } else if (field.equals("liquid_dispensing_start_date_time")) {
                    } else if (field.equals("liquid_dispensing_finish_date_time")) {
                    } else if (field.equals("capper_start_date_time")) {
                    } else if (field.equals("capper_finish_date_time")) {
                    } else if (field.equals("sonicator_start_date_time")) {
                    } else if (field.equals("sonicator_finish_date_time")) {
                    } else if (field.equals("photocat_start_date_time")) {
                    } else if (field.equals("photocat_finish_date_time")) {
                    } else if (field.equals("gc_submit_job_date_time")) {
                    } else if (field.equals("gc_job_finish_date_time")) {
                    } else if (field.equals("rack_identifier")) {
                        batch.rack_identifier = (split[1]);
                        break;
                    } else {
                        throw new UnexpectedException("error in run.csv");
                    }

            }


            List<Solid> solids_used = new LinkedList();
            List<Liquid> liquids_used = new LinkedList();
            List<Compound> compounds = new LinkedList();
            List<SampleColumns> columns = new LinkedList();
            String[] columnNamesSplit;
            //Column stuff
            {
                //SampleIndex,SampleNumber,Name,vial_capped,gc_well_number,hydrogen_evolution,oxygen_evolution,
                line = bufferedReader.readLine();
                while (line.equals(""))
                    line = bufferedReader.readLine();
                columnNamesSplit = line.split(",");
                for (int column = 0; column < columnNamesSplit.length; column++) {
                    String columnname = columnNamesSplit[column];

                    switch (columnname) {
                        case "SampleIndex":
                            columns.add(SampleColumns.SampleIndex);
                            break;
                        case "SampleNumber":
                            columns.add(SampleColumns.SampleNumber);
                            break;
                        case "Name":
                            columns.add(SampleColumns.Name);
                            break;
                        case "gc_well_number":
                            columns.add(SampleColumns.gc_well_number);
                            break;
                        case "hydrogen_evolution":
                            columns.add(SampleColumns.hydrogen_evolution);
                            break;
                        case "oxygen_evolution":
                            columns.add(SampleColumns.oxygen_evolution);
                            break;
                        case "vial_capped":
                            columns.add(SampleColumns.vial_capped);
                            break;
                        default:
                            if (!columnname.contains("dispensed")) {
                                columns.add(SampleColumns.compound);
                                Optional<Solid> sol = Arrays.stream(solids_in_workflow).filter(x -> x.getName().equals(columnname)).findFirst();
                                Optional<Liquid> liq = Arrays.stream(liquids_in_workflow).filter(x -> x.Name.equals(columnname)).findFirst();

                                if (sol.isPresent()) {
                                    solids_used.add(sol.get());
                                    compounds.add(sol.get());
                                } else if (liq.isPresent()) {
                                    liquids_used.add(liq.get());
                                    compounds.add(liq.get());
                                }
                            } else {
                                columns.add(SampleColumns.compounddispense);
                            }
                            break;
                    }
                }
            }
            columns.add(SampleColumns.ErrorPeaks);
            batch.setSolids((Solid[]) solids_used.toArray(new Solid[0]));
            batch.setLiquids((Liquid[]) liquids_used.toArray(new Liquid[0]));

            //SampleIndex,SampleNumber,Name,gc_well_number,hydrogen_evolution,oxygen_evolution,Water,Sand,Sand2,Water_dispensed,Sand_dispensed,Sand2_dispensed


            Compound[] column_compounds = compounds.toArray(new Compound[0]);
            List<Sample> samples = new LinkedList();
            int numbersamples = 0;
            while ((line = bufferedReader.readLine()) != null && !line.equals("")) {
                Sample sample = new Sample();
                sample.setSampleIndex(numbersamples);
                sample.setName(batch.getBatch_name() + "_" + sample.getSampleNumber());
                numbersamples++;

                String[] split = line.split(",", -1);

                List<CompoundDispense> sampleSolidDispenses = new LinkedList();
                List<CompoundDispense> sampleLiquidDispenses = new LinkedList();

                for (int column = 0; column < columns.size(); column++) {
                    try {
                        switch (columns.get(column)) {
                            case SampleIndex:
                                sample.setSampleIndex(Integer.parseInt(split[column]));
                                break;
                            case SampleNumber:
                                sample.setSampleNumber(Integer.parseInt(split[column]));
                                break;
                            case Name:
                                sample.setName((split[column]));
                                break;
                            case gc_well_number:
                                sample.setGC_well_number(Integer.parseInt(split[column]));
                                break;
                            case hydrogen_evolution:
                                sample.setHydrogen_evolution(Integer.parseInt(split[column]));
                                break;
                            case oxygen_evolution: {
                                Integer oxygen_evolution = Integer.parseInt(split[column]);
                                sample.setOxygen_evolution(Integer.parseInt(split[column]));

                            }
                            break;
                            case ErrorPeaks: {
                                LinkedList<Double> numbers = new LinkedList<>();
                                for (int column_peak = column; column_peak < split.length; column_peak++) {
                                    String the_stuff = split[column_peak];
                                    String[] the_stuff_split = the_stuff.split("[\\)\\-\\(]");
                                    for (String one_stuff : the_stuff_split) {
                                        try {
                                            Double result = Double.parseDouble(one_stuff);
                                            if (result != null) {
                                                numbers.add(result);

                                            }
                                        } catch (NumberFormatException e)
                                        {}
                                    }
                                }
                                Integer hydrogen = null;
                                Integer oxygen = null;
                                Iterator<Double> it = numbers.iterator();
                                while (it.hasNext()) {
                                    double rt = it.next();
                                    double area = it.next();

                                    if (rt < 3) {
                                        if(hydrogen == null)
                                            hydrogen = (int) area;
                                        else
                                            hydrogen += (int) area;
                                    } else {
                                        if(oxygen==null)
                                            oxygen = (int) area;
                                        else
                                            oxygen += (int) area;

                                    }
                                }
                                if(hydrogen != null)
                                    sample.setHydrogen_evolution(hydrogen);
                                if(oxygen != null)
                                    sample.setOxygen_evolution(oxygen);
                            }
                            column = split.length;
                            break;
                            case vial_capped:
                                sample.setCapped(split[column].equals("1"));
                                break;
                            case compound: {
                                final int temp_column = column;
                                Compound com = Arrays.stream(column_compounds)
                                        .filter(x -> x.getName().equals(columnNamesSplit[temp_column]))
                                        .findFirst().get();
                                Float amount = Float.parseFloat(split[column]);

                                switch (com.getType()) {
                                    case Solid:
                                        sampleSolidDispenses.add(new CompoundDispense(com, amount));
                                        break;

                                    case Liquid:
                                        sampleLiquidDispenses.add(new CompoundDispense(com, amount));
                                        break;

                                    default:
                                        throw new NotImplementedException();

                                }
                                break;
                            }
                            case compounddispense: {
                                final int temp_column = column;
                                final String columnName = columnNamesSplit[temp_column].substring(0, columnNamesSplit[temp_column].length() - 10);
                                Float amount = Float.parseFloat(split[column]);
                                Compound com = Arrays.stream(column_compounds).filter(x -> x.getName().equals(columnName)).findFirst().get();
                                switch (com.getType()) {
                                    case Solid:
                                        sampleSolidDispenses.stream().filter(x -> x.getCompound().getName().equals(columnName)).findFirst().get().setAmount_dispensed(amount);
                                        break;

                                    case Liquid:
                                        sampleLiquidDispenses.stream().filter(x -> x.getCompound().getName().equals(columnName)).findFirst().get().setAmount_dispensed(amount);
                                        break;

                                    default:
                                        throw new NotImplementedException();

                                }
                                break;
                            }
                        }
                    } catch (NumberFormatException e) {
                    }

                }
                sample.setSolidDispenses(sampleSolidDispenses);
                sample.setLiquidDispenses(sampleLiquidDispenses);
                if (sample.getHydrogen_evolution() == null)
                    sample.setHydrogen_evolution(0);
                if (sample.getOxygen_evolution() == null)
                    sample.setOxygen_evolution(0);
                samples.add(sample);
            }

            batch.setSamples(samples.toArray(new Sample[0]));

            fileReader.close();
            bufferedReader.close();

            return batch;
        } catch (Exception e) {
            if (fileReader != null)
                fileReader.close();
            if (bufferedReader != null)
                bufferedReader.close();
        }
        return null;
    }

    private void setSamples(Sample[] samples) {
        Samples = samples;
    }

    private void setSolids(Solid[] solids) {
        Solids = solids;
        solid_dispensing_finish_date_time = new Date[solids.length];
    }

    private void setLiquids(Liquid[] liquids) {
        Liquids = liquids;
    }

    private void WriteToFile() throws IOException {
        if (SavedToFile && !batch_name.contains("FreshRack")) {
            File new_file = new File(batch_file.getPath() + "_new");

            if (new_file.exists()) {
                new_file.delete();
            }
            new_file.createNewFile();
            {
                PrintWriter writer = new PrintWriter(new_file);
                WriteBatchToWriter(writer);
                writer.flush();
                writer.close();
            }

            if (batch_file.exists()) {
                boolean a = batch_file.delete();

            }

            new_file.renameTo(batch_file);
        }
    }

    private void WriteBatchToWriter(PrintWriter writer) {
        DateFormat df2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

        writer.write("batch_name:" + batch_name + "\r\n");
        writer.write("status:" + status.getStep() + "\r\n");
        writer.write("sonication_time_min:" + sonication_time_min + "\r\n");
        writer.write("illumination_time_min:" + illumination_time_min + "\r\n");
        writer.write("gc_method:" + (gc_method == null ? "" : gc_method) + "\r\n");
        writer.write("purge_time:" + purge_time_sec + "\r\n");
        writer.write("cap_vials:" + (cap_vials ? "1" : "0") + "\r\n");
        writer.write("solid_dispensing_finish_date_time:" + String.join(",", Arrays.stream(solid_dispensing_finish_date_time).map(x -> x == null ? "" : df2.format(x)).toArray(String[]::new)) + "\r\n");
        writer.write("rack_identifier:" + rack_identifier + "\r\n");
        if(Temperature_Photolysis_start != null)
            writer.write("Temperature_Photolysis_start:" + Temperature_Photolysis_start + "\r\n");
        if(Temperature_Photolysis_end != null)
            writer.write("Temperature_Photolysis_end:" + Temperature_Photolysis_end + "\r\n");
        writer.write("\r\n");

        for (Pair<BatchTimestampEnum, BatchTimestampType> time : this.time_stamps.keySet()) {
            writer.write(time.getKey().name() + "_" + time.getValue().name() + "_datetime:" + df2.format(this.time_stamps.get(time)) + "\r\n");
        }
        writer.write("\r\n");

        writer.write("SampleIndex,SampleNumber,Name,vial_capped,gc_well_number,hydrogen_evolution,oxygen_evolution,hydrogen_evolution_micromol,oxygen_evolution_micromol,internal_hydrogen_standard_micromol,weighted_hydrogen_micromol,sample_location_weight,weighted_is_sl_hydrogen_evolution_micromol,");

        for (Solid sol : Solids) {
            writer.write(sol.getName());
            writer.write(',');
        }
        for (Liquid liquid : Liquids) {
            writer.write(liquid.getName());
            writer.write(',');
        }
        for (Solid sol : Solids) {
            writer.write(sol.getName() + "_dispensed");
            writer.write(',');
        }
        for (Liquid liquid : Liquids) {
            writer.write(liquid.getName() + "_dispensed");
            writer.write(',');
        }
        writer.write("\r\n");

        Samples = Arrays.stream(Samples).sorted(Comparator.comparing(Sample::getSampleIndex)).toArray(Sample[]::new);

        for (int i = 0; i < Samples.length; i++) {
            Sample sample = Samples[i];
            writer.write("" + sample.getSampleIndex());
            writer.write(',');
            writer.write("" + sample.getSampleNumber());
            writer.write(',');
            writer.write("" + sample.getName());
            writer.write(',');
            writer.write("" + (sample.getCapped() ? "1" : "0"));
            writer.write(',');

            Integer GC_well = sample.getGC_well_number();
            writer.write(GC_well != null ? GC_well.toString() : "");
            writer.write(',');

            Integer Hydrogen_evolutioon = sample.getHydrogen_evolution();
            writer.write(Hydrogen_evolutioon != null ? Hydrogen_evolutioon.toString() : "");
            writer.write(',');

            Integer Oxygen_evolution = sample.getOxygen_evolution();
            writer.write(Oxygen_evolution != null ? Oxygen_evolution.toString() : "");
            writer.write(',');
            writer.flush();

            Double Hydrogen_evolution_micromol = sample.getHydrogen_evolution_micromol();
            writer.write(Hydrogen_evolution_micromol != null ? Hydrogen_evolution_micromol.toString() : "");
            writer.write(',');
            Double Oxygen_evolution_micromol = sample.getOxygen_evolution_micromol();
            writer.write(Oxygen_evolution_micromol != null ? Oxygen_evolution_micromol.toString() : "");
            writer.write(',');

            Double internal_hydrogen_standard_micromol = sample.getInternalHydrogenStandard();
            writer.write(internal_hydrogen_standard_micromol != null ? internal_hydrogen_standard_micromol.toString() : "");
            writer.write(',');

            Double weighted_hydrogen_micromol = sample.getWeightedHydrogenMicromol();
            writer.write(weighted_hydrogen_micromol != null ? weighted_hydrogen_micromol.toString() : "");
            writer.write(',');

            Double sample_location_weight = sample.getSampleLocationWeight();
            writer.write(sample_location_weight != null ? sample_location_weight.toString() : "");
            writer.write(',');

            Double weighted_is_sl_hydrogen_evolution_micromol = sample.getWeightedISSLHydrogenMicromol();
            writer.write(weighted_is_sl_hydrogen_evolution_micromol != null ? weighted_is_sl_hydrogen_evolution_micromol.toString() : "");



            for (Solid sol : Solids) {
                writer.write(',');
                writer.write(sample.getSolidDispenseAmount(sol).toString());
            }
            for (Liquid liquid : Liquids) {
                writer.write(',');
                writer.write(sample.getLiquidDispenseAmount(liquid).toString());
            }
            for (Solid sol : Solids) {
                writer.write(',');
                Float amount = sample.getSolidDispensed(sol);
                writer.write((amount != null ? amount : "").toString());
            }
            for (Liquid liquid : Liquids) {
                writer.write(',');
                Float amount = sample.getLiquidDispensed(liquid);
                writer.write((amount != null ? amount : "").toString());
            }
            writer.write("\r\n");
        }
        writer.write("\r\n");
        writer.write(comment);
    }


    public String getBatch_name() {
        return batch_name;
    }

    public Sample[] getSamples() {
        return Samples;
    }

    public Solid[] getSolids() {
        return Solids;
    }

    public Liquid[] getLiquids() {
        return Liquids;
    }

    public int getSonication_time_min() {
        synchronized (BatchLock) {
            return sonication_time_min;
        }

    }

    public String getRack_identifier() {
        return rack_identifier;
    }

    public int getIllumination_time_min() {
        synchronized (BatchLock) {
            return illumination_time_min;
        }
    }

    public String getGc_method() {
        synchronized (BatchLock) {
            return gc_method;
        }
    }

    public int getPurge_time_sec() {
        synchronized (BatchLock) {
            return purge_time_sec;
        }
    }

    public Boolean getCap_vials() {
        synchronized (BatchLock) {
            return cap_vials;
        }
    }


    public BatchLocation getBatchLocation() {
        synchronized (BatchLock) {
            return Location;
        }
    }

    public void setSonication_time_min(int sonication_time_min) throws IOException {
        synchronized (BatchLock) {
            this.sonication_time_min = sonication_time_min;
            WriteToFile();
        }
    }

    public void setIllumination_time_min(int illumination_time_min) throws IOException {
        synchronized (BatchLock) {
            this.illumination_time_min = illumination_time_min;
            WriteToFile();
        }
    }

    public void setGc_method(String gc_method) throws IOException {
        synchronized (BatchLock) {
            this.gc_method = gc_method;
            WriteToFile();
        }
    }

    public void setPurge_time_sec(int purge_time_sec) throws IOException {
        synchronized (BatchLock) {
            this.purge_time_sec = purge_time_sec;
            WriteToFile();
        }
    }


    public void setLocation(BatchLocation location) {
        synchronized (BatchLock) {
            this.Location = location;
        }
    }

    public void setCap_vials(Boolean cap_vials) throws IOException {
        synchronized (BatchLock) {
            this.cap_vials = cap_vials;
            WriteToFile();
        }
    }

    public void addSolidDispensed(Sample sample, Solid solid, float amount) throws IOException {
        synchronized (BatchLock) {
            sample.addSolidDispensed(solid, amount);
            WriteToFile();
        }
    }

    public void addLiquidDispensed(Sample sample, Liquid liquid, float amount) throws IOException {
        synchronized (BatchLock) {
            sample.addLiquidDispensed(liquid, amount);
            WriteToFile();
        }
    }

    public void SetGCWell(Sample sample, int gc_well_index) throws IOException {
        synchronized (BatchLock) {
            sample.setGC_well_number(gc_well_index);
            WriteToFile();
        }
    }

    public void setSampleCapped(Sample sampleCapped) throws IOException {
        synchronized (BatchLock) {
            sampleCapped.setCapped(true);
            WriteToFile();
        }
    }

    public boolean NeedToDispenseSolid(Solid solid)
    {
        return Arrays.stream(Samples).filter(x -> (x.getSolidDispenseAmount(solid) - x.getSolidDispensed(solid)) > (0.1f*x.getSolidDispenseAmount(solid)) &&  (x.getSolidDispenseAmount(solid) - x.getSolidDispensed(solid)) > 0.5).findAny().isPresent();
    }

    public void setSampleFakeGCData(Sample sampleCapped) throws IOException {
        synchronized (BatchLock) {
            sampleCapped.setHydrogen_evolution(1000);
            sampleCapped.setOxygen_evolution(1000);
            WriteToFile();
        }
    }

    public void setSampleFakeSolidDispenseData(Sample sample, Solid solid) throws IOException {
        synchronized (BatchLock) {
            sample.addSolidDispensed(solid, sample.getSolidDispenseAmount(solid));
            WriteToFile();
        }
    }

    public void setSampleFakeLiquidDispenseData(Sample sample, Liquid liquid) throws IOException {
        synchronized (BatchLock) {
            sample.addLiquidDispensed(liquid, sample.getLiquidDispenseAmount(liquid));
            WriteToFile();
        }
    }


    public BatchStatus getStatus() {
        synchronized (BatchLock) {
            return status;
        }
    }

    public static Batch FreshBatch(String batch_name) throws InconsistentStateException, IOException {
        if(batch_name.substring(0,8).equals("FreshRack"))
            throw new InconsistentStateException("FreshRack needs to be named FrreshRack!");
        Batch result = new Batch();
        result.batch_name = batch_name;
        result.rack_identifier = Double.parseDouble(batch_name.substring(9))+"";
        result.status = BatchStatus.NoRackAssigned;
        result.progress = Progress.Waiting;
        result.solid_dispensing_finish_date_time = new Date[1];
        return result;
    }

    public static void makeBatches(Solid[] currentSolids, Liquid[] currentLiquids, File csvFile, String[] batchInfo, boolean controlBatchOnly) throws Exception {

        FileReader csvFileReader = new FileReader(csvFile);
        BufferedReader csvRead = new BufferedReader(csvFileReader);
        boolean controlBatch = true;
        boolean writeMoreBatches = true;
        List<Compound> compoundsFound = new LinkedList<Compound>();
        List<Sample> controlSamples = new LinkedList<Sample>();
        List<Solid> solids_used = new LinkedList();
        List<Liquid> liquids_used = new LinkedList();
        String line;

        if ( batchInfo.length != 9 ) {
            throw new Exception("Incorrect Amount of New Batch Information!");
        }

        Integer controlsPerBatch = Integer.parseInt(batchInfo[1]);
        Integer numberOfExpBatches = Integer.parseInt(batchInfo[7]);
        Integer intersperseControls = Integer.parseInt(batchInfo[8]);
        boolean intersperseControlBatch = false;
        if ( intersperseControls > 0 ) {
            intersperseControlBatch = true;
        }

        // if first time through, get compounds from header and check they are in current state
        if ( (line = csvRead.readLine()) != null && !line.equals("") ) {
            String[] compounds = null;
            compounds = line.split(",");
            for (String compound : compounds) {
                String fix_compound_name = compound.replaceAll("\\s","").replaceAll("\uFEFF"/*white space character not in regex /s */,"");
                Optional<Solid> solid = Arrays.stream(currentSolids).filter(x -> x.getName().equals(fix_compound_name)).findFirst();
                Optional<Liquid> liquid = Arrays.stream(currentLiquids).filter(x -> x.getName().equals(fix_compound_name)).findFirst();
                if (solid.isPresent()) {
                    solids_used.add(solid.get());
                    compoundsFound.add(solid.get());
                } else if (liquid.isPresent()) {
                    liquids_used.add(liquid.get());
                    compoundsFound.add(liquid.get());
                } else {
                    throw new Exception("Solid or Liquid is Not in Current State: "+ compound);
                }
            }
        }
        if (compoundsFound.isEmpty()) {
            throw new Exception("No Compounds Found!");
        }

        Integer batchesWritten = 0;
        while (writeMoreBatches) {

            Batch newBatch = new Batch();
            Integer runNumber = 999; // always get at least 4 digits for run number
            String newPostfix = "-";

            // only adds run number greater than in current files, user can write batchname-XXXX, but XXXX is ignored
            runNumber = newBatch.checkRunNumberAllDirectories(runNumber, batchInfo[0], newPostfix);
            Integer tempRunNumber = 0; // next lines fix redundancy if user writes batchname-XXXX in UI
            if (batchInfo[0].contains(newPostfix)) {
                String afterPostFix = batchInfo[0].substring(batchInfo[0].lastIndexOf(newPostfix) + newPostfix.length());
                if ( afterPostFix.matches("\\d+") ) {
                    tempRunNumber = Integer.parseInt(afterPostFix);
                    batchInfo[0] = batchInfo[0].substring(0, batchInfo[0].length() - String.valueOf(tempRunNumber).length() - newPostfix.length());
                }
            }
            newPostfix += runNumber + 1;

            newBatch.batch_name = batchInfo[0] + newPostfix;
            newBatch.batch_file = new File("runqueue\\" + batchInfo[0] + newPostfix + ".run");
            newBatch.cap_vials = (batchInfo[2].equals("1"));
            newBatch.gc_method = batchInfo[3];
            newBatch.illumination_time_min = Integer.parseInt(batchInfo[4]);
            newBatch.purge_time_sec = Integer.parseInt(batchInfo[5]);
            newBatch.sonication_time_min = Integer.parseInt(batchInfo[6]);

            String[] compoundAmounts;
            Float comparisonFloat = 0f;
            List<Sample> newSamples = new LinkedList<Sample>();

            if (controlBatch) {
                for (int j = 0; j < controlsPerBatch; j++) {
                    if ((line = csvRead.readLine()) != null && !line.equals("")) {
                        compoundAmounts = line.split(",");

                        Sample sample = new Sample();
                        List<CompoundDispense> controlSolidDispenses = new LinkedList<CompoundDispense>();
                        List<CompoundDispense> controlLiquidDispenses = new LinkedList<CompoundDispense>();
                        sample.setSampleIndex(15 - j);
                        sample.setName(newBatch.batch_name + "_" + (16 - j));
                        if (compoundsFound.size() != compoundAmounts.length) {
                            throw new Exception("Number of Compound Amounts Do NOT Equal Number of Compounds in Header!");
                        }
                        for (int i = 0; i < compoundsFound.size(); i++) {
                            comparisonFloat = Float.parseFloat(compoundAmounts[i]);
                            if (compoundsFound.get(i).getType() == CompoundType.Liquid) {
                                controlLiquidDispenses.add(new CompoundDispense(compoundsFound.get(i), comparisonFloat));
                            } else {
                                controlSolidDispenses.add(new CompoundDispense(compoundsFound.get(i), comparisonFloat));
                            }
                        }
                        //if (controlBatch) {
                        sample.setSolidDispenses(controlSolidDispenses);
                        sample.setLiquidDispenses(controlLiquidDispenses);
                        controlSamples.add(sample);
                    } else {
                        writeMoreBatches = false;
                        controlBatch = false;
                        break;
                    }
                }
            }
            if (controlSamples.size() == 16) {
                for (Sample fullControl : controlSamples) {
                    newSamples.add(fullControl);
                }
                controlSamples = new LinkedList<Sample>();
            }
            else {
                controlBatch = false;
            }

            if (!controlBatchOnly && !intersperseControlBatch) {
                for (int i = 0; i < controlSamples.size(); i++) {
                    Sample newControl = (Sample) controlSamples.get(i).clone();
                    newControl.setName(newBatch.batch_name + "_" + (16-i));
                    newSamples.add(newControl);
                }
                for (int i = 0; i < (16 - controlsPerBatch); i++) {
                    if ((line = csvRead.readLine()) != null && !line.equals("")) {
                        compoundAmounts = line.split(",");
                        //if (!compoundsFound.isEmpty()) {
                        Sample sample = new Sample();
                        List<CompoundDispense> newSolidDispenses = new LinkedList<CompoundDispense>();
                        List<CompoundDispense> newLiquidDispenses = new LinkedList<CompoundDispense>();
                        sample.setSampleIndex(i);
                        sample.setName(newBatch.batch_name + "_" + (i+1));
                        if (compoundsFound.size() != compoundAmounts.length) {
                            throw new Exception("Number of Compound Amounts Do NOT Equal Number of Compounds in Header!");
                        }
                        for (int j = 0; j < compoundsFound.size(); j++) {
                            comparisonFloat = Float.parseFloat(compoundAmounts[j]);
                            if (compoundsFound.get(j).getType() == CompoundType.Liquid) {
                                newLiquidDispenses.add(new CompoundDispense(compoundsFound.get(j), comparisonFloat));
                            } else {
                                newSolidDispenses.add(new CompoundDispense(compoundsFound.get(j), comparisonFloat));
                            }
                        }
                        sample.setSolidDispenses(newSolidDispenses);
                        sample.setLiquidDispenses(newLiquidDispenses);
                        newSamples.add(sample);
                    } else {
                        writeMoreBatches = false;
                        break;
                    }
                }
            }
            else {
                for ( int i = 0; i < 16; i++ ) {
                    Sample newControl = (Sample) controlSamples.get(i % controlsPerBatch).clone();
                    newControl.setSampleIndex(i);
                    newControl.setName(newBatch.getBatch_name() + "_" + (i+1));
                    newSamples.add(newControl);
                }
                if ( controlBatchOnly ) {
                    writeMoreBatches = false;
                }
            }

            // only controls left, or if making control only batch files - no controls are left, don't write file
            if ( (newSamples.size() == controlsPerBatch && controlsPerBatch != 16) || newSamples.isEmpty() ) {
                break;
            }

            newBatch.setSamples(newSamples.toArray(new Sample[0]));
            newBatch.solid_dispensing_finish_date_time = new Date[newBatch.getSolids().length];
            newBatch.setSolids(solids_used.toArray(new Solid[0]));
            newBatch.setLiquids(liquids_used.toArray(new Liquid[0]));
            newBatch.time_stamps = new HashMap<>();
            newBatch.time_stamps.put(new Pair(BatchTimestampEnum.submit, BatchTimestampType.start), new Date());
            newBatch.setComment("");
            newBatch.AppendComment(String.format("Batch was generated from CSV: %s", csvFile.getName()));
            newBatch.WriteToFile();
            batchesWritten++;
            intersperseControlBatch = (( batchesWritten % ( intersperseControls + numberOfExpBatches ) ) < intersperseControls ) ? true : false;

        }
        csvFileReader.close();
        csvRead.close();
    }

    public void setStatus(BatchStatus status) throws IOException {
        synchronized (BatchLock) {
            this.status = status;
            WriteToFile();
        }
    }

    public void setProgress(Progress progress) {
        synchronized (BatchLock) {
            this.progress = progress;
        }
    }

    public void updateFile(File file) {
        synchronized (BatchLock) {
            this.batch_file = file;
        }

    }

    public void setStatusAndWaiting(BatchStatus selectedItem) throws IOException {
        synchronized (BatchLock) {
            this.status = selectedItem;
            this.progress = Progress.Waiting;
            WriteToFile();
        }
    }

    public void setName(String name) throws IOException {
        synchronized (BatchLock) {
            this.batch_name = name;
            WriteToFile();
        }
    }

    public void setGCResults(Sample sample, int hydrogen, int oxygen) throws IOException {
        synchronized (BatchLock) {
            Optional<Sample> sample_option = Arrays.stream(this.Samples).filter(x -> x.getSampleIndex() == sample.getSampleIndex()).findAny();
            if (sample_option.isPresent()) {
                sample_option.get().setHydrogen_evolution(hydrogen);
                sample_option.get().setOxygen_evolution(oxygen);
                WriteToFile();
            } else {
                throw new NotImplementedException(); //inconsistent data
            }

        }
    }

    public void setRackIdentifier(String rackIdentifier) {
        this.rack_identifier = rackIdentifier;
    }

    public void ResetTimestamps() throws IOException {
        synchronized (BatchLock) {
            this.solid_dispensing_finish_date_time = new Date[Solids.length];
            this.time_stamps = new HashMap<>();
            WriteToFile();
        }
    }

    public void setGCWell(Sample sample, int result) throws IOException {
        synchronized (BatchLock) {
            sample.setGC_well_number(result);
            WriteToFile();
        }
    }

    public void setTimestamp(BatchTimestampEnum process_step, BatchTimestampType time_step, Date time_stamp) throws IOException {
        synchronized (BatchLock) {
            if(time_stamp != null)
                this.time_stamps.put(new Pair(process_step, time_step), time_stamp);
            else
                this.time_stamps.remove(new Pair(process_step, time_step));
            WriteToFile();
        }
    }

    public Date getTimestamp(BatchTimestampEnum process_step, BatchTimestampType time_step) {
        return this.time_stamps.get(new Pair(process_step, time_step));
    }

    @Override
    //This method will order from early workflow step to late
    //within a step it will start with youngest run times, and end with highest
    // such that in the end of the list are the furthest batches, with the oldest run start time-stamps (smallest)
    public int compareTo(Batch b) {
        if (this.getStatus().getStep() > b.getStatus().getStep())
            return 1;
        if (this.getStatus().getStep() < b.getStatus().getStep())
            return -1;

        //If both run dates are not null
        if ((b.getTimestamp(BatchTimestampEnum.run, BatchTimestampType.start) != null && this.getTimestamp(BatchTimestampEnum.run, BatchTimestampType.start) != null)) {
            if (this.getTimestamp(BatchTimestampEnum.run, BatchTimestampType.start).getTime() > b.getTimestamp(BatchTimestampEnum.run, BatchTimestampType.start).getTime())
                return -1;

            if (this.getTimestamp(BatchTimestampEnum.run, BatchTimestampType.start).getTime() < b.getTimestamp(BatchTimestampEnum.run, BatchTimestampType.start).getTime())
                return 1;

            //Time stmaps are equal , so choose by name, we would like to have early alphabet at the end , and late
            // alphabet (z) in the beginning within the same time-stamp (should never occur though)
            // so reverse alphabetical order
            return -1 * this.getBatch_name().compareTo(b.getBatch_name());
        }

        //If both run dates are null
        if (b.getTimestamp(BatchTimestampEnum.run, BatchTimestampType.start) == null)
            return 1;
        return -1;


    }

    public int CalculateWeightedHydrogen() throws IOException, InconsistentStateException {
        //Step 1: Check if GC results are in for all samples && check if sample number 16 is present at all
        for(Sample sample : this.getSamples())
            if(!sample.hasGCResults())
                throw new InconsistentStateException("Not all GC results are in, can not calculate weighted hydrogen.");
        Optional<Sample> optional_standard = Arrays.stream(this.Samples).filter(x -> x.getSampleNumber() == 16 || x.getSampleNumber()  == 15).findFirst();

        if(!optional_standard.isPresent()) {
            throw new InconsistentStateException("No sample with sample number 16 present, can not determine control composition, can not calculate weighted hydrogen.");

        }

        if(this.getBatch_name().equals("hypothesis-1041"))
            System.out.println("test");
        //Step 2: calculate internal standard

        Double[] Sample_location_Weights = new Double[] {

                0.979798,
                1.010101,
                1.030303,
                1.010101,
                0.989899,
                1.020202,
                1.050505,
                1.010101,
                0.959596,
                0.959596,
                1.040404,
                1.040404,
                1.020202,
                1.020202,
                1.020202,
                1.0
        };

        Sample standard = optional_standard.get();
        Double InternalStandard = standard.getHydrogen_evolution_micromol();
        Double Internal_Hydrogen_Standard_Weighted_Location = InternalStandard / Sample_location_Weights[standard.getSampleNumber()-1];
        int internal_standard_samples = 1;
        for(int i=1;i<=16;i++)
        {
            if(i!= standard.getSampleNumber()) {
                int sample_to_find = i;
                Optional<Sample> sample = Arrays.stream(this.Samples).filter(x -> x.getSampleNumber() == sample_to_find).findFirst();
                if (sample.isPresent()) {
                    boolean same_sample_as_standard = true;
                    for (Solid solid : Solids) {
                        if (!standard.getSolidDispenseAmount(solid).equals(sample.get().getSolidDispenseAmount(solid))) {
                            same_sample_as_standard = false;
                            break;
                        }
                    }
                    if (same_sample_as_standard)
                        for (Liquid liquid : Liquids) {
                            if (!standard.getLiquidDispenseAmount(liquid).equals(sample.get().getLiquidDispenseAmount(liquid))) {
                                same_sample_as_standard = false;
                                break;
                            }
                        }

                    if (same_sample_as_standard) {
                        internal_standard_samples++;
                        InternalStandard += sample.get().getHydrogen_evolution_micromol();
                        Internal_Hydrogen_Standard_Weighted_Location += sample.get().getHydrogen_evolution_micromol() / Sample_location_Weights[sample.get().getSampleNumber() - 1];
                    }
                }
            }
        }

        InternalStandard = InternalStandard / Double.valueOf(internal_standard_samples);
        Internal_Hydrogen_Standard_Weighted_Location = Internal_Hydrogen_Standard_Weighted_Location/ Double.valueOf(internal_standard_samples);

        //Step 3: calculated weighted hydrogen micromol
        for(Sample sample : this.getSamples())
            sample.calculate_weighted_hydrogen(InternalStandard, Sample_location_Weights[sample.getSampleNumber() - 1],Internal_Hydrogen_Standard_Weighted_Location );

        WriteToFile();
        return internal_standard_samples;
    }
}
