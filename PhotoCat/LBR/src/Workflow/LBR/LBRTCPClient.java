package Workflow.LBR;

import Workflow.TCP.TCPRobotClientSuper;
import com.kuka.roboticsAPI.controllerModel.sunrise.SunriseSafetyState;
import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.task.ITaskLogger;

import java.io.IOException;

public class LBRTCPClient extends TCPRobotClientSuper<LBRRobotStatus> {

    public LBRTCPClient(LBRRobotStatus status, String IP, int Port,ITaskLogger logger) {
        super(status, IP, Port,logger);
    }

    public void setCubeOrigin(Frame cube)
    {
        synchronized(Status)
        {
            Status.setCubeOrigin(new SerializableFrame(cube.getX(),cube.getY(),cube.getZ(),cube.getAlphaRad(),cube.getBetaRad(),cube.getGammaRad()));
        }
    }

    public SerializableFrame getCubeOrigin()
    {
        synchronized(Status)
        {
            return Status.getCubeOrigin();
        }
    }

    public void ForceHeartBreat() throws IOException, CloneNotSupportedException {
        SentHeartBeat(inFromServer, outToServer);
    }

    public void SetCartesianPosition(Frame frame)
    {
        synchronized (Status)
        {
            Status.setCurrentCartesisnPosition(frame);
        }
    }

    public void SetSafety(SunriseSafetyState.SafetyStopType stopType)
    {
        synchronized (Status)
        {
            Status.setSafetyStopType(stopType);
        }
    }

}
