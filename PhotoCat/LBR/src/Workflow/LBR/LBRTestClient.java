package Workflow.LBR;

import Workflow.Configuration.Location;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TCPRobotClientSuper;
import com.kuka.roboticsAPI.controllerModel.sunrise.SunriseSafetyState;

import java.util.Date;

public class LBRTestClient {


    public static void main(String[] args) {
        LBRRobotStatus Status = new LBRRobotStatus(new Date(),
                RobotStatusEnum.Commandable, null, null, null, Location.Unknown,null, SunriseSafetyState.SafetyStopType.NOSTOP);
        TCPRobotClientSuper client = new TCPRobotClientSuper<LBRRobotStatus>(Status, "localhost" ,
                667,null);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (Status)
        {
            Status.setCubeOrigin(new SerializableFrame(5,5,5,5,5,5));
        }
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
