package Workflow.LBR.Messages;

public class LBRPlaceSampleInPhotolysisStation extends LBRTask{

    public int getIndex() {
        return index;
    }

    private int index;
    public LBRPlaceSampleInPhotolysisStation( int i) {
        super(LBRTaskTypeEnum.PlaceSampleInPhotolysisStation);
index = i;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LBRPlaceSampleInPhotolysisStation)) {
            return false;
        }

        LBRPlaceSampleInPhotolysisStation cc = (LBRPlaceSampleInPhotolysisStation) o;
        if (cc.getType().equals(this.getType()) && cc.index == this.index )
            return true;
        return false;
    }
}
