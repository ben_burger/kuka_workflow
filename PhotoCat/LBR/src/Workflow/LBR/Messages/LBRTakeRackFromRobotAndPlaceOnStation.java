package Workflow.LBR.Messages;

/**
 * Created by Benjamin on 8/4/2018.
 */
public class LBRTakeRackFromRobotAndPlaceOnStation extends LBRTask {

    private int robot_index;
    private int station_index;
    public LBRTakeRackFromRobotAndPlaceOnStation(LBRTaskTypeEnum type, int robot_index, int station_index) {
        super(type);
        this.robot_index = robot_index;
        this.station_index = station_index;
    }

    public int getRobotIndex()
    {
        return robot_index;
    }
    public int getStationIndex()
    {
        return station_index;
    }
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("robot_index: " + Integer.toString(robot_index));
        sb.append("\r\n");
        sb.append("station_index: " + Integer.toString(station_index));
        sb.append("\r\n");

        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LBRTakeRackFromRobotAndPlaceOnStation)) {
            return false;
        }

        LBRTakeRackFromRobotAndPlaceOnStation cc = (LBRTakeRackFromRobotAndPlaceOnStation) o;
        if (cc.getType().equals(this.getType()) && cc.robot_index == this.robot_index && cc.station_index == this.station_index)
            return true;
        return false;
    }
}
