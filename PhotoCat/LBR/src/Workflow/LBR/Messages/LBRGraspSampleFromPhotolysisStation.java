package Workflow.LBR.Messages;

public class LBRGraspSampleFromPhotolysisStation extends LBRTask{
    public int getIndex() {
        return index;
    }

    private int index;
    public LBRGraspSampleFromPhotolysisStation( int i) {
        super(LBRTaskTypeEnum.GraspSampleFromPhotolysisStation);
        index = i;
    }


    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LBRGraspVialFromBuffer)) {
            return false;
        }

        LBRGraspSampleFromPhotolysisStation cc = (LBRGraspSampleFromPhotolysisStation) o;
        if (cc.getType().equals(this.getType()) && cc.index == this.index )
            return true;
        return false;
    }
}
