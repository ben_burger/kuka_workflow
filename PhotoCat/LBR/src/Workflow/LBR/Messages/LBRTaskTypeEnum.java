package Workflow.LBR.Messages;

public enum LBRTaskTypeEnum {
    //KMR - LBR interaction tasks
    Finish,DrivePos,   AllowCharge, DisallowCharge,
    // LBR Util
    CalibratePosAndTorque,
    // LBR Workflow generic Operations
    SixPointCalibration,SixPointCalibrationVariance,


    //Shared: Quantos-Sample, Photocat, Capper, Sonicator, GC, LiquidStation, VibratoryPhotolysis
    TakeRackFromRobotAndPlaceOnStation,TakeRackFromStationAndPlaceOnRobot,

    //Shared: Quantos-Sample, Capper, Sonicator, VibratoryPhotolysis
    StartVialHandling, PlaceVialInBuffer, GraspVialFromBuffer,StopVialHandling,

    //LBR Station Cartridge
    CartridgeStart, CartridgePlaceInHotel, CartridgeRemoveFromHotel, CartridgeFinish, CartridgePlaceInQuantos, CartridgeRemoveFromQuantos,


    //LBR Station Quantos
    PlaceVialInQuantos, GraspVialFromQuantos,

    //LBR Station Liquid Station
    PlaceVialInCapper,GraspVialFromCapper,

    //LBR Station Sonicator
    PlaceVialInSonicator, GraspVialFromSonicator,

    //LBR Station GC
    PressHeadSpaceParkButton,

    //LBR Station Liquid Handlign Station
    PlaceVialInLiquidStation, MoveVialFromLiquidStationOneToTwo, GraspVialInLiquidStation,

    //LBR station Drying
    GraspSign,

    //LBR station VibratoryPhotolysis
    PlaceSampleInPhotolysisStation,
    GraspSampleFromPhotolysisStation,
    MoveToDryPosition,
    DryProcedure,
    MoveToNeutralAfterDry,


    }
