package Workflow.LBR.Messages;

public class LBRCartridgeRemoveCartidgeFromHotel extends LBRTask {
    private int index;
    public LBRCartridgeRemoveCartidgeFromHotel(LBRTaskTypeEnum type, int index) {
        super(type);
        this.index = index;
    }

    public int getIndex()
    {
        return index;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("index: " + Integer.toString(index));
        sb.append("\r\n");

        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LBRCartridgeRemoveCartidgeFromHotel)) {
            return false;
        }

        LBRCartridgeRemoveCartidgeFromHotel cc = (LBRCartridgeRemoveCartidgeFromHotel) o;
        if (cc.getType().equals(this.getType()) && cc.index == this.index )
            return true;
        return false;
    }

}
