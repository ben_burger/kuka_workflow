package Workflow.LBR.Messages;


import Workflow.TCP.TaskSuper;
import com.kuka.roboticsAPI.geometricModel.Frame;

import java.io.Serializable;

public class LBRTask extends TaskSuper implements Serializable {
    public LBRTaskTypeEnum getType() {
        return Type;
    }
    private LBRTaskTypeEnum Type;

    public LBRTask(LBRTaskTypeEnum type) {
        super();
        Type = type;

    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("LBRTask\r\n" + super.toString());
        sb.append("Type: " + Type.toString());
        sb.append("\r\n");

        return sb.toString();
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LBRTask)) {
            return false;
        }

        LBRTask cc = (LBRTask) o;
        if (cc.getType().equals(this.getType()))
            return true;
        return false;
    }
}
