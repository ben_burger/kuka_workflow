package Workflow.LBR.Messages;

public class LBRPlaceVialInBuffer extends LBRTask {
    private int index;
    public LBRPlaceVialInBuffer(LBRTaskTypeEnum type, int index) {
        super(type);
        this.index = index;
    }

    public int getIndex()
    {
        return index;
    }
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("index: " + Integer.toString(index));
        sb.append("\r\n");

        return sb.toString();
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LBRPlaceVialInBuffer)) {
            return false;
        }

        LBRPlaceVialInBuffer cc = (LBRPlaceVialInBuffer) o;
        if (cc.getType().equals(this.getType()) && cc.index == this.index )
            return true;
        return false;
    }

}
