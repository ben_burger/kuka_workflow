package Workflow.LBR;

import Workflow.Configuration.Location;
import Workflow.TCP.RobotStatus;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TaskSuper;


import com.kuka.roboticsAPI.controllerModel.sunrise.SunriseSafetyState;
import com.kuka.roboticsAPI.geometricModel.Frame;

import java.util.Date;

public class LBRRobotStatus extends RobotStatus{

    private SunriseSafetyState.SafetyStopType SafetyStopType;
    private Frame CurrentCartesisnPosition;
    private SerializableFrame CubeOrigin;
    private Location Station;

    public Location getStation() {
        return Station;
    }

    public void setStation(Location station) {
        Station = station;
    }

    public SerializableFrame getCubeOrigin() {
        return CubeOrigin;
    }

    public void setCubeOrigin(SerializableFrame cubeOrigin) {
        CubeOrigin = cubeOrigin;
    }

    public SunriseSafetyState.SafetyStopType getSafetyStopType() {
        return SafetyStopType;
    }

    public void setSafetyStopType(SunriseSafetyState.SafetyStopType safetyStopType) {
        SafetyStopType = safetyStopType;
    }

    public Frame getCurrentCartesisnPosition() {
        return CurrentCartesisnPosition;
    }

    public void setCurrentCartesisnPosition(Frame currentCartesisnPosition) {
        CurrentCartesisnPosition = currentCartesisnPosition;
    }

    public LBRRobotStatus(Date timestamp, RobotStatusEnum status, TaskSuper task, String error, Date taskStarted, Location station, Frame currentPositon, SunriseSafetyState.SafetyStopType safetyStopType) {
        super(timestamp, status, task, error, taskStarted);
        Station = station;
        CubeOrigin = null;
        this.SafetyStopType = safetyStopType;
        this.CurrentCartesisnPosition = currentPositon;
    }
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("LBRRobotStatus: \r\n" + super.toString());
        if(Station != null)
            sb.append("   Station: " + Station.toString());
        else
            sb.append("   Station: ");

        sb.append("\r\n");
        if(CubeOrigin != null)
            sb.append("   Station: " + CubeOrigin.toString());
        else
            sb.append("   CubeOrigin: ");
        sb.append("\r\n");
        return sb.toString();
    }



}
