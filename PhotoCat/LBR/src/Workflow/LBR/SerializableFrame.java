package Workflow.LBR;

import java.io.Serializable;
import java.util.Set;

public class SerializableFrame implements Serializable{
    public double X;
    public double Y;
    public double Z;
    public double A;

    public SerializableFrame(double x, double y, double z, double a, double b, double c) {
        X = x;
        Y = y;
        Z = z;
        A = a;
        B = b;
        C = c;
    }

    public double B;
    public double C;

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("X: ");
        sb.append(X);
        sb.append(" Y: ");
        sb.append(Y);
        sb.append(" Z: ");
        sb.append(Z);
        sb.append(" A: ");
        sb.append(A);
        sb.append(" B: ");
        sb.append(B);
        sb.append(" C: ");
        sb.append(C);
        return sb.toString();
    }

}
