package Workflow.LBR;


import Workflow.Configuration.Configuration;
import Workflow.Configuration.Location;
import Workflow.LBR.Messages.LBRTask;
import Workflow.LBR.Messages.LBRTaskTypeEnum;
import Workflow.TCP.Messages.TCPMessageSuper;
import Workflow.TCP.Messages.TaskRequest;
import Workflow.TCP.Messages.TaskResponse;
import Workflow.TCP.RobotStatusEnum;
import Workflow.TCP.TCPRobotServerSuper;

import com.kuka.roboticsAPI.controllerModel.sunrise.SunriseSafetyState;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.Date;

public class LBRTCPServer extends TCPRobotServerSuper<LBRRobotStatus> {

    public LBRTCPServer() throws IOException {
        super(
                new LBRRobotStatus(null, RobotStatusEnum.Disconnected, null, null, null, Location.Unknown,null, SunriseSafetyState.SafetyStopType.NOSTOP)
                , Configuration.getSideLBRPort()
                ,"LBR-TCP"
        );
    }

    public TaskResponse SixPointTest() throws IOException, InterruptedException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.SixPointCalibration);
        return SentTask(task);
    }

    public TaskResponse TorqueAndPositionReferencing() throws IOException, InterruptedException {
        LBRTask task = new LBRTask(LBRTaskTypeEnum.CalibratePosAndTorque);
        return SentTask(task);
    }


    public TaskResponse SentTask(LBRTask task) throws IOException, InterruptedException {
        TaskResponse result = null;
        TaskRequest request = new TaskRequest(new Date(), task);
        logger.LogMessage(true,request.toString());
        synchronized (SendingMessageLock) {
            outToClient.writeObject(request);
            outToClient.flush();
        }

        TCPMessageSuper message = null;
        while (message == null) {

            synchronized (UnprocessedMessages) {
                if (UnprocessedMessages.size() > 0)
                    message = UnprocessedMessages.remove();
                else {
                    message = null;
                }
            }

            if (message == null)
                Thread.sleep(100);

        }
        switch (message.getType()) {
            case Response:
                result = (TaskResponse) message;
                break;
            default:
                throw new NotImplementedException();
        }


        return result;
    }

    public LBRRobotStatus getStatus() {
        return this.Status;
    }
}
